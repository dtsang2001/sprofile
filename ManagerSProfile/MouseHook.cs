﻿using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using CoreSProfile.Entity;
using CoreSProfile.Helper;


namespace ManagerSProfile
{
    public class MouseHook
    {
        public event EventHandler<MouseEventArgs> MouseAction = delegate { };

        private delegate IntPtr LowLevelMouseProc(int nCode, IntPtr wParam, IntPtr lParam);

        private const int WH_MOUSE_LL = 14;
        private const int WM_LBUTTONDOWN = 0x0201;
        private const int WM_LBUTTONUP = 0x0202;
        private const int WM_LBUTTONDBLCLK = 0x203; //Left mousebutton doubleclick
        private const int WM_MOUSEMOVE = 0x0200;
        private const int WM_MOUSEWHEEL = 0x020A;
        private const int WM_RBUTTONDOWN = 0x0204;
        private const int WM_RBUTTONUP = 0x0205;

        private LowLevelMouseProc mouseProc;
        private GCHandle _gcHandler;
        private IntPtr mouseHookHandle = IntPtr.Zero;

        public Macro CurrentMacro { get; private set; }
        public void Start()
        {
            try
            {
                mouseProc = HookCallback;
                _gcHandler = GCHandle.Alloc(mouseProc);
                mouseHookHandle = SetHook(mouseProc);
            }
            catch (Exception) { }
        }

        public void Stop()
        {
            try
            {
                UnhookWindowsHookEx(mouseHookHandle);
            }
            catch (Exception) { }
        }

        private IntPtr SetHook(LowLevelMouseProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule ?? null)
            {
                return SetWindowsHookEx(WH_MOUSE_LL, proc, GetModuleHandle(curModule.ModuleName ?? null), 0);
            }
        }

        private IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            try
            {
                if (nCode >= 0)
                {
                    MSLLHOOKSTRUCT hookStruct = (MSLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(MSLLHOOKSTRUCT));
                    int delta = (int)((hookStruct.mouseData >> 16) & 0xFFFF);
                    int clicks = (int)(hookStruct.mouseData & 0xFFFF);

                    //Click chuột trái
                    if (wParam == (IntPtr)WM_LBUTTONDOWN)
                    {
                        MouseAction(this, new MouseEventArgs(MouseButtons.Left, clicks, hookStruct.pt.X, hookStruct.pt.Y, delta));
                        
                    }

                    //Click chuột phải
                    if (wParam == (IntPtr)WM_RBUTTONDOWN)
                    {

                        MouseAction(this, new MouseEventArgs(MouseButtons.Right, clicks, hookStruct.pt.X, hookStruct.pt.Y, delta));
                    }

                    //Cuộn chuột
                    if (wParam == (IntPtr)WM_MOUSEWHEEL)
                    {
                        //Utils.Logs($"Chuột giữa clicks:{clicks}; delta:{delta}", "Cuộn chuột");
                        MouseAction(this, new MouseEventArgs(MouseButtons.Middle, clicks, hookStruct.pt.X, hookStruct.pt.Y, delta));
                    }

                    //Di chuyển chuột
                    if (wParam == (IntPtr)WM_MOUSEMOVE)
                    {
                    }

                    // Debug.WriteLine($"Mouse Event: {wParam}, Position: ({hookStruct.pt.X}, {hookStruct.pt.Y} ,delta {delta})");



                    /* POINT p;
                     GetCursorPos(out p);
                     StringBuilder sb = new StringBuilder(256);
                     Debug.WriteLine($"Mouse Event: {wParam}, Position: ({p.X}, {p.Y}), Window Title: {sb}");

                     AddDelayEvent();
                     CurrentMacro.AddEvent();*/


                }
            }
            catch (Exception)
            { }
            

            return CallNextHookEx(mouseHookHandle, nCode, wParam, lParam);
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelMouseProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        private struct MSLLHOOKSTRUCT
        {
            public Point pt;
            public uint mouseData;
            public uint flags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        
    }
}
