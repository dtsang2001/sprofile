﻿using CoreSProfile;
using CoreSProfile.Entity;
using CoreSProfile.Helper;
using KAutoHelper;
using ManagerSProfile.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using Newtonsoft.Json.Linq;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Configuration;
using System.Data.Entity.Core.Mapping;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using WebSocketSharp;

namespace ManagerSProfile
{
    public partial class MainForm : MaterialForm
    {
        private List<ProfileEntity> profiles;

        private List<GroupEntity> groups;

        private List<RecordEntity> records;

        private string group_id = "0";

        private int group_index = 0;

        private IntPtr simalateProfileMain = IntPtr.Zero;

        private List<IntPtr> simalateProfileOther;

        private KeyboardHook keyboardHook = new KeyboardHook();
        private MouseHook mouseHook = new MouseHook();
        private BrowserService browserService = new();

        private List<ProfileEntity> profilesStarted;
        private List<ProfileEntity>? profilesRecorded;
        private ProfileEntity profileRecorded;
        private JObject yahooMailData;

        private JObject contentData;

        // macro
        public Macro CurrentMacro = new Macro();

        private long lastEventTime = 0;

        private Dictionary<string, JObject> DictionaryApiData = new Dictionary<string, JObject>();
        private bool IsRecorded = false;
        private Process mainProcess;
        private bool IsPlayRecord;

        public static CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        public MainForm()
        {
            InitializeComponent();
            initSkin();
            initTabSettings();
            LoadComboBoxGroup();
            LoadListProfiles();
            LoadListGroup();
            LoadListRecord();
            LoadComboboxRecord();
            try
            {
                keyboardHook.KeyAction += KeyboardHook_KeyAction;
                mouseHook.MouseAction += MouseHook_MouseAction;
            }
            catch (Exception)
            { }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Process[] processlist = Process.GetProcessesByName("chrome");

            foreach (Process process in processlist)
            {
                try
                {
                    string des = process.MainModule.FileVersionInfo.FileDescription;

                    if (des == "Orbita")
                    {
                        process.Kill();
                    }
                }
                catch { }
            }
        }

        /* Init Loading Form*/

        private void initSkin()
        {
            var materialSkinManager = MaterialSkinManager.Instance;

            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;

            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Blue800, Primary.Blue900,
                Primary.Blue500, Accent.LightBlue200,
                TextShade.WHITE
            );
            this.Size = new Size(1280, 720);
        }

        /*Init Loading Form*/
        /*Tab Profiles*/

        private void LoadListRecord()
        {
            new Task(() =>
            {
                records = CurrentMacro.LoadAllRecord(Config.fileRecord);

                this.Invoke((Action)(() =>
                {
                    this.dataRecord.DataSource = records;
                }));
            }).Start();
        }

        private void LoadListProfiles()
        {
            new Task(() =>
            {
                profiles = SQLite.GetProfiles(int.Parse(this.group_id));

                this.Invoke((Action)(() =>
                {
                    this.ListProfiles.DataSource = profiles;
                }));
            }).Start();
        }

        private void LoadListGroup()
        {
            new Task(() =>
            {
                groups = SQLite.GetGroupsCountProfiles();

                this.Invoke((Action)(() =>
                {
                    this.ListGroups.DataSource = groups;
                }));
            }).Start();
        }

        private void LoadComboBoxGroup()
        {
            List<ComboBoxEntity> l = new List<ComboBoxEntity>
            {
                new ComboBoxEntity() { id = 0, name = "Select Group" }
            };

            List<GroupEntity> groups = SQLite.GetGroupsCountProfiles();
            foreach (GroupEntity item in groups)
            {
                l.Add(new ComboBoxEntity() { id = item.id, name = item.name });
            }

            Thread.Sleep(1000);

            this.Invoke((Action)(() =>
            {
                this.filterByGroup.DataSource = l;
                this.filterByGroup.DisplayMember = "name";
                this.filterByGroup.ValueMember = "id";
                this.filterByGroup.SelectedIndex = this.group_index;
            }));
        }

        private void LoadComboboxRecord()
        {
            List<ComboBoxEntity> l = new List<ComboBoxEntity>
            {
                new ComboBoxEntity() { id = -1, name = "Select Record" }
            };

            records = CurrentMacro.LoadAllRecord(Config.fileRecord);

            int i = 0;
            int iSelected = 0;
            foreach (RecordEntity item in records)
            {
                i++;
                l.Add(new ComboBoxEntity() { id = item.id, name = item.record_name });
            }

            Thread.Sleep(500);

            this.Invoke((Action)(() =>
            {
                //RunRecordTable.DataSource = new BindingList<ProfileEntity>(); // Initialize with an empty BindingList
                this.ComboBoxRecord.DataSource = l;
                this.ComboBoxRecord.DisplayMember = "name";
                this.ComboBoxRecord.ValueMember = "id";
                this.ComboBoxRecord.SelectedIndex = iSelected;
                this.ComboBoxRecord.AutoResize = true;
            }));
        }

        private void ListProfiles_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            string profileID = ListProfiles.Rows[e.RowIndex].Cells["profile_id"].Value.ToString();

            if (profileID == iProfileID.Text) return;

            ProfileEntity profile = SQLite.SelectProfile(profileID);

            if (profile == null) return;

            iProfileID.Text = profile.profile_id;
            iProxy.Text = profile.proxy;
            iUserAgent.Text = profile.ua;
            iScreen.Text = profile.screen_resolution;
        }

        private void ButtonCreateProfiles_Click(object sender, EventArgs e)
        {
            AddProfileForm form = new AddProfileForm
            {
                StartPosition = FormStartPosition.CenterParent,
                Group_id = this.group_id
            };
            form.ShowDialog();

            new Task(() =>
            {
                if (form.DialogResult == DialogResult.OK)
                {
                    LoadListProfiles();
                    LoadListGroup();
                }
            }).Start();
        }

        private void StartProfile_Click(object sender, EventArgs e)
        {
            List<ProfileEntity> data = LoadSeletedProfiles();

            profilesStarted = data;

            SimulateOptForm form = new()
            {
                StartPosition = FormStartPosition.Manual,
                Location = new Point(this.DesktopLocation.X + 70, this.DesktopLocation.Y + 70),
                profilesStarted = profilesStarted,
            };
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                //materialTabControl1.SelectedIndex = 1; // Ok = switch to tab simulator and more 2 pro
                form.Close();
            }
        }

        private void EditProfile_Click(object sender, EventArgs e)
        {
            ProfileEntity profile = LoadSeletedProfiles()[0];

            EditProfileForm form = new EditProfileForm
            {
                StartPosition = FormStartPosition.Manual,
                ProfileID = profile.profile_id,
                BaseProfilePath = Config.folderProfile
            };
            form.ShowDialog();

            new Task(() =>
            {
                if (form.DialogResult == DialogResult.OK)
                {
                    LoadListProfiles();
                    LoadListGroup();
                    LoadComboBoxGroup();
                }
            }).Start();
        }

        private void CloneProfile_Click(object sender, EventArgs e)
        {
            ProfileEntity clone = LoadSeletedProfiles()[0];

            clone = SQLite.SelectProfile(clone.profile_id);

            ProfileEntity profile = GoLogin.CloneProfile(clone, Config.folderProfile, Config.fileName);

            if (String.IsNullOrEmpty(profile.folders))
            {
                MessageBox.Show(profile.note);
                return;
            }

            SQLite.InsertProfile(profile);

            LoadListProfiles();
        }

        private void DeleteProfile_Click(object sender, EventArgs e)
        {
            List<ProfileEntity> data = LoadSeletedProfiles();

            DialogResult dialogResult = MessageBox.Show("Do you want to delete these profiles?", "Remove profile", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                List<Task> tasks = new List<Task>();

                foreach (ProfileEntity item in data)
                {
                    tasks.Add(Task.Run(() =>
                    {
                        GoLogin.RemoveProfile(item);
                    }));
                }

                Task t = Task.WhenAll(tasks);
                t.Wait();

                if (t.Status == TaskStatus.RanToCompletion)
                {
                    base.Invoke((Action)(() =>
                    {
                        LoadListProfiles();
                        LoadListGroup();
                    }));
                }
            }
        }

        private void moveGroup_Click(object sender, EventArgs e)
        {
            List<ProfileEntity> data = LoadSeletedProfiles();

            MoveToForm form = new MoveToForm
            {
                StartPosition = FormStartPosition.CenterParent,
                Data = data,
                Group_id = this.group_index
            };
            form.ShowDialog();

            new Task(() =>
            {
                if (form.DialogResult == DialogResult.OK)
                {
                    LoadListProfiles();
                    LoadListGroup();
                    LoadComboBoxGroup();
                }
            }).Start();
        }

        private void updateProxyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<ProfileEntity> data = LoadSeletedProfiles();
            foreach (var item in data)
            {
                string proxyTxt = Utils.RandomLineFileTxt(Application.StartupPath + "\\Data\\proxy.txt");
                string Preferences = File.ReadAllText($"{Config.folderProfile}\\{item.profile_id}\\Default\\Preferences");
                ProxyEntity proxy = new ProxyEntity(proxyTxt);

                string pUserProxy = Regex.Match(Preferences, "\"username\": \"(.*?)\"").Groups[1].ToString();
                string pPassProxy = Regex.Match(Preferences, "\"password\": \"(.*?)\"").Groups[1].ToString();
                string pPublicip = Regex.Match(Preferences, "\"public_ip\": \"(.*?)\"").Groups[1].ToString();
                if (String.IsNullOrEmpty(pUserProxy))
                {
                    Preferences = Preferences.Replace("\"password\": \"\"", $"\"password\": \"{proxy.pass}\"");
                    Preferences = Preferences.Replace("\"username\": \"\"", $"\"username\": \"{proxy.user}\"");
                    Preferences = Preferences.Replace("\"public_ip\": \"\"", $"\"public_ip\": \"{proxy.ip}\"");
                }
                else
                {
                    Preferences = Preferences.Replace(pUserProxy, proxy.user);
                    Preferences = Preferences.Replace(pPassProxy, proxy.pass);
                    Preferences = Preferences.Replace(pPublicip, proxy.ip);
                }

                item.proxy = proxy.origin;

                File.WriteAllText($"{Config.folderProfile}\\{item.profile_id}\\Default\\Preferences", Preferences);
                SQLite.UpdateProfile(item);
            }

            LoadListProfiles();
        }

        // profile tab record

        private List<ProfileEntity> LoadSeletedProfiles()
        {
            List<ProfileEntity> seleted = new List<ProfileEntity>();

            int id = 0;
            foreach (DataGridViewRow row in this.ListProfiles.SelectedRows)
            {
                ProfileEntity profile = profiles.Find(item => item.profile_id == row.Cells["profile_id"].Value.ToString());

                if (profile == null) continue;

                seleted.Add(new ProfileEntity
                {
                    id = id,
                    profile_id = profile.profile_id,
                    group_id = profile.group_id,
                    folders = profile.folders,
                    proxy = profile.proxy,
                    ua = profile.ua,

                    // table
                    profile_last_update = profile.last_update,
                    profile_proxy = profile.proxy,
                    profile_status = "Ready"
                });

                id++;
            }

            return seleted;
        }

        // group table
        private List<GroupEntity> LoadSeletedGroups()
        {
            List<GroupEntity> seleted = new List<GroupEntity>();

            int id = 0;
            foreach (DataGridViewRow row in this.ListGroups.SelectedRows)
            {
                GroupEntity group = groups.Find(item => item.group_name == row.Cells["group_name"].Value.ToString());

                if (group == null) continue;

                seleted.Add(new GroupEntity
                {
                    id = group.id,
                    group_name = group.group_name,
                    name = group.group_name,
                    group_total = group.group_total,
                    count = group.group_total
                });

                id++;
            }

            return seleted;
        }

        private void ButtonCreateGroup_Click(object sender, EventArgs e)
        {
            AddGroupForm form = new AddGroupForm
            {
                StartPosition = FormStartPosition.CenterParent,
            };
            form.ShowDialog();

            new Task(() =>
            {
                if (form.DialogResult == DialogResult.OK)
                {
                    LoadListGroup();
                    LoadComboBoxGroup();
                }
            }).Start();
        }

        private void EditGroup_Click(object sender, EventArgs e)
        {
            GroupEntity g = this.LoadSeletedGroups()[0];

            AddGroupForm form = new AddGroupForm
            {
                StartPosition = FormStartPosition.CenterParent,
                isEdit = true,
                group = g
            };
            form.ShowDialog();

            new Task(() =>
            {
                if (form.DialogResult == DialogResult.OK)
                {
                    LoadListGroup();
                    LoadComboBoxGroup();
                }
            }).Start();
        }

        private void DeleteGroup_Click(object sender, EventArgs e)
        {
            List<GroupEntity> data = this.LoadSeletedGroups();

            DialogResult dialogResult = MessageBox.Show("Do you want to delete these groups?", "Remove groups", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                List<Task> tasks = new List<Task>();

                foreach (GroupEntity item in data)
                {
                    tasks.Add(Task.Run(() =>
                    {
                        GoLogin.RemoveProfileWhenDelGroup(item);
                    }));
                }

                Task t = Task.WhenAll(tasks);
                t.Wait();

                if (t.Status == TaskStatus.RanToCompletion)
                {
                    base.Invoke((Action)(() =>
                    {
                        LoadListGroup();
                        LoadComboBoxGroup();
                    }));
                }
            }
        }

        /*Tab Setting*/

        private void initTabSettings()
        {
            Config.fileProxy = ConfigurationManager.AppSettings[Config.CONFIG_FILE_PROXY] ?? "";
            Config.fileName = ConfigurationManager.AppSettings[Config.CONFIG_FILE_NAME] ?? "";
            Config.fileExt = ConfigurationManager.AppSettings[Config.CONFIG_FILE_EXT] ?? "";
            Config.folderProfile = ConfigurationManager.AppSettings[Config.CONFIG_FOLDER_PROFILE] ?? "";
            Config.fileRecord = ConfigurationManager.AppSettings[Config.CONFIG_FILE_RECORD] ?? "";

            if (String.IsNullOrEmpty(Config.fileRecord))
            {
                Config.fileRecord = $"{Application.StartupPath}\\Data\\Record";
            }

            if (String.IsNullOrEmpty(Config.fileProxy))
            {
                Config.fileProxy = $"{Application.StartupPath}\\Data\\proxy.txt";
            }

            if (String.IsNullOrEmpty(Config.fileName))
            {
                Config.fileName = $"{Application.StartupPath}\\Data\\name.json";
            }

            if (String.IsNullOrEmpty(Config.fileExt))
            {
                Config.fileExt = $"{Application.StartupPath}\\Data\\ext.txt";
            }

            if (String.IsNullOrEmpty(Config.folderProfile))
            {
                Config.folderProfile = $"{Application.StartupPath}\\Profiles";
            }

            this.tFileProxy.Text = Config.fileProxy;
            this.tFileName.Text = Config.fileName;
            this.tFileExt.Text = Config.fileExt;
            this.tFolderProfiles.Text = Config.folderProfile;

            List<string> list = new List<string>();
            try
            {
                list = new List<string>(File.ReadAllLines(Config.fileExt));
                foreach (string item in list)
                {
                    this.multiLineTextExt.Text += item + "\r\n";
                }
            }
            catch
            {
                this.tFileExt.Text = "error path";
            }
        }

        // setting button action
        private void SaveSetting_Click(object sender, EventArgs e)
        {
            Config.fileProxy = this.tFileProxy.Text;
            Config.fileName = this.tFileName.Text;
            Config.folderProfile = this.tFolderProfiles.Text;
            Config.fileExt = this.tFileExt.Text;

            Utils.SaveConfig(Config.CONFIG_FILE_PROXY, Config.fileProxy);
            Utils.SaveConfig(Config.CONFIG_FILE_NAME, Config.fileName);
            Utils.SaveConfig(Config.CONFIG_FOLDER_PROFILE, Config.folderProfile);
            Utils.SaveConfig(Config.CONFIG_FILE_EXT, Config.fileExt);
            addToFileExt_Click();
            initTabSettings();
        }

        private void SelectFolderProfiles_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                this.tFolderProfiles.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void SelectFileProxy_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "TXT Files|*.txt";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.tFileProxy.Text = openFileDialog.FileName;
            }
        }

        private void SelectFileExt_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "TXT Files|*.txt";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.tFileExt.Text = openFileDialog.FileName;
            }
        }

        private void SelectFileName_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "JSON Files|*.json";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.tFileName.Text = openFileDialog.FileName;
            }
        }

        private void addToFileExt_Click()
        {
            string ext = this.multiLineTextExt.Text;
            string filePath = this.tFileExt.Text;

            if (string.IsNullOrWhiteSpace(filePath) && ext.IsNullOrEmpty())
            {
                MessageBox.Show("Please enter a valid file path.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                File.WriteAllText(filePath, ext);
                this.multiLineTextExt.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /*Filtered main tab by group*/

        private void filterByGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxEntity? comboBox = filterByGroup.SelectedItem as ComboBoxEntity;
            this.group_index = filterByGroup.SelectedIndex;
            this.group_id = comboBox.id.ToString();
            LoadListProfiles();
        }

        // Tab Simulate - Mô Phóng Người Dùng
        internal const int WM_KEYDOWN = 0x0100;

        internal const int WM_KEYUP = 0x0101;
        internal const int WM_MOUSEWHEEL = 0x020A;
        internal const int VK_KEY_W = 0x57;

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        private static extern IntPtr SetForegroundWindow(IntPtr hWnd);

        [Flags]
        public enum WinMsgMouseKey : int
        {
            MK_NONE = 0x0000,
            MK_LBUTTON = 0x0001,
            MK_RBUTTON = 0x0002,
            MK_SHIFT = 0x0004,
            MK_CONTROL = 0x0008,
            MK_MBUTTON = 0x0010,
            MK_XBUTTON1 = 0x0020,
            MK_XBUTTON2 = 0x0040
        }

        // unused
        private void SimulateProfile_Click(object sender, EventArgs e)
        {
            List<ProfileEntity> p = LoadSeletedProfiles();

            if (p == null || p.Count <= 1)
            {
                MessageBox.Show("Profiles phải nhiều hơn 1!");
                return;
            }

            SimulateForm form = new SimulateForm
            {
                StartPosition = FormStartPosition.Manual,
                Location = new Point(this.DesktopLocation.X + 70, this.DesktopLocation.Y + 70),
                Profiles = p
            };
            form.ShowDialog();

            new Task(() =>
            {
                if (form.DialogResult == DialogResult.OK)
                {
                }
            }).Start();
        }

        /*simulate Tab  ==================*/

        // button action
        private void ComboBoxPopUpSelected_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxEntity? comboBox = ComboBoxPopUpSelected.SelectedItem as ComboBoxEntity;
            if (comboBox != null)
            {
                KeyboardHelper.SendAllToBack(simalateProfileMain, simalateProfileOther);

                simalateProfileOther.Clear();
                // set main control again
                foreach (ProfileEntity item in profilesStarted)
                {
                    var ps = KeyboardHelper.FindWindowReturnPsc(item.profile_id);
                    if (ps == null) { return; }
                    if (ps.MainWindowHandle.ToString() == comboBox.value)
                    {
                        simalateProfileMain = ps.MainWindowHandle;
                    }
                    else
                    {
                        simalateProfileOther.Add(ps.MainWindowHandle);
                    }
                }

                KeyboardHelper.BringAllToFront(simalateProfileMain, simalateProfileOther);
            }
        }

        private void buttonReload_Click(object sender, EventArgs e)
        {
            LoadComboBoxPopUp();
        }

        private void LoadComboBoxPopUp()
        {
            if (mainProcess == null)
            {
                return;
            }

            string MainName = KeyboardHelper.GetNameWindowByHandle(mainProcess.MainWindowHandle);
            List<ComboBoxEntity> l = new List<ComboBoxEntity>
            {
                new ComboBoxEntity() { id = mainProcess.Id, value = mainProcess.MainWindowHandle.ToString(), name = MainName }
            };

            foreach (ProfileEntity item in profilesStarted)
            {
                var ps = KeyboardHelper.FindWindowReturnPsc(item.profile_id);
                if (ps == null)
                {
                    return;
                }
                if (ps.Id == mainProcess.Id && ps.MainWindowHandle != mainProcess.MainWindowHandle)
                {
                    l.Add(new ComboBoxEntity() { id = ps.Id, value = ps.MainWindowHandle.ToString(), name = KeyboardHelper.GetNameWindowByHandle(ps.MainWindowHandle) });
                }
            }
            new Task(() =>
            {
                this.Invoke((Action)(() =>
                {
                    this.ComboBoxPopUpSelected.DataSource = l;
                    this.ComboBoxPopUpSelected.DisplayMember = "name";
                    this.ComboBoxPopUpSelected.ValueMember = "value";
                }));
            }).Start();
        }

        private async void SimulateLoad_ClickAsync(object sender, EventArgs e)
        {
            if (APITxt.Text.IsNullOrEmpty() || channelTxt.Text.IsNullOrEmpty())
            {
                MessageBox.Show("Please fill API and channel name!");
                return;
            }
            simulateStart.Text = "Start";
            StatusApply("Loading...", Color.Green);

            if (profilesStarted == null)
            {
                StatusApply("Error: Profiles null!", Color.Red);
                return;
            }

            this.dataSimulator.DataSource = profilesStarted;
            this.simulateStart.Enabled = false;
            this.simulateStop.Enabled = false;

            // clear old getTask data
            DictionaryApiData.Clear();

            // loading getTask data to each profile
            List<Task> tasks = new List<Task>();

            // if loaded record  not need do this??

            foreach (ProfileEntity item in profilesStarted)
            {
                tasks.Add(Task.Run(() =>
                {
                    item.hanlde = KeyboardHelper.FindWindowHandleFromProcesses(item.profile_id);
                    JObject data = browserService.getTask(APITxt.Text, channelTxt.Text);
                    data["handle"] = item.hanlde.ToInt32();
                    data["yahoo"] = "false";
                    data["getContent"] = "false";

                    lock (DictionaryApiData)
                    {
                        DictionaryApiData[item.profile_id] = data;
                    }
                }));
            }
            await Task.WhenAll(tasks);

            this.simulateStart.Enabled = true;
            // Task completed here
            StatusApply("-Load completed- Please select one profile to start", Color.Green);
        }

        private void simulateStart_Click(object sender, EventArgs e)
        {
            if (simalateProfileMain == IntPtr.Zero)
            {
                StatusApply("Error: Empty Profile Main!", Color.Red);
                return;
            }
            mainProcess = AutoControl.FindProcess(simalateProfileMain);

            ComboBoxPopUpSelected.Enabled = true;
            LoadComboBoxPopUp();

            keyboardHook.Start();
            mouseHook.Start();

            this.simulateStart.Enabled = false;
            this.simulateStop.Enabled = true;
        }

        private void simulateStop_Click(object sender, EventArgs e)
        {
            StatusApply("Ready", Color.Green);

            keyboardHook.Stop();
            mouseHook.Stop();

            this.simulateLoad.Enabled = true;
            this.simulateStart.Enabled = true;
            this.simulateStop.Enabled = false;
        }

        // menu drop actioon simulate Tab
        private void DropDataAPI(object sender, ToolStripItemClickedEventArgs e)
        {
            if (IsRecorded)
            {
                AddDelayEvent();
                CurrentMacro.AddEvent(e.ClickedItem.Text);
            }
            List<ProfileEntity> sData = LoadSeletedSimu();
            List<Task> tasks = new List<Task>();

            foreach (ProfileEntity item in sData)
            {
                tasks.Add(Task.Run(() =>
                {
                    IntPtr handle = new IntPtr(int.Parse(DictionaryApiData[item.profile_id]["handle"].ToString()));
                    switch (e.ClickedItem.Text)
                    {
                        case "full_name":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["full_name"].ToString());
                            break;

                        case "first_name":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["first_name"].ToString());
                            break;

                        case "last_name":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["last_name"].ToString());
                            break;

                        case "phone":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["phone"].ToString());
                            break;

                        case "phone2":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["phone2"].ToString());
                            break;

                        case "email":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["email"].ToString());
                            break;

                        case "password":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["password"].ToString());
                            break;

                        case "password2":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["password2"].ToString());
                            break;

                        case "username":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["username"].ToString());
                            break;

                        case "username2":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["username2"].ToString());
                            break;

                        case "dob2":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["dob2"].ToString());
                            break;

                        case "dod":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["dod"].ToString());
                            break;

                        case "dom":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["dom"].ToString());
                            break;

                        case "doy":
                            KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["doy"].ToString());
                            break;
                    }
                }));
            }
            Task t1 = Task.WhenAll(tasks);
            t1.Wait();
        }

        private void getOTPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsRecorded)
            {
                AddDelayEvent();
                CurrentMacro.AddEvent(sender.ToString());
            }
            if (brandTxt.Text.IsNullOrEmpty())
            {
                MessageBox.Show("Please fill brand name!");
                this.brandTxt.SelectAll();
                return;
            }
            List<Task> tasks = new List<Task>();
            List<ProfileEntity> sData = LoadSeletedSimu();

            foreach (ProfileEntity item in sData)
            {
                tasks.Add(Task.Run(async () =>
                {
                    IntPtr handle = new IntPtr(int.Parse(DictionaryApiData[item.profile_id]["handle"].ToString()));
                    string otp = browserService.getOTP(APITxt.Text, DictionaryApiData[item.profile_id]["phone2"].ToString(), brandTxt.Text, 10);

                    Utils.ChangeGridView(dataSimulator, "simu_id", item.profile_id, "OTP: " + otp, Color.Black);
                    KeyboardHelper.SendVieChar(handle, otp);
                    //MessageBox.Show(bs.getOTP(DictionaryApiData[item.profile_id]["phone"].ToString(), brandTxt.Text, 10));
                }));
            }
            Task.WhenAll(tasks);
        }

        private void saveAccoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsRecorded)
            {
                AddDelayEvent();
                CurrentMacro.AddEvent(sender.ToString());
                //Debug.WriteLine(sender.ToString());
            }
            if (sqlTabtxt.Text.IsNullOrEmpty())
            {
                MessageBox.Show("Please fill database name!");
                sqlTabtxt.Select();
                return;
            }

            // nếu sử dụng email => data[email] = yahoo
            List<ProfileEntity> sData = LoadSeletedSimu();
            foreach (ProfileEntity item in sData)
            {
                JObject data = browserService.saveAccount(APITxt.Text, DictionaryApiData[item.profile_id], sqlTabtxt.Text);
                if (data != null && data["code"]?.ToString() == "200")
                {
                    Utils.ChangeGridView(dataSimulator, "simu_id", item.profile_id, "Saved", Color.Green);
                    Utils.ChangeGridView(ListProfiles, "profile_id", item.profile_id, "Saved", Color.Green);
                    List<Task> tasks = new List<Task>
                    {
                        // xoá
                        Task.Run(() =>
                        {
                            GoLogin.RemoveProfile(item);
                        })
                    };
                }
                else
                {
                    Utils.ChangeGridView(dataSimulator, "simu_id", item.profile_id, "Error save", Color.Green);
                    Utils.ChangeGridView(ListProfiles, "profile_id", item.profile_id, "Error save", Color.Green);
                }
            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<ProfileEntity> sData = LoadSeletedSimu();
            List<Task> tasks = new List<Task>();
            //List<string> copyValues = new List<string>();

            foreach (ProfileEntity item in sData)
            {
                IntPtr handle = new IntPtr(int.Parse(DictionaryApiData[item.profile_id]["handle"].ToString()));

                string copiedText = "";

                AutoControl.BringToFront(handle);
                copiedText = KeyboardHelper.SendCopy(handle);

                DictionaryApiData[item.profile_id]["copy_value"] = copiedText;

                //Utils.ChangeGridView(dataSimulator, "simu_id", item.profile_id, copiedText, Color.Green);
                //copyValues.Add(copiedText);
            }
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<ProfileEntity> sData = LoadSeletedSimu();
            foreach (ProfileEntity item in sData)
            {
                IntPtr handle = new IntPtr(int.Parse(DictionaryApiData[item.profile_id]["handle"].ToString()));
                KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]?["copy_value"].ToString());
            }
        }

        private void DropYahooClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (IsRecorded)
            {
                AddDelayEvent();
                CurrentMacro.AddEvent(e.ClickedItem.Text);
            }
            List<ProfileEntity> sData = LoadSeletedSimu();

            foreach (ProfileEntity item in sData)
            {
                if (DictionaryApiData[item.profile_id]["yahoo"].ToString() == "false")
                {
                    yahooMailData = browserService.getYahooMail();
                    Utils.ChangeGridView(dataSimulator, "simu_id", item.profile_id, $"code: {yahooMailData["code"]}, email: {yahooMailData["data"]?["email"]}", Color.Black);
                    DictionaryApiData[item.profile_id]["yahoo"] = yahooMailData;
                }

                IntPtr handle = new IntPtr(int.Parse(DictionaryApiData[item.profile_id]["handle"].ToString()));

                switch (e.ClickedItem.Text)
                {
                    case "yahoo mail":
                        KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["yahoo"]["data"]["email"].ToString());
                        break;

                    case "password":
                        KeyboardHelper.SendVieChar(handle, DictionaryApiData[item.profile_id]["yahoo"]["data"]["password"].ToString());
                        break;
                }
            }
        }

        private void DropContentMailClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (brandTxt.Text.IsNullOrEmpty())
            {
                MessageBox.Show("Please fill brand name!");
                this.brandTxt.SelectAll();
                return;
            }
            List<ProfileEntity> sData = LoadSeletedSimu();

            foreach (ProfileEntity item in sData)
            {
                if (DictionaryApiData[item.profile_id]["yahoo"].ToString() != "false")
                {
                    string email = yahooMailData["data"]["email"].ToString();
                    string password = yahooMailData["data"]["password"].ToString();
                    if (DictionaryApiData[item.profile_id]["getContent"].ToString() == "false")
                    {
                        contentData = browserService.getContentMail(email, password, "yahoo", brandTxt.Text);
                        // contentData = bs.getContentMail("daohongnh6631@yahoo.com", "lrudqviepwhgreig", "yahoo", "Twilio");
                        if (contentData == null)
                        {
                            MessageBox.Show("Please check brand name!");
                            return;
                        }
                        DictionaryApiData[item.profile_id]["getContent"] = contentData;
                    }
                }
                else
                {
                    MessageBox.Show("Please get an account before receiving their content");
                    return;
                }

                IntPtr handle = new IntPtr(int.Parse(DictionaryApiData[item.profile_id]["handle"].ToString()));

                string content = "";
                switch (e.ClickedItem.Text)
                {
                    case "OTP":
                        content = DictionaryApiData[item.profile_id]["getContent"]["data"]["otp"].ToString();
                        KeyboardHelper.SendVieChar(handle, content);
                        break;

                    case "Link":
                        content = DictionaryApiData[item.profile_id]["getContent"]["data"]["link"].ToString();
                        KeyboardHelper.OpenMailVerify(handle, "link");
                        break;

                    case "View raw":
                        ViewContentMailForm form = new()
                        {
                            StartPosition = FormStartPosition.CenterParent,
                            ListLink = DictionaryApiData[item.profile_id]["getContent"]["data"]["match"].ToString(),
                            MainContent = DictionaryApiData[item.profile_id]["getContent"]["data"]["body"].ToString(),
                        };
                        form.ShowDialog();
                        break;
                }
                /*Utils.ChangeGridView(dataSimulator, "simu_id", item.profile_id, $"message: {contentData["message"]}, content: {content}", Color.Black);*/
            }
        }

        private void recordToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            profileRecorded = LoadSeletedProfiles()[0];
            materialTabControl1.SelectedIndex = 2;
        }

        // table
        private List<ProfileEntity> LoadSeletedSimu()
        {
            List<ProfileEntity> seleted = new List<ProfileEntity>();

            int id = 0;
            foreach (DataGridViewRow row in this.dataSimulator.SelectedRows)
            {
                ProfileEntity profile = profiles.Find(item => item.profile_id == row.Cells["simu_id"].Value.ToString());

                if (profile == null) continue;

                seleted.Add(new ProfileEntity
                {
                    id = id,
                    profile_id = profile.profile_id,
                });

                id++;
            }

            return seleted;
        }

        private void Simu_contentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.simulateStop.Enabled == true)
            {
                return;
            }
            if (e.RowIndex < 0)
            {
                return;
            }
            if (this.StatusMess.Text == "Loading...")
            {
                return;
            }
            string profileID = dataSimulator.Rows[e.RowIndex].Cells["simu_id"].Value.ToString();

            if (profileID == null) return;

            string name = profileID;

            simalateProfileMain = new IntPtr(int.Parse(DictionaryApiData[name]["handle"].ToString()));

            if (simalateProfileMain == IntPtr.Zero)
            {
                return;
            }
            simalateProfileOther = new List<IntPtr>();
            foreach (ProfileEntity item in profilesStarted)
            {
                if (item.profile_id != name)
                {
                    if (!DictionaryApiData.ContainsKey(item.profile_id))
                    {
                        StatusApply("Try loading button", Color.Red);
                        return;
                    }

                    IntPtr intPtr = new IntPtr(int.Parse(DictionaryApiData[item.profile_id]["handle"].ToString()));
                    if (intPtr == IntPtr.Zero) return;
                    simalateProfileOther.Add(intPtr);
                }
            }

            new Task(() =>
            {
                this.Invoke((Action)(() =>
                {
                    StatusApply("Selected: " + $"-{name}-", Color.Green);
                }));
            }).Start();
        }

        // collase
        private void ButtonCollapsed_Click(object sender, EventArgs e)
        {
            if (panelCollapsed.Height == 120)
            {
                panelCollapsed.Height = 0;
            }
            else
            {
                panelCollapsed.Height = 120;
            }
        }

        /*tab record===================*/

        // button control
        private void startRecord_Click(object sender, EventArgs e)
        {
            if (profileRecorded == null)
            {
                try
                {
                    profileRecorded = profilesStarted[0];
                }
                catch
                {
                    MessageBox.Show("Try load a browsers");
                    return;
                }
            }

            CurrentMacro.Stop();
            IsRecorded = true;
            startRecord.Enabled = false; stopRecord.Enabled = true; pauseRecord.Enabled = true;
            StatusRecordApply("Recoding", Color.Green);

            keyboardHook.Start();
            mouseHook.Start();
        }

        private void pauseRecord_Click(object sender, EventArgs e)
        {
            if (pauseRecord.Text == "Pause")
            {
                pauseRecord.Text = "Continus";
                keyboardHook.Stop();
                mouseHook.Stop();
            }
            else
            {
                pauseRecord.Text = "Pause";
                keyboardHook.Start();
                mouseHook.Start();
            }
        }

        private void stopRecord_Click(object sender, EventArgs e)
        {
            if (IsPlayRecord)
            {
                keyboardHook.Stop();
                mouseHook.Stop();
                cancellationTokenSource.Cancel();
                IsPlayRecord = false;
                startRecord.Enabled = true; stopRecord.Enabled = false; pauseRecord.Enabled = false;
                StatusRecordApply("Cancelling", Color.Green);
            }
            if (IsRecorded)
            {
                if (CurrentMacro.IsExistEvent())
                {
                    startRecord.Enabled = true; stopRecord.Enabled = false; pauseRecord.Enabled = false;
                    return;
                }
                DialogResult result = MessageBox.Show("Do you want to save this record?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    keyboardHook.Stop();
                    mouseHook.Stop();

                    DateTime now = DateTime.Now;
                    string fileRecordName = $"Record_{profileRecorded.profile_id}_{now:yyyy_MM_dd_HH_mm}.txt";
                    // auto save
                    string fileRecord = $"{Application.StartupPath}\\Data\\Record\\{fileRecordName}";
                    CurrentMacro.Save(fileRecord);
                    IsRecorded = false;

                    LoadListRecord();
                    startRecord.Enabled = true; stopRecord.Enabled = false; pauseRecord.Enabled = false;
                    StatusRecordApply("Ready", Color.Green);
                }
                CurrentMacro.Stop();
            }
        }

        // table of record control
        private List<RecordEntity> LoadSeletedRecord()
        {
            List<RecordEntity> seleted = new List<RecordEntity>();

            int id = 0;
            foreach (DataGridViewRow row in this.dataRecord.SelectedRows)
            {
                RecordEntity record = records.Find(item => item.record_link == row.Cells["record_link"].Value.ToString());

                if (record == null) continue;

                seleted.Add(new RecordEntity
                {
                    id = id,
                    record_link = record.record_link,
                    record_name = record.record_name,
                });

                id++;
            }

            return seleted;
        }

        private void CellContentedEditFInish(object sender, DataGridViewCellEventArgs e)
        {
            //Debug.WriteLine(e.RowIndex.ToString());

            if (e.RowIndex < 0)
            {
                return;
            }

            string oldFilePath = dataRecord.Rows[e.RowIndex].Cells["record_link"].Value.ToString();
            string newName = dataRecord.Rows[e.RowIndex].Cells["record_name"].Value.ToString();
            if (!string.IsNullOrEmpty(newName) && !newName.EndsWith(".txt"))
            {
                newName += ".txt";
            }
            string newFilePath = $"{Application.StartupPath}\\Data\\Record\\{newName}";

            try
            {
                if (File.Exists(oldFilePath))
                {
                    // Rename the file
                    File.Move(oldFilePath, newFilePath);
                    Debug.WriteLine("File renamed successfully.");
                }
                LoadListRecord();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("An error occurred: " + ex.Message);
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<RecordEntity> record = LoadSeletedRecord();
            DialogResult result = MessageBox.Show("Do you want to delete this record?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                List<Task> tasks = new List<Task>();

                foreach (RecordEntity item in record)
                {
                    tasks.Add(Task.Run(() =>
                    {
                        try
                        {
                            File.Delete(item.record_link);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("RemoveProfile: " + ex.Message);
                        }
                    }));
                }

                Task t = Task.WhenAll(tasks);
                t.Wait();

                if (t.Status == TaskStatus.RanToCompletion)
                {
                    base.Invoke((Action)(() =>
                    {
                        LoadListRecord();
                    }));
                }
            }
            else { return; }
        }

        private async void playToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (profilesStarted == null)
            {
                return;
            }
            StatusRecordApply("PLaying Recoded", Color.Green);

            RecordEntity record = LoadSeletedRecord()[0];

            // params
            int times = 1;
            if (RadioBtnLoop.Checked)
            {
                times = (int)numericTimes.Value;
            }
            if (RadioBtnLoop4Ever.Checked)
            {
                times = -1;
            }

            cancellationTokenSource.Dispose();
            cancellationTokenSource = new CancellationTokenSource();

            int timeInterval = 0;
            bool success = int.TryParse(LoopIntervalTxt.Text, out timeInterval);

            string API = APITxt.Text;
            string channel = channelTxt.Text;
            string brand = brandTxt.Text;
            string sqlTab = sqlTabtxt.Text;

            // btn status
            IsPlayRecord = true;
            startRecord.Enabled = false; stopRecord.Enabled = true; pauseRecord.Enabled = true;
            if (IsRecorded)
            {
                keyboardHook.Stop();
                mouseHook.Stop();
            }

            // play
            List<Task> tasks = new List<Task>();
            foreach (ProfileEntity item in profilesStarted)
            {
                var task = Task.Run(() =>
                {
                    Thread.Sleep(1000 * item.id);

                    Macro macro = new Macro();
                    macro.Load(Config.fileRecord + "\\" + record.record_name);
                    macro.loopInterval = timeInterval;
                    macro.hanlde = KeyboardHelper.FindWindowHandleFromProcesses(item.profile_id);
                    macro.APITxt = API;
                    macro.channelTxt = channel;
                    macro.brandTxt = brand;
                    macro.sqlTabtxt = sqlTab;

                    if (times < 0)
                    {
                        while (true)
                        {
                            macro.Play();
                        }
                    }
                    else
                    {
                        for (int i = 0; i < times; i++)
                        {
                            macro.Play();
                        }
                    }
                });

                tasks.Add(task);
            }

            await Task.WhenAll(tasks);

            startRecord.Enabled = true; stopRecord.Enabled = false; pauseRecord.Enabled = false;
            IsPlayRecord = false;
            StatusRecordApply("Ready", Color.Green);
        }

        // collase setting
        private void BtnSettingRecord_Click(object sender, EventArgs e)
        {
            if (panelSettingRecord.Height == 120)
            {
                panelSettingRecord.Height = 0;
            }
            else
            {
                panelSettingRecord.Height = 120;
            }
        }

        /*mouse action============>*/

        private void KeyboardHook_KeyAction(object? sender, VKeys key)
        {
            try
            {
                if (IsRecorded)
                {
                    AddDelayEvent();
                    CurrentMacro.AddEvent($"{sender},{key}");
                    return;
                }

                List<IntPtr> list = simalateProfileOther;
                IntPtr handle = GetForegroundWindow();
                if (handle != simalateProfileMain) { return; }

                uint repeatCount = 0;
                uint scanCode = (UInt32)key;
                uint extended = 0;
                uint context = 0;
                uint previousState = 0;
                uint transition = 0;

                uint lParamDown;
                uint lParamUp;

                lParamDown = repeatCount
                    | (scanCode << 16)
                    | (extended << 24)
                    | (context << 29)
                    | (previousState << 30)
                    | (transition << 31);
                previousState = 1;
                transition = 1;
                lParamUp = repeatCount
                    | (scanCode << 16)
                    | (extended << 24)
                    | (context << 29)
                    | (previousState << 30)
                    | (transition << 31);

                foreach (IntPtr item in list)
                {
                    new Task(() =>
                    {
                        try
                        {
                            if (sender == "down")
                            {
                                //AutoControl.SendKeyBoardDown(item, key);
                                //AutoControl.PostMessage(item, WM_KEYDOWN, new IntPtr((int)key), unchecked((IntPtr)(int)lParamDown));
                                KeyboardHelper.PostMessage(item, WM_KEYDOWN, new IntPtr((int)key), unchecked((IntPtr)(int)lParamDown));
                            }
                            else if (sender == "up")
                            {
                                //AutoControl.SendKeyBoardUp(item, key);
                                //AutoControl.PostMessage(item, WM_KEYUP, new IntPtr((int)key), unchecked((IntPtr)(int)lParamUp));
                                KeyboardHelper.PostMessage(item, WM_KEYUP, new IntPtr((int)key), unchecked((IntPtr)(int)lParamUp));
                            }
                        }
                        catch (Exception e)
                        {
                            Utils.Logs(e.Message, "KeyboardHook_KeyAction");
                        }
                    }).Start();
                }
            }
            catch (Exception) { }
        }

        internal static IntPtr MAKEWPARAM(int direction, int delta, WinMsgMouseKey button)
        {
            return (IntPtr)(((delta << 16) * Math.Sign(direction) | (ushort)button));
        }

        internal static IntPtr MAKELPARAM(int low, int high)
        {
            return (IntPtr)((high << 16) | (low & 0xFFFF));
        }

        private void MouseHook_MouseAction(object? sender, MouseEventArgs e)
        {
            try
            {
                new Task(() =>
                {
                    /*Debug.WriteLine($"{e.Button} 2");*/

                    RECT main = AutoControl.GetWindowRect(simalateProfileMain);
                    if (profileRecorded != null)
                    {
                        main = AutoControl.GetWindowRect(KeyboardHelper.FindWindowHandleFromProcesses(profileRecorded.profile_id));
                    }
                    int x = e.X - main.Left - 8;
                    int y = e.Y - main.Top;
                    // Debug.WriteLine($"{e.Button}");
                    // Thao tác bên ngoài cửa số
                    if (y <= 0 || x <= 0 || e.X > main.Right || e.Y > main.Bottom) { return; }
                    //Debug.WriteLine($"{e.Button} || {e.X}+{e.Y} || {main.Right}+{main.Bottom}");

                    if (IsRecorded)
                    {
                        AddDelayEvent();
                        CurrentMacro.AddEvent($"{e.Button},{x},{y},{e.Delta}");
                        return;
                    }

                    List<Task> tasks = new List<Task>();

                    foreach (IntPtr item in simalateProfileOther)
                    {
                        if (item == simalateProfileMain) continue;
                        tasks.Add(Task.Run(() =>
                        {
                            if (e.Button == MouseButtons.Middle)
                            {
                                RECT main = AutoControl.GetWindowRect(item);

                                int xNew = x + main.Left - 8;
                                int yNew = y + main.Top;
                                //Debug.WriteLine($"{e.X}+{e.Y} || {xNew}+{yNew}");
                                IntPtr wParam = MAKEWPARAM(1, e.Delta, WinMsgMouseKey.MK_LBUTTON);
                                IntPtr lParam = MAKELPARAM(xNew, yNew);

                                AutoControl.PostMessage(item, WM_MOUSEWHEEL, wParam, lParam);
                            }
                            else
                            {
                                AutoControl.SendClickOnPosition(item, x, y);
                            }
                        }));
                    }

                    Task t = Task.WhenAll(tasks);
                    t.Wait();

                    if (t.Status == TaskStatus.RanToCompletion)
                    {
                        Thread.Sleep(100);
                        SetForegroundWindow(simalateProfileMain);
                    }
                }).Start();
            }
            catch (Exception)
            { }
        }

        // ngoài lề
        // status strip
        private void StatusApply(string msg, Color color)
        {
            //this.StatusLabel.Text = "Status";
            this.StatusLabel.ForeColor = color;
            this.StatusMess.Text = msg;
            this.StatusMess.ForeColor = color;
        }

        private void StatusRecordApply(string msg, Color color)
        {
            this.StatusTxt.ForeColor = color;
            this.StatusRecord.Text = msg;
            this.StatusRecord.ForeColor = color;
        }

        private void AddDelayEvent()
        {
            long timeNow = DateTime.Now.Ticks;
            CurrentMacro.AddEvent($"delay,{timeNow - lastEventTime}");
            lastEventTime = timeNow;
        }

        private void onlyNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && (((TextBox)sender).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        // my lab
        private void playWithNewProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // add status
            // current scrips

            List<Point> lstPos = new List<Point>();
            string threadNum;
            int success;
            int fail;
            Point size = new Point();

            // params here
            RecordAsNewProfile form = new RecordAsNewProfile
            {
                StartPosition = FormStartPosition.CenterParent,
            };
            form.ShowDialog();
            if (form.DialogResult != DialogResult.OK)
            {
                return;
                // save to sql
            }
            threadNum = form.thread;
            lstPos = form.lstPos;
            size = form.size;
            totalSuccess = form.success;
            totalFail = form.fail;

            StatusRecordApply("Playing records", Color.Green);
            startRecord.Enabled = false; stopRecord.Enabled = true; pauseRecord.Enabled = true;
            IsPlayRecord = true;
            cancellationTokenSource.Dispose();
            cancellationTokenSource = new CancellationTokenSource();

            RtotalSuccess = 0;
            RtotalFail = 0;
            for (int j = 0; j < int.Parse(threadNum); j++)
            {
                int threadId = j;
                Thread thread = new Thread(() => playWithNewProfile(threadId, size, lstPos, ""));
                thread.Start();
            }
        }

        private void playWithNewProfile(int index, Point size, List<Point> lstPos, string recordName)
        {
            if (StatusRecord.Text == "Cancelling")
            {
                return;
            }
            //chill
            Random random = new Random();
            int sleepDuration = random.Next(500, 1001);
            Thread.Sleep(sleepDuration);

            // status
            this.Invoke(() =>
            {
                RunRecordTable.DataSource = new BindingList<ProfileEntity>(); // Initialize with an empty BindingList
                StatusRecordApply("Success: " + RtotalSuccess + " Fail: " + RtotalFail, Color.Green);
                LoadListProfiles();
            });

            int i = 0;
            // đk dừng
            while (RtotalSuccess < totalSuccess && RtotalFail < totalFail)
            {
                // khi nhấn stop
                if (cancellationTokenSource.Token.IsCancellationRequested)
                {
                    return;
                }

                // created new profile
                ProfileEntity profile = GoLogin.CreateProfile(Config.folderProfile, Config.fileName, Config.fileProxy);
                profile.group_id = "0";
                profile.profile_status = "On recording";
                profile.id = i++;

                if (string.IsNullOrEmpty(profile.folders))
                {
                    return;
                }

                this.Invoke(() =>
                {
                    ((BindingList<ProfileEntity>)RunRecordTable.DataSource).Add(profile);
                });

                // open them
                new Browser
                {
                    baseProfilePath = Config.folderProfile,
                    pos = lstPos.ElementAt(index),
                    proxy = false,
                    size = size
                }.Open(profile, profile.id);

                int timeInterval = 0;
                bool testtimeInterval = int.TryParse(LoopIntervalTxt.Text, out timeInterval);

                Macro macro = new Macro();
                macro.Load(Config.fileRecord + "\\" + recordName);
                macro.loopInterval = timeInterval;
                IntPtr ptr = KeyboardHelper.FindWindowHandleFromProcesses(profile.profile_id);
                macro.hanlde = ptr;
                macro.APITxt = APITxt.Text;
                macro.channelTxt = channelTxt.Text;
                macro.sqlTabtxt = sqlTabtxt.Text;
                macro.brandTxt = brandTxt.Text;
                // task
                macro.Play();
                // check sucess function save accout false delete it
                if (macro.IsSuscess)
                {
                    SQLite.InsertProfile(profile);
                    this.Invoke(() =>
                    {
                        Utils.ChangeGridView(RunRecordTable, "record_profile_id", profile.profile_id, "Succeed", Color.Black);
                    });
                    AutoControl.FindProcess(ptr).Kill();
                    lock (lockObject)
                    {
                        RtotalSuccess++;
                    }
                }
                else
                {
                    AutoControl.FindProcess(ptr).Kill();
                    this.Invoke(() =>
                    {
                        Utils.ChangeGridView(RunRecordTable, "record_profile_id", profile.profile_id, "Failed", Color.Black);
                    });
                    lock (lockObject)
                    {
                        RtotalFail++;
                        Thread.Sleep(index * 1000 + 1000);
                        GoLogin.RemoveProfile(profile);
                    }
                }

                playWithNewProfile(index, size, lstPos, recordName);
                // add num of susscess,  num of false
            }

            return;
        }

        private static int RtotalSuccess = 0;
        private static int RtotalFail = 0;
        private static int totalSuccess = 0;
        private static int totalFail = 0;
        private static readonly object lockObject = new object();

        private void StopRunScripsBtn_Click(object sender, EventArgs e)
        {
            if (IsPlayRecord)
            {
                keyboardHook.Stop();
                mouseHook.Stop();
                cancellationTokenSource.Cancel();
                IsPlayRecord = false;
                this.Invoke(() =>
                {
                    RunAsNewBtn.Enabled = true;
                    StopRunScripsBtn.Enabled = false;
                    IsPlayRecord = false;
                });
                StatusRecordApply("Cancelling", Color.Green);
            }
        }

        private void runWithRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            profilesRecorded = LoadSeletedProfiles();

            this.RunRecordTable.DataSource = profilesRecorded;
         
            materialTabControl1.SelectedIndex = 3;
           
        }

        private void RunBtn_Click(object sender, EventArgs e)
        {
            if (profilesRecorded == null)
            {
                MessageBox.Show("Selected some profiles");
                return;
            }
            // init
            List<Point> lstPos = new List<Point>();
            string threadNum;
            Point size = new Point();
            ComboBoxEntity? comboBox = ComboBoxRecord.SelectedItem as ComboBoxEntity;
            string recordsName = comboBox.name.ToString();
            if (comboBox.id == -1)
            {
                MessageBox.Show("No record file selected");
                return;
            }
            // show form
            RecordAsNewProfile form = new RecordAsNewProfile
            {
                StartPosition = FormStartPosition.CenterParent,
            };
            form.ShowDialog();
            if (form.DialogResult != DialogResult.OK)
            {
                return;
            }

            // params
            threadNum = form.thread;
            lstPos = form.lstPos;
            size = form.size;

            StatusRecordApply("Playing records", Color.Green);
            // turn off
            RunAsNewBtn.Enabled = false;
            StopRunScripsBtn.Enabled = true;
            IsPlayRecord = true;
            cancellationTokenSource.Dispose(); // re new token
            cancellationTokenSource = new CancellationTokenSource();
            // run

            // queue
            ConcurrentQueue<ProfileEntity> queue = new ConcurrentQueue<ProfileEntity>(profilesRecorded);
            Task[] tasks = new Task[int.Parse(threadNum)];
            for (int j = 0; j < int.Parse(threadNum); j++)
            {
                int currentIndex = j;
                tasks[j] = Task.Factory.StartNew(() => PlayRecord(queue, recordsName, currentIndex, size, lstPos));
            }
        }

        private void PlayRecord(ConcurrentQueue<ProfileEntity> queue, string recordName, int index, Point size, List<Point> lstPos)
        {
            ProfileEntity profile;
            while (queue.TryDequeue(out profile))
            {
                Random random = new Random();
                int sleepDuration = random.Next(500, 1001);
                Thread.Sleep(sleepDuration);

                // status
                this.Invoke(() =>
                {
                    StatusRecordApply("Success: " + RtotalSuccess + " Fail: " + RtotalFail, Color.Green);
                    LoadListProfiles();
                });
                new Browser
                {
                    baseProfilePath = Config.folderProfile,
                    pos = lstPos.ElementAt(index),
                    proxy = false,
                    size = size
                }.Open(profile, profile.id);

                int timeInterval = 0;
                bool testtimeInterval = int.TryParse(LoopIntervalTxt.Text, out timeInterval);

                Macro macro = new Macro();
                macro.Load(Config.fileRecord + "\\" + recordName);
                macro.loopInterval = timeInterval;
                IntPtr ptr = KeyboardHelper.FindWindowHandleFromProcesses(profile.profile_id);
                macro.hanlde = ptr;
                macro.APITxt = APITxt.Text;
                macro.channelTxt = channelTxt.Text;
                // Accessing sqlTabtxt on the UI thread
                sqlTabtxt.Invoke((MethodInvoker)delegate
                {
                    macro.sqlTabtxt = sqlTabtxt.Text;
                });

                // Accessing brandTxt on the UI thread
                brandTxt.Invoke((MethodInvoker)delegate
                {
                    macro.brandTxt = brandTxt.Text;
                });
                // task
                macro.Play();
                // check sucess function save accout false delete it
                if (macro.IsSuscess)
                {
                    this.Invoke(() =>
                    {
                        Utils.ChangeGridView(RunRecordTable, "record_profile_id", profile.profile_id, "Succeed", Color.Black);
                    });
                    AutoControl.FindProcess(ptr).Kill();
                    lock (lockObject)
                    {
                        RtotalSuccess++;
                    }
                }
                else
                {
                    AutoControl.FindProcess(ptr).Kill();
                    this.Invoke(() =>
                    {
                        Utils.ChangeGridView(RunRecordTable, "record_profile_id", profile.profile_id, "Failed", Color.Black);
                        Thread.Sleep(1000 * index);
                        GoLogin.RemoveProfile(profile);
                    });
                    RtotalFail++;
                }
            }
            this.Invoke(() =>
            {
                StatusRecordApply("Success: " + RtotalSuccess + " Fail: " + RtotalFail, Color.Green);
                LoadListProfiles();
                profilesRecorded = null;
            });
        }

        private void RunAsNewBtn_Click(object sender, EventArgs e)
        {
            // init
            List<Point> lstPos = new List<Point>();
            string threadNum;
            Point size = new Point();
            ComboBoxEntity? comboBox = ComboBoxRecord.SelectedItem as ComboBoxEntity;
            string recordsName = comboBox.name.ToString();
            if (comboBox.id == -1)
            {
                MessageBox.Show("No record file selected");
                return;
            }
            // show form
            RecordAsNewProfile form = new RecordAsNewProfile
            {
                StartPosition = FormStartPosition.CenterParent,
                RunAsNew = true
            };
            form.ShowDialog();
            if (form.DialogResult != DialogResult.OK)
            {
                return;
            }

            // params
            threadNum = form.thread;
            lstPos = form.lstPos;
            size = form.size;
            totalSuccess = form.success;
            totalFail = form.fail;
            RtotalSuccess = 0;
            RtotalFail = 0;

            // status
            StatusRecordApply("Playing records", Color.Green);
            RunAsNewBtn.Enabled = false;
            StopRunScripsBtn.Enabled = true;
            IsPlayRecord = true;
            cancellationTokenSource.Dispose(); // re new token
            cancellationTokenSource = new CancellationTokenSource();
            // run
            for (int j = 0; j < int.Parse(threadNum); j++)
            {
                int threadId = j;
                Thread thread = new(() => playWithNewProfile(threadId, size, lstPos, recordsName));
                thread.Start();
            }
        }

        private void materialTabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (materialTabControl1.SelectedIndex == 3)
            {
                LoadComboboxRecord();
            }
            else
            {
                return;
            }
        }


    }
}

