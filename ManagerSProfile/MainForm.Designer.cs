﻿namespace ManagerSProfile
{

    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle4 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle5 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle6 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle7 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle8 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle9 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle10 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle11 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle12 = new DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            DataGridViewCellStyle dataGridViewCellStyle13 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle14 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle15 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle16 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle17 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle18 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle19 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle20 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle21 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle22 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle23 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle24 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle25 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle26 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle27 = new DataGridViewCellStyle();
            materialTabControl1 = new MaterialSkin.Controls.MaterialTabControl();
            tabProfiles = new TabPage();
            tableLayoutPanel2 = new TableLayoutPanel();
            materialCard2 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel7 = new TableLayoutPanel();
            ButtonCreateProfiles = new MaterialSkin.Controls.MaterialButton();
            ButtonCreateGroup = new MaterialSkin.Controls.MaterialButton();
            filterByGroup = new MaterialSkin.Controls.MaterialComboBox();
            tableLayoutPanel5 = new TableLayoutPanel();
            materialCard6 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel12 = new TableLayoutPanel();
            tableLayoutPanel13 = new TableLayoutPanel();
            panel9 = new Panel();
            materialLabel14 = new MaterialSkin.Controls.MaterialLabel();
            panel10 = new Panel();
            tableLayoutPanel14 = new TableLayoutPanel();
            iScreen = new MaterialSkin.Controls.MaterialLabel();
            materialLabel22 = new MaterialSkin.Controls.MaterialLabel();
            iProxy = new MaterialSkin.Controls.MaterialLabel();
            materialLabel20 = new MaterialSkin.Controls.MaterialLabel();
            iUserAgent = new MaterialSkin.Controls.MaterialLabel();
            materialLabel18 = new MaterialSkin.Controls.MaterialLabel();
            iProfileID = new MaterialSkin.Controls.MaterialLabel();
            materialLabel16 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel24 = new MaterialSkin.Controls.MaterialLabel();
            iFacebook = new MaterialSkin.Controls.MaterialLabel();
            materialLabel26 = new MaterialSkin.Controls.MaterialLabel();
            iGoogle = new MaterialSkin.Controls.MaterialLabel();
            tableLayoutPanel1 = new TableLayoutPanel();
            materialCard1 = new MaterialSkin.Controls.MaterialCard();
            ListGroups = new DataGridView();
            group_name = new DataGridViewTextBoxColumn();
            group_total = new DataGridViewTextBoxColumn();
            MenuGroups = new ContextMenuStrip(components);
            EditGroup = new ToolStripMenuItem();
            toolStripSeparator2 = new ToolStripSeparator();
            DeleteGroup = new ToolStripMenuItem();
            materialCard5 = new MaterialSkin.Controls.MaterialCard();
            ListProfiles = new DataGridView();
            profile_id = new DataGridViewTextBoxColumn();
            profile_status = new DataGridViewTextBoxColumn();
            profile_proxy = new DataGridViewTextBoxColumn();
            profile_last_update = new DataGridViewTextBoxColumn();
            MenuProfiles = new ContextMenuStrip(components);
            StartProfile = new ToolStripMenuItem();
            EditProfile = new ToolStripMenuItem();
            moveGroup = new ToolStripMenuItem();
            updateProxyToolStripMenuItem = new ToolStripMenuItem();
            CloneProfile = new ToolStripMenuItem();
            toolStripSeparator3 = new ToolStripSeparator();
            SimulateProfile = new ToolStripMenuItem();
            toolStripSeparator1 = new ToolStripSeparator();
            DeleteProfile = new ToolStripMenuItem();
            runWithRecordToolStripMenuItem = new ToolStripMenuItem();
            tabSimulate = new TabPage();
            tableLayoutPanel34 = new TableLayoutPanel();
            materialCard22 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel35 = new TableLayoutPanel();
            ComboBoxPopUpSelected = new MaterialSkin.Controls.MaterialComboBox();
            simulateStop = new MaterialSkin.Controls.MaterialButton();
            simulateStart = new MaterialSkin.Controls.MaterialButton();
            simulateLoad = new MaterialSkin.Controls.MaterialButton();
            buttonReload = new Button();
            tableLayoutPanel32 = new TableLayoutPanel();
            statusStrip1 = new StatusStrip();
            StatusLabel = new ToolStripStatusLabel();
            StatusMess = new ToolStripStatusLabel();
            materialCard25 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel37 = new TableLayoutPanel();
            dataSimulator = new DataGridView();
            simu_id = new DataGridViewTextBoxColumn();
            simu_status = new DataGridViewTextBoxColumn();
            update_time = new DataGridViewTextBoxColumn();
            profile_simu_proxy = new DataGridViewTextBoxColumn();
            MenuSimu = new ContextMenuStrip(components);
            getTaskToolStripMenuItem = new ToolStripMenuItem();
            getName = new ToolStripMenuItem();
            firstNameToolStripMenuItem = new ToolStripMenuItem();
            lastnameToolStripMenuItem = new ToolStripMenuItem();
            getPhone = new ToolStripMenuItem();
            phone2ToolStripMenuItem = new ToolStripMenuItem();
            userNameToolStripMenuItem = new ToolStripMenuItem();
            username2ToolStripMenuItem = new ToolStripMenuItem();
            emailToolStripMenuItem = new ToolStripMenuItem();
            passwordToolStripMenuItem = new ToolStripMenuItem();
            password2ToolStripMenuItem = new ToolStripMenuItem();
            birthDayToolStripMenuItem = new ToolStripMenuItem();
            dodToolStripMenuItem = new ToolStripMenuItem();
            domToolStripMenuItem = new ToolStripMenuItem();
            doyToolStripMenuItem = new ToolStripMenuItem();
            getOTPToolStripMenuItem = new ToolStripMenuItem();
            saveAccoutToolStripMenuItem = new ToolStripMenuItem();
            getYahooMailToolStripMenuItem = new ToolStripMenuItem();
            yahooMailToolStripMenuItem = new ToolStripMenuItem();
            passwordToolStripMenuItem1 = new ToolStripMenuItem();
            getContentMailToolStripMenuItem = new ToolStripMenuItem();
            oTPToolStripMenuItem = new ToolStripMenuItem();
            linkToolStripMenuItem = new ToolStripMenuItem();
            viewRawToolStripMenuItem = new ToolStripMenuItem();
            copyToolStripMenuItem = new ToolStripMenuItem();
            pasteToolStripMenuItem = new ToolStripMenuItem();
            recordToolStripMenuItem1 = new ToolStripMenuItem();
            panelCollapsed = new Panel();
            tableLayoutPanel38 = new TableLayoutPanel();
            tableLayoutPanel40 = new TableLayoutPanel();
            label2 = new Label();
            channelTxt = new MaterialSkin.Controls.MaterialTextBox2();
            tableLayoutPanel41 = new TableLayoutPanel();
            label3 = new Label();
            brandTxt = new MaterialSkin.Controls.MaterialTextBox2();
            tableLayoutPanel42 = new TableLayoutPanel();
            label4 = new Label();
            sqlTabtxt = new MaterialSkin.Controls.MaterialTextBox();
            tableLayoutPanel39 = new TableLayoutPanel();
            label1 = new Label();
            APITxt = new MaterialSkin.Controls.MaterialTextBox2();
            ButtonCollapsed = new MaterialSkin.Controls.MaterialButton();
            tabRecord = new TabPage();
            tableLayoutPanel36 = new TableLayoutPanel();
            materialCard23 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel43 = new TableLayoutPanel();
            stopRecord = new MaterialSkin.Controls.MaterialButton();
            pauseRecord = new MaterialSkin.Controls.MaterialButton();
            startRecord = new MaterialSkin.Controls.MaterialButton();
            materialCard26 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel49 = new TableLayoutPanel();
            BtnSettingRecord = new MaterialSkin.Controls.MaterialButton();
            dataRecord = new DataGridView();
            record_name = new DataGridViewTextBoxColumn();
            record_time = new DataGridViewTextBoxColumn();
            record_last_modify = new DataGridViewTextBoxColumn();
            record_link = new DataGridViewTextBoxColumn();
            MenuRecord = new ContextMenuStrip(components);
            playToolStripMenuItem = new ToolStripMenuItem();
            deleteToolStripMenuItem = new ToolStripMenuItem();
            playWithNewProfileToolStripMenuItem = new ToolStripMenuItem();
            panelSettingRecord = new Panel();
            materialLabel64 = new MaterialSkin.Controls.MaterialLabel();
            LoopIntervalTxt = new TextBox();
            materialLabel63 = new MaterialSkin.Controls.MaterialLabel();
            label9 = new Label();
            numericTimes = new NumericUpDown();
            RadioBtnLoop4Ever = new MaterialSkin.Controls.MaterialRadioButton();
            RadioBtnLoop = new MaterialSkin.Controls.MaterialRadioButton();
            materialLabel62 = new MaterialSkin.Controls.MaterialLabel();
            tabRunRecord = new TabPage();
            tableLayoutPanel57 = new TableLayoutPanel();
            statusStrip2 = new StatusStrip();
            StatusTxt = new ToolStripStatusLabel();
            StatusRecord = new ToolStripStatusLabel();
            materialCard30 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel58 = new TableLayoutPanel();
            ComboBoxRecord = new MaterialSkin.Controls.MaterialComboBox();
            RunBtn = new MaterialSkin.Controls.MaterialButton();
            RunAsNewBtn = new MaterialSkin.Controls.MaterialButton();
            StopRunScripsBtn = new MaterialSkin.Controls.MaterialButton();
            materialCard31 = new MaterialSkin.Controls.MaterialCard();
            RunRecordTable = new DataGridView();
            record_profile_id = new DataGridViewTextBoxColumn();
            run_record_status = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn17 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn18 = new DataGridViewTextBoxColumn();
            tabSetting = new TabPage();
            tableLayoutPanel4 = new TableLayoutPanel();
            materialCard4 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel9 = new TableLayoutPanel();
            SaveSetting = new MaterialSkin.Controls.MaterialButton();
            flowLayoutPanel1 = new FlowLayoutPanel();
            materialCard10 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel11 = new TableLayoutPanel();
            panel7 = new Panel();
            materialLabel13 = new MaterialSkin.Controls.MaterialLabel();
            SelectFolderProfiles = new MaterialSkin.Controls.MaterialButton();
            panel8 = new Panel();
            materialLabel15 = new MaterialSkin.Controls.MaterialLabel();
            tFolderProfiles = new MaterialSkin.Controls.MaterialLabel();
            materialCard7 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel6 = new TableLayoutPanel();
            panel1 = new Panel();
            materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            SelectFileProxy = new MaterialSkin.Controls.MaterialButton();
            panel2 = new Panel();
            materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            tFileProxy = new MaterialSkin.Controls.MaterialLabel();
            materialCard8 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel8 = new TableLayoutPanel();
            panel3 = new Panel();
            materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            SelectFileName = new MaterialSkin.Controls.MaterialButton();
            panel4 = new Panel();
            materialLabel10 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            tFileName = new MaterialSkin.Controls.MaterialLabel();
            materialCard21 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel33 = new TableLayoutPanel();
            panel21 = new Panel();
            materialLabel59 = new MaterialSkin.Controls.MaterialLabel();
            SelectFileExt = new MaterialSkin.Controls.MaterialButton();
            panel22 = new Panel();
            panel23 = new Panel();
            multiLineTextExt = new MaterialSkin.Controls.MaterialMultiLineTextBox2();
            materialLabel60 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel61 = new MaterialSkin.Controls.MaterialLabel();
            tFileExt = new MaterialSkin.Controls.MaterialLabel();
            IconSidebar = new ImageList(components);
            Status = new DataGridViewTextBoxColumn();
            Proxy = new DataGridViewTextBoxColumn();
            materialCard9 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel10 = new TableLayoutPanel();
            panel5 = new Panel();
            materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            materialButton1 = new MaterialSkin.Controls.MaterialButton();
            panel6 = new Panel();
            materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel11 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            tableLayoutPanel15 = new TableLayoutPanel();
            materialCard11 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel16 = new TableLayoutPanel();
            materialButton2 = new MaterialSkin.Controls.MaterialButton();
            tableLayoutPanel17 = new TableLayoutPanel();
            materialCard12 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel18 = new TableLayoutPanel();
            tableLayoutPanel19 = new TableLayoutPanel();
            panel11 = new Panel();
            materialLabel17 = new MaterialSkin.Controls.MaterialLabel();
            panel12 = new Panel();
            tableLayoutPanel20 = new TableLayoutPanel();
            materialLabel19 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel21 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel23 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel25 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel27 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel28 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel29 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel30 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel31 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel32 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel33 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel34 = new MaterialSkin.Controls.MaterialLabel();
            materialCard13 = new MaterialSkin.Controls.MaterialCard();
            dataGridView1 = new DataGridView();
            dataGridViewTextBoxColumn1 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn2 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn3 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn4 = new DataGridViewTextBoxColumn();
            tabPage1 = new TabPage();
            tableLayoutPanel3 = new TableLayoutPanel();
            materialCard3 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel21 = new TableLayoutPanel();
            materialButton3 = new MaterialSkin.Controls.MaterialButton();
            materialButton4 = new MaterialSkin.Controls.MaterialButton();
            materialComboBox1 = new MaterialSkin.Controls.MaterialComboBox();
            tableLayoutPanel22 = new TableLayoutPanel();
            materialCard14 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel23 = new TableLayoutPanel();
            tableLayoutPanel24 = new TableLayoutPanel();
            panel13 = new Panel();
            materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            panel14 = new Panel();
            tableLayoutPanel25 = new TableLayoutPanel();
            materialLabel35 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel36 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel37 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel38 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel39 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel40 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel41 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel42 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel43 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel44 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel45 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel46 = new MaterialSkin.Controls.MaterialLabel();
            tableLayoutPanel26 = new TableLayoutPanel();
            materialCard15 = new MaterialSkin.Controls.MaterialCard();
            dataGridView2 = new DataGridView();
            dataGridViewTextBoxColumn5 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn6 = new DataGridViewTextBoxColumn();
            materialCard16 = new MaterialSkin.Controls.MaterialCard();
            dataGridView3 = new DataGridView();
            dataGridViewTextBoxColumn7 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn8 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn9 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn10 = new DataGridViewTextBoxColumn();
            tabPage2 = new TabPage();
            tableLayoutPanel27 = new TableLayoutPanel();
            materialCard17 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel28 = new TableLayoutPanel();
            materialButton5 = new MaterialSkin.Controls.MaterialButton();
            flowLayoutPanel2 = new FlowLayoutPanel();
            materialCard18 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel29 = new TableLayoutPanel();
            panel15 = new Panel();
            materialLabel47 = new MaterialSkin.Controls.MaterialLabel();
            materialButton6 = new MaterialSkin.Controls.MaterialButton();
            panel16 = new Panel();
            materialLabel48 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel49 = new MaterialSkin.Controls.MaterialLabel();
            materialCard19 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel30 = new TableLayoutPanel();
            panel17 = new Panel();
            materialLabel50 = new MaterialSkin.Controls.MaterialLabel();
            materialButton7 = new MaterialSkin.Controls.MaterialButton();
            panel18 = new Panel();
            materialLabel51 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel52 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel53 = new MaterialSkin.Controls.MaterialLabel();
            materialCard20 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel31 = new TableLayoutPanel();
            panel19 = new Panel();
            materialLabel54 = new MaterialSkin.Controls.MaterialLabel();
            materialButton8 = new MaterialSkin.Controls.MaterialButton();
            panel20 = new Panel();
            materialLabel55 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel56 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel57 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel58 = new MaterialSkin.Controls.MaterialLabel();
            materialTabControl2 = new MaterialSkin.Controls.MaterialTabControl();
            materialExpansionPanel2 = new MaterialSkin.Controls.MaterialExpansionPanel();
            tableLayoutPanel44 = new TableLayoutPanel();
            tableLayoutPanel45 = new TableLayoutPanel();
            label5 = new Label();
            materialTextBox21 = new MaterialSkin.Controls.MaterialTextBox2();
            tableLayoutPanel46 = new TableLayoutPanel();
            label6 = new Label();
            materialTextBox22 = new MaterialSkin.Controls.MaterialTextBox2();
            tableLayoutPanel47 = new TableLayoutPanel();
            label7 = new Label();
            materialTextBox1 = new MaterialSkin.Controls.MaterialTextBox();
            tableLayoutPanel48 = new TableLayoutPanel();
            label8 = new Label();
            materialTextBox23 = new MaterialSkin.Controls.MaterialTextBox2();
            materialLabel65 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel66 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel67 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel68 = new MaterialSkin.Controls.MaterialLabel();
            tableLayoutPanel50 = new TableLayoutPanel();
            materialLabel69 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel70 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel71 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel72 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel73 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel74 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel75 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel76 = new MaterialSkin.Controls.MaterialLabel();
            panel24 = new Panel();
            materialLabel77 = new MaterialSkin.Controls.MaterialLabel();
            tableLayoutPanel51 = new TableLayoutPanel();
            materialCard24 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel52 = new TableLayoutPanel();
            panel25 = new Panel();
            tableLayoutPanel53 = new TableLayoutPanel();
            materialCard27 = new MaterialSkin.Controls.MaterialCard();
            dataGridView4 = new DataGridView();
            dataGridViewTextBoxColumn11 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn12 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn13 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn14 = new DataGridViewTextBoxColumn();
            materialButton9 = new MaterialSkin.Controls.MaterialButton();
            tableLayoutPanel54 = new TableLayoutPanel();
            materialCard28 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel55 = new TableLayoutPanel();
            panel26 = new Panel();
            materialLabel78 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel79 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel80 = new MaterialSkin.Controls.MaterialLabel();
            materialCard29 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel56 = new TableLayoutPanel();
            panel27 = new Panel();
            materialLabel81 = new MaterialSkin.Controls.MaterialLabel();
            materialButton10 = new MaterialSkin.Controls.MaterialButton();
            panel28 = new Panel();
            materialLabel82 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel83 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel84 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel98 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel99 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel100 = new MaterialSkin.Controls.MaterialLabel();
            panel31 = new Panel();
            materialLabel101 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel102 = new MaterialSkin.Controls.MaterialLabel();
            materialButton13 = new MaterialSkin.Controls.MaterialButton();
            panel32 = new Panel();
            tableLayoutPanel64 = new TableLayoutPanel();
            materialCard34 = new MaterialSkin.Controls.MaterialCard();
            materialTextBox24 = new MaterialSkin.Controls.MaterialTextBox2();
            materialCard35 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel65 = new TableLayoutPanel();
            panel33 = new Panel();
            materialLabel103 = new MaterialSkin.Controls.MaterialLabel();
            materialButton14 = new MaterialSkin.Controls.MaterialButton();
            panel34 = new Panel();
            panel35 = new Panel();
            materialMultiLineTextBox21 = new MaterialSkin.Controls.MaterialMultiLineTextBox2();
            materialLabel104 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel105 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel106 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel107 = new MaterialSkin.Controls.MaterialLabel();
            panel36 = new Panel();
            materialButton15 = new MaterialSkin.Controls.MaterialButton();
            tableLayoutPanel66 = new TableLayoutPanel();
            dataGridViewTextBoxColumn21 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn22 = new DataGridViewTextBoxColumn();
            imageList1 = new ImageList(components);
            panel37 = new Panel();
            materialLabel108 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel109 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel110 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel111 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel112 = new MaterialSkin.Controls.MaterialLabel();
            panel38 = new Panel();
            materialButton16 = new MaterialSkin.Controls.MaterialButton();
            tableLayoutPanel67 = new TableLayoutPanel();
            materialCard37 = new MaterialSkin.Controls.MaterialCard();
            panel39 = new Panel();
            materialLabel113 = new MaterialSkin.Controls.MaterialLabel();
            materialButton17 = new MaterialSkin.Controls.MaterialButton();
            materialLabel114 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel115 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel116 = new MaterialSkin.Controls.MaterialLabel();
            panel40 = new Panel();
            tableLayoutPanel68 = new TableLayoutPanel();
            materialCard38 = new MaterialSkin.Controls.MaterialCard();
            materialLabel117 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel118 = new MaterialSkin.Controls.MaterialLabel();
            tabPage4 = new TabPage();
            label10 = new Label();
            tableLayoutPanel69 = new TableLayoutPanel();
            panel41 = new Panel();
            materialLabel119 = new MaterialSkin.Controls.MaterialLabel();
            tableLayoutPanel74 = new TableLayoutPanel();
            panel42 = new Panel();
            materialButton18 = new MaterialSkin.Controls.MaterialButton();
            materialCard39 = new MaterialSkin.Controls.MaterialCard();
            flowLayoutPanel3 = new FlowLayoutPanel();
            materialButton19 = new MaterialSkin.Controls.MaterialButton();
            tableLayoutPanel75 = new TableLayoutPanel();
            materialCard40 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel76 = new TableLayoutPanel();
            materialLabel120 = new MaterialSkin.Controls.MaterialLabel();
            dataGridViewTextBoxColumn23 = new DataGridViewTextBoxColumn();
            materialCard41 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel77 = new TableLayoutPanel();
            materialComboBox3 = new MaterialSkin.Controls.MaterialComboBox();
            materialButton20 = new MaterialSkin.Controls.MaterialButton();
            materialButton21 = new MaterialSkin.Controls.MaterialButton();
            materialButton22 = new MaterialSkin.Controls.MaterialButton();
            button1 = new Button();
            tableLayoutPanel78 = new TableLayoutPanel();
            tableLayoutPanel79 = new TableLayoutPanel();
            statusStrip3 = new StatusStrip();
            toolStripStatusLabel1 = new ToolStripStatusLabel();
            toolStripStatusLabel2 = new ToolStripStatusLabel();
            materialCard42 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel80 = new TableLayoutPanel();
            dataGridView7 = new DataGridView();
            dataGridViewTextBoxColumn24 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn25 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn26 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn27 = new DataGridViewTextBoxColumn();
            panel43 = new Panel();
            tableLayoutPanel81 = new TableLayoutPanel();
            tableLayoutPanel82 = new TableLayoutPanel();
            label14 = new Label();
            materialTextBox27 = new MaterialSkin.Controls.MaterialTextBox2();
            tableLayoutPanel83 = new TableLayoutPanel();
            label15 = new Label();
            materialTextBox28 = new MaterialSkin.Controls.MaterialTextBox2();
            tableLayoutPanel84 = new TableLayoutPanel();
            label16 = new Label();
            materialTextBox3 = new MaterialSkin.Controls.MaterialTextBox();
            tableLayoutPanel85 = new TableLayoutPanel();
            label17 = new Label();
            materialTextBox29 = new MaterialSkin.Controls.MaterialTextBox2();
            materialButton23 = new MaterialSkin.Controls.MaterialButton();
            tabPage6 = new TabPage();
            dataGridViewTextBoxColumn28 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn29 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn30 = new DataGridViewTextBoxColumn();
            dataGridView8 = new DataGridView();
            materialCard43 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel86 = new TableLayoutPanel();
            materialLabel121 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel122 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel123 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel124 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel125 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel126 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel127 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel128 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel129 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel130 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel131 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel132 = new MaterialSkin.Controls.MaterialLabel();
            panel44 = new Panel();
            materialLabel133 = new MaterialSkin.Controls.MaterialLabel();
            materialCard44 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel87 = new TableLayoutPanel();
            materialButton24 = new MaterialSkin.Controls.MaterialButton();
            materialButton25 = new MaterialSkin.Controls.MaterialButton();
            materialComboBox4 = new MaterialSkin.Controls.MaterialComboBox();
            panel45 = new Panel();
            tableLayoutPanel88 = new TableLayoutPanel();
            tableLayoutPanel89 = new TableLayoutPanel();
            materialCard45 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel90 = new TableLayoutPanel();
            tableLayoutPanel91 = new TableLayoutPanel();
            materialCard46 = new MaterialSkin.Controls.MaterialCard();
            dataGridView9 = new DataGridView();
            dataGridViewTextBoxColumn31 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn32 = new DataGridViewTextBoxColumn();
            tableLayoutPanel92 = new TableLayoutPanel();
            tabPage7 = new TabPage();
            materialTabControl4 = new MaterialSkin.Controls.MaterialTabControl();
            tabPage8 = new TabPage();
            tableLayoutPanel93 = new TableLayoutPanel();
            materialCard47 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel94 = new TableLayoutPanel();
            materialButton26 = new MaterialSkin.Controls.MaterialButton();
            materialButton27 = new MaterialSkin.Controls.MaterialButton();
            materialButton28 = new MaterialSkin.Controls.MaterialButton();
            materialCard48 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel95 = new TableLayoutPanel();
            materialButton29 = new MaterialSkin.Controls.MaterialButton();
            dataGridView10 = new DataGridView();
            dataGridViewTextBoxColumn33 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn34 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn35 = new DataGridViewTextBoxColumn();
            dataGridViewTextBoxColumn36 = new DataGridViewTextBoxColumn();
            panel46 = new Panel();
            materialLabel134 = new MaterialSkin.Controls.MaterialLabel();
            textBox1 = new TextBox();
            materialLabel135 = new MaterialSkin.Controls.MaterialLabel();
            label18 = new Label();
            numericUpDown1 = new NumericUpDown();
            materialRadioButton1 = new MaterialSkin.Controls.MaterialRadioButton();
            materialRadioButton2 = new MaterialSkin.Controls.MaterialRadioButton();
            materialLabel136 = new MaterialSkin.Controls.MaterialLabel();
            tabPage9 = new TabPage();
            tableLayoutPanel96 = new TableLayoutPanel();
            materialCard49 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel97 = new TableLayoutPanel();
            materialButton30 = new MaterialSkin.Controls.MaterialButton();
            flowLayoutPanel4 = new FlowLayoutPanel();
            materialCard50 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel98 = new TableLayoutPanel();
            panel47 = new Panel();
            materialLabel137 = new MaterialSkin.Controls.MaterialLabel();
            materialButton31 = new MaterialSkin.Controls.MaterialButton();
            panel48 = new Panel();
            materialLabel138 = new MaterialSkin.Controls.MaterialLabel();
            materialTabControl1.SuspendLayout();
            tabProfiles.SuspendLayout();
            tableLayoutPanel2.SuspendLayout();
            materialCard2.SuspendLayout();
            tableLayoutPanel7.SuspendLayout();
            tableLayoutPanel5.SuspendLayout();
            materialCard6.SuspendLayout();
            tableLayoutPanel12.SuspendLayout();
            tableLayoutPanel13.SuspendLayout();
            panel10.SuspendLayout();
            tableLayoutPanel14.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            materialCard1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)ListGroups).BeginInit();
            MenuGroups.SuspendLayout();
            materialCard5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)ListProfiles).BeginInit();
            MenuProfiles.SuspendLayout();
            tabSimulate.SuspendLayout();
            tableLayoutPanel34.SuspendLayout();
            materialCard22.SuspendLayout();
            tableLayoutPanel35.SuspendLayout();
            tableLayoutPanel32.SuspendLayout();
            statusStrip1.SuspendLayout();
            materialCard25.SuspendLayout();
            tableLayoutPanel37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataSimulator).BeginInit();
            MenuSimu.SuspendLayout();
            panelCollapsed.SuspendLayout();
            tableLayoutPanel38.SuspendLayout();
            tableLayoutPanel40.SuspendLayout();
            tableLayoutPanel41.SuspendLayout();
            tableLayoutPanel42.SuspendLayout();
            tableLayoutPanel39.SuspendLayout();
            tabRecord.SuspendLayout();
            tableLayoutPanel36.SuspendLayout();
            materialCard23.SuspendLayout();
            tableLayoutPanel43.SuspendLayout();
            materialCard26.SuspendLayout();
            tableLayoutPanel49.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataRecord).BeginInit();
            MenuRecord.SuspendLayout();
            panelSettingRecord.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numericTimes).BeginInit();
            tabRunRecord.SuspendLayout();
            tableLayoutPanel57.SuspendLayout();
            statusStrip2.SuspendLayout();
            materialCard30.SuspendLayout();
            tableLayoutPanel58.SuspendLayout();
            materialCard31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)RunRecordTable).BeginInit();
            tabSetting.SuspendLayout();
            tableLayoutPanel4.SuspendLayout();
            materialCard4.SuspendLayout();
            tableLayoutPanel9.SuspendLayout();
            flowLayoutPanel1.SuspendLayout();
            materialCard10.SuspendLayout();
            tableLayoutPanel11.SuspendLayout();
            panel7.SuspendLayout();
            panel8.SuspendLayout();
            materialCard7.SuspendLayout();
            tableLayoutPanel6.SuspendLayout();
            panel1.SuspendLayout();
            panel2.SuspendLayout();
            materialCard8.SuspendLayout();
            tableLayoutPanel8.SuspendLayout();
            panel3.SuspendLayout();
            panel4.SuspendLayout();
            materialCard21.SuspendLayout();
            tableLayoutPanel33.SuspendLayout();
            panel21.SuspendLayout();
            panel22.SuspendLayout();
            panel23.SuspendLayout();
            tableLayoutPanel10.SuspendLayout();
            panel5.SuspendLayout();
            panel6.SuspendLayout();
            tableLayoutPanel15.SuspendLayout();
            materialCard11.SuspendLayout();
            tableLayoutPanel16.SuspendLayout();
            tableLayoutPanel17.SuspendLayout();
            materialCard12.SuspendLayout();
            tableLayoutPanel18.SuspendLayout();
            tableLayoutPanel19.SuspendLayout();
            panel12.SuspendLayout();
            tableLayoutPanel20.SuspendLayout();
            materialCard13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            tabPage1.SuspendLayout();
            tableLayoutPanel3.SuspendLayout();
            materialCard3.SuspendLayout();
            tableLayoutPanel21.SuspendLayout();
            tableLayoutPanel22.SuspendLayout();
            materialCard14.SuspendLayout();
            tableLayoutPanel23.SuspendLayout();
            tableLayoutPanel24.SuspendLayout();
            panel14.SuspendLayout();
            tableLayoutPanel25.SuspendLayout();
            tableLayoutPanel26.SuspendLayout();
            materialCard15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView2).BeginInit();
            materialCard16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView3).BeginInit();
            tableLayoutPanel27.SuspendLayout();
            materialCard17.SuspendLayout();
            tableLayoutPanel28.SuspendLayout();
            flowLayoutPanel2.SuspendLayout();
            materialCard18.SuspendLayout();
            tableLayoutPanel29.SuspendLayout();
            panel15.SuspendLayout();
            panel16.SuspendLayout();
            materialCard19.SuspendLayout();
            tableLayoutPanel30.SuspendLayout();
            panel17.SuspendLayout();
            panel18.SuspendLayout();
            materialCard20.SuspendLayout();
            tableLayoutPanel31.SuspendLayout();
            panel19.SuspendLayout();
            panel20.SuspendLayout();
            materialTabControl2.SuspendLayout();
            materialExpansionPanel2.SuspendLayout();
            tableLayoutPanel44.SuspendLayout();
            tableLayoutPanel45.SuspendLayout();
            tableLayoutPanel46.SuspendLayout();
            tableLayoutPanel47.SuspendLayout();
            tableLayoutPanel48.SuspendLayout();
            tableLayoutPanel50.SuspendLayout();
            tableLayoutPanel51.SuspendLayout();
            materialCard24.SuspendLayout();
            tableLayoutPanel52.SuspendLayout();
            panel25.SuspendLayout();
            tableLayoutPanel53.SuspendLayout();
            materialCard27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView4).BeginInit();
            tableLayoutPanel54.SuspendLayout();
            materialCard28.SuspendLayout();
            tableLayoutPanel55.SuspendLayout();
            panel26.SuspendLayout();
            materialCard29.SuspendLayout();
            tableLayoutPanel56.SuspendLayout();
            panel27.SuspendLayout();
            panel28.SuspendLayout();
            panel31.SuspendLayout();
            panel32.SuspendLayout();
            tableLayoutPanel64.SuspendLayout();
            materialCard34.SuspendLayout();
            materialCard35.SuspendLayout();
            tableLayoutPanel65.SuspendLayout();
            panel33.SuspendLayout();
            panel34.SuspendLayout();
            panel35.SuspendLayout();
            panel36.SuspendLayout();
            tableLayoutPanel66.SuspendLayout();
            panel37.SuspendLayout();
            panel38.SuspendLayout();
            tableLayoutPanel67.SuspendLayout();
            materialCard37.SuspendLayout();
            panel39.SuspendLayout();
            panel40.SuspendLayout();
            tableLayoutPanel68.SuspendLayout();
            materialCard38.SuspendLayout();
            tableLayoutPanel69.SuspendLayout();
            panel41.SuspendLayout();
            tableLayoutPanel74.SuspendLayout();
            panel42.SuspendLayout();
            materialCard39.SuspendLayout();
            flowLayoutPanel3.SuspendLayout();
            tableLayoutPanel75.SuspendLayout();
            materialCard40.SuspendLayout();
            tableLayoutPanel76.SuspendLayout();
            materialCard41.SuspendLayout();
            tableLayoutPanel77.SuspendLayout();
            tableLayoutPanel78.SuspendLayout();
            tableLayoutPanel79.SuspendLayout();
            statusStrip3.SuspendLayout();
            materialCard42.SuspendLayout();
            tableLayoutPanel80.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView7).BeginInit();
            panel43.SuspendLayout();
            tableLayoutPanel81.SuspendLayout();
            tableLayoutPanel82.SuspendLayout();
            tableLayoutPanel83.SuspendLayout();
            tableLayoutPanel84.SuspendLayout();
            tableLayoutPanel85.SuspendLayout();
            tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView8).BeginInit();
            materialCard43.SuspendLayout();
            tableLayoutPanel86.SuspendLayout();
            panel44.SuspendLayout();
            materialCard44.SuspendLayout();
            tableLayoutPanel87.SuspendLayout();
            tableLayoutPanel88.SuspendLayout();
            tableLayoutPanel89.SuspendLayout();
            materialCard45.SuspendLayout();
            tableLayoutPanel90.SuspendLayout();
            tableLayoutPanel91.SuspendLayout();
            materialCard46.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView9).BeginInit();
            tableLayoutPanel92.SuspendLayout();
            tabPage7.SuspendLayout();
            materialTabControl4.SuspendLayout();
            tabPage8.SuspendLayout();
            tableLayoutPanel93.SuspendLayout();
            materialCard47.SuspendLayout();
            tableLayoutPanel94.SuspendLayout();
            materialCard48.SuspendLayout();
            tableLayoutPanel95.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView10).BeginInit();
            panel46.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDown1).BeginInit();
            tabPage9.SuspendLayout();
            tableLayoutPanel96.SuspendLayout();
            materialCard49.SuspendLayout();
            tableLayoutPanel97.SuspendLayout();
            flowLayoutPanel4.SuspendLayout();
            materialCard50.SuspendLayout();
            tableLayoutPanel98.SuspendLayout();
            panel47.SuspendLayout();
            panel48.SuspendLayout();
            SuspendLayout();
            // 
            // materialTabControl1
            // 
            materialTabControl1.Appearance = TabAppearance.Buttons;
            materialTabControl1.Controls.Add(tabProfiles);
            materialTabControl1.Controls.Add(tabSimulate);
            materialTabControl1.Controls.Add(tabRecord);
            materialTabControl1.Controls.Add(tabRunRecord);
            materialTabControl1.Controls.Add(tabSetting);
            materialTabControl1.Depth = 0;
            materialTabControl1.Dock = DockStyle.Fill;
            materialTabControl1.ImageList = IconSidebar;
            materialTabControl1.Location = new Point(3, 64);
            materialTabControl1.MouseState = MaterialSkin.MouseState.HOVER;
            materialTabControl1.Multiline = true;
            materialTabControl1.Name = "materialTabControl1";
            materialTabControl1.SelectedIndex = 0;
            materialTabControl1.Size = new Size(1712, 1003);
            materialTabControl1.TabIndex = 1;
            materialTabControl1.SelectedIndexChanged += materialTabControl1_SelectedIndexChanged;
            // 
            // tabProfiles
            // 
            tabProfiles.Controls.Add(tableLayoutPanel2);
            tabProfiles.ImageKey = "browser.png";
            tabProfiles.Location = new Point(4, 32);
            tabProfiles.Name = "tabProfiles";
            tabProfiles.Padding = new Padding(3);
            tabProfiles.Size = new Size(1704, 967);
            tabProfiles.TabIndex = 1;
            tabProfiles.Text = "Profiles";
            tabProfiles.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.ColumnCount = 1;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel2.Controls.Add(materialCard2, 0, 0);
            tableLayoutPanel2.Controls.Add(tableLayoutPanel5, 0, 1);
            tableLayoutPanel2.Dock = DockStyle.Fill;
            tableLayoutPanel2.Location = new Point(3, 3);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 2;
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Absolute, 75F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel2.Size = new Size(1698, 961);
            tableLayoutPanel2.TabIndex = 1;
            // 
            // materialCard2
            // 
            materialCard2.BackColor = Color.FromArgb(255, 255, 255);
            materialCard2.Controls.Add(tableLayoutPanel7);
            materialCard2.Depth = 0;
            materialCard2.Dock = DockStyle.Fill;
            materialCard2.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard2.Location = new Point(10, 10);
            materialCard2.Margin = new Padding(10);
            materialCard2.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard2.Name = "materialCard2";
            materialCard2.Padding = new Padding(5);
            materialCard2.Size = new Size(1678, 55);
            materialCard2.TabIndex = 0;
            // 
            // tableLayoutPanel7
            // 
            tableLayoutPanel7.ColumnCount = 4;
            tableLayoutPanel7.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 240F));
            tableLayoutPanel7.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel7.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 120F));
            tableLayoutPanel7.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 130F));
            tableLayoutPanel7.Controls.Add(ButtonCreateProfiles, 3, 0);
            tableLayoutPanel7.Controls.Add(ButtonCreateGroup, 2, 0);
            tableLayoutPanel7.Controls.Add(filterByGroup, 0, 0);
            tableLayoutPanel7.Dock = DockStyle.Fill;
            tableLayoutPanel7.Location = new Point(5, 5);
            tableLayoutPanel7.Margin = new Padding(0);
            tableLayoutPanel7.Name = "tableLayoutPanel7";
            tableLayoutPanel7.RowCount = 1;
            tableLayoutPanel7.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel7.Size = new Size(1668, 45);
            tableLayoutPanel7.TabIndex = 0;
            // 
            // ButtonCreateProfiles
            // 
            ButtonCreateProfiles.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            ButtonCreateProfiles.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            ButtonCreateProfiles.Depth = 0;
            ButtonCreateProfiles.Dock = DockStyle.Fill;
            ButtonCreateProfiles.HighEmphasis = true;
            ButtonCreateProfiles.Icon = null;
            ButtonCreateProfiles.Location = new Point(1543, 5);
            ButtonCreateProfiles.Margin = new Padding(5);
            ButtonCreateProfiles.MouseState = MaterialSkin.MouseState.HOVER;
            ButtonCreateProfiles.Name = "ButtonCreateProfiles";
            ButtonCreateProfiles.NoAccentTextColor = Color.Empty;
            ButtonCreateProfiles.Size = new Size(120, 35);
            ButtonCreateProfiles.TabIndex = 0;
            ButtonCreateProfiles.Text = "Create Profiles";
            ButtonCreateProfiles.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            ButtonCreateProfiles.UseAccentColor = false;
            ButtonCreateProfiles.UseVisualStyleBackColor = true;
            ButtonCreateProfiles.Click += ButtonCreateProfiles_Click;
            // 
            // ButtonCreateGroup
            // 
            ButtonCreateGroup.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            ButtonCreateGroup.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            ButtonCreateGroup.Depth = 0;
            ButtonCreateGroup.Dock = DockStyle.Fill;
            ButtonCreateGroup.HighEmphasis = true;
            ButtonCreateGroup.Icon = null;
            ButtonCreateGroup.Location = new Point(1423, 5);
            ButtonCreateGroup.Margin = new Padding(5);
            ButtonCreateGroup.MouseState = MaterialSkin.MouseState.HOVER;
            ButtonCreateGroup.Name = "ButtonCreateGroup";
            ButtonCreateGroup.NoAccentTextColor = Color.Empty;
            ButtonCreateGroup.Size = new Size(110, 35);
            ButtonCreateGroup.TabIndex = 1;
            ButtonCreateGroup.Text = "Create Group";
            ButtonCreateGroup.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            ButtonCreateGroup.UseAccentColor = false;
            ButtonCreateGroup.UseVisualStyleBackColor = true;
            ButtonCreateGroup.Click += ButtonCreateGroup_Click;
            // 
            // filterByGroup
            // 
            filterByGroup.AutoResize = false;
            filterByGroup.BackColor = Color.FromArgb(255, 255, 255);
            filterByGroup.Depth = 0;
            filterByGroup.Dock = DockStyle.Fill;
            filterByGroup.DrawMode = DrawMode.OwnerDrawVariable;
            filterByGroup.DropDownHeight = 174;
            filterByGroup.DropDownStyle = ComboBoxStyle.DropDownList;
            filterByGroup.DropDownWidth = 121;
            filterByGroup.Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            filterByGroup.ForeColor = Color.FromArgb(222, 0, 0, 0);
            filterByGroup.FormattingEnabled = true;
            filterByGroup.IntegralHeight = false;
            filterByGroup.ItemHeight = 43;
            filterByGroup.Location = new Point(3, 3);
            filterByGroup.MaxDropDownItems = 4;
            filterByGroup.MouseState = MaterialSkin.MouseState.OUT;
            filterByGroup.Name = "filterByGroup";
            filterByGroup.Size = new Size(234, 49);
            filterByGroup.StartIndex = 0;
            filterByGroup.TabIndex = 2;
            filterByGroup.SelectedIndexChanged += filterByGroup_SelectedIndexChanged;
            // 
            // tableLayoutPanel5
            // 
            tableLayoutPanel5.ColumnCount = 2;
            tableLayoutPanel5.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel5.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 300F));
            tableLayoutPanel5.Controls.Add(materialCard6, 1, 0);
            tableLayoutPanel5.Controls.Add(tableLayoutPanel1, 0, 0);
            tableLayoutPanel5.Dock = DockStyle.Fill;
            tableLayoutPanel5.Location = new Point(0, 75);
            tableLayoutPanel5.Margin = new Padding(0);
            tableLayoutPanel5.Name = "tableLayoutPanel5";
            tableLayoutPanel5.RowCount = 1;
            tableLayoutPanel5.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel5.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel5.Size = new Size(1698, 886);
            tableLayoutPanel5.TabIndex = 1;
            // 
            // materialCard6
            // 
            materialCard6.BackColor = Color.FromArgb(255, 255, 255);
            materialCard6.Controls.Add(tableLayoutPanel12);
            materialCard6.Depth = 0;
            materialCard6.Dock = DockStyle.Fill;
            materialCard6.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard6.Location = new Point(1412, 2);
            materialCard6.Margin = new Padding(14, 2, 14, 14);
            materialCard6.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard6.Name = "materialCard6";
            materialCard6.Padding = new Padding(14);
            materialCard6.Size = new Size(272, 870);
            materialCard6.TabIndex = 2;
            // 
            // tableLayoutPanel12
            // 
            tableLayoutPanel12.ColumnCount = 1;
            tableLayoutPanel12.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel12.Controls.Add(tableLayoutPanel13, 0, 0);
            tableLayoutPanel12.Controls.Add(panel10, 0, 1);
            tableLayoutPanel12.Dock = DockStyle.Fill;
            tableLayoutPanel12.Location = new Point(14, 14);
            tableLayoutPanel12.Margin = new Padding(0);
            tableLayoutPanel12.Name = "tableLayoutPanel12";
            tableLayoutPanel12.RowCount = 2;
            tableLayoutPanel12.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel12.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel12.Size = new Size(244, 842);
            tableLayoutPanel12.TabIndex = 0;
            // 
            // tableLayoutPanel13
            // 
            tableLayoutPanel13.ColumnCount = 2;
            tableLayoutPanel13.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 48F));
            tableLayoutPanel13.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel13.Controls.Add(panel9, 0, 0);
            tableLayoutPanel13.Controls.Add(materialLabel14, 1, 0);
            tableLayoutPanel13.Dock = DockStyle.Fill;
            tableLayoutPanel13.Location = new Point(0, 0);
            tableLayoutPanel13.Margin = new Padding(0);
            tableLayoutPanel13.Name = "tableLayoutPanel13";
            tableLayoutPanel13.RowCount = 1;
            tableLayoutPanel13.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel13.Size = new Size(244, 40);
            tableLayoutPanel13.TabIndex = 0;
            // 
            // panel9
            // 
            panel9.BackgroundImage = Properties.Resources.fingerprint;
            panel9.BackgroundImageLayout = ImageLayout.Zoom;
            panel9.Location = new Point(6, 6);
            panel9.Margin = new Padding(6);
            panel9.Name = "panel9";
            panel9.Size = new Size(36, 28);
            panel9.TabIndex = 0;
            // 
            // materialLabel14
            // 
            materialLabel14.Depth = 0;
            materialLabel14.Dock = DockStyle.Fill;
            materialLabel14.Font = new Font("Roboto", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel14.FontType = MaterialSkin.MaterialSkinManager.fontType.Button;
            materialLabel14.Location = new Point(53, 5);
            materialLabel14.Margin = new Padding(5);
            materialLabel14.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel14.Name = "materialLabel14";
            materialLabel14.Size = new Size(186, 30);
            materialLabel14.TabIndex = 1;
            materialLabel14.Text = "Profile Info";
            materialLabel14.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // panel10
            // 
            panel10.AutoScroll = true;
            panel10.Controls.Add(tableLayoutPanel14);
            panel10.Dock = DockStyle.Fill;
            panel10.Location = new Point(0, 52);
            panel10.Margin = new Padding(0, 12, 0, 0);
            panel10.Name = "panel10";
            panel10.Size = new Size(244, 790);
            panel10.TabIndex = 1;
            // 
            // tableLayoutPanel14
            // 
            tableLayoutPanel14.ColumnCount = 2;
            tableLayoutPanel14.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanel14.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel14.Controls.Add(iScreen, 1, 3);
            tableLayoutPanel14.Controls.Add(materialLabel22, 0, 3);
            tableLayoutPanel14.Controls.Add(iProxy, 1, 2);
            tableLayoutPanel14.Controls.Add(materialLabel20, 0, 2);
            tableLayoutPanel14.Controls.Add(iUserAgent, 1, 1);
            tableLayoutPanel14.Controls.Add(materialLabel18, 0, 1);
            tableLayoutPanel14.Controls.Add(iProfileID, 1, 0);
            tableLayoutPanel14.Controls.Add(materialLabel16, 0, 0);
            tableLayoutPanel14.Controls.Add(materialLabel24, 0, 4);
            tableLayoutPanel14.Controls.Add(iFacebook, 1, 4);
            tableLayoutPanel14.Controls.Add(materialLabel26, 0, 5);
            tableLayoutPanel14.Controls.Add(iGoogle, 1, 5);
            tableLayoutPanel14.Dock = DockStyle.Top;
            tableLayoutPanel14.Location = new Point(0, 0);
            tableLayoutPanel14.Name = "tableLayoutPanel14";
            tableLayoutPanel14.RowCount = 7;
            tableLayoutPanel14.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel14.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel14.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel14.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel14.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel14.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel14.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel14.Size = new Size(244, 352);
            tableLayoutPanel14.TabIndex = 0;
            // 
            // iScreen
            // 
            iScreen.AutoSize = true;
            iScreen.Depth = 0;
            iScreen.Dock = DockStyle.Fill;
            iScreen.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            iScreen.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            iScreen.Location = new Point(73, 160);
            iScreen.Margin = new Padding(3, 10, 3, 10);
            iScreen.MouseState = MaterialSkin.MouseState.HOVER;
            iScreen.Name = "iScreen";
            iScreen.Size = new Size(168, 30);
            iScreen.TabIndex = 7;
            iScreen.Text = "...";
            // 
            // materialLabel22
            // 
            materialLabel22.Depth = 0;
            materialLabel22.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel22.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel22.Location = new Point(3, 160);
            materialLabel22.Margin = new Padding(3, 10, 3, 10);
            materialLabel22.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel22.Name = "materialLabel22";
            materialLabel22.Size = new Size(60, 30);
            materialLabel22.TabIndex = 6;
            materialLabel22.Text = "Screen Resolution";
            // 
            // iProxy
            // 
            iProxy.AutoSize = true;
            iProxy.Depth = 0;
            iProxy.Dock = DockStyle.Fill;
            iProxy.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            iProxy.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            iProxy.Location = new Point(73, 110);
            iProxy.Margin = new Padding(3, 10, 3, 10);
            iProxy.MouseState = MaterialSkin.MouseState.HOVER;
            iProxy.Name = "iProxy";
            iProxy.Size = new Size(168, 30);
            iProxy.TabIndex = 5;
            iProxy.Text = "...";
            // 
            // materialLabel20
            // 
            materialLabel20.AutoSize = true;
            materialLabel20.Depth = 0;
            materialLabel20.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel20.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel20.Location = new Point(3, 110);
            materialLabel20.Margin = new Padding(3, 10, 3, 10);
            materialLabel20.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel20.Name = "materialLabel20";
            materialLabel20.Size = new Size(32, 14);
            materialLabel20.TabIndex = 4;
            materialLabel20.Text = "Proxy";
            // 
            // iUserAgent
            // 
            iUserAgent.AutoSize = true;
            iUserAgent.Depth = 0;
            iUserAgent.Dock = DockStyle.Fill;
            iUserAgent.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            iUserAgent.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            iUserAgent.Location = new Point(73, 60);
            iUserAgent.Margin = new Padding(3, 10, 3, 10);
            iUserAgent.MouseState = MaterialSkin.MouseState.HOVER;
            iUserAgent.Name = "iUserAgent";
            iUserAgent.Size = new Size(168, 30);
            iUserAgent.TabIndex = 3;
            iUserAgent.Text = "...";
            // 
            // materialLabel18
            // 
            materialLabel18.AutoSize = true;
            materialLabel18.Depth = 0;
            materialLabel18.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel18.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel18.Location = new Point(3, 60);
            materialLabel18.Margin = new Padding(3, 10, 3, 10);
            materialLabel18.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel18.Name = "materialLabel18";
            materialLabel18.Size = new Size(60, 14);
            materialLabel18.TabIndex = 2;
            materialLabel18.Text = "User Agent";
            // 
            // iProfileID
            // 
            iProfileID.Depth = 0;
            iProfileID.Dock = DockStyle.Fill;
            iProfileID.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            iProfileID.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            iProfileID.Location = new Point(73, 10);
            iProfileID.Margin = new Padding(3, 10, 3, 10);
            iProfileID.MouseState = MaterialSkin.MouseState.HOVER;
            iProfileID.Name = "iProfileID";
            iProfileID.Size = new Size(168, 30);
            iProfileID.TabIndex = 1;
            iProfileID.Text = "...";
            // 
            // materialLabel16
            // 
            materialLabel16.AutoSize = true;
            materialLabel16.Depth = 0;
            materialLabel16.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel16.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel16.Location = new Point(3, 10);
            materialLabel16.Margin = new Padding(3, 10, 3, 10);
            materialLabel16.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel16.Name = "materialLabel16";
            materialLabel16.Size = new Size(50, 14);
            materialLabel16.TabIndex = 0;
            materialLabel16.Text = "Profile ID";
            // 
            // materialLabel24
            // 
            materialLabel24.AutoSize = true;
            materialLabel24.Depth = 0;
            materialLabel24.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel24.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel24.Location = new Point(3, 210);
            materialLabel24.Margin = new Padding(3, 10, 3, 10);
            materialLabel24.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel24.Name = "materialLabel24";
            materialLabel24.Size = new Size(54, 14);
            materialLabel24.TabIndex = 8;
            materialLabel24.Text = "Facebook";
            // 
            // iFacebook
            // 
            iFacebook.AutoSize = true;
            iFacebook.Depth = 0;
            iFacebook.Dock = DockStyle.Fill;
            iFacebook.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            iFacebook.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            iFacebook.Location = new Point(73, 210);
            iFacebook.Margin = new Padding(3, 10, 3, 10);
            iFacebook.MouseState = MaterialSkin.MouseState.HOVER;
            iFacebook.Name = "iFacebook";
            iFacebook.Size = new Size(168, 30);
            iFacebook.TabIndex = 9;
            iFacebook.Text = "...";
            // 
            // materialLabel26
            // 
            materialLabel26.AutoSize = true;
            materialLabel26.Depth = 0;
            materialLabel26.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel26.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel26.Location = new Point(3, 260);
            materialLabel26.Margin = new Padding(3, 10, 3, 10);
            materialLabel26.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel26.Name = "materialLabel26";
            materialLabel26.Size = new Size(39, 14);
            materialLabel26.TabIndex = 10;
            materialLabel26.Text = "Google";
            // 
            // iGoogle
            // 
            iGoogle.AutoSize = true;
            iGoogle.Depth = 0;
            iGoogle.Dock = DockStyle.Fill;
            iGoogle.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            iGoogle.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            iGoogle.Location = new Point(73, 260);
            iGoogle.Margin = new Padding(3, 10, 3, 10);
            iGoogle.MouseState = MaterialSkin.MouseState.HOVER;
            iGoogle.Name = "iGoogle";
            iGoogle.Size = new Size(168, 30);
            iGoogle.TabIndex = 11;
            iGoogle.Text = "...";
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.Controls.Add(materialCard1, 0, 1);
            tableLayoutPanel1.Controls.Add(materialCard5, 0, 0);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Margin = new Padding(0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 66.7263F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 33.2737F));
            tableLayoutPanel1.Size = new Size(1398, 886);
            tableLayoutPanel1.TabIndex = 3;
            // 
            // materialCard1
            // 
            materialCard1.BackColor = Color.FromArgb(255, 255, 255);
            materialCard1.Controls.Add(ListGroups);
            materialCard1.Depth = 0;
            materialCard1.Dock = DockStyle.Fill;
            materialCard1.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard1.Location = new Point(14, 593);
            materialCard1.Margin = new Padding(14, 2, 2, 14);
            materialCard1.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard1.Name = "materialCard1";
            materialCard1.Padding = new Padding(14);
            materialCard1.Size = new Size(1382, 279);
            materialCard1.TabIndex = 3;
            // 
            // ListGroups
            // 
            ListGroups.AllowUserToAddRows = false;
            ListGroups.AllowUserToDeleteRows = false;
            ListGroups.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            ListGroups.BackgroundColor = SystemColors.ButtonHighlight;
            ListGroups.BorderStyle = BorderStyle.None;
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = SystemColors.Control;
            dataGridViewCellStyle1.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle1.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = DataGridViewTriState.True;
            ListGroups.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            ListGroups.ColumnHeadersHeight = 40;
            ListGroups.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            ListGroups.Columns.AddRange(new DataGridViewColumn[] { group_name, group_total });
            ListGroups.ContextMenuStrip = MenuGroups;
            ListGroups.Cursor = Cursors.Hand;
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = SystemColors.Window;
            dataGridViewCellStyle2.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = Color.FromArgb(222, 0, 0, 0);
            dataGridViewCellStyle2.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = DataGridViewTriState.False;
            ListGroups.DefaultCellStyle = dataGridViewCellStyle2;
            ListGroups.Dock = DockStyle.Fill;
            ListGroups.Location = new Point(14, 14);
            ListGroups.Name = "ListGroups";
            dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = SystemColors.Control;
            dataGridViewCellStyle3.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle3.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle3.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = DataGridViewTriState.True;
            ListGroups.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            ListGroups.RowHeadersVisible = false;
            ListGroups.RowHeadersWidth = 50;
            ListGroups.RowTemplate.Height = 42;
            ListGroups.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            ListGroups.Size = new Size(1354, 251);
            ListGroups.TabIndex = 0;
            // 
            // group_name
            // 
            group_name.DataPropertyName = "group_name";
            group_name.FillWeight = 109.817F;
            group_name.HeaderText = "Group Name";
            group_name.MinimumWidth = 6;
            group_name.Name = "group_name";
            // 
            // group_total
            // 
            group_total.DataPropertyName = "group_total";
            group_total.FillWeight = 42.66524F;
            group_total.HeaderText = "Total Profiles";
            group_total.MinimumWidth = 6;
            group_total.Name = "group_total";
            // 
            // MenuGroups
            // 
            MenuGroups.ImageScalingSize = new Size(20, 20);
            MenuGroups.Items.AddRange(new ToolStripItem[] { EditGroup, toolStripSeparator2, DeleteGroup });
            MenuGroups.Name = "MenuProfiles";
            MenuGroups.Size = new Size(127, 62);
            // 
            // EditGroup
            // 
            EditGroup.Image = Properties.Resources.edit;
            EditGroup.Name = "EditGroup";
            EditGroup.Size = new Size(126, 26);
            EditGroup.Text = "Edit";
            EditGroup.Click += EditGroup_Click;
            // 
            // toolStripSeparator2
            // 
            toolStripSeparator2.Name = "toolStripSeparator2";
            toolStripSeparator2.Size = new Size(123, 6);
            // 
            // DeleteGroup
            // 
            DeleteGroup.ForeColor = Color.LightCoral;
            DeleteGroup.Image = Properties.Resources.delete2;
            DeleteGroup.ImageTransparentColor = Color.IndianRed;
            DeleteGroup.Name = "DeleteGroup";
            DeleteGroup.Size = new Size(126, 26);
            DeleteGroup.Text = "Delete";
            DeleteGroup.Click += DeleteGroup_Click;
            // 
            // materialCard5
            // 
            materialCard5.BackColor = Color.FromArgb(255, 255, 255);
            materialCard5.Controls.Add(ListProfiles);
            materialCard5.Depth = 0;
            materialCard5.Dock = DockStyle.Fill;
            materialCard5.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard5.Location = new Point(14, 2);
            materialCard5.Margin = new Padding(14, 2, 2, 14);
            materialCard5.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard5.Name = "materialCard5";
            materialCard5.Padding = new Padding(14);
            materialCard5.Size = new Size(1382, 575);
            materialCard5.TabIndex = 2;
            // 
            // ListProfiles
            // 
            ListProfiles.AllowUserToAddRows = false;
            ListProfiles.AllowUserToDeleteRows = false;
            ListProfiles.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            ListProfiles.BackgroundColor = SystemColors.ButtonHighlight;
            ListProfiles.BorderStyle = BorderStyle.None;
            dataGridViewCellStyle4.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = SystemColors.Control;
            dataGridViewCellStyle4.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle4.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = Color.Orange;
            dataGridViewCellStyle4.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = DataGridViewTriState.True;
            ListProfiles.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            ListProfiles.ColumnHeadersHeight = 40;
            ListProfiles.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            ListProfiles.Columns.AddRange(new DataGridViewColumn[] { profile_id, profile_status, profile_proxy, profile_last_update });
            ListProfiles.ContextMenuStrip = MenuProfiles;
            ListProfiles.Cursor = Cursors.Hand;
            dataGridViewCellStyle5.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = SystemColors.Window;
            dataGridViewCellStyle5.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle5.ForeColor = Color.FromArgb(222, 0, 0, 0);
            dataGridViewCellStyle5.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle5.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = DataGridViewTriState.False;
            ListProfiles.DefaultCellStyle = dataGridViewCellStyle5;
            ListProfiles.Dock = DockStyle.Fill;
            ListProfiles.Location = new Point(14, 14);
            ListProfiles.Name = "ListProfiles";
            dataGridViewCellStyle6.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = SystemColors.Control;
            dataGridViewCellStyle6.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle6.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle6.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = DataGridViewTriState.True;
            ListProfiles.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            ListProfiles.RowHeadersVisible = false;
            ListProfiles.RowHeadersWidth = 50;
            ListProfiles.RowTemplate.Height = 42;
            ListProfiles.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            ListProfiles.Size = new Size(1354, 547);
            ListProfiles.TabIndex = 0;
            ListProfiles.CellClick += ListProfiles_CellClick;
            // 
            // profile_id
            // 
            profile_id.DataPropertyName = "profile_id";
            profile_id.FillWeight = 109.817F;
            profile_id.HeaderText = "ID";
            profile_id.MinimumWidth = 6;
            profile_id.Name = "profile_id";
            // 
            // profile_status
            // 
            profile_status.DataPropertyName = "profile_status";
            profile_status.FillWeight = 42.66524F;
            profile_status.HeaderText = "Status";
            profile_status.MinimumWidth = 6;
            profile_status.Name = "profile_status";
            // 
            // profile_proxy
            // 
            profile_proxy.DataPropertyName = "profile_proxy";
            profile_proxy.FillWeight = 78.28705F;
            profile_proxy.HeaderText = "Proxy";
            profile_proxy.MinimumWidth = 6;
            profile_proxy.Name = "profile_proxy";
            // 
            // profile_last_update
            // 
            profile_last_update.DataPropertyName = "profile_last_update";
            profile_last_update.FillWeight = 78.28705F;
            profile_last_update.HeaderText = "Last update";
            profile_last_update.MinimumWidth = 6;
            profile_last_update.Name = "profile_last_update";
            // 
            // MenuProfiles
            // 
            MenuProfiles.ImageScalingSize = new Size(20, 20);
            MenuProfiles.Items.AddRange(new ToolStripItem[] { StartProfile, EditProfile, moveGroup, updateProxyToolStripMenuItem, CloneProfile, toolStripSeparator3, SimulateProfile, toolStripSeparator1, DeleteProfile, runWithRecordToolStripMenuItem });
            MenuProfiles.Name = "MenuProfiles";
            MenuProfiles.Size = new Size(187, 224);
            // 
            // StartProfile
            // 
            StartProfile.Image = Properties.Resources.play;
            StartProfile.Name = "StartProfile";
            StartProfile.Size = new Size(186, 26);
            StartProfile.Text = "Start";
            StartProfile.Click += StartProfile_Click;
            // 
            // EditProfile
            // 
            EditProfile.Image = Properties.Resources.edit;
            EditProfile.Name = "EditProfile";
            EditProfile.Size = new Size(186, 26);
            EditProfile.Text = "Edit";
            EditProfile.Click += EditProfile_Click;
            // 
            // moveGroup
            // 
            moveGroup.Image = Properties.Resources.clone1;
            moveGroup.Name = "moveGroup";
            moveGroup.Size = new Size(186, 26);
            moveGroup.Text = "Move";
            moveGroup.Click += moveGroup_Click;
            // 
            // updateProxyToolStripMenuItem
            // 
            updateProxyToolStripMenuItem.Image = Properties.Resources.delete1;
            updateProxyToolStripMenuItem.Name = "updateProxyToolStripMenuItem";
            updateProxyToolStripMenuItem.Size = new Size(186, 26);
            updateProxyToolStripMenuItem.Text = "Update Proxy";
            updateProxyToolStripMenuItem.Click += updateProxyToolStripMenuItem_Click;
            // 
            // CloneProfile
            // 
            CloneProfile.Image = Properties.Resources.clone1;
            CloneProfile.Name = "CloneProfile";
            CloneProfile.Size = new Size(186, 26);
            CloneProfile.Text = "Clone";
            CloneProfile.Click += CloneProfile_Click;
            // 
            // toolStripSeparator3
            // 
            toolStripSeparator3.Name = "toolStripSeparator3";
            toolStripSeparator3.Size = new Size(183, 6);
            // 
            // SimulateProfile
            // 
            SimulateProfile.Image = Properties.Resources.sensor_on;
            SimulateProfile.Name = "SimulateProfile";
            SimulateProfile.Size = new Size(186, 26);
            SimulateProfile.Text = "Simulate";
            SimulateProfile.Click += SimulateProfile_Click;
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new Size(183, 6);
            // 
            // DeleteProfile
            // 
            DeleteProfile.ForeColor = Color.LightCoral;
            DeleteProfile.Image = Properties.Resources.delete2;
            DeleteProfile.ImageTransparentColor = Color.IndianRed;
            DeleteProfile.Name = "DeleteProfile";
            DeleteProfile.Size = new Size(186, 26);
            DeleteProfile.Text = "Delete";
            DeleteProfile.Click += DeleteProfile_Click;
            // 
            // runWithRecordToolStripMenuItem
            // 
            runWithRecordToolStripMenuItem.Name = "runWithRecordToolStripMenuItem";
            runWithRecordToolStripMenuItem.Size = new Size(186, 26);
            runWithRecordToolStripMenuItem.Text = "Run with record";
            runWithRecordToolStripMenuItem.Click += runWithRecordToolStripMenuItem_Click;
            // 
            // tabSimulate
            // 
            tabSimulate.Controls.Add(tableLayoutPanel34);
            tabSimulate.ImageKey = "sensor.png";
            tabSimulate.Location = new Point(4, 32);
            tabSimulate.Name = "tabSimulate";
            tabSimulate.Padding = new Padding(3);
            tabSimulate.Size = new Size(1686, 967);
            tabSimulate.TabIndex = 4;
            tabSimulate.Text = "Simulate";
            tabSimulate.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel34
            // 
            tableLayoutPanel34.ColumnCount = 1;
            tableLayoutPanel34.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel34.Controls.Add(materialCard22, 0, 0);
            tableLayoutPanel34.Controls.Add(tableLayoutPanel32, 0, 1);
            tableLayoutPanel34.Dock = DockStyle.Fill;
            tableLayoutPanel34.Location = new Point(3, 3);
            tableLayoutPanel34.Name = "tableLayoutPanel34";
            tableLayoutPanel34.RowCount = 2;
            tableLayoutPanel34.RowStyles.Add(new RowStyle(SizeType.Absolute, 75F));
            tableLayoutPanel34.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel34.Size = new Size(1680, 961);
            tableLayoutPanel34.TabIndex = 3;
            // 
            // materialCard22
            // 
            materialCard22.BackColor = Color.FromArgb(255, 255, 255);
            materialCard22.Controls.Add(tableLayoutPanel35);
            materialCard22.Depth = 0;
            materialCard22.Dock = DockStyle.Fill;
            materialCard22.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard22.Location = new Point(8, 8);
            materialCard22.Margin = new Padding(8);
            materialCard22.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard22.Name = "materialCard22";
            materialCard22.Padding = new Padding(4);
            materialCard22.Size = new Size(1664, 59);
            materialCard22.TabIndex = 0;
            // 
            // tableLayoutPanel35
            // 
            tableLayoutPanel35.ColumnCount = 6;
            tableLayoutPanel35.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 200F));
            tableLayoutPanel35.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 43F));
            tableLayoutPanel35.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel35.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel35.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel35.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel35.Controls.Add(ComboBoxPopUpSelected, 0, 0);
            tableLayoutPanel35.Controls.Add(simulateStop, 5, 0);
            tableLayoutPanel35.Controls.Add(simulateStart, 4, 0);
            tableLayoutPanel35.Controls.Add(simulateLoad, 3, 0);
            tableLayoutPanel35.Controls.Add(buttonReload, 1, 0);
            tableLayoutPanel35.Dock = DockStyle.Fill;
            tableLayoutPanel35.Location = new Point(4, 4);
            tableLayoutPanel35.Margin = new Padding(0);
            tableLayoutPanel35.Name = "tableLayoutPanel35";
            tableLayoutPanel35.RowCount = 1;
            tableLayoutPanel35.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel35.Size = new Size(1656, 51);
            tableLayoutPanel35.TabIndex = 1;
            // 
            // ComboBoxPopUpSelected
            // 
            ComboBoxPopUpSelected.AutoResize = false;
            ComboBoxPopUpSelected.BackColor = Color.FromArgb(255, 255, 255);
            ComboBoxPopUpSelected.Depth = 0;
            ComboBoxPopUpSelected.Dock = DockStyle.Fill;
            ComboBoxPopUpSelected.DrawMode = DrawMode.OwnerDrawVariable;
            ComboBoxPopUpSelected.DropDownHeight = 174;
            ComboBoxPopUpSelected.DropDownStyle = ComboBoxStyle.DropDownList;
            ComboBoxPopUpSelected.DropDownWidth = 121;
            ComboBoxPopUpSelected.Enabled = false;
            ComboBoxPopUpSelected.Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            ComboBoxPopUpSelected.ForeColor = Color.FromArgb(222, 0, 0, 0);
            ComboBoxPopUpSelected.FormattingEnabled = true;
            ComboBoxPopUpSelected.IntegralHeight = false;
            ComboBoxPopUpSelected.ItemHeight = 43;
            ComboBoxPopUpSelected.Location = new Point(3, 3);
            ComboBoxPopUpSelected.MaxDropDownItems = 4;
            ComboBoxPopUpSelected.MouseState = MaterialSkin.MouseState.OUT;
            ComboBoxPopUpSelected.Name = "ComboBoxPopUpSelected";
            ComboBoxPopUpSelected.Size = new Size(194, 49);
            ComboBoxPopUpSelected.StartIndex = 0;
            ComboBoxPopUpSelected.TabIndex = 9;
            ComboBoxPopUpSelected.DropDownClosed += ComboBoxPopUpSelected_SelectedIndexChanged;
            // 
            // simulateStop
            // 
            simulateStop.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            simulateStop.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            simulateStop.Depth = 0;
            simulateStop.Dock = DockStyle.Fill;
            simulateStop.Enabled = false;
            simulateStop.HighEmphasis = true;
            simulateStop.Icon = null;
            simulateStop.Location = new Point(1581, 5);
            simulateStop.Margin = new Padding(5);
            simulateStop.MouseState = MaterialSkin.MouseState.HOVER;
            simulateStop.Name = "simulateStop";
            simulateStop.NoAccentTextColor = Color.Empty;
            simulateStop.Size = new Size(70, 41);
            simulateStop.TabIndex = 0;
            simulateStop.Text = "Stop";
            simulateStop.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            simulateStop.UseAccentColor = false;
            simulateStop.UseVisualStyleBackColor = true;
            simulateStop.Click += simulateStop_Click;
            // 
            // simulateStart
            // 
            simulateStart.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            simulateStart.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            simulateStart.Depth = 0;
            simulateStart.Dock = DockStyle.Fill;
            simulateStart.Enabled = false;
            simulateStart.HighEmphasis = true;
            simulateStart.Icon = null;
            simulateStart.Location = new Point(1501, 5);
            simulateStart.Margin = new Padding(5);
            simulateStart.MouseState = MaterialSkin.MouseState.HOVER;
            simulateStart.Name = "simulateStart";
            simulateStart.NoAccentTextColor = Color.Empty;
            simulateStart.Size = new Size(70, 41);
            simulateStart.TabIndex = 1;
            simulateStart.Text = "Start";
            simulateStart.TextAlign = ContentAlignment.MiddleRight;
            simulateStart.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            simulateStart.UseAccentColor = false;
            simulateStart.UseVisualStyleBackColor = true;
            simulateStart.Click += simulateStart_Click;
            // 
            // simulateLoad
            // 
            simulateLoad.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            simulateLoad.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            simulateLoad.Depth = 0;
            simulateLoad.Dock = DockStyle.Fill;
            simulateLoad.HighEmphasis = true;
            simulateLoad.Icon = null;
            simulateLoad.Location = new Point(1421, 5);
            simulateLoad.Margin = new Padding(5);
            simulateLoad.MouseState = MaterialSkin.MouseState.HOVER;
            simulateLoad.Name = "simulateLoad";
            simulateLoad.NoAccentTextColor = Color.Empty;
            simulateLoad.Size = new Size(70, 41);
            simulateLoad.TabIndex = 4;
            simulateLoad.Text = "Load";
            simulateLoad.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            simulateLoad.UseAccentColor = false;
            simulateLoad.UseVisualStyleBackColor = true;
            simulateLoad.Click += SimulateLoad_ClickAsync;
            // 
            // buttonReload
            // 
            buttonReload.Anchor = AnchorStyles.Left;
            buttonReload.Font = new Font("Wingdings 3", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            buttonReload.Location = new Point(203, 10);
            buttonReload.Name = "buttonReload";
            buttonReload.Size = new Size(30, 30);
            buttonReload.TabIndex = 10;
            buttonReload.Text = "Q";
            buttonReload.UseVisualStyleBackColor = true;
            buttonReload.Click += buttonReload_Click;
            // 
            // tableLayoutPanel32
            // 
            tableLayoutPanel32.ColumnCount = 1;
            tableLayoutPanel32.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel32.Controls.Add(statusStrip1, 0, 1);
            tableLayoutPanel32.Controls.Add(materialCard25, 0, 0);
            tableLayoutPanel32.Dock = DockStyle.Fill;
            tableLayoutPanel32.Location = new Point(3, 78);
            tableLayoutPanel32.Name = "tableLayoutPanel32";
            tableLayoutPanel32.RowCount = 2;
            tableLayoutPanel32.RowStyles.Add(new RowStyle(SizeType.Percent, 95F));
            tableLayoutPanel32.RowStyles.Add(new RowStyle(SizeType.Percent, 5F));
            tableLayoutPanel32.Size = new Size(1674, 880);
            tableLayoutPanel32.TabIndex = 1;
            // 
            // statusStrip1
            // 
            statusStrip1.Dock = DockStyle.Fill;
            statusStrip1.ImageScalingSize = new Size(20, 20);
            statusStrip1.Items.AddRange(new ToolStripItem[] { StatusLabel, StatusMess });
            statusStrip1.Location = new Point(0, 836);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(1674, 44);
            statusStrip1.TabIndex = 3;
            statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            StatusLabel.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            StatusLabel.ForeColor = Color.Green;
            StatusLabel.Name = "StatusLabel";
            StatusLabel.Size = new Size(70, 38);
            StatusLabel.Text = "Status :";
            // 
            // StatusMess
            // 
            StatusMess.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            StatusMess.ForeColor = Color.Green;
            StatusMess.Name = "StatusMess";
            StatusMess.Size = new Size(52, 38);
            StatusMess.Text = "Ready";
            // 
            // materialCard25
            // 
            materialCard25.BackColor = Color.FromArgb(255, 255, 255);
            materialCard25.Controls.Add(tableLayoutPanel37);
            materialCard25.Depth = 0;
            materialCard25.Dock = DockStyle.Fill;
            materialCard25.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard25.Location = new Point(5, 5);
            materialCard25.Margin = new Padding(5);
            materialCard25.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard25.Name = "materialCard25";
            materialCard25.Padding = new Padding(5);
            materialCard25.Size = new Size(1664, 826);
            materialCard25.TabIndex = 4;
            // 
            // tableLayoutPanel37
            // 
            tableLayoutPanel37.ColumnCount = 1;
            tableLayoutPanel37.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel37.Controls.Add(dataSimulator, 0, 2);
            tableLayoutPanel37.Controls.Add(panelCollapsed, 0, 1);
            tableLayoutPanel37.Controls.Add(ButtonCollapsed, 0, 0);
            tableLayoutPanel37.Dock = DockStyle.Fill;
            tableLayoutPanel37.Location = new Point(5, 5);
            tableLayoutPanel37.Name = "tableLayoutPanel37";
            tableLayoutPanel37.RowCount = 3;
            tableLayoutPanel37.RowStyles.Add(new RowStyle());
            tableLayoutPanel37.RowStyles.Add(new RowStyle());
            tableLayoutPanel37.RowStyles.Add(new RowStyle());
            tableLayoutPanel37.Size = new Size(1654, 816);
            tableLayoutPanel37.TabIndex = 0;
            // 
            // dataSimulator
            // 
            dataSimulator.AllowUserToAddRows = false;
            dataSimulator.AllowUserToDeleteRows = false;
            dataSimulator.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataSimulator.BackgroundColor = SystemColors.ButtonHighlight;
            dataSimulator.BorderStyle = BorderStyle.None;
            dataGridViewCellStyle7.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = SystemColors.Control;
            dataGridViewCellStyle7.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle7.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = Color.Orange;
            dataGridViewCellStyle7.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = DataGridViewTriState.True;
            dataSimulator.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            dataSimulator.ColumnHeadersHeight = 40;
            dataSimulator.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataSimulator.Columns.AddRange(new DataGridViewColumn[] { simu_id, simu_status, update_time, profile_simu_proxy });
            dataSimulator.ContextMenuStrip = MenuSimu;
            dataSimulator.Cursor = Cursors.Hand;
            dataGridViewCellStyle8.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = SystemColors.Window;
            dataGridViewCellStyle8.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle8.ForeColor = Color.FromArgb(222, 0, 0, 0);
            dataGridViewCellStyle8.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle8.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = DataGridViewTriState.False;
            dataSimulator.DefaultCellStyle = dataGridViewCellStyle8;
            dataSimulator.Dock = DockStyle.Fill;
            dataSimulator.Location = new Point(3, 58);
            dataSimulator.Name = "dataSimulator";
            dataSimulator.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = SystemColors.Control;
            dataGridViewCellStyle9.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle9.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle9.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = DataGridViewTriState.True;
            dataSimulator.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            dataSimulator.RowHeadersVisible = false;
            dataSimulator.RowHeadersWidth = 50;
            dataSimulator.RowTemplate.Height = 42;
            dataSimulator.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataSimulator.Size = new Size(1648, 765);
            dataSimulator.TabIndex = 8;
            dataSimulator.CellClick += Simu_contentClick;
            // 
            // simu_id
            // 
            simu_id.DataPropertyName = "profile_id";
            simu_id.FillWeight = 109.817F;
            simu_id.HeaderText = "ID";
            simu_id.MinimumWidth = 6;
            simu_id.Name = "simu_id";
            simu_id.ReadOnly = true;
            // 
            // simu_status
            // 
            simu_status.DataPropertyName = "profile_status";
            simu_status.FillWeight = 42.66524F;
            simu_status.HeaderText = "Status";
            simu_status.MinimumWidth = 6;
            simu_status.Name = "simu_status";
            simu_status.ReadOnly = true;
            // 
            // update_time
            // 
            update_time.DataPropertyName = "profile_last_update";
            update_time.HeaderText = "Last update";
            update_time.MinimumWidth = 6;
            update_time.Name = "update_time";
            update_time.ReadOnly = true;
            // 
            // profile_simu_proxy
            // 
            profile_simu_proxy.DataPropertyName = "profile_proxy";
            profile_simu_proxy.HeaderText = "Proxy";
            profile_simu_proxy.MinimumWidth = 6;
            profile_simu_proxy.Name = "profile_simu_proxy";
            profile_simu_proxy.ReadOnly = true;
            // 
            // MenuSimu
            // 
            MenuSimu.ImageScalingSize = new Size(20, 20);
            MenuSimu.Items.AddRange(new ToolStripItem[] { getTaskToolStripMenuItem, getOTPToolStripMenuItem, saveAccoutToolStripMenuItem, getYahooMailToolStripMenuItem, getContentMailToolStripMenuItem, copyToolStripMenuItem, pasteToolStripMenuItem, recordToolStripMenuItem1 });
            MenuSimu.Name = "MenuSimu";
            MenuSimu.Size = new Size(188, 196);
            // 
            // getTaskToolStripMenuItem
            // 
            getTaskToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { getName, firstNameToolStripMenuItem, lastnameToolStripMenuItem, getPhone, phone2ToolStripMenuItem, userNameToolStripMenuItem, username2ToolStripMenuItem, emailToolStripMenuItem, passwordToolStripMenuItem, password2ToolStripMenuItem, birthDayToolStripMenuItem, dodToolStripMenuItem, domToolStripMenuItem, doyToolStripMenuItem });
            getTaskToolStripMenuItem.Name = "getTaskToolStripMenuItem";
            getTaskToolStripMenuItem.Size = new Size(187, 24);
            getTaskToolStripMenuItem.Text = "get Task";
            getTaskToolStripMenuItem.DropDownItemClicked += DropDataAPI;
            // 
            // getName
            // 
            getName.Name = "getName";
            getName.Size = new Size(164, 26);
            getName.Text = "full_name";
            // 
            // firstNameToolStripMenuItem
            // 
            firstNameToolStripMenuItem.Name = "firstNameToolStripMenuItem";
            firstNameToolStripMenuItem.Size = new Size(164, 26);
            firstNameToolStripMenuItem.Text = "first_name";
            // 
            // lastnameToolStripMenuItem
            // 
            lastnameToolStripMenuItem.Name = "lastnameToolStripMenuItem";
            lastnameToolStripMenuItem.Size = new Size(164, 26);
            lastnameToolStripMenuItem.Text = "last_name";
            // 
            // getPhone
            // 
            getPhone.Name = "getPhone";
            getPhone.Size = new Size(164, 26);
            getPhone.Text = "phone";
            // 
            // phone2ToolStripMenuItem
            // 
            phone2ToolStripMenuItem.Name = "phone2ToolStripMenuItem";
            phone2ToolStripMenuItem.Size = new Size(164, 26);
            phone2ToolStripMenuItem.Text = "phone2";
            // 
            // userNameToolStripMenuItem
            // 
            userNameToolStripMenuItem.Name = "userNameToolStripMenuItem";
            userNameToolStripMenuItem.Size = new Size(164, 26);
            userNameToolStripMenuItem.Text = "username";
            // 
            // username2ToolStripMenuItem
            // 
            username2ToolStripMenuItem.Name = "username2ToolStripMenuItem";
            username2ToolStripMenuItem.Size = new Size(164, 26);
            username2ToolStripMenuItem.Text = "username2";
            // 
            // emailToolStripMenuItem
            // 
            emailToolStripMenuItem.Name = "emailToolStripMenuItem";
            emailToolStripMenuItem.Size = new Size(164, 26);
            emailToolStripMenuItem.Text = "email";
            // 
            // passwordToolStripMenuItem
            // 
            passwordToolStripMenuItem.Name = "passwordToolStripMenuItem";
            passwordToolStripMenuItem.Size = new Size(164, 26);
            passwordToolStripMenuItem.Text = "password";
            // 
            // password2ToolStripMenuItem
            // 
            password2ToolStripMenuItem.Name = "password2ToolStripMenuItem";
            password2ToolStripMenuItem.Size = new Size(164, 26);
            password2ToolStripMenuItem.Text = "password2";
            // 
            // birthDayToolStripMenuItem
            // 
            birthDayToolStripMenuItem.Name = "birthDayToolStripMenuItem";
            birthDayToolStripMenuItem.Size = new Size(164, 26);
            birthDayToolStripMenuItem.Text = "dob2";
            // 
            // dodToolStripMenuItem
            // 
            dodToolStripMenuItem.Name = "dodToolStripMenuItem";
            dodToolStripMenuItem.Size = new Size(164, 26);
            dodToolStripMenuItem.Text = "dod";
            // 
            // domToolStripMenuItem
            // 
            domToolStripMenuItem.Name = "domToolStripMenuItem";
            domToolStripMenuItem.Size = new Size(164, 26);
            domToolStripMenuItem.Text = "dom";
            // 
            // doyToolStripMenuItem
            // 
            doyToolStripMenuItem.Name = "doyToolStripMenuItem";
            doyToolStripMenuItem.Size = new Size(164, 26);
            doyToolStripMenuItem.Text = "doy";
            // 
            // getOTPToolStripMenuItem
            // 
            getOTPToolStripMenuItem.Name = "getOTPToolStripMenuItem";
            getOTPToolStripMenuItem.Size = new Size(187, 24);
            getOTPToolStripMenuItem.Text = "get OTP";
            getOTPToolStripMenuItem.Click += getOTPToolStripMenuItem_Click;
            // 
            // saveAccoutToolStripMenuItem
            // 
            saveAccoutToolStripMenuItem.Name = "saveAccoutToolStripMenuItem";
            saveAccoutToolStripMenuItem.Size = new Size(187, 24);
            saveAccoutToolStripMenuItem.Text = "save Accout";
            saveAccoutToolStripMenuItem.Click += saveAccoutToolStripMenuItem_Click;
            // 
            // getYahooMailToolStripMenuItem
            // 
            getYahooMailToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { yahooMailToolStripMenuItem, passwordToolStripMenuItem1 });
            getYahooMailToolStripMenuItem.Name = "getYahooMailToolStripMenuItem";
            getYahooMailToolStripMenuItem.Size = new Size(187, 24);
            getYahooMailToolStripMenuItem.Text = "get yahoo mail";
            getYahooMailToolStripMenuItem.DropDownItemClicked += DropYahooClicked;
            // 
            // yahooMailToolStripMenuItem
            // 
            yahooMailToolStripMenuItem.Name = "yahooMailToolStripMenuItem";
            yahooMailToolStripMenuItem.Size = new Size(166, 26);
            yahooMailToolStripMenuItem.Text = "yahoo mail";
            // 
            // passwordToolStripMenuItem1
            // 
            passwordToolStripMenuItem1.Name = "passwordToolStripMenuItem1";
            passwordToolStripMenuItem1.Size = new Size(166, 26);
            passwordToolStripMenuItem1.Text = "password";
            // 
            // getContentMailToolStripMenuItem
            // 
            getContentMailToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { oTPToolStripMenuItem, linkToolStripMenuItem, viewRawToolStripMenuItem });
            getContentMailToolStripMenuItem.Name = "getContentMailToolStripMenuItem";
            getContentMailToolStripMenuItem.Size = new Size(187, 24);
            getContentMailToolStripMenuItem.Text = "get content mail";
            getContentMailToolStripMenuItem.DropDownItemClicked += DropContentMailClicked;
            // 
            // oTPToolStripMenuItem
            // 
            oTPToolStripMenuItem.Name = "oTPToolStripMenuItem";
            oTPToolStripMenuItem.Size = new Size(152, 26);
            oTPToolStripMenuItem.Text = "OTP";
            // 
            // linkToolStripMenuItem
            // 
            linkToolStripMenuItem.Name = "linkToolStripMenuItem";
            linkToolStripMenuItem.Size = new Size(152, 26);
            linkToolStripMenuItem.Text = "Link";
            // 
            // viewRawToolStripMenuItem
            // 
            viewRawToolStripMenuItem.Name = "viewRawToolStripMenuItem";
            viewRawToolStripMenuItem.Size = new Size(152, 26);
            viewRawToolStripMenuItem.Text = "View raw";
            // 
            // copyToolStripMenuItem
            // 
            copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            copyToolStripMenuItem.Size = new Size(187, 24);
            copyToolStripMenuItem.Text = "copy";
            copyToolStripMenuItem.Click += copyToolStripMenuItem_Click;
            // 
            // pasteToolStripMenuItem
            // 
            pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            pasteToolStripMenuItem.Size = new Size(187, 24);
            pasteToolStripMenuItem.Text = "paste";
            pasteToolStripMenuItem.Click += pasteToolStripMenuItem_Click;
            // 
            // recordToolStripMenuItem1
            // 
            recordToolStripMenuItem1.Name = "recordToolStripMenuItem1";
            recordToolStripMenuItem1.Size = new Size(187, 24);
            recordToolStripMenuItem1.Text = "record";
            recordToolStripMenuItem1.Click += recordToolStripMenuItem1_Click;
            // 
            // panelCollapsed
            // 
            panelCollapsed.BackColor = Color.White;
            panelCollapsed.Controls.Add(tableLayoutPanel38);
            panelCollapsed.Dock = DockStyle.Fill;
            panelCollapsed.Location = new Point(3, 51);
            panelCollapsed.Name = "panelCollapsed";
            panelCollapsed.Size = new Size(1648, 1);
            panelCollapsed.TabIndex = 9;
            // 
            // tableLayoutPanel38
            // 
            tableLayoutPanel38.ColumnCount = 2;
            tableLayoutPanel38.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel38.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel38.Controls.Add(tableLayoutPanel40, 0, 1);
            tableLayoutPanel38.Controls.Add(tableLayoutPanel41, 1, 0);
            tableLayoutPanel38.Controls.Add(tableLayoutPanel42, 1, 1);
            tableLayoutPanel38.Controls.Add(tableLayoutPanel39, 0, 0);
            tableLayoutPanel38.Dock = DockStyle.Fill;
            tableLayoutPanel38.Location = new Point(0, 0);
            tableLayoutPanel38.Name = "tableLayoutPanel38";
            tableLayoutPanel38.RowCount = 2;
            tableLayoutPanel38.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel38.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel38.Size = new Size(1648, 1);
            tableLayoutPanel38.TabIndex = 2;
            // 
            // tableLayoutPanel40
            // 
            tableLayoutPanel40.ColumnCount = 2;
            tableLayoutPanel40.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanel40.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            tableLayoutPanel40.Controls.Add(label2, 0, 0);
            tableLayoutPanel40.Controls.Add(channelTxt, 1, 0);
            tableLayoutPanel40.Dock = DockStyle.Fill;
            tableLayoutPanel40.Location = new Point(3, 3);
            tableLayoutPanel40.Name = "tableLayoutPanel40";
            tableLayoutPanel40.RowCount = 1;
            tableLayoutPanel40.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel40.Size = new Size(818, 1);
            tableLayoutPanel40.TabIndex = 1;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Dock = DockStyle.Fill;
            label2.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label2.Location = new Point(10, 10);
            label2.Margin = new Padding(10);
            label2.Name = "label2";
            label2.Size = new Size(143, 1);
            label2.TabIndex = 0;
            label2.Text = "Channel";
            label2.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // channelTxt
            // 
            channelTxt.AnimateReadOnly = false;
            channelTxt.BackgroundImageLayout = ImageLayout.None;
            channelTxt.CharacterCasing = CharacterCasing.Normal;
            channelTxt.Depth = 0;
            channelTxt.Dock = DockStyle.Fill;
            channelTxt.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            channelTxt.HideSelection = true;
            channelTxt.Hint = "vnm, vt, ..";
            channelTxt.LeadingIcon = null;
            channelTxt.Location = new Point(173, 10);
            channelTxt.Margin = new Padding(10);
            channelTxt.MaxLength = 32767;
            channelTxt.MouseState = MaterialSkin.MouseState.OUT;
            channelTxt.Name = "channelTxt";
            channelTxt.PasswordChar = '\0';
            channelTxt.PrefixSuffixText = null;
            channelTxt.ReadOnly = false;
            channelTxt.RightToLeft = RightToLeft.No;
            channelTxt.SelectedText = "";
            channelTxt.SelectionLength = 0;
            channelTxt.SelectionStart = 0;
            channelTxt.ShortcutsEnabled = true;
            channelTxt.Size = new Size(635, 48);
            channelTxt.TabIndex = 1;
            channelTxt.TabStop = false;
            channelTxt.Text = "chung_duck";
            channelTxt.TextAlign = HorizontalAlignment.Left;
            channelTxt.TrailingIcon = null;
            channelTxt.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel41
            // 
            tableLayoutPanel41.ColumnCount = 2;
            tableLayoutPanel41.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanel41.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            tableLayoutPanel41.Controls.Add(label3, 0, 0);
            tableLayoutPanel41.Controls.Add(brandTxt, 1, 0);
            tableLayoutPanel41.Dock = DockStyle.Fill;
            tableLayoutPanel41.Location = new Point(827, 3);
            tableLayoutPanel41.Name = "tableLayoutPanel41";
            tableLayoutPanel41.RowCount = 1;
            tableLayoutPanel41.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel41.Size = new Size(818, 1);
            tableLayoutPanel41.TabIndex = 2;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Dock = DockStyle.Fill;
            label3.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label3.Location = new Point(10, 10);
            label3.Margin = new Padding(10);
            label3.Name = "label3";
            label3.Size = new Size(143, 1);
            label3.TabIndex = 0;
            label3.Text = "Brand";
            label3.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // brandTxt
            // 
            brandTxt.AnimateReadOnly = false;
            brandTxt.BackgroundImageLayout = ImageLayout.None;
            brandTxt.CharacterCasing = CharacterCasing.Normal;
            brandTxt.Depth = 0;
            brandTxt.Dock = DockStyle.Fill;
            brandTxt.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            brandTxt.HideSelection = true;
            brandTxt.Hint = "facebook,..";
            brandTxt.LeadingIcon = null;
            brandTxt.Location = new Point(173, 10);
            brandTxt.Margin = new Padding(10);
            brandTxt.MaxLength = 32767;
            brandTxt.MouseState = MaterialSkin.MouseState.OUT;
            brandTxt.Name = "brandTxt";
            brandTxt.PasswordChar = '\0';
            brandTxt.PrefixSuffixText = null;
            brandTxt.ReadOnly = false;
            brandTxt.RightToLeft = RightToLeft.No;
            brandTxt.SelectedText = "";
            brandTxt.SelectionLength = 0;
            brandTxt.SelectionStart = 0;
            brandTxt.ShortcutsEnabled = true;
            brandTxt.Size = new Size(635, 48);
            brandTxt.TabIndex = 1;
            brandTxt.TabStop = false;
            brandTxt.TextAlign = HorizontalAlignment.Left;
            brandTxt.TrailingIcon = null;
            brandTxt.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel42
            // 
            tableLayoutPanel42.ColumnCount = 2;
            tableLayoutPanel42.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanel42.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            tableLayoutPanel42.Controls.Add(label4, 0, 0);
            tableLayoutPanel42.Controls.Add(sqlTabtxt, 1, 0);
            tableLayoutPanel42.Dock = DockStyle.Fill;
            tableLayoutPanel42.Location = new Point(827, 3);
            tableLayoutPanel42.Name = "tableLayoutPanel42";
            tableLayoutPanel42.RowCount = 1;
            tableLayoutPanel42.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel42.Size = new Size(818, 1);
            tableLayoutPanel42.TabIndex = 3;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Dock = DockStyle.Fill;
            label4.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label4.Location = new Point(10, 10);
            label4.Margin = new Padding(10);
            label4.Name = "label4";
            label4.Size = new Size(143, 1);
            label4.TabIndex = 0;
            label4.Text = "Table Saving";
            label4.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // sqlTabtxt
            // 
            sqlTabtxt.AnimateReadOnly = false;
            sqlTabtxt.BorderStyle = BorderStyle.None;
            sqlTabtxt.Depth = 0;
            sqlTabtxt.Dock = DockStyle.Fill;
            sqlTabtxt.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            sqlTabtxt.Hint = "database table";
            sqlTabtxt.LeadingIcon = null;
            sqlTabtxt.Location = new Point(173, 10);
            sqlTabtxt.Margin = new Padding(10);
            sqlTabtxt.MaxLength = 50;
            sqlTabtxt.MouseState = MaterialSkin.MouseState.OUT;
            sqlTabtxt.Multiline = false;
            sqlTabtxt.Name = "sqlTabtxt";
            sqlTabtxt.Size = new Size(635, 50);
            sqlTabtxt.TabIndex = 1;
            sqlTabtxt.Text = "";
            sqlTabtxt.TrailingIcon = null;
            // 
            // tableLayoutPanel39
            // 
            tableLayoutPanel39.ColumnCount = 2;
            tableLayoutPanel39.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanel39.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            tableLayoutPanel39.Controls.Add(label1, 0, 0);
            tableLayoutPanel39.Controls.Add(APITxt, 1, 0);
            tableLayoutPanel39.Dock = DockStyle.Fill;
            tableLayoutPanel39.Location = new Point(3, 3);
            tableLayoutPanel39.Name = "tableLayoutPanel39";
            tableLayoutPanel39.RowCount = 1;
            tableLayoutPanel39.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel39.Size = new Size(818, 1);
            tableLayoutPanel39.TabIndex = 4;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Dock = DockStyle.Fill;
            label1.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(10, 10);
            label1.Margin = new Padding(10);
            label1.Name = "label1";
            label1.Size = new Size(143, 1);
            label1.TabIndex = 0;
            label1.Text = "API";
            label1.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // APITxt
            // 
            APITxt.AnimateReadOnly = false;
            APITxt.BackgroundImageLayout = ImageLayout.None;
            APITxt.CharacterCasing = CharacterCasing.Normal;
            APITxt.Cursor = Cursors.IBeam;
            APITxt.Depth = 0;
            APITxt.Dock = DockStyle.Fill;
            APITxt.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            APITxt.HideSelection = false;
            APITxt.Hint = "abc/xzy";
            APITxt.LeadingIcon = null;
            APITxt.Location = new Point(173, 10);
            APITxt.Margin = new Padding(10);
            APITxt.MaxLength = 32767;
            APITxt.MouseState = MaterialSkin.MouseState.OUT;
            APITxt.Name = "APITxt";
            APITxt.Padding = new Padding(1);
            APITxt.PasswordChar = '\0';
            APITxt.PrefixSuffixText = null;
            APITxt.ReadOnly = false;
            APITxt.RightToLeft = RightToLeft.No;
            APITxt.SelectedText = "";
            APITxt.SelectionLength = 0;
            APITxt.SelectionStart = 0;
            APITxt.ShortcutsEnabled = true;
            APITxt.Size = new Size(635, 48);
            APITxt.TabIndex = 1;
            APITxt.TabStop = false;
            APITxt.Text = "http://cbp.hta2p.xyz";
            APITxt.TextAlign = HorizontalAlignment.Left;
            APITxt.TrailingIcon = null;
            APITxt.UseSystemPasswordChar = false;
            // 
            // ButtonCollapsed
            // 
            ButtonCollapsed.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            ButtonCollapsed.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            ButtonCollapsed.Depth = 0;
            ButtonCollapsed.HighEmphasis = true;
            ButtonCollapsed.Icon = null;
            ButtonCollapsed.Location = new Point(4, 6);
            ButtonCollapsed.Margin = new Padding(4, 6, 4, 6);
            ButtonCollapsed.MouseState = MaterialSkin.MouseState.HOVER;
            ButtonCollapsed.Name = "ButtonCollapsed";
            ButtonCollapsed.NoAccentTextColor = Color.Empty;
            ButtonCollapsed.Size = new Size(139, 36);
            ButtonCollapsed.TabIndex = 10;
            ButtonCollapsed.Text = "Modify params";
            ButtonCollapsed.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            ButtonCollapsed.UseAccentColor = false;
            ButtonCollapsed.UseVisualStyleBackColor = true;
            ButtonCollapsed.Click += ButtonCollapsed_Click;
            // 
            // tabRecord
            // 
            tabRecord.Controls.Add(tableLayoutPanel36);
            tabRecord.ImageKey = "network-cloud.png";
            tabRecord.Location = new Point(4, 32);
            tabRecord.Name = "tabRecord";
            tabRecord.Size = new Size(1686, 967);
            tabRecord.TabIndex = 5;
            tabRecord.Text = "Recorded ";
            tabRecord.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel36
            // 
            tableLayoutPanel36.ColumnCount = 1;
            tableLayoutPanel36.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel36.Controls.Add(materialCard23, 0, 0);
            tableLayoutPanel36.Controls.Add(materialCard26, 0, 1);
            tableLayoutPanel36.Dock = DockStyle.Fill;
            tableLayoutPanel36.Location = new Point(0, 0);
            tableLayoutPanel36.Name = "tableLayoutPanel36";
            tableLayoutPanel36.RowCount = 2;
            tableLayoutPanel36.RowStyles.Add(new RowStyle(SizeType.Absolute, 75F));
            tableLayoutPanel36.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel36.Size = new Size(1686, 967);
            tableLayoutPanel36.TabIndex = 0;
            // 
            // materialCard23
            // 
            materialCard23.BackColor = Color.FromArgb(255, 255, 255);
            materialCard23.Controls.Add(tableLayoutPanel43);
            materialCard23.Depth = 0;
            materialCard23.Dock = DockStyle.Fill;
            materialCard23.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard23.Location = new Point(8, 8);
            materialCard23.Margin = new Padding(8);
            materialCard23.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard23.Name = "materialCard23";
            materialCard23.Padding = new Padding(4);
            materialCard23.Size = new Size(1670, 59);
            materialCard23.TabIndex = 0;
            // 
            // tableLayoutPanel43
            // 
            tableLayoutPanel43.ColumnCount = 4;
            tableLayoutPanel43.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel43.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel43.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel43.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel43.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
            tableLayoutPanel43.Controls.Add(stopRecord, 3, 0);
            tableLayoutPanel43.Controls.Add(pauseRecord, 2, 0);
            tableLayoutPanel43.Controls.Add(startRecord, 1, 0);
            tableLayoutPanel43.Dock = DockStyle.Fill;
            tableLayoutPanel43.Location = new Point(4, 4);
            tableLayoutPanel43.Margin = new Padding(2);
            tableLayoutPanel43.Name = "tableLayoutPanel43";
            tableLayoutPanel43.RowCount = 1;
            tableLayoutPanel43.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel43.Size = new Size(1662, 51);
            tableLayoutPanel43.TabIndex = 0;
            // 
            // stopRecord
            // 
            stopRecord.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            stopRecord.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            stopRecord.Depth = 0;
            stopRecord.Dock = DockStyle.Fill;
            stopRecord.Enabled = false;
            stopRecord.HighEmphasis = true;
            stopRecord.Icon = null;
            stopRecord.Location = new Point(1587, 5);
            stopRecord.Margin = new Padding(5);
            stopRecord.MouseState = MaterialSkin.MouseState.HOVER;
            stopRecord.Name = "stopRecord";
            stopRecord.NoAccentTextColor = Color.Empty;
            stopRecord.Size = new Size(70, 41);
            stopRecord.TabIndex = 1;
            stopRecord.Text = "Stop";
            stopRecord.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            stopRecord.UseAccentColor = false;
            stopRecord.UseVisualStyleBackColor = true;
            stopRecord.Click += stopRecord_Click;
            // 
            // pauseRecord
            // 
            pauseRecord.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            pauseRecord.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            pauseRecord.Depth = 0;
            pauseRecord.Dock = DockStyle.Fill;
            pauseRecord.Enabled = false;
            pauseRecord.HighEmphasis = true;
            pauseRecord.Icon = null;
            pauseRecord.Location = new Point(1507, 5);
            pauseRecord.Margin = new Padding(5);
            pauseRecord.MouseState = MaterialSkin.MouseState.HOVER;
            pauseRecord.Name = "pauseRecord";
            pauseRecord.NoAccentTextColor = Color.Empty;
            pauseRecord.Size = new Size(70, 41);
            pauseRecord.TabIndex = 2;
            pauseRecord.Text = "Pause";
            pauseRecord.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            pauseRecord.UseAccentColor = false;
            pauseRecord.UseVisualStyleBackColor = true;
            pauseRecord.Click += pauseRecord_Click;
            // 
            // startRecord
            // 
            startRecord.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            startRecord.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            startRecord.Depth = 0;
            startRecord.Dock = DockStyle.Fill;
            startRecord.HighEmphasis = true;
            startRecord.Icon = null;
            startRecord.Location = new Point(1427, 5);
            startRecord.Margin = new Padding(5);
            startRecord.MouseState = MaterialSkin.MouseState.HOVER;
            startRecord.Name = "startRecord";
            startRecord.NoAccentTextColor = Color.Empty;
            startRecord.Size = new Size(70, 41);
            startRecord.TabIndex = 0;
            startRecord.Text = "Record";
            startRecord.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            startRecord.UseAccentColor = false;
            startRecord.UseVisualStyleBackColor = true;
            startRecord.Click += startRecord_Click;
            // 
            // materialCard26
            // 
            materialCard26.BackColor = Color.FromArgb(255, 255, 255);
            materialCard26.Controls.Add(tableLayoutPanel49);
            materialCard26.Depth = 0;
            materialCard26.Dock = DockStyle.Fill;
            materialCard26.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard26.Location = new Point(14, 89);
            materialCard26.Margin = new Padding(14);
            materialCard26.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard26.Name = "materialCard26";
            materialCard26.Padding = new Padding(14);
            materialCard26.Size = new Size(1658, 864);
            materialCard26.TabIndex = 2;
            // 
            // tableLayoutPanel49
            // 
            tableLayoutPanel49.ColumnCount = 1;
            tableLayoutPanel49.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel49.Controls.Add(BtnSettingRecord, 0, 0);
            tableLayoutPanel49.Controls.Add(dataRecord, 0, 2);
            tableLayoutPanel49.Controls.Add(panelSettingRecord, 0, 1);
            tableLayoutPanel49.Dock = DockStyle.Fill;
            tableLayoutPanel49.Location = new Point(14, 14);
            tableLayoutPanel49.Name = "tableLayoutPanel49";
            tableLayoutPanel49.RowCount = 3;
            tableLayoutPanel49.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel49.RowStyles.Add(new RowStyle());
            tableLayoutPanel49.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel49.Size = new Size(1630, 836);
            tableLayoutPanel49.TabIndex = 0;
            // 
            // BtnSettingRecord
            // 
            BtnSettingRecord.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            BtnSettingRecord.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            BtnSettingRecord.Depth = 0;
            BtnSettingRecord.HighEmphasis = true;
            BtnSettingRecord.Icon = null;
            BtnSettingRecord.Location = new Point(4, 6);
            BtnSettingRecord.Margin = new Padding(4, 6, 4, 6);
            BtnSettingRecord.MouseState = MaterialSkin.MouseState.HOVER;
            BtnSettingRecord.Name = "BtnSettingRecord";
            BtnSettingRecord.NoAccentTextColor = Color.Empty;
            BtnSettingRecord.Size = new Size(81, 36);
            BtnSettingRecord.TabIndex = 0;
            BtnSettingRecord.Text = "Setting";
            BtnSettingRecord.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            BtnSettingRecord.UseAccentColor = false;
            BtnSettingRecord.UseVisualStyleBackColor = true;
            BtnSettingRecord.Click += BtnSettingRecord_Click;
            // 
            // dataRecord
            // 
            dataRecord.AllowUserToAddRows = false;
            dataRecord.AllowUserToDeleteRows = false;
            dataRecord.AllowUserToResizeRows = false;
            dataRecord.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataRecord.BackgroundColor = SystemColors.ButtonHighlight;
            dataRecord.BorderStyle = BorderStyle.None;
            dataRecord.ColumnHeadersHeight = 40;
            dataRecord.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataRecord.Columns.AddRange(new DataGridViewColumn[] { record_name, record_time, record_last_modify, record_link });
            dataRecord.ContextMenuStrip = MenuRecord;
            dataRecord.Cursor = Cursors.Hand;
            dataRecord.Dock = DockStyle.Fill;
            dataRecord.Location = new Point(3, 58);
            dataRecord.Name = "dataRecord";
            dataRecord.RowHeadersVisible = false;
            dataRecord.RowHeadersWidth = 51;
            dataRecord.RowTemplate.Height = 40;
            dataRecord.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataRecord.Size = new Size(1624, 775);
            dataRecord.TabIndex = 0;
            dataRecord.CellEndEdit += CellContentedEditFInish;
            // 
            // record_name
            // 
            record_name.DataPropertyName = "record_name";
            record_name.HeaderText = "Name";
            record_name.MinimumWidth = 6;
            record_name.Name = "record_name";
            // 
            // record_time
            // 
            record_time.DataPropertyName = "record_time";
            record_time.HeaderText = "Created time";
            record_time.MinimumWidth = 6;
            record_time.Name = "record_time";
            record_time.ReadOnly = true;
            // 
            // record_last_modify
            // 
            record_last_modify.DataPropertyName = "record_last_modify";
            record_last_modify.HeaderText = "Last modify";
            record_last_modify.MinimumWidth = 6;
            record_last_modify.Name = "record_last_modify";
            record_last_modify.ReadOnly = true;
            // 
            // record_link
            // 
            record_link.DataPropertyName = "record_link";
            record_link.HeaderText = "Link";
            record_link.MinimumWidth = 6;
            record_link.Name = "record_link";
            record_link.ReadOnly = true;
            // 
            // MenuRecord
            // 
            MenuRecord.ImageScalingSize = new Size(20, 20);
            MenuRecord.Items.AddRange(new ToolStripItem[] { playToolStripMenuItem, deleteToolStripMenuItem, playWithNewProfileToolStripMenuItem });
            MenuRecord.Name = "MenuRecord";
            MenuRecord.Size = new Size(215, 76);
            // 
            // playToolStripMenuItem
            // 
            playToolStripMenuItem.Name = "playToolStripMenuItem";
            playToolStripMenuItem.Size = new Size(214, 24);
            playToolStripMenuItem.Text = "Run";
            playToolStripMenuItem.Click += playToolStripMenuItem_Click;
            // 
            // deleteToolStripMenuItem
            // 
            deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            deleteToolStripMenuItem.Size = new Size(214, 24);
            deleteToolStripMenuItem.Text = "Delete";
            deleteToolStripMenuItem.Click += deleteToolStripMenuItem_Click;
            // 
            // playWithNewProfileToolStripMenuItem
            // 
            playWithNewProfileToolStripMenuItem.Name = "playWithNewProfileToolStripMenuItem";
            playWithNewProfileToolStripMenuItem.Size = new Size(214, 24);
            playWithNewProfileToolStripMenuItem.Text = "Run with new profile";
            playWithNewProfileToolStripMenuItem.Click += playWithNewProfileToolStripMenuItem_Click;
            // 
            // panelSettingRecord
            // 
            panelSettingRecord.BackColor = Color.WhiteSmoke;
            panelSettingRecord.Controls.Add(materialLabel64);
            panelSettingRecord.Controls.Add(LoopIntervalTxt);
            panelSettingRecord.Controls.Add(materialLabel63);
            panelSettingRecord.Controls.Add(label9);
            panelSettingRecord.Controls.Add(numericTimes);
            panelSettingRecord.Controls.Add(RadioBtnLoop4Ever);
            panelSettingRecord.Controls.Add(RadioBtnLoop);
            panelSettingRecord.Controls.Add(materialLabel62);
            panelSettingRecord.Dock = DockStyle.Fill;
            panelSettingRecord.Location = new Point(2, 52);
            panelSettingRecord.Margin = new Padding(2);
            panelSettingRecord.Name = "panelSettingRecord";
            panelSettingRecord.Size = new Size(1626, 1);
            panelSettingRecord.TabIndex = 1;
            // 
            // materialLabel64
            // 
            materialLabel64.AutoSize = true;
            materialLabel64.Depth = 0;
            materialLabel64.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel64.Location = new Point(194, 82);
            materialLabel64.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel64.Name = "materialLabel64";
            materialLabel64.Size = new Size(25, 19);
            materialLabel64.TabIndex = 8;
            materialLabel64.Text = "sec";
            // 
            // LoopIntervalTxt
            // 
            LoopIntervalTxt.Location = new Point(132, 77);
            LoopIntervalTxt.Name = "LoopIntervalTxt";
            LoopIntervalTxt.Size = new Size(56, 27);
            LoopIntervalTxt.TabIndex = 7;
            LoopIntervalTxt.Text = "0";
            LoopIntervalTxt.KeyPress += onlyNumber_KeyPress;
            // 
            // materialLabel63
            // 
            materialLabel63.AutoSize = true;
            materialLabel63.Depth = 0;
            materialLabel63.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel63.Location = new Point(14, 82);
            materialLabel63.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel63.Name = "materialLabel63";
            materialLabel63.Size = new Size(101, 19);
            materialLabel63.TabIndex = 6;
            materialLabel63.Text = "Loop interval :";
            // 
            // label9
            // 
            label9.Anchor = AnchorStyles.Left;
            label9.AutoSize = true;
            label9.Location = new Point(259, -106);
            label9.Name = "label9";
            label9.Size = new Size(45, 20);
            label9.TabIndex = 5;
            label9.Text = "times";
            // 
            // numericTimes
            // 
            numericTimes.Location = new Point(205, 12);
            numericTimes.Maximum = new decimal(new int[] { 50, 0, 0, 0 });
            numericTimes.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            numericTimes.Name = "numericTimes";
            numericTimes.Size = new Size(50, 27);
            numericTimes.TabIndex = 4;
            numericTimes.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // RadioBtnLoop4Ever
            // 
            RadioBtnLoop4Ever.AutoSize = true;
            RadioBtnLoop4Ever.Depth = 0;
            RadioBtnLoop4Ever.Location = new Point(124, 35);
            RadioBtnLoop4Ever.Margin = new Padding(0);
            RadioBtnLoop4Ever.MouseLocation = new Point(-1, -1);
            RadioBtnLoop4Ever.MouseState = MaterialSkin.MouseState.HOVER;
            RadioBtnLoop4Ever.Name = "RadioBtnLoop4Ever";
            RadioBtnLoop4Ever.Ripple = true;
            RadioBtnLoop4Ever.Size = new Size(102, 37);
            RadioBtnLoop4Ever.TabIndex = 2;
            RadioBtnLoop4Ever.TabStop = true;
            RadioBtnLoop4Ever.Text = "Until stop";
            RadioBtnLoop4Ever.UseVisualStyleBackColor = true;
            // 
            // RadioBtnLoop
            // 
            RadioBtnLoop.AutoSize = true;
            RadioBtnLoop.Checked = true;
            RadioBtnLoop.Depth = 0;
            RadioBtnLoop.Location = new Point(124, 6);
            RadioBtnLoop.Margin = new Padding(0);
            RadioBtnLoop.MouseLocation = new Point(-1, -1);
            RadioBtnLoop.MouseState = MaterialSkin.MouseState.HOVER;
            RadioBtnLoop.Name = "RadioBtnLoop";
            RadioBtnLoop.Ripple = true;
            RadioBtnLoop.Size = new Size(71, 37);
            RadioBtnLoop.TabIndex = 1;
            RadioBtnLoop.TabStop = true;
            RadioBtnLoop.Text = "Loop";
            RadioBtnLoop.UseVisualStyleBackColor = true;
            // 
            // materialLabel62
            // 
            materialLabel62.AutoSize = true;
            materialLabel62.Depth = 0;
            materialLabel62.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel62.Location = new Point(14, 15);
            materialLabel62.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel62.Name = "materialLabel62";
            materialLabel62.Size = new Size(58, 19);
            materialLabel62.TabIndex = 0;
            materialLabel62.Text = "Repeat :";
            // 
            // tabRunRecord
            // 
            tabRunRecord.Controls.Add(tableLayoutPanel57);
            tabRunRecord.ImageKey = "user-check.png";
            tabRunRecord.Location = new Point(4, 32);
            tabRunRecord.Name = "tabRunRecord";
            tabRunRecord.Size = new Size(1686, 967);
            tabRunRecord.TabIndex = 6;
            tabRunRecord.Text = "Run";
            tabRunRecord.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel57
            // 
            tableLayoutPanel57.ColumnCount = 1;
            tableLayoutPanel57.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel57.Controls.Add(statusStrip2, 0, 2);
            tableLayoutPanel57.Controls.Add(materialCard30, 0, 0);
            tableLayoutPanel57.Controls.Add(materialCard31, 0, 1);
            tableLayoutPanel57.Dock = DockStyle.Fill;
            tableLayoutPanel57.Location = new Point(0, 0);
            tableLayoutPanel57.Name = "tableLayoutPanel57";
            tableLayoutPanel57.RowCount = 3;
            tableLayoutPanel57.RowStyles.Add(new RowStyle(SizeType.Absolute, 80F));
            tableLayoutPanel57.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel57.RowStyles.Add(new RowStyle(SizeType.Absolute, 45F));
            tableLayoutPanel57.Size = new Size(1686, 967);
            tableLayoutPanel57.TabIndex = 0;
            // 
            // statusStrip2
            // 
            statusStrip2.Dock = DockStyle.Fill;
            statusStrip2.ImageScalingSize = new Size(20, 20);
            statusStrip2.Items.AddRange(new ToolStripItem[] { StatusTxt, StatusRecord });
            statusStrip2.Location = new Point(0, 922);
            statusStrip2.Name = "statusStrip2";
            statusStrip2.Size = new Size(1686, 45);
            statusStrip2.TabIndex = 4;
            statusStrip2.Text = "statusStrip2";
            // 
            // StatusTxt
            // 
            StatusTxt.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            StatusTxt.ForeColor = Color.Green;
            StatusTxt.Name = "StatusTxt";
            StatusTxt.Size = new Size(61, 39);
            StatusTxt.Text = "Status :";
            StatusTxt.VisitedLinkColor = Color.Transparent;
            // 
            // StatusRecord
            // 
            StatusRecord.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            StatusRecord.ForeColor = Color.Green;
            StatusRecord.Name = "StatusRecord";
            StatusRecord.Size = new Size(52, 39);
            StatusRecord.Text = "Ready";
            // 
            // materialCard30
            // 
            materialCard30.BackColor = Color.FromArgb(255, 255, 255);
            materialCard30.Controls.Add(tableLayoutPanel58);
            materialCard30.Depth = 0;
            materialCard30.Dock = DockStyle.Fill;
            materialCard30.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard30.Location = new Point(8, 8);
            materialCard30.Margin = new Padding(8);
            materialCard30.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard30.Name = "materialCard30";
            materialCard30.Padding = new Padding(4);
            materialCard30.Size = new Size(1670, 64);
            materialCard30.TabIndex = 0;
            // 
            // tableLayoutPanel58
            // 
            tableLayoutPanel58.ColumnCount = 5;
            tableLayoutPanel58.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel58.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel58.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel58.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 120F));
            tableLayoutPanel58.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel58.Controls.Add(ComboBoxRecord, 0, 0);
            tableLayoutPanel58.Controls.Add(RunBtn, 2, 0);
            tableLayoutPanel58.Controls.Add(RunAsNewBtn, 3, 0);
            tableLayoutPanel58.Controls.Add(StopRunScripsBtn, 4, 0);
            tableLayoutPanel58.Dock = DockStyle.Fill;
            tableLayoutPanel58.Location = new Point(4, 4);
            tableLayoutPanel58.Name = "tableLayoutPanel58";
            tableLayoutPanel58.RowCount = 1;
            tableLayoutPanel58.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel58.Size = new Size(1662, 56);
            tableLayoutPanel58.TabIndex = 1;
            // 
            // ComboBoxRecord
            // 
            ComboBoxRecord.AutoResize = false;
            ComboBoxRecord.BackColor = Color.FromArgb(255, 255, 255);
            ComboBoxRecord.Depth = 0;
            ComboBoxRecord.Dock = DockStyle.Fill;
            ComboBoxRecord.DrawMode = DrawMode.OwnerDrawVariable;
            ComboBoxRecord.DropDownHeight = 174;
            ComboBoxRecord.DropDownStyle = ComboBoxStyle.DropDownList;
            ComboBoxRecord.DropDownWidth = 121;
            ComboBoxRecord.Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            ComboBoxRecord.ForeColor = Color.FromArgb(222, 0, 0, 0);
            ComboBoxRecord.FormattingEnabled = true;
            ComboBoxRecord.IntegralHeight = false;
            ComboBoxRecord.ItemHeight = 43;
            ComboBoxRecord.Location = new Point(3, 3);
            ComboBoxRecord.MaxDropDownItems = 4;
            ComboBoxRecord.MouseState = MaterialSkin.MouseState.OUT;
            ComboBoxRecord.Name = "ComboBoxRecord";
            ComboBoxRecord.Size = new Size(250, 49);
            ComboBoxRecord.StartIndex = 0;
            ComboBoxRecord.TabIndex = 0;
            // 
            // RunBtn
            // 
            RunBtn.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            RunBtn.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            RunBtn.Depth = 0;
            RunBtn.Dock = DockStyle.Fill;
            RunBtn.HighEmphasis = true;
            RunBtn.Icon = null;
            RunBtn.Location = new Point(1386, 6);
            RunBtn.Margin = new Padding(4, 6, 4, 6);
            RunBtn.MouseState = MaterialSkin.MouseState.HOVER;
            RunBtn.Name = "RunBtn";
            RunBtn.NoAccentTextColor = Color.Empty;
            RunBtn.Size = new Size(72, 44);
            RunBtn.TabIndex = 1;
            RunBtn.Text = "Run";
            RunBtn.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            RunBtn.UseAccentColor = false;
            RunBtn.UseVisualStyleBackColor = true;
            RunBtn.Click += RunBtn_Click;
            // 
            // RunAsNewBtn
            // 
            RunAsNewBtn.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            RunAsNewBtn.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            RunAsNewBtn.Depth = 0;
            RunAsNewBtn.Dock = DockStyle.Fill;
            RunAsNewBtn.HighEmphasis = true;
            RunAsNewBtn.Icon = null;
            RunAsNewBtn.Location = new Point(1466, 6);
            RunAsNewBtn.Margin = new Padding(4, 6, 4, 6);
            RunAsNewBtn.MouseState = MaterialSkin.MouseState.HOVER;
            RunAsNewBtn.Name = "RunAsNewBtn";
            RunAsNewBtn.NoAccentTextColor = Color.Empty;
            RunAsNewBtn.Size = new Size(112, 44);
            RunAsNewBtn.TabIndex = 2;
            RunAsNewBtn.Text = "Run as new";
            RunAsNewBtn.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            RunAsNewBtn.UseAccentColor = false;
            RunAsNewBtn.UseVisualStyleBackColor = true;
            RunAsNewBtn.Click += RunAsNewBtn_Click;
            // 
            // StopRunScripsBtn
            // 
            StopRunScripsBtn.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            StopRunScripsBtn.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            StopRunScripsBtn.Depth = 0;
            StopRunScripsBtn.Dock = DockStyle.Fill;
            StopRunScripsBtn.Enabled = false;
            StopRunScripsBtn.HighEmphasis = true;
            StopRunScripsBtn.Icon = null;
            StopRunScripsBtn.Location = new Point(1586, 6);
            StopRunScripsBtn.Margin = new Padding(4, 6, 4, 6);
            StopRunScripsBtn.MouseState = MaterialSkin.MouseState.HOVER;
            StopRunScripsBtn.Name = "StopRunScripsBtn";
            StopRunScripsBtn.NoAccentTextColor = Color.Empty;
            StopRunScripsBtn.Size = new Size(72, 44);
            StopRunScripsBtn.TabIndex = 3;
            StopRunScripsBtn.Text = "Stop";
            StopRunScripsBtn.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            StopRunScripsBtn.UseAccentColor = false;
            StopRunScripsBtn.UseVisualStyleBackColor = true;
            StopRunScripsBtn.Click += StopRunScripsBtn_Click;
            // 
            // materialCard31
            // 
            materialCard31.BackColor = Color.FromArgb(255, 255, 255);
            materialCard31.Controls.Add(RunRecordTable);
            materialCard31.Depth = 0;
            materialCard31.Dock = DockStyle.Fill;
            materialCard31.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard31.Location = new Point(8, 88);
            materialCard31.Margin = new Padding(8);
            materialCard31.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard31.Name = "materialCard31";
            materialCard31.Padding = new Padding(4);
            materialCard31.Size = new Size(1670, 826);
            materialCard31.TabIndex = 1;
            // 
            // RunRecordTable
            // 
            RunRecordTable.AllowUserToAddRows = false;
            RunRecordTable.AllowUserToDeleteRows = false;
            RunRecordTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            RunRecordTable.BackgroundColor = SystemColors.ButtonHighlight;
            RunRecordTable.BorderStyle = BorderStyle.None;
            dataGridViewCellStyle10.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = SystemColors.Control;
            dataGridViewCellStyle10.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle10.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = Color.Orange;
            dataGridViewCellStyle10.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = DataGridViewTriState.True;
            RunRecordTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            RunRecordTable.ColumnHeadersHeight = 40;
            RunRecordTable.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            RunRecordTable.Columns.AddRange(new DataGridViewColumn[] { record_profile_id, run_record_status, dataGridViewTextBoxColumn17, dataGridViewTextBoxColumn18 });
            RunRecordTable.Cursor = Cursors.Hand;
            dataGridViewCellStyle11.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = SystemColors.Window;
            dataGridViewCellStyle11.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle11.ForeColor = Color.FromArgb(222, 0, 0, 0);
            dataGridViewCellStyle11.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle11.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = DataGridViewTriState.False;
            RunRecordTable.DefaultCellStyle = dataGridViewCellStyle11;
            RunRecordTable.Dock = DockStyle.Fill;
            RunRecordTable.Location = new Point(4, 4);
            RunRecordTable.Name = "RunRecordTable";
            RunRecordTable.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = SystemColors.Control;
            dataGridViewCellStyle12.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle12.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle12.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = DataGridViewTriState.True;
            RunRecordTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            RunRecordTable.RowHeadersVisible = false;
            RunRecordTable.RowHeadersWidth = 50;
            RunRecordTable.RowTemplate.Height = 42;
            RunRecordTable.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            RunRecordTable.Size = new Size(1662, 818);
            RunRecordTable.TabIndex = 9;
            // 
            // record_profile_id
            // 
            record_profile_id.DataPropertyName = "profile_id";
            record_profile_id.FillWeight = 109.817F;
            record_profile_id.HeaderText = "ID";
            record_profile_id.MinimumWidth = 6;
            record_profile_id.Name = "record_profile_id";
            record_profile_id.ReadOnly = true;
            // 
            // run_record_status
            // 
            run_record_status.DataPropertyName = "profile_status";
            run_record_status.FillWeight = 42.66524F;
            run_record_status.HeaderText = "Status";
            run_record_status.MinimumWidth = 6;
            run_record_status.Name = "run_record_status";
            run_record_status.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewTextBoxColumn17.DataPropertyName = "profile_last_update";
            dataGridViewTextBoxColumn17.HeaderText = "Last update";
            dataGridViewTextBoxColumn17.MinimumWidth = 6;
            dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            dataGridViewTextBoxColumn17.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn18
            // 
            dataGridViewTextBoxColumn18.DataPropertyName = "profile_proxy";
            dataGridViewTextBoxColumn18.HeaderText = "Proxy";
            dataGridViewTextBoxColumn18.MinimumWidth = 6;
            dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            dataGridViewTextBoxColumn18.ReadOnly = true;
            // 
            // tabSetting
            // 
            tabSetting.Controls.Add(tableLayoutPanel4);
            tabSetting.ImageKey = "settings-sliders.png";
            tabSetting.Location = new Point(4, 32);
            tabSetting.Name = "tabSetting";
            tabSetting.Size = new Size(1686, 967);
            tabSetting.TabIndex = 3;
            tabSetting.Text = "Setting";
            tabSetting.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            tableLayoutPanel4.ColumnCount = 1;
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel4.Controls.Add(materialCard4, 0, 0);
            tableLayoutPanel4.Controls.Add(flowLayoutPanel1, 0, 1);
            tableLayoutPanel4.Dock = DockStyle.Fill;
            tableLayoutPanel4.Location = new Point(0, 0);
            tableLayoutPanel4.Name = "tableLayoutPanel4";
            tableLayoutPanel4.RowCount = 2;
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Absolute, 79F));
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel4.Size = new Size(1686, 967);
            tableLayoutPanel4.TabIndex = 1;
            // 
            // materialCard4
            // 
            materialCard4.BackColor = Color.FromArgb(255, 255, 255);
            materialCard4.Controls.Add(tableLayoutPanel9);
            materialCard4.Depth = 0;
            materialCard4.Dock = DockStyle.Fill;
            materialCard4.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard4.Location = new Point(14, 14);
            materialCard4.Margin = new Padding(14);
            materialCard4.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard4.Name = "materialCard4";
            materialCard4.Padding = new Padding(4);
            materialCard4.Size = new Size(1658, 51);
            materialCard4.TabIndex = 0;
            // 
            // tableLayoutPanel9
            // 
            tableLayoutPanel9.ColumnCount = 2;
            tableLayoutPanel9.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel9.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 150F));
            tableLayoutPanel9.Controls.Add(SaveSetting, 1, 0);
            tableLayoutPanel9.Dock = DockStyle.Fill;
            tableLayoutPanel9.Location = new Point(4, 4);
            tableLayoutPanel9.Margin = new Padding(0);
            tableLayoutPanel9.Name = "tableLayoutPanel9";
            tableLayoutPanel9.RowCount = 1;
            tableLayoutPanel9.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel9.Size = new Size(1650, 43);
            tableLayoutPanel9.TabIndex = 1;
            // 
            // SaveSetting
            // 
            SaveSetting.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            SaveSetting.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            SaveSetting.Depth = 0;
            SaveSetting.Dock = DockStyle.Fill;
            SaveSetting.HighEmphasis = true;
            SaveSetting.Icon = null;
            SaveSetting.Location = new Point(1500, 0);
            SaveSetting.Margin = new Padding(0);
            SaveSetting.MouseState = MaterialSkin.MouseState.HOVER;
            SaveSetting.Name = "SaveSetting";
            SaveSetting.NoAccentTextColor = Color.Empty;
            SaveSetting.Size = new Size(150, 43);
            SaveSetting.TabIndex = 0;
            SaveSetting.Text = "Save Seting";
            SaveSetting.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            SaveSetting.UseAccentColor = false;
            SaveSetting.UseVisualStyleBackColor = true;
            SaveSetting.Click += SaveSetting_Click;
            // 
            // flowLayoutPanel1
            // 
            flowLayoutPanel1.AutoScroll = true;
            flowLayoutPanel1.Controls.Add(materialCard10);
            flowLayoutPanel1.Controls.Add(materialCard7);
            flowLayoutPanel1.Controls.Add(materialCard8);
            flowLayoutPanel1.Controls.Add(materialCard21);
            flowLayoutPanel1.Dock = DockStyle.Fill;
            flowLayoutPanel1.Location = new Point(3, 82);
            flowLayoutPanel1.Name = "flowLayoutPanel1";
            flowLayoutPanel1.Size = new Size(1680, 882);
            flowLayoutPanel1.TabIndex = 1;
            // 
            // materialCard10
            // 
            materialCard10.BackColor = Color.FromArgb(255, 255, 255);
            materialCard10.Controls.Add(tableLayoutPanel11);
            materialCard10.Depth = 0;
            materialCard10.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard10.Location = new Point(14, 14);
            materialCard10.Margin = new Padding(14);
            materialCard10.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard10.Name = "materialCard10";
            materialCard10.Padding = new Padding(14);
            materialCard10.Size = new Size(879, 118);
            materialCard10.TabIndex = 3;
            // 
            // tableLayoutPanel11
            // 
            tableLayoutPanel11.ColumnCount = 1;
            tableLayoutPanel11.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel11.Controls.Add(panel7, 0, 0);
            tableLayoutPanel11.Controls.Add(panel8, 0, 1);
            tableLayoutPanel11.Dock = DockStyle.Fill;
            tableLayoutPanel11.Location = new Point(14, 14);
            tableLayoutPanel11.Margin = new Padding(0);
            tableLayoutPanel11.Name = "tableLayoutPanel11";
            tableLayoutPanel11.RowCount = 2;
            tableLayoutPanel11.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel11.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel11.Size = new Size(851, 90);
            tableLayoutPanel11.TabIndex = 0;
            // 
            // panel7
            // 
            panel7.Controls.Add(materialLabel13);
            panel7.Controls.Add(SelectFolderProfiles);
            panel7.Dock = DockStyle.Fill;
            panel7.Location = new Point(0, 0);
            panel7.Margin = new Padding(0);
            panel7.Name = "panel7";
            panel7.Size = new Size(851, 40);
            panel7.TabIndex = 1;
            // 
            // materialLabel13
            // 
            materialLabel13.Depth = 0;
            materialLabel13.Dock = DockStyle.Left;
            materialLabel13.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel13.Location = new Point(0, 0);
            materialLabel13.Margin = new Padding(12, 0, 12, 0);
            materialLabel13.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel13.Name = "materialLabel13";
            materialLabel13.Padding = new Padding(12);
            materialLabel13.Size = new Size(163, 40);
            materialLabel13.TabIndex = 0;
            materialLabel13.Text = "Folder Profiles";
            materialLabel13.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // SelectFolderProfiles
            // 
            SelectFolderProfiles.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            SelectFolderProfiles.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            SelectFolderProfiles.Depth = 0;
            SelectFolderProfiles.Dock = DockStyle.Right;
            SelectFolderProfiles.HighEmphasis = true;
            SelectFolderProfiles.Icon = null;
            SelectFolderProfiles.Location = new Point(745, 0);
            SelectFolderProfiles.Margin = new Padding(4, 6, 4, 6);
            SelectFolderProfiles.MouseState = MaterialSkin.MouseState.HOVER;
            SelectFolderProfiles.Name = "SelectFolderProfiles";
            SelectFolderProfiles.NoAccentTextColor = Color.Empty;
            SelectFolderProfiles.Size = new Size(106, 40);
            SelectFolderProfiles.TabIndex = 2;
            SelectFolderProfiles.Text = "Select File";
            SelectFolderProfiles.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            SelectFolderProfiles.UseAccentColor = false;
            SelectFolderProfiles.UseVisualStyleBackColor = true;
            SelectFolderProfiles.Click += SelectFolderProfiles_Click;
            // 
            // panel8
            // 
            panel8.Controls.Add(materialLabel15);
            panel8.Controls.Add(tFolderProfiles);
            panel8.Dock = DockStyle.Fill;
            panel8.Location = new Point(0, 40);
            panel8.Margin = new Padding(0);
            panel8.Name = "panel8";
            panel8.Size = new Size(851, 50);
            panel8.TabIndex = 2;
            // 
            // materialLabel15
            // 
            materialLabel15.BackColor = Color.White;
            materialLabel15.Depth = 0;
            materialLabel15.Dock = DockStyle.Top;
            materialLabel15.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel15.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel15.ForeColor = Color.LightCoral;
            materialLabel15.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel15.Location = new Point(0, 35);
            materialLabel15.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel15.Name = "materialLabel15";
            materialLabel15.Size = new Size(851, 17);
            materialLabel15.TabIndex = 1;
            materialLabel15.Text = "* Where to save the local profile file";
            // 
            // tFolderProfiles
            // 
            tFolderProfiles.Depth = 0;
            tFolderProfiles.Dock = DockStyle.Top;
            tFolderProfiles.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            tFolderProfiles.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            tFolderProfiles.Location = new Point(0, 0);
            tFolderProfiles.Margin = new Padding(0);
            tFolderProfiles.MouseState = MaterialSkin.MouseState.HOVER;
            tFolderProfiles.Name = "tFolderProfiles";
            tFolderProfiles.Size = new Size(851, 35);
            tFolderProfiles.TabIndex = 0;
            tFolderProfiles.Text = "Path File...";
            tFolderProfiles.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // materialCard7
            // 
            materialCard7.BackColor = Color.FromArgb(255, 255, 255);
            materialCard7.Controls.Add(tableLayoutPanel6);
            materialCard7.Depth = 0;
            materialCard7.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard7.Location = new Point(14, 160);
            materialCard7.Margin = new Padding(14);
            materialCard7.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard7.Name = "materialCard7";
            materialCard7.Padding = new Padding(14);
            materialCard7.Size = new Size(879, 133);
            materialCard7.TabIndex = 1;
            // 
            // tableLayoutPanel6
            // 
            tableLayoutPanel6.ColumnCount = 1;
            tableLayoutPanel6.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel6.Controls.Add(panel1, 0, 0);
            tableLayoutPanel6.Controls.Add(panel2, 0, 1);
            tableLayoutPanel6.Dock = DockStyle.Fill;
            tableLayoutPanel6.Location = new Point(14, 14);
            tableLayoutPanel6.Margin = new Padding(0);
            tableLayoutPanel6.Name = "tableLayoutPanel6";
            tableLayoutPanel6.RowCount = 2;
            tableLayoutPanel6.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel6.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel6.Size = new Size(851, 105);
            tableLayoutPanel6.TabIndex = 0;
            // 
            // panel1
            // 
            panel1.Controls.Add(materialLabel2);
            panel1.Controls.Add(SelectFileProxy);
            panel1.Dock = DockStyle.Fill;
            panel1.Location = new Point(0, 0);
            panel1.Margin = new Padding(0);
            panel1.Name = "panel1";
            panel1.Size = new Size(851, 40);
            panel1.TabIndex = 1;
            // 
            // materialLabel2
            // 
            materialLabel2.Depth = 0;
            materialLabel2.Dock = DockStyle.Left;
            materialLabel2.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel2.Location = new Point(0, 0);
            materialLabel2.Margin = new Padding(12, 0, 12, 0);
            materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel2.Name = "materialLabel2";
            materialLabel2.Padding = new Padding(12);
            materialLabel2.Size = new Size(92, 40);
            materialLabel2.TabIndex = 0;
            materialLabel2.Text = "File Proxies";
            materialLabel2.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // SelectFileProxy
            // 
            SelectFileProxy.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            SelectFileProxy.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            SelectFileProxy.Depth = 0;
            SelectFileProxy.Dock = DockStyle.Right;
            SelectFileProxy.HighEmphasis = true;
            SelectFileProxy.Icon = null;
            SelectFileProxy.Location = new Point(745, 0);
            SelectFileProxy.Margin = new Padding(4, 6, 4, 6);
            SelectFileProxy.MouseState = MaterialSkin.MouseState.HOVER;
            SelectFileProxy.Name = "SelectFileProxy";
            SelectFileProxy.NoAccentTextColor = Color.Empty;
            SelectFileProxy.Size = new Size(106, 40);
            SelectFileProxy.TabIndex = 2;
            SelectFileProxy.Text = "Select File";
            SelectFileProxy.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            SelectFileProxy.UseAccentColor = false;
            SelectFileProxy.UseVisualStyleBackColor = true;
            SelectFileProxy.Click += SelectFileProxy_Click;
            // 
            // panel2
            // 
            panel2.Controls.Add(materialLabel9);
            panel2.Controls.Add(materialLabel4);
            panel2.Controls.Add(tFileProxy);
            panel2.Dock = DockStyle.Fill;
            panel2.Location = new Point(0, 40);
            panel2.Margin = new Padding(0);
            panel2.Name = "panel2";
            panel2.Size = new Size(851, 65);
            panel2.TabIndex = 2;
            // 
            // materialLabel9
            // 
            materialLabel9.BackColor = Color.White;
            materialLabel9.Depth = 0;
            materialLabel9.Dock = DockStyle.Top;
            materialLabel9.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel9.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel9.ForeColor = Color.LightCoral;
            materialLabel9.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel9.Location = new Point(0, 52);
            materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel9.Name = "materialLabel9";
            materialLabel9.Size = new Size(851, 17);
            materialLabel9.TabIndex = 2;
            materialLabel9.Text = "** File .txt";
            // 
            // materialLabel4
            // 
            materialLabel4.BackColor = Color.White;
            materialLabel4.Depth = 0;
            materialLabel4.Dock = DockStyle.Top;
            materialLabel4.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel4.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel4.ForeColor = Color.LightCoral;
            materialLabel4.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel4.Location = new Point(0, 35);
            materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel4.Name = "materialLabel4";
            materialLabel4.Size = new Size(851, 17);
            materialLabel4.TabIndex = 1;
            materialLabel4.Text = "* Required syntax of proxy: ip:port:username:password";
            // 
            // tFileProxy
            // 
            tFileProxy.Depth = 0;
            tFileProxy.Dock = DockStyle.Top;
            tFileProxy.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            tFileProxy.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            tFileProxy.Location = new Point(0, 0);
            tFileProxy.Margin = new Padding(0);
            tFileProxy.MouseState = MaterialSkin.MouseState.HOVER;
            tFileProxy.Name = "tFileProxy";
            tFileProxy.Size = new Size(851, 35);
            tFileProxy.TabIndex = 0;
            tFileProxy.Text = "Path File...";
            tFileProxy.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // materialCard8
            // 
            materialCard8.BackColor = Color.FromArgb(255, 255, 255);
            materialCard8.Controls.Add(tableLayoutPanel8);
            materialCard8.Depth = 0;
            materialCard8.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard8.Location = new Point(14, 321);
            materialCard8.Margin = new Padding(14);
            materialCard8.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard8.Name = "materialCard8";
            materialCard8.Padding = new Padding(14);
            materialCard8.Size = new Size(879, 175);
            materialCard8.TabIndex = 2;
            // 
            // tableLayoutPanel8
            // 
            tableLayoutPanel8.ColumnCount = 1;
            tableLayoutPanel8.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel8.Controls.Add(panel3, 0, 0);
            tableLayoutPanel8.Controls.Add(panel4, 0, 1);
            tableLayoutPanel8.Dock = DockStyle.Fill;
            tableLayoutPanel8.Location = new Point(14, 14);
            tableLayoutPanel8.Margin = new Padding(0);
            tableLayoutPanel8.Name = "tableLayoutPanel8";
            tableLayoutPanel8.RowCount = 2;
            tableLayoutPanel8.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel8.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel8.Size = new Size(851, 147);
            tableLayoutPanel8.TabIndex = 0;
            // 
            // panel3
            // 
            panel3.Controls.Add(materialLabel5);
            panel3.Controls.Add(SelectFileName);
            panel3.Dock = DockStyle.Fill;
            panel3.Location = new Point(0, 0);
            panel3.Margin = new Padding(0);
            panel3.Name = "panel3";
            panel3.Size = new Size(851, 40);
            panel3.TabIndex = 1;
            // 
            // materialLabel5
            // 
            materialLabel5.Depth = 0;
            materialLabel5.Dock = DockStyle.Left;
            materialLabel5.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel5.Location = new Point(0, 0);
            materialLabel5.Margin = new Padding(12, 0, 12, 0);
            materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel5.Name = "materialLabel5";
            materialLabel5.Padding = new Padding(12);
            materialLabel5.Size = new Size(92, 40);
            materialLabel5.TabIndex = 0;
            materialLabel5.Text = "File Name";
            materialLabel5.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // SelectFileName
            // 
            SelectFileName.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            SelectFileName.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            SelectFileName.Depth = 0;
            SelectFileName.Dock = DockStyle.Right;
            SelectFileName.HighEmphasis = true;
            SelectFileName.Icon = null;
            SelectFileName.Location = new Point(745, 0);
            SelectFileName.Margin = new Padding(4, 6, 4, 6);
            SelectFileName.MouseState = MaterialSkin.MouseState.HOVER;
            SelectFileName.Name = "SelectFileName";
            SelectFileName.NoAccentTextColor = Color.Empty;
            SelectFileName.Size = new Size(106, 40);
            SelectFileName.TabIndex = 2;
            SelectFileName.Text = "Select File";
            SelectFileName.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            SelectFileName.UseAccentColor = false;
            SelectFileName.UseVisualStyleBackColor = true;
            SelectFileName.Click += SelectFileName_Click;
            // 
            // panel4
            // 
            panel4.Controls.Add(materialLabel10);
            panel4.Controls.Add(materialLabel8);
            panel4.Controls.Add(materialLabel6);
            panel4.Controls.Add(tFileName);
            panel4.Dock = DockStyle.Fill;
            panel4.Location = new Point(0, 40);
            panel4.Margin = new Padding(0);
            panel4.Name = "panel4";
            panel4.Size = new Size(851, 107);
            panel4.TabIndex = 2;
            // 
            // materialLabel10
            // 
            materialLabel10.BackColor = Color.White;
            materialLabel10.Depth = 0;
            materialLabel10.Dock = DockStyle.Top;
            materialLabel10.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel10.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel10.ForeColor = Color.LightCoral;
            materialLabel10.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel10.Location = new Point(0, 69);
            materialLabel10.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel10.Name = "materialLabel10";
            materialLabel10.Size = new Size(851, 156);
            materialLabel10.TabIndex = 3;
            // 
            // materialLabel8
            // 
            materialLabel8.BackColor = Color.White;
            materialLabel8.Depth = 0;
            materialLabel8.Dock = DockStyle.Top;
            materialLabel8.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel8.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel8.ForeColor = Color.LightCoral;
            materialLabel8.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel8.Location = new Point(0, 52);
            materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel8.Name = "materialLabel8";
            materialLabel8.Size = new Size(851, 17);
            materialLabel8.TabIndex = 2;
            materialLabel8.Text = "** File .json";
            // 
            // materialLabel6
            // 
            materialLabel6.BackColor = Color.White;
            materialLabel6.Depth = 0;
            materialLabel6.Dock = DockStyle.Top;
            materialLabel6.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel6.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel6.ForeColor = Color.LightCoral;
            materialLabel6.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel6.Location = new Point(0, 35);
            materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel6.Name = "materialLabel6";
            materialLabel6.Size = new Size(851, 17);
            materialLabel6.TabIndex = 1;
            materialLabel6.Text = "* Default data name Vietnamese";
            // 
            // tFileName
            // 
            tFileName.Depth = 0;
            tFileName.Dock = DockStyle.Top;
            tFileName.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            tFileName.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            tFileName.Location = new Point(0, 0);
            tFileName.Margin = new Padding(0);
            tFileName.MouseState = MaterialSkin.MouseState.HOVER;
            tFileName.Name = "tFileName";
            tFileName.Size = new Size(851, 35);
            tFileName.TabIndex = 0;
            tFileName.Text = "Path File...";
            tFileName.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // materialCard21
            // 
            materialCard21.BackColor = Color.FromArgb(255, 255, 255);
            materialCard21.Controls.Add(tableLayoutPanel33);
            materialCard21.Depth = 0;
            materialCard21.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard21.Location = new Point(14, 524);
            materialCard21.Margin = new Padding(14);
            materialCard21.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard21.Name = "materialCard21";
            materialCard21.Padding = new Padding(14);
            materialCard21.Size = new Size(879, 319);
            materialCard21.TabIndex = 2;
            // 
            // tableLayoutPanel33
            // 
            tableLayoutPanel33.ColumnCount = 1;
            tableLayoutPanel33.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel33.Controls.Add(panel21, 0, 0);
            tableLayoutPanel33.Controls.Add(panel22, 0, 1);
            tableLayoutPanel33.Dock = DockStyle.Fill;
            tableLayoutPanel33.Location = new Point(14, 14);
            tableLayoutPanel33.Margin = new Padding(0);
            tableLayoutPanel33.Name = "tableLayoutPanel33";
            tableLayoutPanel33.RowCount = 2;
            tableLayoutPanel33.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel33.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel33.Size = new Size(851, 291);
            tableLayoutPanel33.TabIndex = 0;
            // 
            // panel21
            // 
            panel21.Controls.Add(materialLabel59);
            panel21.Controls.Add(SelectFileExt);
            panel21.Dock = DockStyle.Fill;
            panel21.Location = new Point(0, 0);
            panel21.Margin = new Padding(0);
            panel21.Name = "panel21";
            panel21.Size = new Size(851, 40);
            panel21.TabIndex = 1;
            // 
            // materialLabel59
            // 
            materialLabel59.Depth = 0;
            materialLabel59.Dock = DockStyle.Left;
            materialLabel59.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel59.Location = new Point(0, 0);
            materialLabel59.Margin = new Padding(12, 0, 12, 0);
            materialLabel59.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel59.Name = "materialLabel59";
            materialLabel59.Padding = new Padding(12);
            materialLabel59.Size = new Size(106, 40);
            materialLabel59.TabIndex = 0;
            materialLabel59.Text = "File Extention";
            materialLabel59.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // SelectFileExt
            // 
            SelectFileExt.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            SelectFileExt.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            SelectFileExt.Depth = 0;
            SelectFileExt.Dock = DockStyle.Right;
            SelectFileExt.HighEmphasis = true;
            SelectFileExt.Icon = null;
            SelectFileExt.Location = new Point(745, 0);
            SelectFileExt.Margin = new Padding(4, 6, 4, 6);
            SelectFileExt.MouseState = MaterialSkin.MouseState.HOVER;
            SelectFileExt.Name = "SelectFileExt";
            SelectFileExt.NoAccentTextColor = Color.Empty;
            SelectFileExt.Size = new Size(106, 40);
            SelectFileExt.TabIndex = 2;
            SelectFileExt.Text = "Select File";
            SelectFileExt.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            SelectFileExt.UseAccentColor = false;
            SelectFileExt.UseVisualStyleBackColor = true;
            SelectFileExt.Click += SelectFileExt_Click;
            // 
            // panel22
            // 
            panel22.Controls.Add(panel23);
            panel22.Controls.Add(materialLabel60);
            panel22.Controls.Add(materialLabel61);
            panel22.Controls.Add(tFileExt);
            panel22.Dock = DockStyle.Fill;
            panel22.Location = new Point(0, 40);
            panel22.Margin = new Padding(0);
            panel22.Name = "panel22";
            panel22.Size = new Size(851, 251);
            panel22.TabIndex = 2;
            // 
            // panel23
            // 
            panel23.Controls.Add(multiLineTextExt);
            panel23.Location = new Point(3, 97);
            panel23.Name = "panel23";
            panel23.Size = new Size(845, 151);
            panel23.TabIndex = 4;
            // 
            // multiLineTextExt
            // 
            multiLineTextExt.AnimateReadOnly = false;
            multiLineTextExt.BackgroundImageLayout = ImageLayout.None;
            multiLineTextExt.CharacterCasing = CharacterCasing.Normal;
            multiLineTextExt.Depth = 0;
            multiLineTextExt.HideSelection = true;
            multiLineTextExt.Location = new Point(3, 14);
            multiLineTextExt.MaxLength = 32767;
            multiLineTextExt.MouseState = MaterialSkin.MouseState.OUT;
            multiLineTextExt.Name = "multiLineTextExt";
            multiLineTextExt.PasswordChar = '\0';
            multiLineTextExt.ReadOnly = false;
            multiLineTextExt.ScrollBars = ScrollBars.Vertical;
            multiLineTextExt.SelectedText = "";
            multiLineTextExt.SelectionLength = 0;
            multiLineTextExt.SelectionStart = 0;
            multiLineTextExt.ShortcutsEnabled = true;
            multiLineTextExt.Size = new Size(829, 124);
            multiLineTextExt.TabIndex = 3;
            multiLineTextExt.TabStop = false;
            multiLineTextExt.TextAlign = HorizontalAlignment.Left;
            multiLineTextExt.UseSystemPasswordChar = false;
            // 
            // materialLabel60
            // 
            materialLabel60.BackColor = Color.White;
            materialLabel60.Depth = 0;
            materialLabel60.Dock = DockStyle.Top;
            materialLabel60.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel60.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel60.ForeColor = Color.LightCoral;
            materialLabel60.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel60.Location = new Point(0, 52);
            materialLabel60.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel60.Name = "materialLabel60";
            materialLabel60.Size = new Size(851, 17);
            materialLabel60.TabIndex = 2;
            materialLabel60.Text = "** File .txt";
            // 
            // materialLabel61
            // 
            materialLabel61.BackColor = Color.White;
            materialLabel61.Depth = 0;
            materialLabel61.Dock = DockStyle.Top;
            materialLabel61.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel61.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel61.ForeColor = Color.LightCoral;
            materialLabel61.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel61.Location = new Point(0, 35);
            materialLabel61.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel61.Name = "materialLabel61";
            materialLabel61.Size = new Size(851, 17);
            materialLabel61.TabIndex = 1;
            materialLabel61.Text = "* Required each name in one line";
            // 
            // tFileExt
            // 
            tFileExt.Depth = 0;
            tFileExt.Dock = DockStyle.Top;
            tFileExt.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            tFileExt.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            tFileExt.Location = new Point(0, 0);
            tFileExt.Margin = new Padding(0);
            tFileExt.MouseState = MaterialSkin.MouseState.HOVER;
            tFileExt.Name = "tFileExt";
            tFileExt.Size = new Size(851, 35);
            tFileExt.TabIndex = 0;
            tFileExt.Text = "Path File...";
            tFileExt.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // IconSidebar
            // 
            IconSidebar.ColorDepth = ColorDepth.Depth32Bit;
            IconSidebar.ImageStream = (ImageListStreamer)resources.GetObject("IconSidebar.ImageStream");
            IconSidebar.TransparentColor = Color.Transparent;
            IconSidebar.Images.SetKeyName(0, "settings-sliders.png");
            IconSidebar.Images.SetKeyName(1, "network-cloud.png");
            IconSidebar.Images.SetKeyName(2, "home.png");
            IconSidebar.Images.SetKeyName(3, "browser.png");
            IconSidebar.Images.SetKeyName(4, "user-check.png");
            IconSidebar.Images.SetKeyName(5, "users.png");
            IconSidebar.Images.SetKeyName(6, "sensor.png");
            IconSidebar.Images.SetKeyName(7, "sensor-on.png");
            // 
            // Status
            // 
            Status.HeaderText = "Status";
            Status.MinimumWidth = 6;
            Status.Name = "Status";
            Status.Width = 125;
            // 
            // Proxy
            // 
            Proxy.HeaderText = "Proxy";
            Proxy.MinimumWidth = 6;
            Proxy.Name = "Proxy";
            Proxy.Width = 125;
            // 
            // materialCard9
            // 
            materialCard9.BackColor = Color.FromArgb(255, 255, 255);
            materialCard9.Depth = 0;
            materialCard9.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard9.Location = new Point(0, 0);
            materialCard9.Margin = new Padding(14);
            materialCard9.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard9.Name = "materialCard9";
            materialCard9.Padding = new Padding(14);
            materialCard9.Size = new Size(200, 100);
            materialCard9.TabIndex = 0;
            // 
            // tableLayoutPanel10
            // 
            tableLayoutPanel10.ColumnCount = 1;
            tableLayoutPanel10.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel10.Controls.Add(panel5, 0, 0);
            tableLayoutPanel10.Dock = DockStyle.Fill;
            tableLayoutPanel10.Location = new Point(0, 0);
            tableLayoutPanel10.Margin = new Padding(0);
            tableLayoutPanel10.Name = "tableLayoutPanel10";
            tableLayoutPanel10.RowCount = 2;
            tableLayoutPanel10.Size = new Size(200, 100);
            tableLayoutPanel10.TabIndex = 0;
            // 
            // panel5
            // 
            panel5.Controls.Add(materialLabel3);
            panel5.Controls.Add(materialButton1);
            panel5.Dock = DockStyle.Fill;
            panel5.Location = new Point(0, 0);
            panel5.Margin = new Padding(0);
            panel5.Name = "panel5";
            panel5.Size = new Size(200, 40);
            panel5.TabIndex = 1;
            // 
            // materialLabel3
            // 
            materialLabel3.Depth = 0;
            materialLabel3.Dock = DockStyle.Left;
            materialLabel3.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel3.Location = new Point(0, 0);
            materialLabel3.Margin = new Padding(12, 0, 12, 0);
            materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel3.Name = "materialLabel3";
            materialLabel3.Padding = new Padding(12);
            materialLabel3.Size = new Size(92, 40);
            materialLabel3.TabIndex = 0;
            materialLabel3.Text = "File Proxies";
            materialLabel3.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // materialButton1
            // 
            materialButton1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton1.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton1.Depth = 0;
            materialButton1.Dock = DockStyle.Right;
            materialButton1.HighEmphasis = true;
            materialButton1.Icon = null;
            materialButton1.Location = new Point(94, 0);
            materialButton1.Margin = new Padding(4, 6, 4, 6);
            materialButton1.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton1.Name = "materialButton1";
            materialButton1.NoAccentTextColor = Color.Empty;
            materialButton1.Size = new Size(106, 40);
            materialButton1.TabIndex = 2;
            materialButton1.Text = "Select File";
            materialButton1.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton1.UseAccentColor = false;
            materialButton1.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            panel6.Controls.Add(materialLabel7);
            panel6.Controls.Add(materialLabel11);
            panel6.Controls.Add(materialLabel12);
            panel6.Dock = DockStyle.Fill;
            panel6.Location = new Point(0, 40);
            panel6.Margin = new Padding(0);
            panel6.Name = "panel6";
            panel6.Size = new Size(200, 65);
            panel6.TabIndex = 2;
            // 
            // materialLabel7
            // 
            materialLabel7.BackColor = Color.White;
            materialLabel7.Depth = 0;
            materialLabel7.Dock = DockStyle.Top;
            materialLabel7.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel7.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel7.ForeColor = Color.LightCoral;
            materialLabel7.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel7.Location = new Point(0, 52);
            materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel7.Name = "materialLabel7";
            materialLabel7.Size = new Size(200, 17);
            materialLabel7.TabIndex = 2;
            materialLabel7.Text = "** File .txt";
            // 
            // materialLabel11
            // 
            materialLabel11.BackColor = Color.White;
            materialLabel11.Depth = 0;
            materialLabel11.Dock = DockStyle.Top;
            materialLabel11.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel11.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel11.ForeColor = Color.LightCoral;
            materialLabel11.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel11.Location = new Point(0, 35);
            materialLabel11.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel11.Name = "materialLabel11";
            materialLabel11.Size = new Size(200, 17);
            materialLabel11.TabIndex = 1;
            materialLabel11.Text = "* Required syntax of proxy: ip:port:username:password";
            // 
            // materialLabel12
            // 
            materialLabel12.Depth = 0;
            materialLabel12.Dock = DockStyle.Top;
            materialLabel12.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel12.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            materialLabel12.Location = new Point(0, 0);
            materialLabel12.Margin = new Padding(0);
            materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel12.Name = "materialLabel12";
            materialLabel12.Size = new Size(200, 35);
            materialLabel12.TabIndex = 0;
            materialLabel12.Text = "Path File...";
            materialLabel12.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel15
            // 
            tableLayoutPanel15.ColumnCount = 1;
            tableLayoutPanel15.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel15.Controls.Add(materialCard11, 0, 0);
            tableLayoutPanel15.Dock = DockStyle.Fill;
            tableLayoutPanel15.Location = new Point(0, 0);
            tableLayoutPanel15.Name = "tableLayoutPanel15";
            tableLayoutPanel15.RowCount = 2;
            tableLayoutPanel15.Size = new Size(200, 100);
            tableLayoutPanel15.TabIndex = 0;
            // 
            // materialCard11
            // 
            materialCard11.BackColor = Color.FromArgb(255, 255, 255);
            materialCard11.Controls.Add(tableLayoutPanel16);
            materialCard11.Depth = 0;
            materialCard11.Dock = DockStyle.Fill;
            materialCard11.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard11.Location = new Point(14, 14);
            materialCard11.Margin = new Padding(14);
            materialCard11.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard11.Name = "materialCard11";
            materialCard11.Padding = new Padding(5);
            materialCard11.Size = new Size(172, 51);
            materialCard11.TabIndex = 0;
            // 
            // tableLayoutPanel16
            // 
            tableLayoutPanel16.ColumnCount = 2;
            tableLayoutPanel16.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 85F));
            tableLayoutPanel16.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 15F));
            tableLayoutPanel16.Controls.Add(materialButton2, 1, 0);
            tableLayoutPanel16.Dock = DockStyle.Fill;
            tableLayoutPanel16.Location = new Point(5, 5);
            tableLayoutPanel16.Margin = new Padding(0);
            tableLayoutPanel16.Name = "tableLayoutPanel16";
            tableLayoutPanel16.RowCount = 1;
            tableLayoutPanel16.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel16.Size = new Size(162, 41);
            tableLayoutPanel16.TabIndex = 0;
            // 
            // materialButton2
            // 
            materialButton2.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton2.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton2.Depth = 0;
            materialButton2.HighEmphasis = true;
            materialButton2.Icon = null;
            materialButton2.Location = new Point(137, 0);
            materialButton2.Margin = new Padding(0);
            materialButton2.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton2.Name = "materialButton2";
            materialButton2.NoAccentTextColor = Color.Empty;
            materialButton2.Size = new Size(25, 36);
            materialButton2.TabIndex = 0;
            materialButton2.Text = "Create Profiles";
            materialButton2.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton2.UseAccentColor = false;
            materialButton2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel17
            // 
            tableLayoutPanel17.ColumnCount = 2;
            tableLayoutPanel17.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel17.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 300F));
            tableLayoutPanel17.Controls.Add(materialCard12, 1, 0);
            tableLayoutPanel17.Controls.Add(materialCard13, 0, 0);
            tableLayoutPanel17.Dock = DockStyle.Fill;
            tableLayoutPanel17.Location = new Point(0, 79);
            tableLayoutPanel17.Margin = new Padding(0);
            tableLayoutPanel17.Name = "tableLayoutPanel17";
            tableLayoutPanel17.RowCount = 1;
            tableLayoutPanel17.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel17.Size = new Size(200, 697);
            tableLayoutPanel17.TabIndex = 1;
            // 
            // materialCard12
            // 
            materialCard12.BackColor = Color.FromArgb(255, 255, 255);
            materialCard12.Controls.Add(tableLayoutPanel18);
            materialCard12.Depth = 0;
            materialCard12.Dock = DockStyle.Fill;
            materialCard12.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard12.Location = new Point(-86, 2);
            materialCard12.Margin = new Padding(14, 2, 14, 14);
            materialCard12.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard12.Name = "materialCard12";
            materialCard12.Padding = new Padding(14);
            materialCard12.Size = new Size(272, 681);
            materialCard12.TabIndex = 2;
            // 
            // tableLayoutPanel18
            // 
            tableLayoutPanel18.ColumnCount = 1;
            tableLayoutPanel18.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel18.Controls.Add(tableLayoutPanel19, 0, 0);
            tableLayoutPanel18.Controls.Add(panel12, 0, 1);
            tableLayoutPanel18.Dock = DockStyle.Fill;
            tableLayoutPanel18.Location = new Point(14, 14);
            tableLayoutPanel18.Margin = new Padding(0);
            tableLayoutPanel18.Name = "tableLayoutPanel18";
            tableLayoutPanel18.RowCount = 2;
            tableLayoutPanel18.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel18.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel18.Size = new Size(244, 653);
            tableLayoutPanel18.TabIndex = 0;
            // 
            // tableLayoutPanel19
            // 
            tableLayoutPanel19.ColumnCount = 2;
            tableLayoutPanel19.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 48F));
            tableLayoutPanel19.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel19.Controls.Add(panel11, 0, 0);
            tableLayoutPanel19.Controls.Add(materialLabel17, 1, 0);
            tableLayoutPanel19.Dock = DockStyle.Fill;
            tableLayoutPanel19.Location = new Point(0, 0);
            tableLayoutPanel19.Margin = new Padding(0);
            tableLayoutPanel19.Name = "tableLayoutPanel19";
            tableLayoutPanel19.RowCount = 1;
            tableLayoutPanel19.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel19.Size = new Size(244, 40);
            tableLayoutPanel19.TabIndex = 0;
            // 
            // panel11
            // 
            panel11.BackgroundImageLayout = ImageLayout.Zoom;
            panel11.Location = new Point(6, 6);
            panel11.Margin = new Padding(6);
            panel11.Name = "panel11";
            panel11.Size = new Size(36, 28);
            panel11.TabIndex = 0;
            // 
            // materialLabel17
            // 
            materialLabel17.Depth = 0;
            materialLabel17.Dock = DockStyle.Fill;
            materialLabel17.Font = new Font("Roboto", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel17.FontType = MaterialSkin.MaterialSkinManager.fontType.Button;
            materialLabel17.Location = new Point(53, 5);
            materialLabel17.Margin = new Padding(5);
            materialLabel17.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel17.Name = "materialLabel17";
            materialLabel17.Size = new Size(186, 30);
            materialLabel17.TabIndex = 1;
            materialLabel17.Text = "Profile Info";
            materialLabel17.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // panel12
            // 
            panel12.AutoScroll = true;
            panel12.Controls.Add(tableLayoutPanel20);
            panel12.Dock = DockStyle.Fill;
            panel12.Location = new Point(0, 52);
            panel12.Margin = new Padding(0, 12, 0, 0);
            panel12.Name = "panel12";
            panel12.Size = new Size(244, 601);
            panel12.TabIndex = 1;
            // 
            // tableLayoutPanel20
            // 
            tableLayoutPanel20.ColumnCount = 2;
            tableLayoutPanel20.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanel20.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel20.Controls.Add(materialLabel19, 1, 3);
            tableLayoutPanel20.Controls.Add(materialLabel21, 0, 3);
            tableLayoutPanel20.Controls.Add(materialLabel23, 1, 2);
            tableLayoutPanel20.Controls.Add(materialLabel25, 0, 2);
            tableLayoutPanel20.Controls.Add(materialLabel27, 1, 1);
            tableLayoutPanel20.Controls.Add(materialLabel28, 0, 1);
            tableLayoutPanel20.Controls.Add(materialLabel29, 1, 0);
            tableLayoutPanel20.Controls.Add(materialLabel30, 0, 0);
            tableLayoutPanel20.Controls.Add(materialLabel31, 0, 4);
            tableLayoutPanel20.Controls.Add(materialLabel32, 1, 4);
            tableLayoutPanel20.Controls.Add(materialLabel33, 0, 5);
            tableLayoutPanel20.Controls.Add(materialLabel34, 1, 5);
            tableLayoutPanel20.Dock = DockStyle.Top;
            tableLayoutPanel20.Location = new Point(0, 0);
            tableLayoutPanel20.Name = "tableLayoutPanel20";
            tableLayoutPanel20.RowCount = 7;
            tableLayoutPanel20.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel20.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel20.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel20.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel20.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel20.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel20.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel20.Size = new Size(244, 352);
            tableLayoutPanel20.TabIndex = 0;
            // 
            // materialLabel19
            // 
            materialLabel19.AutoSize = true;
            materialLabel19.Depth = 0;
            materialLabel19.Dock = DockStyle.Fill;
            materialLabel19.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel19.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel19.Location = new Point(73, 160);
            materialLabel19.Margin = new Padding(3, 10, 3, 10);
            materialLabel19.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel19.Name = "materialLabel19";
            materialLabel19.Size = new Size(168, 30);
            materialLabel19.TabIndex = 7;
            materialLabel19.Text = "...";
            // 
            // materialLabel21
            // 
            materialLabel21.Depth = 0;
            materialLabel21.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel21.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel21.Location = new Point(3, 160);
            materialLabel21.Margin = new Padding(3, 10, 3, 10);
            materialLabel21.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel21.Name = "materialLabel21";
            materialLabel21.Size = new Size(60, 30);
            materialLabel21.TabIndex = 6;
            materialLabel21.Text = "Screen Resolution";
            // 
            // materialLabel23
            // 
            materialLabel23.AutoSize = true;
            materialLabel23.Depth = 0;
            materialLabel23.Dock = DockStyle.Fill;
            materialLabel23.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel23.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel23.Location = new Point(73, 110);
            materialLabel23.Margin = new Padding(3, 10, 3, 10);
            materialLabel23.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel23.Name = "materialLabel23";
            materialLabel23.Size = new Size(168, 30);
            materialLabel23.TabIndex = 5;
            materialLabel23.Text = "...";
            // 
            // materialLabel25
            // 
            materialLabel25.AutoSize = true;
            materialLabel25.Depth = 0;
            materialLabel25.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel25.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel25.Location = new Point(3, 110);
            materialLabel25.Margin = new Padding(3, 10, 3, 10);
            materialLabel25.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel25.Name = "materialLabel25";
            materialLabel25.Size = new Size(32, 14);
            materialLabel25.TabIndex = 4;
            materialLabel25.Text = "Proxy";
            // 
            // materialLabel27
            // 
            materialLabel27.AutoSize = true;
            materialLabel27.Depth = 0;
            materialLabel27.Dock = DockStyle.Fill;
            materialLabel27.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel27.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel27.Location = new Point(73, 60);
            materialLabel27.Margin = new Padding(3, 10, 3, 10);
            materialLabel27.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel27.Name = "materialLabel27";
            materialLabel27.Size = new Size(168, 30);
            materialLabel27.TabIndex = 3;
            materialLabel27.Text = "...";
            // 
            // materialLabel28
            // 
            materialLabel28.AutoSize = true;
            materialLabel28.Depth = 0;
            materialLabel28.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel28.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel28.Location = new Point(3, 60);
            materialLabel28.Margin = new Padding(3, 10, 3, 10);
            materialLabel28.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel28.Name = "materialLabel28";
            materialLabel28.Size = new Size(60, 14);
            materialLabel28.TabIndex = 2;
            materialLabel28.Text = "User Agent";
            // 
            // materialLabel29
            // 
            materialLabel29.Depth = 0;
            materialLabel29.Dock = DockStyle.Fill;
            materialLabel29.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel29.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel29.Location = new Point(73, 10);
            materialLabel29.Margin = new Padding(3, 10, 3, 10);
            materialLabel29.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel29.Name = "materialLabel29";
            materialLabel29.Size = new Size(168, 30);
            materialLabel29.TabIndex = 1;
            materialLabel29.Text = "...";
            // 
            // materialLabel30
            // 
            materialLabel30.AutoSize = true;
            materialLabel30.Depth = 0;
            materialLabel30.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel30.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel30.Location = new Point(3, 10);
            materialLabel30.Margin = new Padding(3, 10, 3, 10);
            materialLabel30.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel30.Name = "materialLabel30";
            materialLabel30.Size = new Size(50, 14);
            materialLabel30.TabIndex = 0;
            materialLabel30.Text = "Profile ID";
            // 
            // materialLabel31
            // 
            materialLabel31.AutoSize = true;
            materialLabel31.Depth = 0;
            materialLabel31.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel31.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel31.Location = new Point(3, 210);
            materialLabel31.Margin = new Padding(3, 10, 3, 10);
            materialLabel31.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel31.Name = "materialLabel31";
            materialLabel31.Size = new Size(54, 14);
            materialLabel31.TabIndex = 8;
            materialLabel31.Text = "Facebook";
            // 
            // materialLabel32
            // 
            materialLabel32.AutoSize = true;
            materialLabel32.Depth = 0;
            materialLabel32.Dock = DockStyle.Fill;
            materialLabel32.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel32.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel32.Location = new Point(73, 210);
            materialLabel32.Margin = new Padding(3, 10, 3, 10);
            materialLabel32.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel32.Name = "materialLabel32";
            materialLabel32.Size = new Size(168, 30);
            materialLabel32.TabIndex = 9;
            materialLabel32.Text = "...";
            // 
            // materialLabel33
            // 
            materialLabel33.AutoSize = true;
            materialLabel33.Depth = 0;
            materialLabel33.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel33.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel33.Location = new Point(3, 260);
            materialLabel33.Margin = new Padding(3, 10, 3, 10);
            materialLabel33.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel33.Name = "materialLabel33";
            materialLabel33.Size = new Size(39, 14);
            materialLabel33.TabIndex = 10;
            materialLabel33.Text = "Google";
            // 
            // materialLabel34
            // 
            materialLabel34.AutoSize = true;
            materialLabel34.Depth = 0;
            materialLabel34.Dock = DockStyle.Fill;
            materialLabel34.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel34.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel34.Location = new Point(73, 260);
            materialLabel34.Margin = new Padding(3, 10, 3, 10);
            materialLabel34.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel34.Name = "materialLabel34";
            materialLabel34.Size = new Size(168, 30);
            materialLabel34.TabIndex = 11;
            materialLabel34.Text = "...";
            // 
            // materialCard13
            // 
            materialCard13.BackColor = Color.FromArgb(255, 255, 255);
            materialCard13.Controls.Add(dataGridView1);
            materialCard13.Depth = 0;
            materialCard13.Dock = DockStyle.Fill;
            materialCard13.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard13.Location = new Point(14, 2);
            materialCard13.Margin = new Padding(14, 2, 2, 14);
            materialCard13.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard13.Name = "materialCard13";
            materialCard13.Padding = new Padding(14);
            materialCard13.Size = new Size(1, 681);
            materialCard13.TabIndex = 1;
            // 
            // dataGridView1
            // 
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.BackgroundColor = SystemColors.ButtonHighlight;
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.ColumnHeadersHeight = 40;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridView1.Columns.AddRange(new DataGridViewColumn[] { dataGridViewTextBoxColumn1, dataGridViewTextBoxColumn2, dataGridViewTextBoxColumn3, dataGridViewTextBoxColumn4 });
            dataGridView1.ContextMenuStrip = MenuProfiles;
            dataGridView1.Cursor = Cursors.Hand;
            dataGridView1.Dock = DockStyle.Fill;
            dataGridView1.Location = new Point(14, 14);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.RowHeadersVisible = false;
            dataGridView1.RowHeadersWidth = 50;
            dataGridView1.RowTemplate.Height = 42;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.Size = new Size(-27, 653);
            dataGridView1.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewTextBoxColumn1.DataPropertyName = "profile_id";
            dataGridViewTextBoxColumn1.FillWeight = 109.817F;
            dataGridViewTextBoxColumn1.HeaderText = "ID";
            dataGridViewTextBoxColumn1.MinimumWidth = 6;
            dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewTextBoxColumn2.DataPropertyName = "profile_status";
            dataGridViewTextBoxColumn2.FillWeight = 42.66524F;
            dataGridViewTextBoxColumn2.HeaderText = "Status";
            dataGridViewTextBoxColumn2.MinimumWidth = 6;
            dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewTextBoxColumn3.DataPropertyName = "profile_proxy";
            dataGridViewTextBoxColumn3.FillWeight = 78.28705F;
            dataGridViewTextBoxColumn3.HeaderText = "Proxy";
            dataGridViewTextBoxColumn3.MinimumWidth = 6;
            dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewTextBoxColumn4.DataPropertyName = "profile_last_update";
            dataGridViewTextBoxColumn4.FillWeight = 78.28705F;
            dataGridViewTextBoxColumn4.HeaderText = "Last update";
            dataGridViewTextBoxColumn4.MinimumWidth = 6;
            dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // tabPage1
            // 
            tabPage1.Controls.Add(tableLayoutPanel3);
            tabPage1.ImageKey = "browser.png";
            tabPage1.Location = new Point(4, 24);
            tabPage1.Name = "tabPage1";
            tabPage1.Padding = new Padding(3);
            tabPage1.Size = new Size(1007, 673);
            tabPage1.TabIndex = 1;
            tabPage1.Text = "Profiles";
            tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3.ColumnCount = 1;
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel3.Controls.Add(materialCard3, 0, 0);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel22, 0, 1);
            tableLayoutPanel3.Dock = DockStyle.Fill;
            tableLayoutPanel3.Location = new Point(3, 3);
            tableLayoutPanel3.Name = "tableLayoutPanel3";
            tableLayoutPanel3.RowCount = 2;
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 79F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel3.Size = new Size(1001, 667);
            tableLayoutPanel3.TabIndex = 1;
            // 
            // materialCard3
            // 
            materialCard3.BackColor = Color.FromArgb(255, 255, 255);
            materialCard3.Controls.Add(tableLayoutPanel21);
            materialCard3.Depth = 0;
            materialCard3.Dock = DockStyle.Fill;
            materialCard3.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard3.Location = new Point(14, 14);
            materialCard3.Margin = new Padding(14);
            materialCard3.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard3.Name = "materialCard3";
            materialCard3.Padding = new Padding(5);
            materialCard3.Size = new Size(973, 51);
            materialCard3.TabIndex = 0;
            // 
            // tableLayoutPanel21
            // 
            tableLayoutPanel21.ColumnCount = 6;
            tableLayoutPanel21.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 240F));
            tableLayoutPanel21.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel21.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 67F));
            tableLayoutPanel21.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 64F));
            tableLayoutPanel21.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 132F));
            tableLayoutPanel21.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 150F));
            tableLayoutPanel21.Controls.Add(materialButton3, 5, 0);
            tableLayoutPanel21.Controls.Add(materialButton4, 4, 0);
            tableLayoutPanel21.Controls.Add(materialComboBox1, 0, 0);
            tableLayoutPanel21.Dock = DockStyle.Fill;
            tableLayoutPanel21.Location = new Point(5, 5);
            tableLayoutPanel21.Margin = new Padding(0);
            tableLayoutPanel21.Name = "tableLayoutPanel21";
            tableLayoutPanel21.RowCount = 1;
            tableLayoutPanel21.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel21.Size = new Size(963, 41);
            tableLayoutPanel21.TabIndex = 0;
            // 
            // materialButton3
            // 
            materialButton3.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton3.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton3.Depth = 0;
            materialButton3.HighEmphasis = true;
            materialButton3.Icon = null;
            materialButton3.Location = new Point(813, 0);
            materialButton3.Margin = new Padding(0);
            materialButton3.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton3.Name = "materialButton3";
            materialButton3.NoAccentTextColor = Color.Empty;
            materialButton3.Size = new Size(146, 36);
            materialButton3.TabIndex = 0;
            materialButton3.Text = "Create Profiles";
            materialButton3.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton3.UseAccentColor = false;
            materialButton3.UseVisualStyleBackColor = true;
            materialButton3.Click += ButtonCreateProfiles_Click;
            // 
            // materialButton4
            // 
            materialButton4.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton4.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton4.Depth = 0;
            materialButton4.HighEmphasis = true;
            materialButton4.Icon = null;
            materialButton4.Location = new Point(686, 0);
            materialButton4.Margin = new Padding(5, 0, 5, 0);
            materialButton4.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton4.Name = "materialButton4";
            materialButton4.NoAccentTextColor = Color.Empty;
            materialButton4.Size = new Size(122, 36);
            materialButton4.TabIndex = 1;
            materialButton4.Text = "Create Group";
            materialButton4.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton4.UseAccentColor = false;
            materialButton4.UseVisualStyleBackColor = true;
            materialButton4.Click += ButtonCreateGroup_Click;
            // 
            // materialComboBox1
            // 
            materialComboBox1.AutoResize = false;
            materialComboBox1.BackColor = Color.FromArgb(255, 255, 255);
            materialComboBox1.Depth = 0;
            materialComboBox1.Dock = DockStyle.Fill;
            materialComboBox1.DrawMode = DrawMode.OwnerDrawVariable;
            materialComboBox1.DropDownHeight = 174;
            materialComboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            materialComboBox1.DropDownWidth = 121;
            materialComboBox1.Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialComboBox1.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialComboBox1.FormattingEnabled = true;
            materialComboBox1.IntegralHeight = false;
            materialComboBox1.ItemHeight = 43;
            materialComboBox1.Location = new Point(3, 3);
            materialComboBox1.MaxDropDownItems = 4;
            materialComboBox1.MouseState = MaterialSkin.MouseState.OUT;
            materialComboBox1.Name = "materialComboBox1";
            materialComboBox1.Size = new Size(234, 49);
            materialComboBox1.StartIndex = 0;
            materialComboBox1.TabIndex = 2;
            materialComboBox1.SelectedIndexChanged += filterByGroup_SelectedIndexChanged;
            // 
            // tableLayoutPanel22
            // 
            tableLayoutPanel22.ColumnCount = 2;
            tableLayoutPanel22.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel22.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 300F));
            tableLayoutPanel22.Controls.Add(materialCard14, 1, 0);
            tableLayoutPanel22.Controls.Add(tableLayoutPanel26, 0, 0);
            tableLayoutPanel22.Dock = DockStyle.Fill;
            tableLayoutPanel22.Location = new Point(0, 79);
            tableLayoutPanel22.Margin = new Padding(0);
            tableLayoutPanel22.Name = "tableLayoutPanel22";
            tableLayoutPanel22.RowCount = 1;
            tableLayoutPanel22.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel22.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel22.Size = new Size(1001, 588);
            tableLayoutPanel22.TabIndex = 1;
            // 
            // materialCard14
            // 
            materialCard14.BackColor = Color.FromArgb(255, 255, 255);
            materialCard14.Controls.Add(tableLayoutPanel23);
            materialCard14.Depth = 0;
            materialCard14.Dock = DockStyle.Fill;
            materialCard14.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard14.Location = new Point(715, 2);
            materialCard14.Margin = new Padding(14, 2, 14, 14);
            materialCard14.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard14.Name = "materialCard14";
            materialCard14.Padding = new Padding(14);
            materialCard14.Size = new Size(272, 572);
            materialCard14.TabIndex = 2;
            // 
            // tableLayoutPanel23
            // 
            tableLayoutPanel23.ColumnCount = 1;
            tableLayoutPanel23.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel23.Controls.Add(tableLayoutPanel24, 0, 0);
            tableLayoutPanel23.Controls.Add(panel14, 0, 1);
            tableLayoutPanel23.Dock = DockStyle.Fill;
            tableLayoutPanel23.Location = new Point(14, 14);
            tableLayoutPanel23.Margin = new Padding(0);
            tableLayoutPanel23.Name = "tableLayoutPanel23";
            tableLayoutPanel23.RowCount = 2;
            tableLayoutPanel23.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel23.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel23.Size = new Size(244, 544);
            tableLayoutPanel23.TabIndex = 0;
            // 
            // tableLayoutPanel24
            // 
            tableLayoutPanel24.ColumnCount = 2;
            tableLayoutPanel24.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 48F));
            tableLayoutPanel24.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel24.Controls.Add(panel13, 0, 0);
            tableLayoutPanel24.Controls.Add(materialLabel1, 1, 0);
            tableLayoutPanel24.Dock = DockStyle.Fill;
            tableLayoutPanel24.Location = new Point(0, 0);
            tableLayoutPanel24.Margin = new Padding(0);
            tableLayoutPanel24.Name = "tableLayoutPanel24";
            tableLayoutPanel24.RowCount = 1;
            tableLayoutPanel24.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel24.Size = new Size(244, 40);
            tableLayoutPanel24.TabIndex = 0;
            // 
            // panel13
            // 
            panel13.BackgroundImage = Properties.Resources.fingerprint;
            panel13.BackgroundImageLayout = ImageLayout.Zoom;
            panel13.Location = new Point(6, 6);
            panel13.Margin = new Padding(6);
            panel13.Name = "panel13";
            panel13.Size = new Size(36, 28);
            panel13.TabIndex = 0;
            // 
            // materialLabel1
            // 
            materialLabel1.Depth = 0;
            materialLabel1.Dock = DockStyle.Fill;
            materialLabel1.Font = new Font("Roboto", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel1.FontType = MaterialSkin.MaterialSkinManager.fontType.Button;
            materialLabel1.Location = new Point(53, 5);
            materialLabel1.Margin = new Padding(5);
            materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel1.Name = "materialLabel1";
            materialLabel1.Size = new Size(186, 30);
            materialLabel1.TabIndex = 1;
            materialLabel1.Text = "Profile Info";
            materialLabel1.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // panel14
            // 
            panel14.AutoScroll = true;
            panel14.Controls.Add(tableLayoutPanel25);
            panel14.Dock = DockStyle.Fill;
            panel14.Location = new Point(0, 52);
            panel14.Margin = new Padding(0, 12, 0, 0);
            panel14.Name = "panel14";
            panel14.Size = new Size(244, 492);
            panel14.TabIndex = 1;
            // 
            // tableLayoutPanel25
            // 
            tableLayoutPanel25.ColumnCount = 2;
            tableLayoutPanel25.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanel25.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel25.Controls.Add(materialLabel35, 1, 3);
            tableLayoutPanel25.Controls.Add(materialLabel36, 0, 3);
            tableLayoutPanel25.Controls.Add(materialLabel37, 1, 2);
            tableLayoutPanel25.Controls.Add(materialLabel38, 0, 2);
            tableLayoutPanel25.Controls.Add(materialLabel39, 1, 1);
            tableLayoutPanel25.Controls.Add(materialLabel40, 0, 1);
            tableLayoutPanel25.Controls.Add(materialLabel41, 1, 0);
            tableLayoutPanel25.Controls.Add(materialLabel42, 0, 0);
            tableLayoutPanel25.Controls.Add(materialLabel43, 0, 4);
            tableLayoutPanel25.Controls.Add(materialLabel44, 1, 4);
            tableLayoutPanel25.Controls.Add(materialLabel45, 0, 5);
            tableLayoutPanel25.Controls.Add(materialLabel46, 1, 5);
            tableLayoutPanel25.Dock = DockStyle.Top;
            tableLayoutPanel25.Location = new Point(0, 0);
            tableLayoutPanel25.Name = "tableLayoutPanel25";
            tableLayoutPanel25.RowCount = 7;
            tableLayoutPanel25.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel25.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel25.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel25.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel25.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel25.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel25.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel25.Size = new Size(244, 352);
            tableLayoutPanel25.TabIndex = 0;
            // 
            // materialLabel35
            // 
            materialLabel35.AutoSize = true;
            materialLabel35.Depth = 0;
            materialLabel35.Dock = DockStyle.Fill;
            materialLabel35.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel35.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel35.Location = new Point(73, 160);
            materialLabel35.Margin = new Padding(3, 10, 3, 10);
            materialLabel35.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel35.Name = "materialLabel35";
            materialLabel35.Size = new Size(168, 30);
            materialLabel35.TabIndex = 7;
            materialLabel35.Text = "...";
            // 
            // materialLabel36
            // 
            materialLabel36.Depth = 0;
            materialLabel36.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel36.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel36.Location = new Point(3, 160);
            materialLabel36.Margin = new Padding(3, 10, 3, 10);
            materialLabel36.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel36.Name = "materialLabel36";
            materialLabel36.Size = new Size(60, 30);
            materialLabel36.TabIndex = 6;
            materialLabel36.Text = "Screen Resolution";
            // 
            // materialLabel37
            // 
            materialLabel37.AutoSize = true;
            materialLabel37.Depth = 0;
            materialLabel37.Dock = DockStyle.Fill;
            materialLabel37.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel37.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel37.Location = new Point(73, 110);
            materialLabel37.Margin = new Padding(3, 10, 3, 10);
            materialLabel37.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel37.Name = "materialLabel37";
            materialLabel37.Size = new Size(168, 30);
            materialLabel37.TabIndex = 5;
            materialLabel37.Text = "...";
            // 
            // materialLabel38
            // 
            materialLabel38.AutoSize = true;
            materialLabel38.Depth = 0;
            materialLabel38.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel38.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel38.Location = new Point(3, 110);
            materialLabel38.Margin = new Padding(3, 10, 3, 10);
            materialLabel38.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel38.Name = "materialLabel38";
            materialLabel38.Size = new Size(32, 14);
            materialLabel38.TabIndex = 4;
            materialLabel38.Text = "Proxy";
            // 
            // materialLabel39
            // 
            materialLabel39.AutoSize = true;
            materialLabel39.Depth = 0;
            materialLabel39.Dock = DockStyle.Fill;
            materialLabel39.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel39.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel39.Location = new Point(73, 60);
            materialLabel39.Margin = new Padding(3, 10, 3, 10);
            materialLabel39.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel39.Name = "materialLabel39";
            materialLabel39.Size = new Size(168, 30);
            materialLabel39.TabIndex = 3;
            materialLabel39.Text = "...";
            // 
            // materialLabel40
            // 
            materialLabel40.AutoSize = true;
            materialLabel40.Depth = 0;
            materialLabel40.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel40.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel40.Location = new Point(3, 60);
            materialLabel40.Margin = new Padding(3, 10, 3, 10);
            materialLabel40.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel40.Name = "materialLabel40";
            materialLabel40.Size = new Size(60, 14);
            materialLabel40.TabIndex = 2;
            materialLabel40.Text = "User Agent";
            // 
            // materialLabel41
            // 
            materialLabel41.Depth = 0;
            materialLabel41.Dock = DockStyle.Fill;
            materialLabel41.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel41.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel41.Location = new Point(73, 10);
            materialLabel41.Margin = new Padding(3, 10, 3, 10);
            materialLabel41.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel41.Name = "materialLabel41";
            materialLabel41.Size = new Size(168, 30);
            materialLabel41.TabIndex = 1;
            materialLabel41.Text = "...";
            // 
            // materialLabel42
            // 
            materialLabel42.AutoSize = true;
            materialLabel42.Depth = 0;
            materialLabel42.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel42.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel42.Location = new Point(3, 10);
            materialLabel42.Margin = new Padding(3, 10, 3, 10);
            materialLabel42.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel42.Name = "materialLabel42";
            materialLabel42.Size = new Size(50, 14);
            materialLabel42.TabIndex = 0;
            materialLabel42.Text = "Profile ID";
            // 
            // materialLabel43
            // 
            materialLabel43.AutoSize = true;
            materialLabel43.Depth = 0;
            materialLabel43.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel43.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel43.Location = new Point(3, 210);
            materialLabel43.Margin = new Padding(3, 10, 3, 10);
            materialLabel43.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel43.Name = "materialLabel43";
            materialLabel43.Size = new Size(54, 14);
            materialLabel43.TabIndex = 8;
            materialLabel43.Text = "Facebook";
            // 
            // materialLabel44
            // 
            materialLabel44.AutoSize = true;
            materialLabel44.Depth = 0;
            materialLabel44.Dock = DockStyle.Fill;
            materialLabel44.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel44.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel44.Location = new Point(73, 210);
            materialLabel44.Margin = new Padding(3, 10, 3, 10);
            materialLabel44.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel44.Name = "materialLabel44";
            materialLabel44.Size = new Size(168, 30);
            materialLabel44.TabIndex = 9;
            materialLabel44.Text = "...";
            // 
            // materialLabel45
            // 
            materialLabel45.AutoSize = true;
            materialLabel45.Depth = 0;
            materialLabel45.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel45.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel45.Location = new Point(3, 260);
            materialLabel45.Margin = new Padding(3, 10, 3, 10);
            materialLabel45.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel45.Name = "materialLabel45";
            materialLabel45.Size = new Size(39, 14);
            materialLabel45.TabIndex = 10;
            materialLabel45.Text = "Google";
            // 
            // materialLabel46
            // 
            materialLabel46.AutoSize = true;
            materialLabel46.Depth = 0;
            materialLabel46.Dock = DockStyle.Fill;
            materialLabel46.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel46.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel46.Location = new Point(73, 260);
            materialLabel46.Margin = new Padding(3, 10, 3, 10);
            materialLabel46.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel46.Name = "materialLabel46";
            materialLabel46.Size = new Size(168, 30);
            materialLabel46.TabIndex = 11;
            materialLabel46.Text = "...";
            // 
            // tableLayoutPanel26
            // 
            tableLayoutPanel26.ColumnCount = 1;
            tableLayoutPanel26.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel26.Controls.Add(materialCard15, 0, 1);
            tableLayoutPanel26.Controls.Add(materialCard16, 0, 0);
            tableLayoutPanel26.Dock = DockStyle.Fill;
            tableLayoutPanel26.Location = new Point(0, 0);
            tableLayoutPanel26.Margin = new Padding(0);
            tableLayoutPanel26.Name = "tableLayoutPanel26";
            tableLayoutPanel26.RowCount = 2;
            tableLayoutPanel26.RowStyles.Add(new RowStyle(SizeType.Percent, 66.7263F));
            tableLayoutPanel26.RowStyles.Add(new RowStyle(SizeType.Percent, 33.2737F));
            tableLayoutPanel26.Size = new Size(701, 588);
            tableLayoutPanel26.TabIndex = 3;
            // 
            // materialCard15
            // 
            materialCard15.BackColor = Color.FromArgb(255, 255, 255);
            materialCard15.Controls.Add(dataGridView2);
            materialCard15.Depth = 0;
            materialCard15.Dock = DockStyle.Fill;
            materialCard15.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard15.Location = new Point(14, 394);
            materialCard15.Margin = new Padding(14, 2, 2, 14);
            materialCard15.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard15.Name = "materialCard15";
            materialCard15.Padding = new Padding(14);
            materialCard15.Size = new Size(685, 180);
            materialCard15.TabIndex = 3;
            // 
            // dataGridView2
            // 
            dataGridView2.AllowUserToAddRows = false;
            dataGridView2.AllowUserToDeleteRows = false;
            dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView2.BackgroundColor = SystemColors.ButtonHighlight;
            dataGridView2.BorderStyle = BorderStyle.None;
            dataGridViewCellStyle13.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = SystemColors.Control;
            dataGridViewCellStyle13.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle13.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = Color.Orange;
            dataGridViewCellStyle13.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = DataGridViewTriState.True;
            dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            dataGridView2.ColumnHeadersHeight = 40;
            dataGridView2.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridView2.Columns.AddRange(new DataGridViewColumn[] { dataGridViewTextBoxColumn5, dataGridViewTextBoxColumn6 });
            dataGridView2.ContextMenuStrip = MenuGroups;
            dataGridView2.Cursor = Cursors.Hand;
            dataGridViewCellStyle14.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = SystemColors.Window;
            dataGridViewCellStyle14.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle14.ForeColor = Color.FromArgb(222, 0, 0, 0);
            dataGridViewCellStyle14.SelectionBackColor = Color.Orange;
            dataGridViewCellStyle14.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = DataGridViewTriState.False;
            dataGridView2.DefaultCellStyle = dataGridViewCellStyle14;
            dataGridView2.Dock = DockStyle.Fill;
            dataGridView2.Location = new Point(14, 14);
            dataGridView2.Name = "dataGridView2";
            dataGridViewCellStyle15.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = SystemColors.Control;
            dataGridViewCellStyle15.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle15.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = Color.Orange;
            dataGridViewCellStyle15.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = DataGridViewTriState.True;
            dataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            dataGridView2.RowHeadersVisible = false;
            dataGridView2.RowHeadersWidth = 50;
            dataGridView2.RowTemplate.Height = 42;
            dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.Size = new Size(657, 152);
            dataGridView2.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewTextBoxColumn5.DataPropertyName = "group_name";
            dataGridViewTextBoxColumn5.FillWeight = 109.817F;
            dataGridViewTextBoxColumn5.HeaderText = "Group Name";
            dataGridViewTextBoxColumn5.MinimumWidth = 6;
            dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewTextBoxColumn6.DataPropertyName = "group_total";
            dataGridViewTextBoxColumn6.FillWeight = 42.66524F;
            dataGridViewTextBoxColumn6.HeaderText = "Total Profiles";
            dataGridViewTextBoxColumn6.MinimumWidth = 6;
            dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // materialCard16
            // 
            materialCard16.BackColor = Color.FromArgb(255, 255, 255);
            materialCard16.Controls.Add(dataGridView3);
            materialCard16.Depth = 0;
            materialCard16.Dock = DockStyle.Fill;
            materialCard16.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard16.Location = new Point(14, 2);
            materialCard16.Margin = new Padding(14, 2, 2, 14);
            materialCard16.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard16.Name = "materialCard16";
            materialCard16.Padding = new Padding(14);
            materialCard16.Size = new Size(685, 376);
            materialCard16.TabIndex = 2;
            // 
            // dataGridView3
            // 
            dataGridView3.AllowUserToAddRows = false;
            dataGridView3.AllowUserToDeleteRows = false;
            dataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView3.BackgroundColor = SystemColors.ButtonHighlight;
            dataGridView3.BorderStyle = BorderStyle.None;
            dataGridViewCellStyle16.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = SystemColors.Control;
            dataGridViewCellStyle16.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle16.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = Color.Orange;
            dataGridViewCellStyle16.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = DataGridViewTriState.True;
            dataGridView3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            dataGridView3.ColumnHeadersHeight = 40;
            dataGridView3.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridView3.Columns.AddRange(new DataGridViewColumn[] { dataGridViewTextBoxColumn7, dataGridViewTextBoxColumn8, dataGridViewTextBoxColumn9, dataGridViewTextBoxColumn10 });
            dataGridView3.ContextMenuStrip = MenuProfiles;
            dataGridView3.Cursor = Cursors.Hand;
            dataGridViewCellStyle17.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = SystemColors.Window;
            dataGridViewCellStyle17.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle17.ForeColor = Color.FromArgb(222, 0, 0, 0);
            dataGridViewCellStyle17.SelectionBackColor = Color.Orange;
            dataGridViewCellStyle17.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = DataGridViewTriState.False;
            dataGridView3.DefaultCellStyle = dataGridViewCellStyle17;
            dataGridView3.Dock = DockStyle.Fill;
            dataGridView3.Location = new Point(14, 14);
            dataGridView3.Name = "dataGridView3";
            dataGridViewCellStyle18.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = SystemColors.Control;
            dataGridViewCellStyle18.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle18.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle18.SelectionBackColor = Color.Orange;
            dataGridViewCellStyle18.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = DataGridViewTriState.True;
            dataGridView3.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            dataGridView3.RowHeadersVisible = false;
            dataGridView3.RowHeadersWidth = 50;
            dataGridView3.RowTemplate.Height = 42;
            dataGridView3.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView3.Size = new Size(657, 348);
            dataGridView3.TabIndex = 0;
            dataGridView3.CellClick += ListProfiles_CellClick;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewTextBoxColumn7.DataPropertyName = "profile_id";
            dataGridViewTextBoxColumn7.FillWeight = 109.817F;
            dataGridViewTextBoxColumn7.HeaderText = "ID";
            dataGridViewTextBoxColumn7.MinimumWidth = 6;
            dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewTextBoxColumn8.DataPropertyName = "profile_status";
            dataGridViewTextBoxColumn8.FillWeight = 42.66524F;
            dataGridViewTextBoxColumn8.HeaderText = "Status";
            dataGridViewTextBoxColumn8.MinimumWidth = 6;
            dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewTextBoxColumn9.DataPropertyName = "profile_proxy";
            dataGridViewTextBoxColumn9.FillWeight = 78.28705F;
            dataGridViewTextBoxColumn9.HeaderText = "Proxy";
            dataGridViewTextBoxColumn9.MinimumWidth = 6;
            dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            dataGridViewTextBoxColumn10.DataPropertyName = "profile_last_update";
            dataGridViewTextBoxColumn10.FillWeight = 78.28705F;
            dataGridViewTextBoxColumn10.HeaderText = "Last update";
            dataGridViewTextBoxColumn10.MinimumWidth = 6;
            dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // tabPage2
            // 
            tabPage2.Location = new Point(0, 0);
            tabPage2.Name = "tabPage2";
            tabPage2.Size = new Size(200, 100);
            tabPage2.TabIndex = 0;
            // 
            // tableLayoutPanel27
            // 
            tableLayoutPanel27.ColumnCount = 1;
            tableLayoutPanel27.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel27.Controls.Add(materialCard17, 0, 0);
            tableLayoutPanel27.Dock = DockStyle.Fill;
            tableLayoutPanel27.Location = new Point(0, 0);
            tableLayoutPanel27.Name = "tableLayoutPanel27";
            tableLayoutPanel27.RowCount = 2;
            tableLayoutPanel27.Size = new Size(200, 100);
            tableLayoutPanel27.TabIndex = 0;
            // 
            // materialCard17
            // 
            materialCard17.BackColor = Color.FromArgb(255, 255, 255);
            materialCard17.Controls.Add(tableLayoutPanel28);
            materialCard17.Depth = 0;
            materialCard17.Dock = DockStyle.Fill;
            materialCard17.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard17.Location = new Point(14, 14);
            materialCard17.Margin = new Padding(14);
            materialCard17.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard17.Name = "materialCard17";
            materialCard17.Padding = new Padding(4);
            materialCard17.Size = new Size(172, 51);
            materialCard17.TabIndex = 0;
            // 
            // tableLayoutPanel28
            // 
            tableLayoutPanel28.ColumnCount = 2;
            tableLayoutPanel28.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 87.59048F));
            tableLayoutPanel28.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 12.40951F));
            tableLayoutPanel28.Controls.Add(materialButton5, 1, 0);
            tableLayoutPanel28.Dock = DockStyle.Fill;
            tableLayoutPanel28.Location = new Point(4, 4);
            tableLayoutPanel28.Margin = new Padding(0);
            tableLayoutPanel28.Name = "tableLayoutPanel28";
            tableLayoutPanel28.RowCount = 1;
            tableLayoutPanel28.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel28.Size = new Size(164, 43);
            tableLayoutPanel28.TabIndex = 1;
            // 
            // materialButton5
            // 
            materialButton5.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton5.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton5.Depth = 0;
            materialButton5.HighEmphasis = true;
            materialButton5.Icon = null;
            materialButton5.Location = new Point(143, 0);
            materialButton5.Margin = new Padding(0);
            materialButton5.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton5.Name = "materialButton5";
            materialButton5.NoAccentTextColor = Color.Empty;
            materialButton5.Size = new Size(21, 36);
            materialButton5.TabIndex = 0;
            materialButton5.Text = "Save Seting";
            materialButton5.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton5.UseAccentColor = false;
            materialButton5.UseVisualStyleBackColor = true;
            materialButton5.Click += SaveSetting_Click;
            // 
            // flowLayoutPanel2
            // 
            flowLayoutPanel2.AutoScroll = true;
            flowLayoutPanel2.Controls.Add(materialCard18);
            flowLayoutPanel2.Controls.Add(materialCard19);
            flowLayoutPanel2.Controls.Add(materialCard20);
            flowLayoutPanel2.Dock = DockStyle.Fill;
            flowLayoutPanel2.Location = new Point(3, 82);
            flowLayoutPanel2.Name = "flowLayoutPanel2";
            flowLayoutPanel2.Size = new Size(194, 588);
            flowLayoutPanel2.TabIndex = 1;
            // 
            // materialCard18
            // 
            materialCard18.BackColor = Color.FromArgb(255, 255, 255);
            materialCard18.Controls.Add(tableLayoutPanel29);
            materialCard18.Depth = 0;
            materialCard18.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard18.Location = new Point(14, 14);
            materialCard18.Margin = new Padding(14);
            materialCard18.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard18.Name = "materialCard18";
            materialCard18.Padding = new Padding(14);
            materialCard18.Size = new Size(879, 118);
            materialCard18.TabIndex = 3;
            // 
            // tableLayoutPanel29
            // 
            tableLayoutPanel29.ColumnCount = 1;
            tableLayoutPanel29.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel29.Controls.Add(panel15, 0, 0);
            tableLayoutPanel29.Controls.Add(panel16, 0, 1);
            tableLayoutPanel29.Dock = DockStyle.Fill;
            tableLayoutPanel29.Location = new Point(14, 14);
            tableLayoutPanel29.Margin = new Padding(0);
            tableLayoutPanel29.Name = "tableLayoutPanel29";
            tableLayoutPanel29.RowCount = 2;
            tableLayoutPanel29.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel29.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel29.Size = new Size(851, 90);
            tableLayoutPanel29.TabIndex = 0;
            // 
            // panel15
            // 
            panel15.Controls.Add(materialLabel47);
            panel15.Controls.Add(materialButton6);
            panel15.Dock = DockStyle.Fill;
            panel15.Location = new Point(0, 0);
            panel15.Margin = new Padding(0);
            panel15.Name = "panel15";
            panel15.Size = new Size(851, 40);
            panel15.TabIndex = 1;
            // 
            // materialLabel47
            // 
            materialLabel47.Depth = 0;
            materialLabel47.Dock = DockStyle.Left;
            materialLabel47.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel47.Location = new Point(0, 0);
            materialLabel47.Margin = new Padding(12, 0, 12, 0);
            materialLabel47.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel47.Name = "materialLabel47";
            materialLabel47.Padding = new Padding(12);
            materialLabel47.Size = new Size(163, 40);
            materialLabel47.TabIndex = 0;
            materialLabel47.Text = "Folder Profiles";
            materialLabel47.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialButton6
            // 
            materialButton6.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton6.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton6.Depth = 0;
            materialButton6.Dock = DockStyle.Right;
            materialButton6.HighEmphasis = true;
            materialButton6.Icon = null;
            materialButton6.Location = new Point(745, 0);
            materialButton6.Margin = new Padding(4, 6, 4, 6);
            materialButton6.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton6.Name = "materialButton6";
            materialButton6.NoAccentTextColor = Color.Empty;
            materialButton6.Size = new Size(106, 40);
            materialButton6.TabIndex = 2;
            materialButton6.Text = "Select File";
            materialButton6.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton6.UseAccentColor = false;
            materialButton6.UseVisualStyleBackColor = true;
            materialButton6.Click += SelectFolderProfiles_Click;
            // 
            // panel16
            // 
            panel16.Controls.Add(materialLabel48);
            panel16.Controls.Add(materialLabel49);
            panel16.Dock = DockStyle.Fill;
            panel16.Location = new Point(0, 40);
            panel16.Margin = new Padding(0);
            panel16.Name = "panel16";
            panel16.Size = new Size(851, 50);
            panel16.TabIndex = 2;
            // 
            // materialLabel48
            // 
            materialLabel48.BackColor = Color.White;
            materialLabel48.Depth = 0;
            materialLabel48.Dock = DockStyle.Top;
            materialLabel48.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel48.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel48.ForeColor = Color.LightCoral;
            materialLabel48.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel48.Location = new Point(0, 35);
            materialLabel48.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel48.Name = "materialLabel48";
            materialLabel48.Size = new Size(851, 17);
            materialLabel48.TabIndex = 1;
            materialLabel48.Text = "* Where to save the local profile file";
            // 
            // materialLabel49
            // 
            materialLabel49.Depth = 0;
            materialLabel49.Dock = DockStyle.Top;
            materialLabel49.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel49.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            materialLabel49.Location = new Point(0, 0);
            materialLabel49.Margin = new Padding(0);
            materialLabel49.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel49.Name = "materialLabel49";
            materialLabel49.Size = new Size(851, 35);
            materialLabel49.TabIndex = 0;
            materialLabel49.Text = "Path File...";
            materialLabel49.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // materialCard19
            // 
            materialCard19.BackColor = Color.FromArgb(255, 255, 255);
            materialCard19.Controls.Add(tableLayoutPanel30);
            materialCard19.Depth = 0;
            materialCard19.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard19.Location = new Point(14, 160);
            materialCard19.Margin = new Padding(14);
            materialCard19.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard19.Name = "materialCard19";
            materialCard19.Padding = new Padding(14);
            materialCard19.Size = new Size(879, 133);
            materialCard19.TabIndex = 1;
            // 
            // tableLayoutPanel30
            // 
            tableLayoutPanel30.ColumnCount = 1;
            tableLayoutPanel30.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel30.Controls.Add(panel17, 0, 0);
            tableLayoutPanel30.Controls.Add(panel18, 0, 1);
            tableLayoutPanel30.Dock = DockStyle.Fill;
            tableLayoutPanel30.Location = new Point(14, 14);
            tableLayoutPanel30.Margin = new Padding(0);
            tableLayoutPanel30.Name = "tableLayoutPanel30";
            tableLayoutPanel30.RowCount = 2;
            tableLayoutPanel30.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel30.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel30.Size = new Size(851, 105);
            tableLayoutPanel30.TabIndex = 0;
            // 
            // panel17
            // 
            panel17.Controls.Add(materialLabel50);
            panel17.Controls.Add(materialButton7);
            panel17.Dock = DockStyle.Fill;
            panel17.Location = new Point(0, 0);
            panel17.Margin = new Padding(0);
            panel17.Name = "panel17";
            panel17.Size = new Size(851, 40);
            panel17.TabIndex = 1;
            // 
            // materialLabel50
            // 
            materialLabel50.Depth = 0;
            materialLabel50.Dock = DockStyle.Left;
            materialLabel50.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel50.Location = new Point(0, 0);
            materialLabel50.Margin = new Padding(12, 0, 12, 0);
            materialLabel50.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel50.Name = "materialLabel50";
            materialLabel50.Padding = new Padding(12);
            materialLabel50.Size = new Size(92, 40);
            materialLabel50.TabIndex = 0;
            materialLabel50.Text = "File Proxies";
            materialLabel50.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialButton7
            // 
            materialButton7.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton7.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton7.Depth = 0;
            materialButton7.Dock = DockStyle.Right;
            materialButton7.HighEmphasis = true;
            materialButton7.Icon = null;
            materialButton7.Location = new Point(745, 0);
            materialButton7.Margin = new Padding(4, 6, 4, 6);
            materialButton7.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton7.Name = "materialButton7";
            materialButton7.NoAccentTextColor = Color.Empty;
            materialButton7.Size = new Size(106, 40);
            materialButton7.TabIndex = 2;
            materialButton7.Text = "Select File";
            materialButton7.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton7.UseAccentColor = false;
            materialButton7.UseVisualStyleBackColor = true;
            materialButton7.Click += SelectFileProxy_Click;
            // 
            // panel18
            // 
            panel18.Controls.Add(materialLabel51);
            panel18.Controls.Add(materialLabel52);
            panel18.Controls.Add(materialLabel53);
            panel18.Dock = DockStyle.Fill;
            panel18.Location = new Point(0, 40);
            panel18.Margin = new Padding(0);
            panel18.Name = "panel18";
            panel18.Size = new Size(851, 65);
            panel18.TabIndex = 2;
            // 
            // materialLabel51
            // 
            materialLabel51.BackColor = Color.White;
            materialLabel51.Depth = 0;
            materialLabel51.Dock = DockStyle.Top;
            materialLabel51.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel51.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel51.ForeColor = Color.LightCoral;
            materialLabel51.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel51.Location = new Point(0, 52);
            materialLabel51.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel51.Name = "materialLabel51";
            materialLabel51.Size = new Size(851, 17);
            materialLabel51.TabIndex = 2;
            materialLabel51.Text = "** File .txt";
            // 
            // materialLabel52
            // 
            materialLabel52.BackColor = Color.White;
            materialLabel52.Depth = 0;
            materialLabel52.Dock = DockStyle.Top;
            materialLabel52.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel52.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel52.ForeColor = Color.LightCoral;
            materialLabel52.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel52.Location = new Point(0, 35);
            materialLabel52.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel52.Name = "materialLabel52";
            materialLabel52.Size = new Size(851, 17);
            materialLabel52.TabIndex = 1;
            materialLabel52.Text = "* Required syntax of proxy: ip:port:username:password";
            // 
            // materialLabel53
            // 
            materialLabel53.Depth = 0;
            materialLabel53.Dock = DockStyle.Top;
            materialLabel53.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel53.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            materialLabel53.Location = new Point(0, 0);
            materialLabel53.Margin = new Padding(0);
            materialLabel53.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel53.Name = "materialLabel53";
            materialLabel53.Size = new Size(851, 35);
            materialLabel53.TabIndex = 0;
            materialLabel53.Text = "Path File...";
            materialLabel53.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // materialCard20
            // 
            materialCard20.BackColor = Color.FromArgb(255, 255, 255);
            materialCard20.Controls.Add(tableLayoutPanel31);
            materialCard20.Depth = 0;
            materialCard20.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard20.Location = new Point(14, 321);
            materialCard20.Margin = new Padding(14);
            materialCard20.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard20.Name = "materialCard20";
            materialCard20.Padding = new Padding(14);
            materialCard20.Size = new Size(879, 298);
            materialCard20.TabIndex = 2;
            // 
            // tableLayoutPanel31
            // 
            tableLayoutPanel31.ColumnCount = 1;
            tableLayoutPanel31.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel31.Controls.Add(panel19, 0, 0);
            tableLayoutPanel31.Controls.Add(panel20, 0, 1);
            tableLayoutPanel31.Dock = DockStyle.Fill;
            tableLayoutPanel31.Location = new Point(14, 14);
            tableLayoutPanel31.Margin = new Padding(0);
            tableLayoutPanel31.Name = "tableLayoutPanel31";
            tableLayoutPanel31.RowCount = 2;
            tableLayoutPanel31.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel31.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel31.Size = new Size(851, 270);
            tableLayoutPanel31.TabIndex = 0;
            // 
            // panel19
            // 
            panel19.Controls.Add(materialLabel54);
            panel19.Controls.Add(materialButton8);
            panel19.Dock = DockStyle.Fill;
            panel19.Location = new Point(0, 0);
            panel19.Margin = new Padding(0);
            panel19.Name = "panel19";
            panel19.Size = new Size(851, 40);
            panel19.TabIndex = 1;
            // 
            // materialLabel54
            // 
            materialLabel54.Depth = 0;
            materialLabel54.Dock = DockStyle.Left;
            materialLabel54.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel54.Location = new Point(0, 0);
            materialLabel54.Margin = new Padding(12, 0, 12, 0);
            materialLabel54.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel54.Name = "materialLabel54";
            materialLabel54.Padding = new Padding(12);
            materialLabel54.Size = new Size(92, 40);
            materialLabel54.TabIndex = 0;
            materialLabel54.Text = "File Name";
            materialLabel54.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialButton8
            // 
            materialButton8.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton8.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton8.Depth = 0;
            materialButton8.Dock = DockStyle.Right;
            materialButton8.HighEmphasis = true;
            materialButton8.Icon = null;
            materialButton8.Location = new Point(745, 0);
            materialButton8.Margin = new Padding(4, 6, 4, 6);
            materialButton8.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton8.Name = "materialButton8";
            materialButton8.NoAccentTextColor = Color.Empty;
            materialButton8.Size = new Size(106, 40);
            materialButton8.TabIndex = 2;
            materialButton8.Text = "Select File";
            materialButton8.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton8.UseAccentColor = false;
            materialButton8.UseVisualStyleBackColor = true;
            materialButton8.Click += SelectFileName_Click;
            // 
            // panel20
            // 
            panel20.Controls.Add(materialLabel55);
            panel20.Controls.Add(materialLabel56);
            panel20.Controls.Add(materialLabel57);
            panel20.Controls.Add(materialLabel58);
            panel20.Dock = DockStyle.Fill;
            panel20.Location = new Point(0, 40);
            panel20.Margin = new Padding(0);
            panel20.Name = "panel20";
            panel20.Size = new Size(851, 230);
            panel20.TabIndex = 2;
            // 
            // materialLabel55
            // 
            materialLabel55.BackColor = Color.White;
            materialLabel55.Depth = 0;
            materialLabel55.Dock = DockStyle.Top;
            materialLabel55.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel55.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel55.ForeColor = Color.LightCoral;
            materialLabel55.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel55.Location = new Point(0, 69);
            materialLabel55.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel55.Name = "materialLabel55";
            materialLabel55.Size = new Size(851, 156);
            materialLabel55.TabIndex = 3;
            // 
            // materialLabel56
            // 
            materialLabel56.BackColor = Color.White;
            materialLabel56.Depth = 0;
            materialLabel56.Dock = DockStyle.Top;
            materialLabel56.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel56.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel56.ForeColor = Color.LightCoral;
            materialLabel56.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel56.Location = new Point(0, 52);
            materialLabel56.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel56.Name = "materialLabel56";
            materialLabel56.Size = new Size(851, 17);
            materialLabel56.TabIndex = 2;
            materialLabel56.Text = "** File .json";
            // 
            // materialLabel57
            // 
            materialLabel57.BackColor = Color.White;
            materialLabel57.Depth = 0;
            materialLabel57.Dock = DockStyle.Top;
            materialLabel57.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel57.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel57.ForeColor = Color.LightCoral;
            materialLabel57.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel57.Location = new Point(0, 35);
            materialLabel57.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel57.Name = "materialLabel57";
            materialLabel57.Size = new Size(851, 17);
            materialLabel57.TabIndex = 1;
            materialLabel57.Text = "* Default data name Vietnamese";
            // 
            // materialLabel58
            // 
            materialLabel58.Depth = 0;
            materialLabel58.Dock = DockStyle.Top;
            materialLabel58.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel58.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            materialLabel58.Location = new Point(0, 0);
            materialLabel58.Margin = new Padding(0);
            materialLabel58.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel58.Name = "materialLabel58";
            materialLabel58.Size = new Size(851, 35);
            materialLabel58.TabIndex = 0;
            materialLabel58.Text = "Path File...";
            materialLabel58.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // materialTabControl2
            // 
            materialTabControl2.Controls.Add(tabPage1);
            materialTabControl2.Depth = 0;
            materialTabControl2.Location = new Point(0, 0);
            materialTabControl2.MouseState = MaterialSkin.MouseState.HOVER;
            materialTabControl2.Multiline = true;
            materialTabControl2.Name = "materialTabControl2";
            materialTabControl2.SelectedIndex = 0;
            materialTabControl2.Size = new Size(200, 100);
            materialTabControl2.TabIndex = 0;
            // 
            // materialExpansionPanel2
            // 
            materialExpansionPanel2.BackColor = Color.FromArgb(255, 255, 255);
            materialExpansionPanel2.CancelButtonText = "";
            materialExpansionPanel2.Controls.Add(tableLayoutPanel44);
            materialExpansionPanel2.Depth = 0;
            materialExpansionPanel2.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialExpansionPanel2.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialExpansionPanel2.Location = new Point(0, 0);
            materialExpansionPanel2.Margin = new Padding(3, 16, 3, 16);
            materialExpansionPanel2.MouseState = MaterialSkin.MouseState.HOVER;
            materialExpansionPanel2.Name = "materialExpansionPanel2";
            materialExpansionPanel2.Padding = new Padding(24, 64, 24, 16);
            materialExpansionPanel2.Size = new Size(480, 240);
            materialExpansionPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel44
            // 
            tableLayoutPanel44.ColumnCount = 2;
            tableLayoutPanel44.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel44.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel44.Controls.Add(tableLayoutPanel45, 0, 1);
            tableLayoutPanel44.Controls.Add(tableLayoutPanel46, 1, 0);
            tableLayoutPanel44.Controls.Add(tableLayoutPanel47, 1, 1);
            tableLayoutPanel44.Location = new Point(0, 0);
            tableLayoutPanel44.Name = "tableLayoutPanel44";
            tableLayoutPanel44.RowCount = 2;
            tableLayoutPanel44.Size = new Size(200, 100);
            tableLayoutPanel44.TabIndex = 0;
            // 
            // tableLayoutPanel45
            // 
            tableLayoutPanel45.ColumnCount = 2;
            tableLayoutPanel45.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanel45.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            tableLayoutPanel45.Controls.Add(label5, 0, 0);
            tableLayoutPanel45.Controls.Add(materialTextBox21, 1, 0);
            tableLayoutPanel45.Dock = DockStyle.Fill;
            tableLayoutPanel45.Location = new Point(3, 57);
            tableLayoutPanel45.Name = "tableLayoutPanel45";
            tableLayoutPanel45.RowCount = 1;
            tableLayoutPanel45.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel45.Size = new Size(94, 48);
            tableLayoutPanel45.TabIndex = 1;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Dock = DockStyle.Fill;
            label5.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label5.Location = new Point(10, 10);
            label5.Margin = new Padding(10);
            label5.Name = "label5";
            label5.Size = new Size(1, 28);
            label5.TabIndex = 0;
            label5.Text = "Channel";
            label5.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialTextBox21
            // 
            materialTextBox21.AnimateReadOnly = false;
            materialTextBox21.BackgroundImageLayout = ImageLayout.None;
            materialTextBox21.CharacterCasing = CharacterCasing.Normal;
            materialTextBox21.Depth = 0;
            materialTextBox21.Dock = DockStyle.Fill;
            materialTextBox21.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialTextBox21.HideSelection = true;
            materialTextBox21.Hint = "vnm, vt, ..";
            materialTextBox21.LeadingIcon = null;
            materialTextBox21.Location = new Point(28, 10);
            materialTextBox21.Margin = new Padding(10);
            materialTextBox21.MaxLength = 32767;
            materialTextBox21.MouseState = MaterialSkin.MouseState.OUT;
            materialTextBox21.Name = "materialTextBox21";
            materialTextBox21.PasswordChar = '\0';
            materialTextBox21.PrefixSuffixText = null;
            materialTextBox21.ReadOnly = false;
            materialTextBox21.RightToLeft = RightToLeft.No;
            materialTextBox21.SelectedText = "";
            materialTextBox21.SelectionLength = 0;
            materialTextBox21.SelectionStart = 0;
            materialTextBox21.ShortcutsEnabled = true;
            materialTextBox21.Size = new Size(56, 48);
            materialTextBox21.TabIndex = 1;
            materialTextBox21.TabStop = false;
            materialTextBox21.Text = "chung_duck";
            materialTextBox21.TextAlign = HorizontalAlignment.Left;
            materialTextBox21.TrailingIcon = null;
            materialTextBox21.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel46
            // 
            tableLayoutPanel46.ColumnCount = 2;
            tableLayoutPanel46.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanel46.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            tableLayoutPanel46.Controls.Add(label6, 0, 0);
            tableLayoutPanel46.Controls.Add(materialTextBox22, 1, 0);
            tableLayoutPanel46.Dock = DockStyle.Fill;
            tableLayoutPanel46.Location = new Point(103, 3);
            tableLayoutPanel46.Name = "tableLayoutPanel46";
            tableLayoutPanel46.RowCount = 1;
            tableLayoutPanel46.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel46.Size = new Size(94, 48);
            tableLayoutPanel46.TabIndex = 2;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Dock = DockStyle.Fill;
            label6.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label6.Location = new Point(10, 10);
            label6.Margin = new Padding(10);
            label6.Name = "label6";
            label6.Size = new Size(1, 28);
            label6.TabIndex = 0;
            label6.Text = "Brand";
            label6.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialTextBox22
            // 
            materialTextBox22.AnimateReadOnly = false;
            materialTextBox22.BackgroundImageLayout = ImageLayout.None;
            materialTextBox22.CharacterCasing = CharacterCasing.Normal;
            materialTextBox22.Depth = 0;
            materialTextBox22.Dock = DockStyle.Fill;
            materialTextBox22.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialTextBox22.HideSelection = true;
            materialTextBox22.Hint = "facebook,..";
            materialTextBox22.LeadingIcon = null;
            materialTextBox22.Location = new Point(28, 10);
            materialTextBox22.Margin = new Padding(10);
            materialTextBox22.MaxLength = 32767;
            materialTextBox22.MouseState = MaterialSkin.MouseState.OUT;
            materialTextBox22.Name = "materialTextBox22";
            materialTextBox22.PasswordChar = '\0';
            materialTextBox22.PrefixSuffixText = null;
            materialTextBox22.ReadOnly = false;
            materialTextBox22.RightToLeft = RightToLeft.No;
            materialTextBox22.SelectedText = "";
            materialTextBox22.SelectionLength = 0;
            materialTextBox22.SelectionStart = 0;
            materialTextBox22.ShortcutsEnabled = true;
            materialTextBox22.Size = new Size(56, 48);
            materialTextBox22.TabIndex = 1;
            materialTextBox22.TabStop = false;
            materialTextBox22.TextAlign = HorizontalAlignment.Left;
            materialTextBox22.TrailingIcon = null;
            materialTextBox22.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel47
            // 
            tableLayoutPanel47.ColumnCount = 2;
            tableLayoutPanel47.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanel47.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            tableLayoutPanel47.Controls.Add(label7, 0, 0);
            tableLayoutPanel47.Controls.Add(materialTextBox1, 1, 0);
            tableLayoutPanel47.Dock = DockStyle.Fill;
            tableLayoutPanel47.Location = new Point(103, 57);
            tableLayoutPanel47.Name = "tableLayoutPanel47";
            tableLayoutPanel47.RowCount = 1;
            tableLayoutPanel47.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel47.Size = new Size(94, 48);
            tableLayoutPanel47.TabIndex = 3;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Dock = DockStyle.Fill;
            label7.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label7.Location = new Point(10, 10);
            label7.Margin = new Padding(10);
            label7.Name = "label7";
            label7.Size = new Size(1, 28);
            label7.TabIndex = 0;
            label7.Text = "Table Saving";
            label7.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialTextBox1
            // 
            materialTextBox1.AnimateReadOnly = false;
            materialTextBox1.BorderStyle = BorderStyle.None;
            materialTextBox1.Depth = 0;
            materialTextBox1.Dock = DockStyle.Fill;
            materialTextBox1.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialTextBox1.Hint = "database table";
            materialTextBox1.LeadingIcon = null;
            materialTextBox1.Location = new Point(28, 10);
            materialTextBox1.Margin = new Padding(10);
            materialTextBox1.MaxLength = 50;
            materialTextBox1.MouseState = MaterialSkin.MouseState.OUT;
            materialTextBox1.Multiline = false;
            materialTextBox1.Name = "materialTextBox1";
            materialTextBox1.Size = new Size(56, 50);
            materialTextBox1.TabIndex = 1;
            materialTextBox1.Text = "";
            materialTextBox1.TrailingIcon = null;
            // 
            // tableLayoutPanel48
            // 
            tableLayoutPanel48.ColumnCount = 2;
            tableLayoutPanel48.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanel48.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            tableLayoutPanel48.Controls.Add(label8, 0, 0);
            tableLayoutPanel48.Dock = DockStyle.Fill;
            tableLayoutPanel48.Location = new Point(0, 0);
            tableLayoutPanel48.Name = "tableLayoutPanel48";
            tableLayoutPanel48.RowCount = 1;
            tableLayoutPanel48.Size = new Size(200, 100);
            tableLayoutPanel48.TabIndex = 0;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Dock = DockStyle.Fill;
            label8.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label8.Location = new Point(10, 10);
            label8.Margin = new Padding(10);
            label8.Name = "label8";
            label8.Size = new Size(20, 80);
            label8.TabIndex = 0;
            label8.Text = "API";
            label8.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialTextBox23
            // 
            materialTextBox23.AnimateReadOnly = false;
            materialTextBox23.BackgroundImageLayout = ImageLayout.None;
            materialTextBox23.CharacterCasing = CharacterCasing.Normal;
            materialTextBox23.Cursor = Cursors.IBeam;
            materialTextBox23.Depth = 0;
            materialTextBox23.Dock = DockStyle.Fill;
            materialTextBox23.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialTextBox23.HideSelection = false;
            materialTextBox23.Hint = "abc/xzy";
            materialTextBox23.LeadingIcon = null;
            materialTextBox23.Location = new Point(50, 10);
            materialTextBox23.Margin = new Padding(10);
            materialTextBox23.MaxLength = 32767;
            materialTextBox23.MouseState = MaterialSkin.MouseState.OUT;
            materialTextBox23.Name = "materialTextBox23";
            materialTextBox23.Padding = new Padding(1);
            materialTextBox23.PasswordChar = '\0';
            materialTextBox23.PrefixSuffixText = null;
            materialTextBox23.ReadOnly = false;
            materialTextBox23.RightToLeft = RightToLeft.No;
            materialTextBox23.SelectedText = "";
            materialTextBox23.SelectionLength = 0;
            materialTextBox23.SelectionStart = 0;
            materialTextBox23.ShortcutsEnabled = true;
            materialTextBox23.Size = new Size(140, 48);
            materialTextBox23.TabIndex = 1;
            materialTextBox23.TabStop = false;
            materialTextBox23.Text = "http://cbp.hta2p.xyz";
            materialTextBox23.TextAlign = HorizontalAlignment.Left;
            materialTextBox23.TrailingIcon = null;
            materialTextBox23.UseSystemPasswordChar = false;
            // 
            // materialLabel65
            // 
            materialLabel65.AutoSize = true;
            materialLabel65.Depth = 0;
            materialLabel65.Dock = DockStyle.Fill;
            materialLabel65.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel65.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel65.Location = new Point(73, 160);
            materialLabel65.Margin = new Padding(3, 10, 3, 10);
            materialLabel65.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel65.Name = "materialLabel65";
            materialLabel65.Size = new Size(168, 30);
            materialLabel65.TabIndex = 7;
            materialLabel65.Text = "...";
            // 
            // materialLabel66
            // 
            materialLabel66.Depth = 0;
            materialLabel66.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel66.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel66.Location = new Point(3, 160);
            materialLabel66.Margin = new Padding(3, 10, 3, 10);
            materialLabel66.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel66.Name = "materialLabel66";
            materialLabel66.Size = new Size(60, 30);
            materialLabel66.TabIndex = 6;
            materialLabel66.Text = "Screen Resolution";
            // 
            // materialLabel67
            // 
            materialLabel67.AutoSize = true;
            materialLabel67.Depth = 0;
            materialLabel67.Dock = DockStyle.Fill;
            materialLabel67.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel67.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel67.Location = new Point(73, 110);
            materialLabel67.Margin = new Padding(3, 10, 3, 10);
            materialLabel67.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel67.Name = "materialLabel67";
            materialLabel67.Size = new Size(168, 30);
            materialLabel67.TabIndex = 5;
            materialLabel67.Text = "...";
            // 
            // materialLabel68
            // 
            materialLabel68.AutoSize = true;
            materialLabel68.Depth = 0;
            materialLabel68.Dock = DockStyle.Fill;
            materialLabel68.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel68.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel68.Location = new Point(73, 60);
            materialLabel68.Margin = new Padding(3, 10, 3, 10);
            materialLabel68.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel68.Name = "materialLabel68";
            materialLabel68.Size = new Size(168, 30);
            materialLabel68.TabIndex = 3;
            materialLabel68.Text = "...";
            // 
            // tableLayoutPanel50
            // 
            tableLayoutPanel50.ColumnCount = 2;
            tableLayoutPanel50.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanel50.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel50.Controls.Add(materialLabel65, 1, 3);
            tableLayoutPanel50.Controls.Add(materialLabel66, 0, 3);
            tableLayoutPanel50.Controls.Add(materialLabel67, 1, 2);
            tableLayoutPanel50.Controls.Add(materialLabel69, 0, 2);
            tableLayoutPanel50.Controls.Add(materialLabel68, 1, 1);
            tableLayoutPanel50.Controls.Add(materialLabel70, 0, 1);
            tableLayoutPanel50.Controls.Add(materialLabel71, 1, 0);
            tableLayoutPanel50.Controls.Add(materialLabel72, 0, 0);
            tableLayoutPanel50.Controls.Add(materialLabel73, 0, 4);
            tableLayoutPanel50.Controls.Add(materialLabel74, 1, 4);
            tableLayoutPanel50.Controls.Add(materialLabel75, 0, 5);
            tableLayoutPanel50.Controls.Add(materialLabel76, 1, 5);
            tableLayoutPanel50.Dock = DockStyle.Top;
            tableLayoutPanel50.Location = new Point(0, 0);
            tableLayoutPanel50.Name = "tableLayoutPanel50";
            tableLayoutPanel50.RowCount = 7;
            tableLayoutPanel50.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel50.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel50.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel50.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel50.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel50.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel50.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel50.Size = new Size(244, 352);
            tableLayoutPanel50.TabIndex = 0;
            // 
            // materialLabel69
            // 
            materialLabel69.AutoSize = true;
            materialLabel69.Depth = 0;
            materialLabel69.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel69.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel69.Location = new Point(3, 110);
            materialLabel69.Margin = new Padding(3, 10, 3, 10);
            materialLabel69.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel69.Name = "materialLabel69";
            materialLabel69.Size = new Size(32, 14);
            materialLabel69.TabIndex = 4;
            materialLabel69.Text = "Proxy";
            // 
            // materialLabel70
            // 
            materialLabel70.AutoSize = true;
            materialLabel70.Depth = 0;
            materialLabel70.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel70.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel70.Location = new Point(3, 60);
            materialLabel70.Margin = new Padding(3, 10, 3, 10);
            materialLabel70.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel70.Name = "materialLabel70";
            materialLabel70.Size = new Size(60, 14);
            materialLabel70.TabIndex = 2;
            materialLabel70.Text = "User Agent";
            // 
            // materialLabel71
            // 
            materialLabel71.Depth = 0;
            materialLabel71.Dock = DockStyle.Fill;
            materialLabel71.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel71.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel71.Location = new Point(73, 10);
            materialLabel71.Margin = new Padding(3, 10, 3, 10);
            materialLabel71.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel71.Name = "materialLabel71";
            materialLabel71.Size = new Size(168, 30);
            materialLabel71.TabIndex = 1;
            materialLabel71.Text = "...";
            // 
            // materialLabel72
            // 
            materialLabel72.AutoSize = true;
            materialLabel72.Depth = 0;
            materialLabel72.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel72.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel72.Location = new Point(3, 10);
            materialLabel72.Margin = new Padding(3, 10, 3, 10);
            materialLabel72.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel72.Name = "materialLabel72";
            materialLabel72.Size = new Size(50, 14);
            materialLabel72.TabIndex = 0;
            materialLabel72.Text = "Profile ID";
            // 
            // materialLabel73
            // 
            materialLabel73.AutoSize = true;
            materialLabel73.Depth = 0;
            materialLabel73.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel73.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel73.Location = new Point(3, 210);
            materialLabel73.Margin = new Padding(3, 10, 3, 10);
            materialLabel73.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel73.Name = "materialLabel73";
            materialLabel73.Size = new Size(54, 14);
            materialLabel73.TabIndex = 8;
            materialLabel73.Text = "Facebook";
            // 
            // materialLabel74
            // 
            materialLabel74.AutoSize = true;
            materialLabel74.Depth = 0;
            materialLabel74.Dock = DockStyle.Fill;
            materialLabel74.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel74.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel74.Location = new Point(73, 210);
            materialLabel74.Margin = new Padding(3, 10, 3, 10);
            materialLabel74.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel74.Name = "materialLabel74";
            materialLabel74.Size = new Size(168, 30);
            materialLabel74.TabIndex = 9;
            materialLabel74.Text = "...";
            // 
            // materialLabel75
            // 
            materialLabel75.AutoSize = true;
            materialLabel75.Depth = 0;
            materialLabel75.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel75.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel75.Location = new Point(3, 260);
            materialLabel75.Margin = new Padding(3, 10, 3, 10);
            materialLabel75.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel75.Name = "materialLabel75";
            materialLabel75.Size = new Size(39, 14);
            materialLabel75.TabIndex = 10;
            materialLabel75.Text = "Google";
            // 
            // materialLabel76
            // 
            materialLabel76.AutoSize = true;
            materialLabel76.Depth = 0;
            materialLabel76.Dock = DockStyle.Fill;
            materialLabel76.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel76.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel76.Location = new Point(73, 260);
            materialLabel76.Margin = new Padding(3, 10, 3, 10);
            materialLabel76.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel76.Name = "materialLabel76";
            materialLabel76.Size = new Size(168, 30);
            materialLabel76.TabIndex = 11;
            materialLabel76.Text = "...";
            // 
            // panel24
            // 
            panel24.BackgroundImageLayout = ImageLayout.Zoom;
            panel24.Location = new Point(6, 6);
            panel24.Margin = new Padding(6);
            panel24.Name = "panel24";
            panel24.Size = new Size(36, 28);
            panel24.TabIndex = 0;
            // 
            // materialLabel77
            // 
            materialLabel77.Depth = 0;
            materialLabel77.Dock = DockStyle.Fill;
            materialLabel77.Font = new Font("Roboto", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel77.FontType = MaterialSkin.MaterialSkinManager.fontType.Button;
            materialLabel77.Location = new Point(53, 5);
            materialLabel77.Margin = new Padding(5);
            materialLabel77.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel77.Name = "materialLabel77";
            materialLabel77.Size = new Size(186, 30);
            materialLabel77.TabIndex = 1;
            materialLabel77.Text = "Profile Info";
            materialLabel77.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel51
            // 
            tableLayoutPanel51.ColumnCount = 2;
            tableLayoutPanel51.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 48F));
            tableLayoutPanel51.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel51.Controls.Add(panel24, 0, 0);
            tableLayoutPanel51.Controls.Add(materialLabel77, 1, 0);
            tableLayoutPanel51.Dock = DockStyle.Fill;
            tableLayoutPanel51.Location = new Point(0, 0);
            tableLayoutPanel51.Margin = new Padding(0);
            tableLayoutPanel51.Name = "tableLayoutPanel51";
            tableLayoutPanel51.RowCount = 1;
            tableLayoutPanel51.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel51.Size = new Size(244, 40);
            tableLayoutPanel51.TabIndex = 0;
            // 
            // materialCard24
            // 
            materialCard24.BackColor = Color.FromArgb(255, 255, 255);
            materialCard24.Controls.Add(tableLayoutPanel52);
            materialCard24.Depth = 0;
            materialCard24.Dock = DockStyle.Fill;
            materialCard24.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard24.Location = new Point(1426, 2);
            materialCard24.Margin = new Padding(14, 2, 14, 14);
            materialCard24.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard24.Name = "materialCard24";
            materialCard24.Padding = new Padding(14);
            materialCard24.Size = new Size(272, 987);
            materialCard24.TabIndex = 2;
            // 
            // tableLayoutPanel52
            // 
            tableLayoutPanel52.ColumnCount = 1;
            tableLayoutPanel52.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel52.Controls.Add(tableLayoutPanel51, 0, 0);
            tableLayoutPanel52.Controls.Add(panel25, 0, 1);
            tableLayoutPanel52.Dock = DockStyle.Fill;
            tableLayoutPanel52.Location = new Point(14, 14);
            tableLayoutPanel52.Margin = new Padding(0);
            tableLayoutPanel52.Name = "tableLayoutPanel52";
            tableLayoutPanel52.RowCount = 2;
            tableLayoutPanel52.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel52.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel52.Size = new Size(244, 959);
            tableLayoutPanel52.TabIndex = 0;
            // 
            // panel25
            // 
            panel25.AutoScroll = true;
            panel25.Controls.Add(tableLayoutPanel50);
            panel25.Dock = DockStyle.Fill;
            panel25.Location = new Point(0, 52);
            panel25.Margin = new Padding(0, 12, 0, 0);
            panel25.Name = "panel25";
            panel25.Size = new Size(244, 907);
            panel25.TabIndex = 1;
            // 
            // tableLayoutPanel53
            // 
            tableLayoutPanel53.ColumnCount = 2;
            tableLayoutPanel53.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel53.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 300F));
            tableLayoutPanel53.Controls.Add(materialCard24, 1, 0);
            tableLayoutPanel53.Controls.Add(materialCard27, 0, 0);
            tableLayoutPanel53.Dock = DockStyle.Fill;
            tableLayoutPanel53.Location = new Point(3, 64);
            tableLayoutPanel53.Margin = new Padding(0);
            tableLayoutPanel53.Name = "tableLayoutPanel53";
            tableLayoutPanel53.RowCount = 1;
            tableLayoutPanel53.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel53.Size = new Size(1712, 1003);
            tableLayoutPanel53.TabIndex = 1;
            // 
            // materialCard27
            // 
            materialCard27.BackColor = Color.FromArgb(255, 255, 255);
            materialCard27.Controls.Add(dataGridView4);
            materialCard27.Depth = 0;
            materialCard27.Dock = DockStyle.Fill;
            materialCard27.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard27.Location = new Point(14, 2);
            materialCard27.Margin = new Padding(14, 2, 2, 14);
            materialCard27.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard27.Name = "materialCard27";
            materialCard27.Padding = new Padding(14);
            materialCard27.Size = new Size(1396, 987);
            materialCard27.TabIndex = 1;
            // 
            // dataGridView4
            // 
            dataGridView4.AllowUserToAddRows = false;
            dataGridView4.AllowUserToDeleteRows = false;
            dataGridView4.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView4.BackgroundColor = SystemColors.ButtonHighlight;
            dataGridView4.BorderStyle = BorderStyle.None;
            dataGridView4.ColumnHeadersHeight = 40;
            dataGridView4.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridView4.Columns.AddRange(new DataGridViewColumn[] { dataGridViewTextBoxColumn11, dataGridViewTextBoxColumn12, dataGridViewTextBoxColumn13, dataGridViewTextBoxColumn14 });
            dataGridView4.Cursor = Cursors.Hand;
            dataGridView4.Dock = DockStyle.Fill;
            dataGridView4.Location = new Point(14, 14);
            dataGridView4.Name = "dataGridView4";
            dataGridView4.RowHeadersVisible = false;
            dataGridView4.RowHeadersWidth = 50;
            dataGridView4.RowTemplate.Height = 42;
            dataGridView4.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView4.Size = new Size(1368, 959);
            dataGridView4.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewTextBoxColumn11.DataPropertyName = "profile_id";
            dataGridViewTextBoxColumn11.FillWeight = 109.817F;
            dataGridViewTextBoxColumn11.HeaderText = "ID";
            dataGridViewTextBoxColumn11.MinimumWidth = 6;
            dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            dataGridViewTextBoxColumn12.DataPropertyName = "profile_status";
            dataGridViewTextBoxColumn12.FillWeight = 42.66524F;
            dataGridViewTextBoxColumn12.HeaderText = "Status";
            dataGridViewTextBoxColumn12.MinimumWidth = 6;
            dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            dataGridViewTextBoxColumn13.DataPropertyName = "profile_proxy";
            dataGridViewTextBoxColumn13.FillWeight = 78.28705F;
            dataGridViewTextBoxColumn13.HeaderText = "Proxy";
            dataGridViewTextBoxColumn13.MinimumWidth = 6;
            dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            dataGridViewTextBoxColumn14.DataPropertyName = "profile_last_update";
            dataGridViewTextBoxColumn14.FillWeight = 78.28705F;
            dataGridViewTextBoxColumn14.HeaderText = "Last update";
            dataGridViewTextBoxColumn14.MinimumWidth = 6;
            dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // materialButton9
            // 
            materialButton9.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton9.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton9.Depth = 0;
            materialButton9.HighEmphasis = true;
            materialButton9.Icon = null;
            materialButton9.Location = new Point(1422, 0);
            materialButton9.Margin = new Padding(0);
            materialButton9.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton9.Name = "materialButton9";
            materialButton9.NoAccentTextColor = Color.Empty;
            materialButton9.Size = new Size(146, 1);
            materialButton9.TabIndex = 0;
            materialButton9.Text = "Create Profiles";
            materialButton9.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton9.UseAccentColor = false;
            materialButton9.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel54
            // 
            tableLayoutPanel54.ColumnCount = 2;
            tableLayoutPanel54.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 85F));
            tableLayoutPanel54.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 15F));
            tableLayoutPanel54.Controls.Add(materialButton9, 1, 0);
            tableLayoutPanel54.Dock = DockStyle.Fill;
            tableLayoutPanel54.Location = new Point(5, 5);
            tableLayoutPanel54.Margin = new Padding(0);
            tableLayoutPanel54.Name = "tableLayoutPanel54";
            tableLayoutPanel54.RowCount = 1;
            tableLayoutPanel54.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel54.Size = new Size(1674, 0);
            tableLayoutPanel54.TabIndex = 0;
            // 
            // materialCard28
            // 
            materialCard28.BackColor = Color.FromArgb(255, 255, 255);
            materialCard28.Controls.Add(tableLayoutPanel54);
            materialCard28.Depth = 0;
            materialCard28.Dock = DockStyle.Fill;
            materialCard28.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard28.Location = new Point(14, 14);
            materialCard28.Margin = new Padding(14);
            materialCard28.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard28.Name = "materialCard28";
            materialCard28.Padding = new Padding(5);
            materialCard28.Size = new Size(1684, 1);
            materialCard28.TabIndex = 0;
            // 
            // tableLayoutPanel55
            // 
            tableLayoutPanel55.ColumnCount = 1;
            tableLayoutPanel55.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel55.Controls.Add(materialCard28, 0, 0);
            tableLayoutPanel55.Dock = DockStyle.Fill;
            tableLayoutPanel55.Location = new Point(3, 64);
            tableLayoutPanel55.Name = "tableLayoutPanel55";
            tableLayoutPanel55.RowCount = 2;
            tableLayoutPanel55.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel55.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel55.Size = new Size(1712, 1003);
            tableLayoutPanel55.TabIndex = 0;
            // 
            // panel26
            // 
            panel26.Controls.Add(materialLabel78);
            panel26.Controls.Add(materialLabel79);
            panel26.Controls.Add(materialLabel80);
            panel26.Dock = DockStyle.Fill;
            panel26.Location = new Point(3, 64);
            panel26.Margin = new Padding(0);
            panel26.Name = "panel26";
            panel26.Size = new Size(1712, 1003);
            panel26.TabIndex = 2;
            // 
            // materialLabel78
            // 
            materialLabel78.BackColor = Color.White;
            materialLabel78.Depth = 0;
            materialLabel78.Dock = DockStyle.Top;
            materialLabel78.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel78.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel78.ForeColor = Color.LightCoral;
            materialLabel78.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel78.Location = new Point(0, 52);
            materialLabel78.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel78.Name = "materialLabel78";
            materialLabel78.Size = new Size(1712, 17);
            materialLabel78.TabIndex = 2;
            materialLabel78.Text = "** File .txt";
            // 
            // materialLabel79
            // 
            materialLabel79.BackColor = Color.White;
            materialLabel79.Depth = 0;
            materialLabel79.Dock = DockStyle.Top;
            materialLabel79.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel79.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel79.ForeColor = Color.LightCoral;
            materialLabel79.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel79.Location = new Point(0, 35);
            materialLabel79.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel79.Name = "materialLabel79";
            materialLabel79.Size = new Size(1712, 17);
            materialLabel79.TabIndex = 1;
            materialLabel79.Text = "* Required syntax of proxy: ip:port:username:password";
            // 
            // materialLabel80
            // 
            materialLabel80.Depth = 0;
            materialLabel80.Dock = DockStyle.Top;
            materialLabel80.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel80.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            materialLabel80.Location = new Point(0, 0);
            materialLabel80.Margin = new Padding(0);
            materialLabel80.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel80.Name = "materialLabel80";
            materialLabel80.Size = new Size(1712, 35);
            materialLabel80.TabIndex = 0;
            materialLabel80.Text = "Path File...";
            materialLabel80.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // materialCard29
            // 
            materialCard29.BackColor = Color.FromArgb(255, 255, 255);
            materialCard29.Controls.Add(tableLayoutPanel56);
            materialCard29.Depth = 0;
            materialCard29.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard29.Location = new Point(14, 160);
            materialCard29.Margin = new Padding(14);
            materialCard29.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard29.Name = "materialCard29";
            materialCard29.Padding = new Padding(14);
            materialCard29.Size = new Size(879, 133);
            materialCard29.TabIndex = 1;
            // 
            // tableLayoutPanel56
            // 
            tableLayoutPanel56.ColumnCount = 1;
            tableLayoutPanel56.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel56.Controls.Add(panel27, 0, 0);
            tableLayoutPanel56.Controls.Add(panel28, 0, 1);
            tableLayoutPanel56.Dock = DockStyle.Fill;
            tableLayoutPanel56.Location = new Point(14, 14);
            tableLayoutPanel56.Margin = new Padding(0);
            tableLayoutPanel56.Name = "tableLayoutPanel56";
            tableLayoutPanel56.RowCount = 2;
            tableLayoutPanel56.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel56.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel56.Size = new Size(851, 105);
            tableLayoutPanel56.TabIndex = 0;
            // 
            // panel27
            // 
            panel27.Controls.Add(materialLabel81);
            panel27.Controls.Add(materialButton10);
            panel27.Dock = DockStyle.Fill;
            panel27.Location = new Point(0, 0);
            panel27.Margin = new Padding(0);
            panel27.Name = "panel27";
            panel27.Size = new Size(851, 40);
            panel27.TabIndex = 1;
            // 
            // materialLabel81
            // 
            materialLabel81.Depth = 0;
            materialLabel81.Dock = DockStyle.Left;
            materialLabel81.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel81.Location = new Point(0, 0);
            materialLabel81.Margin = new Padding(12, 0, 12, 0);
            materialLabel81.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel81.Name = "materialLabel81";
            materialLabel81.Padding = new Padding(12);
            materialLabel81.Size = new Size(92, 40);
            materialLabel81.TabIndex = 0;
            materialLabel81.Text = "File Proxies";
            materialLabel81.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialButton10
            // 
            materialButton10.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton10.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton10.Depth = 0;
            materialButton10.Dock = DockStyle.Right;
            materialButton10.HighEmphasis = true;
            materialButton10.Icon = null;
            materialButton10.Location = new Point(745, 0);
            materialButton10.Margin = new Padding(4, 6, 4, 6);
            materialButton10.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton10.Name = "materialButton10";
            materialButton10.NoAccentTextColor = Color.Empty;
            materialButton10.Size = new Size(106, 40);
            materialButton10.TabIndex = 2;
            materialButton10.Text = "Select File";
            materialButton10.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton10.UseAccentColor = false;
            materialButton10.UseVisualStyleBackColor = true;
            // 
            // panel28
            // 
            panel28.Controls.Add(materialLabel82);
            panel28.Controls.Add(materialLabel83);
            panel28.Controls.Add(materialLabel84);
            panel28.Dock = DockStyle.Fill;
            panel28.Location = new Point(0, 40);
            panel28.Margin = new Padding(0);
            panel28.Name = "panel28";
            panel28.Size = new Size(851, 65);
            panel28.TabIndex = 2;
            // 
            // materialLabel82
            // 
            materialLabel82.BackColor = Color.White;
            materialLabel82.Depth = 0;
            materialLabel82.Dock = DockStyle.Top;
            materialLabel82.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel82.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel82.ForeColor = Color.LightCoral;
            materialLabel82.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel82.Location = new Point(0, 52);
            materialLabel82.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel82.Name = "materialLabel82";
            materialLabel82.Size = new Size(851, 17);
            materialLabel82.TabIndex = 2;
            materialLabel82.Text = "** File .txt";
            // 
            // materialLabel83
            // 
            materialLabel83.BackColor = Color.White;
            materialLabel83.Depth = 0;
            materialLabel83.Dock = DockStyle.Top;
            materialLabel83.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel83.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel83.ForeColor = Color.LightCoral;
            materialLabel83.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel83.Location = new Point(0, 35);
            materialLabel83.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel83.Name = "materialLabel83";
            materialLabel83.Size = new Size(851, 17);
            materialLabel83.TabIndex = 1;
            materialLabel83.Text = "* Required syntax of proxy: ip:port:username:password";
            // 
            // materialLabel84
            // 
            materialLabel84.Depth = 0;
            materialLabel84.Dock = DockStyle.Top;
            materialLabel84.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel84.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            materialLabel84.Location = new Point(0, 0);
            materialLabel84.Margin = new Padding(0);
            materialLabel84.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel84.Name = "materialLabel84";
            materialLabel84.Size = new Size(851, 35);
            materialLabel84.TabIndex = 0;
            materialLabel84.Text = "Path File...";
            materialLabel84.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // materialLabel98
            // 
            materialLabel98.BackColor = Color.White;
            materialLabel98.Depth = 0;
            materialLabel98.Dock = DockStyle.Top;
            materialLabel98.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel98.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel98.ForeColor = Color.LightCoral;
            materialLabel98.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel98.Location = new Point(0, 69);
            materialLabel98.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel98.Name = "materialLabel98";
            materialLabel98.Size = new Size(851, 156);
            materialLabel98.TabIndex = 3;
            // 
            // materialLabel99
            // 
            materialLabel99.BackColor = Color.White;
            materialLabel99.Depth = 0;
            materialLabel99.Dock = DockStyle.Top;
            materialLabel99.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel99.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel99.ForeColor = Color.LightCoral;
            materialLabel99.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel99.Location = new Point(0, 52);
            materialLabel99.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel99.Name = "materialLabel99";
            materialLabel99.Size = new Size(851, 17);
            materialLabel99.TabIndex = 2;
            materialLabel99.Text = "** File .json";
            // 
            // materialLabel100
            // 
            materialLabel100.Depth = 0;
            materialLabel100.Dock = DockStyle.Top;
            materialLabel100.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel100.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            materialLabel100.Location = new Point(0, 0);
            materialLabel100.Margin = new Padding(0);
            materialLabel100.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel100.Name = "materialLabel100";
            materialLabel100.Size = new Size(851, 35);
            materialLabel100.TabIndex = 0;
            materialLabel100.Text = "Path File...";
            materialLabel100.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // panel31
            // 
            panel31.Controls.Add(materialLabel98);
            panel31.Controls.Add(materialLabel99);
            panel31.Controls.Add(materialLabel101);
            panel31.Controls.Add(materialLabel100);
            panel31.Dock = DockStyle.Fill;
            panel31.Location = new Point(0, 40);
            panel31.Margin = new Padding(0);
            panel31.Name = "panel31";
            panel31.Size = new Size(851, 107);
            panel31.TabIndex = 2;
            // 
            // materialLabel101
            // 
            materialLabel101.BackColor = Color.White;
            materialLabel101.Depth = 0;
            materialLabel101.Dock = DockStyle.Top;
            materialLabel101.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel101.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel101.ForeColor = Color.LightCoral;
            materialLabel101.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel101.Location = new Point(0, 35);
            materialLabel101.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel101.Name = "materialLabel101";
            materialLabel101.Size = new Size(851, 17);
            materialLabel101.TabIndex = 1;
            materialLabel101.Text = "* Default data name Vietnamese";
            // 
            // materialLabel102
            // 
            materialLabel102.Depth = 0;
            materialLabel102.Dock = DockStyle.Left;
            materialLabel102.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel102.Location = new Point(0, 0);
            materialLabel102.Margin = new Padding(12, 0, 12, 0);
            materialLabel102.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel102.Name = "materialLabel102";
            materialLabel102.Padding = new Padding(12);
            materialLabel102.Size = new Size(92, 40);
            materialLabel102.TabIndex = 0;
            materialLabel102.Text = "File Name";
            materialLabel102.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialButton13
            // 
            materialButton13.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton13.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton13.Depth = 0;
            materialButton13.Dock = DockStyle.Right;
            materialButton13.HighEmphasis = true;
            materialButton13.Icon = null;
            materialButton13.Location = new Point(745, 0);
            materialButton13.Margin = new Padding(4, 6, 4, 6);
            materialButton13.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton13.Name = "materialButton13";
            materialButton13.NoAccentTextColor = Color.Empty;
            materialButton13.Size = new Size(106, 40);
            materialButton13.TabIndex = 2;
            materialButton13.Text = "Select File";
            materialButton13.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton13.UseAccentColor = false;
            materialButton13.UseVisualStyleBackColor = true;
            // 
            // panel32
            // 
            panel32.Controls.Add(materialLabel102);
            panel32.Controls.Add(materialButton13);
            panel32.Dock = DockStyle.Fill;
            panel32.Location = new Point(0, 0);
            panel32.Margin = new Padding(0);
            panel32.Name = "panel32";
            panel32.Size = new Size(851, 40);
            panel32.TabIndex = 1;
            // 
            // tableLayoutPanel64
            // 
            tableLayoutPanel64.ColumnCount = 1;
            tableLayoutPanel64.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel64.Controls.Add(panel32, 0, 0);
            tableLayoutPanel64.Controls.Add(panel31, 0, 1);
            tableLayoutPanel64.Dock = DockStyle.Fill;
            tableLayoutPanel64.Location = new Point(14, 14);
            tableLayoutPanel64.Margin = new Padding(0);
            tableLayoutPanel64.Name = "tableLayoutPanel64";
            tableLayoutPanel64.RowCount = 2;
            tableLayoutPanel64.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel64.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel64.Size = new Size(851, 147);
            tableLayoutPanel64.TabIndex = 0;
            // 
            // materialCard34
            // 
            materialCard34.BackColor = Color.FromArgb(255, 255, 255);
            materialCard34.Controls.Add(tableLayoutPanel64);
            materialCard34.Depth = 0;
            materialCard34.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard34.Location = new Point(14, 321);
            materialCard34.Margin = new Padding(14);
            materialCard34.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard34.Name = "materialCard34";
            materialCard34.Padding = new Padding(14);
            materialCard34.Size = new Size(879, 175);
            materialCard34.TabIndex = 2;
            // 
            // materialTextBox24
            // 
            materialTextBox24.AnimateReadOnly = false;
            materialTextBox24.BackgroundImageLayout = ImageLayout.None;
            materialTextBox24.CharacterCasing = CharacterCasing.Normal;
            materialTextBox24.Cursor = Cursors.IBeam;
            materialTextBox24.Depth = 0;
            materialTextBox24.Dock = DockStyle.Fill;
            materialTextBox24.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialTextBox24.HideSelection = false;
            materialTextBox24.Hint = "abc/xzy";
            materialTextBox24.LeadingIcon = null;
            materialTextBox24.Location = new Point(3, 64);
            materialTextBox24.Margin = new Padding(10);
            materialTextBox24.MaxLength = 32767;
            materialTextBox24.MouseState = MaterialSkin.MouseState.OUT;
            materialTextBox24.Name = "materialTextBox24";
            materialTextBox24.Padding = new Padding(1);
            materialTextBox24.PasswordChar = '\0';
            materialTextBox24.PrefixSuffixText = null;
            materialTextBox24.ReadOnly = false;
            materialTextBox24.RightToLeft = RightToLeft.No;
            materialTextBox24.SelectedText = "";
            materialTextBox24.SelectionLength = 0;
            materialTextBox24.SelectionStart = 0;
            materialTextBox24.ShortcutsEnabled = true;
            materialTextBox24.Size = new Size(1712, 48);
            materialTextBox24.TabIndex = 1;
            materialTextBox24.TabStop = false;
            materialTextBox24.Text = "http://cbp.hta2p.xyz";
            materialTextBox24.TextAlign = HorizontalAlignment.Left;
            materialTextBox24.TrailingIcon = null;
            materialTextBox24.UseSystemPasswordChar = false;
            // 
            // materialCard35
            // 
            materialCard35.BackColor = Color.FromArgb(255, 255, 255);
            materialCard35.Controls.Add(tableLayoutPanel65);
            materialCard35.Depth = 0;
            materialCard35.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard35.Location = new Point(14, 524);
            materialCard35.Margin = new Padding(14);
            materialCard35.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard35.Name = "materialCard35";
            materialCard35.Padding = new Padding(14);
            materialCard35.Size = new Size(879, 319);
            materialCard35.TabIndex = 2;
            // 
            // tableLayoutPanel65
            // 
            tableLayoutPanel65.ColumnCount = 1;
            tableLayoutPanel65.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel65.Controls.Add(panel33, 0, 0);
            tableLayoutPanel65.Controls.Add(panel34, 0, 1);
            tableLayoutPanel65.Dock = DockStyle.Fill;
            tableLayoutPanel65.Location = new Point(14, 14);
            tableLayoutPanel65.Margin = new Padding(0);
            tableLayoutPanel65.Name = "tableLayoutPanel65";
            tableLayoutPanel65.RowCount = 2;
            tableLayoutPanel65.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel65.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel65.Size = new Size(851, 291);
            tableLayoutPanel65.TabIndex = 0;
            // 
            // panel33
            // 
            panel33.Controls.Add(materialLabel103);
            panel33.Controls.Add(materialButton14);
            panel33.Dock = DockStyle.Fill;
            panel33.Location = new Point(0, 0);
            panel33.Margin = new Padding(0);
            panel33.Name = "panel33";
            panel33.Size = new Size(851, 40);
            panel33.TabIndex = 1;
            // 
            // materialLabel103
            // 
            materialLabel103.Depth = 0;
            materialLabel103.Dock = DockStyle.Left;
            materialLabel103.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel103.Location = new Point(0, 0);
            materialLabel103.Margin = new Padding(12, 0, 12, 0);
            materialLabel103.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel103.Name = "materialLabel103";
            materialLabel103.Padding = new Padding(12);
            materialLabel103.Size = new Size(106, 40);
            materialLabel103.TabIndex = 0;
            materialLabel103.Text = "File Extention";
            materialLabel103.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialButton14
            // 
            materialButton14.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton14.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton14.Depth = 0;
            materialButton14.Dock = DockStyle.Right;
            materialButton14.HighEmphasis = true;
            materialButton14.Icon = null;
            materialButton14.Location = new Point(745, 0);
            materialButton14.Margin = new Padding(4, 6, 4, 6);
            materialButton14.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton14.Name = "materialButton14";
            materialButton14.NoAccentTextColor = Color.Empty;
            materialButton14.Size = new Size(106, 40);
            materialButton14.TabIndex = 2;
            materialButton14.Text = "Select File";
            materialButton14.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton14.UseAccentColor = false;
            materialButton14.UseVisualStyleBackColor = true;
            // 
            // panel34
            // 
            panel34.Controls.Add(panel35);
            panel34.Controls.Add(materialLabel104);
            panel34.Controls.Add(materialLabel105);
            panel34.Controls.Add(materialLabel106);
            panel34.Dock = DockStyle.Fill;
            panel34.Location = new Point(0, 40);
            panel34.Margin = new Padding(0);
            panel34.Name = "panel34";
            panel34.Size = new Size(851, 251);
            panel34.TabIndex = 2;
            // 
            // panel35
            // 
            panel35.Controls.Add(materialMultiLineTextBox21);
            panel35.Location = new Point(3, 97);
            panel35.Name = "panel35";
            panel35.Size = new Size(845, 151);
            panel35.TabIndex = 4;
            // 
            // materialMultiLineTextBox21
            // 
            materialMultiLineTextBox21.AnimateReadOnly = false;
            materialMultiLineTextBox21.BackgroundImageLayout = ImageLayout.None;
            materialMultiLineTextBox21.CharacterCasing = CharacterCasing.Normal;
            materialMultiLineTextBox21.Depth = 0;
            materialMultiLineTextBox21.HideSelection = true;
            materialMultiLineTextBox21.Location = new Point(3, 14);
            materialMultiLineTextBox21.MaxLength = 32767;
            materialMultiLineTextBox21.MouseState = MaterialSkin.MouseState.OUT;
            materialMultiLineTextBox21.Name = "materialMultiLineTextBox21";
            materialMultiLineTextBox21.PasswordChar = '\0';
            materialMultiLineTextBox21.ReadOnly = false;
            materialMultiLineTextBox21.ScrollBars = ScrollBars.Vertical;
            materialMultiLineTextBox21.SelectedText = "";
            materialMultiLineTextBox21.SelectionLength = 0;
            materialMultiLineTextBox21.SelectionStart = 0;
            materialMultiLineTextBox21.ShortcutsEnabled = true;
            materialMultiLineTextBox21.Size = new Size(829, 124);
            materialMultiLineTextBox21.TabIndex = 3;
            materialMultiLineTextBox21.TabStop = false;
            materialMultiLineTextBox21.TextAlign = HorizontalAlignment.Left;
            materialMultiLineTextBox21.UseSystemPasswordChar = false;
            // 
            // materialLabel104
            // 
            materialLabel104.BackColor = Color.White;
            materialLabel104.Depth = 0;
            materialLabel104.Dock = DockStyle.Top;
            materialLabel104.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel104.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel104.ForeColor = Color.LightCoral;
            materialLabel104.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel104.Location = new Point(0, 52);
            materialLabel104.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel104.Name = "materialLabel104";
            materialLabel104.Size = new Size(851, 17);
            materialLabel104.TabIndex = 2;
            materialLabel104.Text = "** File .txt";
            // 
            // materialLabel105
            // 
            materialLabel105.BackColor = Color.White;
            materialLabel105.Depth = 0;
            materialLabel105.Dock = DockStyle.Top;
            materialLabel105.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel105.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel105.ForeColor = Color.LightCoral;
            materialLabel105.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel105.Location = new Point(0, 35);
            materialLabel105.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel105.Name = "materialLabel105";
            materialLabel105.Size = new Size(851, 17);
            materialLabel105.TabIndex = 1;
            materialLabel105.Text = "* Required each name in one line";
            // 
            // materialLabel106
            // 
            materialLabel106.Depth = 0;
            materialLabel106.Dock = DockStyle.Top;
            materialLabel106.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel106.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            materialLabel106.Location = new Point(0, 0);
            materialLabel106.Margin = new Padding(0);
            materialLabel106.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel106.Name = "materialLabel106";
            materialLabel106.Size = new Size(851, 35);
            materialLabel106.TabIndex = 0;
            materialLabel106.Text = "Path File...";
            materialLabel106.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // materialLabel107
            // 
            materialLabel107.Depth = 0;
            materialLabel107.Dock = DockStyle.Left;
            materialLabel107.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel107.Location = new Point(0, 0);
            materialLabel107.Margin = new Padding(12, 0, 12, 0);
            materialLabel107.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel107.Name = "materialLabel107";
            materialLabel107.Padding = new Padding(12);
            materialLabel107.Size = new Size(92, 20);
            materialLabel107.TabIndex = 0;
            materialLabel107.Text = "File Proxies";
            materialLabel107.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // panel36
            // 
            panel36.Controls.Add(materialLabel107);
            panel36.Controls.Add(materialButton15);
            panel36.Dock = DockStyle.Fill;
            panel36.Location = new Point(0, 0);
            panel36.Margin = new Padding(0);
            panel36.Name = "panel36";
            panel36.Size = new Size(1712, 20);
            panel36.TabIndex = 1;
            // 
            // materialButton15
            // 
            materialButton15.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton15.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton15.Depth = 0;
            materialButton15.Dock = DockStyle.Right;
            materialButton15.HighEmphasis = true;
            materialButton15.Icon = null;
            materialButton15.Location = new Point(1606, 0);
            materialButton15.Margin = new Padding(4, 6, 4, 6);
            materialButton15.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton15.Name = "materialButton15";
            materialButton15.NoAccentTextColor = Color.Empty;
            materialButton15.Size = new Size(106, 20);
            materialButton15.TabIndex = 2;
            materialButton15.Text = "Select File";
            materialButton15.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton15.UseAccentColor = false;
            materialButton15.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel66
            // 
            tableLayoutPanel66.ColumnCount = 1;
            tableLayoutPanel66.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel66.Controls.Add(panel36, 0, 0);
            tableLayoutPanel66.Dock = DockStyle.Fill;
            tableLayoutPanel66.Location = new Point(3, 64);
            tableLayoutPanel66.Margin = new Padding(0);
            tableLayoutPanel66.Name = "tableLayoutPanel66";
            tableLayoutPanel66.RowCount = 2;
            tableLayoutPanel66.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel66.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel66.Size = new Size(1712, 1003);
            tableLayoutPanel66.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewTextBoxColumn21.HeaderText = "Proxy";
            dataGridViewTextBoxColumn21.MinimumWidth = 6;
            dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            dataGridViewTextBoxColumn21.Width = 125;
            // 
            // dataGridViewTextBoxColumn22
            // 
            dataGridViewTextBoxColumn22.HeaderText = "Status";
            dataGridViewTextBoxColumn22.MinimumWidth = 6;
            dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            dataGridViewTextBoxColumn22.Width = 125;
            // 
            // imageList1
            // 
            imageList1.ColorDepth = ColorDepth.Depth32Bit;
            imageList1.ImageStream = (ImageListStreamer)resources.GetObject("imageList1.ImageStream");
            imageList1.TransparentColor = Color.Transparent;
            imageList1.Images.SetKeyName(0, "settings-sliders.png");
            imageList1.Images.SetKeyName(1, "network-cloud.png");
            imageList1.Images.SetKeyName(2, "home.png");
            imageList1.Images.SetKeyName(3, "browser.png");
            imageList1.Images.SetKeyName(4, "user-check.png");
            imageList1.Images.SetKeyName(5, "users.png");
            imageList1.Images.SetKeyName(6, "sensor.png");
            imageList1.Images.SetKeyName(7, "sensor-on.png");
            // 
            // panel37
            // 
            panel37.Controls.Add(materialLabel108);
            panel37.Controls.Add(materialLabel109);
            panel37.Controls.Add(materialLabel110);
            panel37.Controls.Add(materialLabel111);
            panel37.Dock = DockStyle.Fill;
            panel37.Location = new Point(0, 40);
            panel37.Margin = new Padding(0);
            panel37.Name = "panel37";
            panel37.Size = new Size(851, 230);
            panel37.TabIndex = 2;
            // 
            // materialLabel108
            // 
            materialLabel108.BackColor = Color.White;
            materialLabel108.Depth = 0;
            materialLabel108.Dock = DockStyle.Top;
            materialLabel108.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel108.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel108.ForeColor = Color.LightCoral;
            materialLabel108.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel108.Location = new Point(0, 69);
            materialLabel108.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel108.Name = "materialLabel108";
            materialLabel108.Size = new Size(851, 156);
            materialLabel108.TabIndex = 3;
            // 
            // materialLabel109
            // 
            materialLabel109.BackColor = Color.White;
            materialLabel109.Depth = 0;
            materialLabel109.Dock = DockStyle.Top;
            materialLabel109.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel109.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel109.ForeColor = Color.LightCoral;
            materialLabel109.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel109.Location = new Point(0, 52);
            materialLabel109.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel109.Name = "materialLabel109";
            materialLabel109.Size = new Size(851, 17);
            materialLabel109.TabIndex = 2;
            materialLabel109.Text = "** File .json";
            // 
            // materialLabel110
            // 
            materialLabel110.BackColor = Color.White;
            materialLabel110.Depth = 0;
            materialLabel110.Dock = DockStyle.Top;
            materialLabel110.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel110.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel110.ForeColor = Color.LightCoral;
            materialLabel110.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel110.Location = new Point(0, 35);
            materialLabel110.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel110.Name = "materialLabel110";
            materialLabel110.Size = new Size(851, 17);
            materialLabel110.TabIndex = 1;
            materialLabel110.Text = "* Default data name Vietnamese";
            // 
            // materialLabel111
            // 
            materialLabel111.Depth = 0;
            materialLabel111.Dock = DockStyle.Top;
            materialLabel111.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel111.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            materialLabel111.Location = new Point(0, 0);
            materialLabel111.Margin = new Padding(0);
            materialLabel111.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel111.Name = "materialLabel111";
            materialLabel111.Size = new Size(851, 35);
            materialLabel111.TabIndex = 0;
            materialLabel111.Text = "Path File...";
            materialLabel111.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // materialLabel112
            // 
            materialLabel112.Depth = 0;
            materialLabel112.Dock = DockStyle.Left;
            materialLabel112.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel112.Location = new Point(0, 0);
            materialLabel112.Margin = new Padding(12, 0, 12, 0);
            materialLabel112.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel112.Name = "materialLabel112";
            materialLabel112.Padding = new Padding(12);
            materialLabel112.Size = new Size(92, 40);
            materialLabel112.TabIndex = 0;
            materialLabel112.Text = "File Name";
            materialLabel112.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // panel38
            // 
            panel38.Controls.Add(materialLabel112);
            panel38.Controls.Add(materialButton16);
            panel38.Dock = DockStyle.Fill;
            panel38.Location = new Point(0, 0);
            panel38.Margin = new Padding(0);
            panel38.Name = "panel38";
            panel38.Size = new Size(851, 40);
            panel38.TabIndex = 1;
            // 
            // materialButton16
            // 
            materialButton16.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton16.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton16.Depth = 0;
            materialButton16.Dock = DockStyle.Right;
            materialButton16.HighEmphasis = true;
            materialButton16.Icon = null;
            materialButton16.Location = new Point(745, 0);
            materialButton16.Margin = new Padding(4, 6, 4, 6);
            materialButton16.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton16.Name = "materialButton16";
            materialButton16.NoAccentTextColor = Color.Empty;
            materialButton16.Size = new Size(106, 40);
            materialButton16.TabIndex = 2;
            materialButton16.Text = "Select File";
            materialButton16.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton16.UseAccentColor = false;
            materialButton16.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel67
            // 
            tableLayoutPanel67.ColumnCount = 1;
            tableLayoutPanel67.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel67.Controls.Add(panel38, 0, 0);
            tableLayoutPanel67.Controls.Add(panel37, 0, 1);
            tableLayoutPanel67.Dock = DockStyle.Fill;
            tableLayoutPanel67.Location = new Point(14, 14);
            tableLayoutPanel67.Margin = new Padding(0);
            tableLayoutPanel67.Name = "tableLayoutPanel67";
            tableLayoutPanel67.RowCount = 2;
            tableLayoutPanel67.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel67.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel67.Size = new Size(851, 270);
            tableLayoutPanel67.TabIndex = 0;
            // 
            // materialCard37
            // 
            materialCard37.BackColor = Color.FromArgb(255, 255, 255);
            materialCard37.Controls.Add(tableLayoutPanel67);
            materialCard37.Depth = 0;
            materialCard37.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard37.Location = new Point(14, 321);
            materialCard37.Margin = new Padding(14);
            materialCard37.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard37.Name = "materialCard37";
            materialCard37.Padding = new Padding(14);
            materialCard37.Size = new Size(879, 298);
            materialCard37.TabIndex = 2;
            // 
            // panel39
            // 
            panel39.Controls.Add(materialLabel113);
            panel39.Controls.Add(materialButton17);
            panel39.Dock = DockStyle.Fill;
            panel39.Location = new Point(0, 0);
            panel39.Margin = new Padding(0);
            panel39.Name = "panel39";
            panel39.Size = new Size(851, 40);
            panel39.TabIndex = 1;
            // 
            // materialLabel113
            // 
            materialLabel113.Depth = 0;
            materialLabel113.Dock = DockStyle.Left;
            materialLabel113.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel113.Location = new Point(0, 0);
            materialLabel113.Margin = new Padding(12, 0, 12, 0);
            materialLabel113.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel113.Name = "materialLabel113";
            materialLabel113.Padding = new Padding(12);
            materialLabel113.Size = new Size(92, 40);
            materialLabel113.TabIndex = 0;
            materialLabel113.Text = "File Proxies";
            materialLabel113.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialButton17
            // 
            materialButton17.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton17.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton17.Depth = 0;
            materialButton17.Dock = DockStyle.Right;
            materialButton17.HighEmphasis = true;
            materialButton17.Icon = null;
            materialButton17.Location = new Point(745, 0);
            materialButton17.Margin = new Padding(4, 6, 4, 6);
            materialButton17.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton17.Name = "materialButton17";
            materialButton17.NoAccentTextColor = Color.Empty;
            materialButton17.Size = new Size(106, 40);
            materialButton17.TabIndex = 2;
            materialButton17.Text = "Select File";
            materialButton17.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton17.UseAccentColor = false;
            materialButton17.UseVisualStyleBackColor = true;
            // 
            // materialLabel114
            // 
            materialLabel114.BackColor = Color.White;
            materialLabel114.Depth = 0;
            materialLabel114.Dock = DockStyle.Top;
            materialLabel114.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel114.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel114.ForeColor = Color.LightCoral;
            materialLabel114.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel114.Location = new Point(0, 52);
            materialLabel114.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel114.Name = "materialLabel114";
            materialLabel114.Size = new Size(851, 17);
            materialLabel114.TabIndex = 2;
            materialLabel114.Text = "** File .txt";
            // 
            // materialLabel115
            // 
            materialLabel115.BackColor = Color.White;
            materialLabel115.Depth = 0;
            materialLabel115.Dock = DockStyle.Top;
            materialLabel115.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel115.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel115.ForeColor = Color.LightCoral;
            materialLabel115.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel115.Location = new Point(0, 35);
            materialLabel115.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel115.Name = "materialLabel115";
            materialLabel115.Size = new Size(851, 17);
            materialLabel115.TabIndex = 1;
            materialLabel115.Text = "* Required syntax of proxy: ip:port:username:password";
            // 
            // materialLabel116
            // 
            materialLabel116.Depth = 0;
            materialLabel116.Dock = DockStyle.Top;
            materialLabel116.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel116.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            materialLabel116.Location = new Point(0, 0);
            materialLabel116.Margin = new Padding(0);
            materialLabel116.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel116.Name = "materialLabel116";
            materialLabel116.Size = new Size(851, 35);
            materialLabel116.TabIndex = 0;
            materialLabel116.Text = "Path File...";
            materialLabel116.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // panel40
            // 
            panel40.Controls.Add(materialLabel114);
            panel40.Controls.Add(materialLabel115);
            panel40.Controls.Add(materialLabel116);
            panel40.Dock = DockStyle.Fill;
            panel40.Location = new Point(0, 40);
            panel40.Margin = new Padding(0);
            panel40.Name = "panel40";
            panel40.Size = new Size(851, 65);
            panel40.TabIndex = 2;
            // 
            // tableLayoutPanel68
            // 
            tableLayoutPanel68.ColumnCount = 1;
            tableLayoutPanel68.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel68.Controls.Add(panel39, 0, 0);
            tableLayoutPanel68.Controls.Add(panel40, 0, 1);
            tableLayoutPanel68.Dock = DockStyle.Fill;
            tableLayoutPanel68.Location = new Point(14, 14);
            tableLayoutPanel68.Margin = new Padding(0);
            tableLayoutPanel68.Name = "tableLayoutPanel68";
            tableLayoutPanel68.RowCount = 2;
            tableLayoutPanel68.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel68.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel68.Size = new Size(851, 105);
            tableLayoutPanel68.TabIndex = 0;
            // 
            // materialCard38
            // 
            materialCard38.BackColor = Color.FromArgb(255, 255, 255);
            materialCard38.Controls.Add(tableLayoutPanel68);
            materialCard38.Depth = 0;
            materialCard38.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard38.Location = new Point(14, 160);
            materialCard38.Margin = new Padding(14);
            materialCard38.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard38.Name = "materialCard38";
            materialCard38.Padding = new Padding(14);
            materialCard38.Size = new Size(879, 133);
            materialCard38.TabIndex = 1;
            // 
            // materialLabel117
            // 
            materialLabel117.BackColor = Color.White;
            materialLabel117.Depth = 0;
            materialLabel117.Dock = DockStyle.Top;
            materialLabel117.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel117.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel117.ForeColor = Color.LightCoral;
            materialLabel117.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel117.Location = new Point(0, 35);
            materialLabel117.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel117.Name = "materialLabel117";
            materialLabel117.Size = new Size(851, 17);
            materialLabel117.TabIndex = 1;
            materialLabel117.Text = "* Where to save the local profile file";
            // 
            // materialLabel118
            // 
            materialLabel118.Depth = 0;
            materialLabel118.Dock = DockStyle.Top;
            materialLabel118.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel118.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            materialLabel118.Location = new Point(0, 0);
            materialLabel118.Margin = new Padding(0);
            materialLabel118.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel118.Name = "materialLabel118";
            materialLabel118.Size = new Size(851, 35);
            materialLabel118.TabIndex = 0;
            materialLabel118.Text = "Path File...";
            materialLabel118.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // tabPage4
            // 
            tabPage4.Location = new Point(4, 32);
            tabPage4.Name = "tabPage4";
            tabPage4.Size = new Size(786, 497);
            tabPage4.TabIndex = 6;
            tabPage4.Text = "Run";
            tabPage4.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Dock = DockStyle.Fill;
            label10.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label10.Location = new Point(10, 10);
            label10.Margin = new Padding(10);
            label10.Name = "label10";
            label10.Size = new Size(322, 983);
            label10.TabIndex = 0;
            label10.Text = "API";
            label10.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel69
            // 
            tableLayoutPanel69.ColumnCount = 2;
            tableLayoutPanel69.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanel69.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            tableLayoutPanel69.Controls.Add(label10, 0, 0);
            tableLayoutPanel69.Dock = DockStyle.Fill;
            tableLayoutPanel69.Location = new Point(3, 64);
            tableLayoutPanel69.Name = "tableLayoutPanel69";
            tableLayoutPanel69.RowCount = 1;
            tableLayoutPanel69.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel69.Size = new Size(1712, 1003);
            tableLayoutPanel69.TabIndex = 0;
            // 
            // panel41
            // 
            panel41.Controls.Add(materialLabel117);
            panel41.Controls.Add(materialLabel118);
            panel41.Dock = DockStyle.Fill;
            panel41.Location = new Point(0, 40);
            panel41.Margin = new Padding(0);
            panel41.Name = "panel41";
            panel41.Size = new Size(851, 50);
            panel41.TabIndex = 2;
            // 
            // materialLabel119
            // 
            materialLabel119.Depth = 0;
            materialLabel119.Dock = DockStyle.Left;
            materialLabel119.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel119.Location = new Point(0, 0);
            materialLabel119.Margin = new Padding(12, 0, 12, 0);
            materialLabel119.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel119.Name = "materialLabel119";
            materialLabel119.Padding = new Padding(12);
            materialLabel119.Size = new Size(163, 40);
            materialLabel119.TabIndex = 0;
            materialLabel119.Text = "Folder Profiles";
            materialLabel119.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel74
            // 
            tableLayoutPanel74.ColumnCount = 1;
            tableLayoutPanel74.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel74.Controls.Add(panel42, 0, 0);
            tableLayoutPanel74.Controls.Add(panel41, 0, 1);
            tableLayoutPanel74.Dock = DockStyle.Fill;
            tableLayoutPanel74.Location = new Point(14, 14);
            tableLayoutPanel74.Margin = new Padding(0);
            tableLayoutPanel74.Name = "tableLayoutPanel74";
            tableLayoutPanel74.RowCount = 2;
            tableLayoutPanel74.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel74.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel74.Size = new Size(851, 90);
            tableLayoutPanel74.TabIndex = 0;
            // 
            // panel42
            // 
            panel42.Controls.Add(materialLabel119);
            panel42.Controls.Add(materialButton18);
            panel42.Dock = DockStyle.Fill;
            panel42.Location = new Point(0, 0);
            panel42.Margin = new Padding(0);
            panel42.Name = "panel42";
            panel42.Size = new Size(851, 40);
            panel42.TabIndex = 1;
            // 
            // materialButton18
            // 
            materialButton18.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton18.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton18.Depth = 0;
            materialButton18.Dock = DockStyle.Right;
            materialButton18.HighEmphasis = true;
            materialButton18.Icon = null;
            materialButton18.Location = new Point(745, 0);
            materialButton18.Margin = new Padding(4, 6, 4, 6);
            materialButton18.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton18.Name = "materialButton18";
            materialButton18.NoAccentTextColor = Color.Empty;
            materialButton18.Size = new Size(106, 40);
            materialButton18.TabIndex = 2;
            materialButton18.Text = "Select File";
            materialButton18.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton18.UseAccentColor = false;
            materialButton18.UseVisualStyleBackColor = true;
            // 
            // materialCard39
            // 
            materialCard39.BackColor = Color.FromArgb(255, 255, 255);
            materialCard39.Controls.Add(tableLayoutPanel74);
            materialCard39.Depth = 0;
            materialCard39.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard39.Location = new Point(14, 14);
            materialCard39.Margin = new Padding(14);
            materialCard39.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard39.Name = "materialCard39";
            materialCard39.Padding = new Padding(14);
            materialCard39.Size = new Size(879, 118);
            materialCard39.TabIndex = 3;
            // 
            // flowLayoutPanel3
            // 
            flowLayoutPanel3.AutoScroll = true;
            flowLayoutPanel3.Controls.Add(materialCard39);
            flowLayoutPanel3.Controls.Add(materialCard38);
            flowLayoutPanel3.Controls.Add(materialCard37);
            flowLayoutPanel3.Dock = DockStyle.Fill;
            flowLayoutPanel3.Location = new Point(3, 64);
            flowLayoutPanel3.Name = "flowLayoutPanel3";
            flowLayoutPanel3.Size = new Size(1712, 1003);
            flowLayoutPanel3.TabIndex = 1;
            // 
            // materialButton19
            // 
            materialButton19.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton19.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton19.Depth = 0;
            materialButton19.HighEmphasis = true;
            materialButton19.Icon = null;
            materialButton19.Location = new Point(1468, 0);
            materialButton19.Margin = new Padding(0);
            materialButton19.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton19.Name = "materialButton19";
            materialButton19.NoAccentTextColor = Color.Empty;
            materialButton19.Size = new Size(112, 1);
            materialButton19.TabIndex = 0;
            materialButton19.Text = "Save Seting";
            materialButton19.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton19.UseAccentColor = false;
            materialButton19.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel75
            // 
            tableLayoutPanel75.ColumnCount = 2;
            tableLayoutPanel75.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 87.59048F));
            tableLayoutPanel75.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 12.40951F));
            tableLayoutPanel75.Controls.Add(materialButton19, 1, 0);
            tableLayoutPanel75.Dock = DockStyle.Fill;
            tableLayoutPanel75.Location = new Point(4, 4);
            tableLayoutPanel75.Margin = new Padding(0);
            tableLayoutPanel75.Name = "tableLayoutPanel75";
            tableLayoutPanel75.RowCount = 1;
            tableLayoutPanel75.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel75.Size = new Size(1676, 0);
            tableLayoutPanel75.TabIndex = 1;
            // 
            // materialCard40
            // 
            materialCard40.BackColor = Color.FromArgb(255, 255, 255);
            materialCard40.Controls.Add(tableLayoutPanel75);
            materialCard40.Depth = 0;
            materialCard40.Dock = DockStyle.Fill;
            materialCard40.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard40.Location = new Point(14, 14);
            materialCard40.Margin = new Padding(14);
            materialCard40.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard40.Name = "materialCard40";
            materialCard40.Padding = new Padding(4);
            materialCard40.Size = new Size(1684, 1);
            materialCard40.TabIndex = 0;
            // 
            // tableLayoutPanel76
            // 
            tableLayoutPanel76.ColumnCount = 1;
            tableLayoutPanel76.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel76.Controls.Add(materialCard40, 0, 0);
            tableLayoutPanel76.Dock = DockStyle.Fill;
            tableLayoutPanel76.Location = new Point(3, 64);
            tableLayoutPanel76.Name = "tableLayoutPanel76";
            tableLayoutPanel76.RowCount = 2;
            tableLayoutPanel76.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel76.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel76.Size = new Size(1712, 1003);
            tableLayoutPanel76.TabIndex = 0;
            // 
            // materialLabel120
            // 
            materialLabel120.BackColor = Color.White;
            materialLabel120.Depth = 0;
            materialLabel120.Dock = DockStyle.Top;
            materialLabel120.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel120.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel120.ForeColor = Color.LightCoral;
            materialLabel120.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel120.Location = new Point(0, 35);
            materialLabel120.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel120.Name = "materialLabel120";
            materialLabel120.Size = new Size(851, 17);
            materialLabel120.TabIndex = 1;
            materialLabel120.Text = "* Where to save the local profile file";
            // 
            // dataGridViewTextBoxColumn23
            // 
            dataGridViewTextBoxColumn23.DataPropertyName = "profile_id";
            dataGridViewTextBoxColumn23.FillWeight = 109.817F;
            dataGridViewTextBoxColumn23.HeaderText = "ID";
            dataGridViewTextBoxColumn23.MinimumWidth = 6;
            dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            // 
            // materialCard41
            // 
            materialCard41.BackColor = Color.FromArgb(255, 255, 255);
            materialCard41.Controls.Add(tableLayoutPanel77);
            materialCard41.Depth = 0;
            materialCard41.Dock = DockStyle.Fill;
            materialCard41.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard41.Location = new Point(8, 8);
            materialCard41.Margin = new Padding(8);
            materialCard41.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard41.Name = "materialCard41";
            materialCard41.Padding = new Padding(4);
            materialCard41.Size = new Size(764, 59);
            materialCard41.TabIndex = 0;
            // 
            // tableLayoutPanel77
            // 
            tableLayoutPanel77.ColumnCount = 6;
            tableLayoutPanel77.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 200F));
            tableLayoutPanel77.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 43F));
            tableLayoutPanel77.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel77.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel77.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel77.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel77.Controls.Add(materialComboBox3, 0, 0);
            tableLayoutPanel77.Controls.Add(materialButton20, 5, 0);
            tableLayoutPanel77.Controls.Add(materialButton21, 4, 0);
            tableLayoutPanel77.Controls.Add(materialButton22, 3, 0);
            tableLayoutPanel77.Controls.Add(button1, 1, 0);
            tableLayoutPanel77.Dock = DockStyle.Fill;
            tableLayoutPanel77.Location = new Point(4, 4);
            tableLayoutPanel77.Margin = new Padding(0);
            tableLayoutPanel77.Name = "tableLayoutPanel77";
            tableLayoutPanel77.RowCount = 1;
            tableLayoutPanel77.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel77.Size = new Size(756, 51);
            tableLayoutPanel77.TabIndex = 1;
            // 
            // materialComboBox3
            // 
            materialComboBox3.AutoResize = false;
            materialComboBox3.BackColor = Color.FromArgb(255, 255, 255);
            materialComboBox3.Depth = 0;
            materialComboBox3.Dock = DockStyle.Fill;
            materialComboBox3.DrawMode = DrawMode.OwnerDrawVariable;
            materialComboBox3.DropDownHeight = 174;
            materialComboBox3.DropDownStyle = ComboBoxStyle.DropDownList;
            materialComboBox3.DropDownWidth = 121;
            materialComboBox3.Enabled = false;
            materialComboBox3.Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialComboBox3.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialComboBox3.FormattingEnabled = true;
            materialComboBox3.IntegralHeight = false;
            materialComboBox3.ItemHeight = 43;
            materialComboBox3.Location = new Point(3, 3);
            materialComboBox3.MaxDropDownItems = 4;
            materialComboBox3.MouseState = MaterialSkin.MouseState.OUT;
            materialComboBox3.Name = "materialComboBox3";
            materialComboBox3.Size = new Size(194, 49);
            materialComboBox3.StartIndex = 0;
            materialComboBox3.TabIndex = 9;
            materialComboBox3.DropDownClosed += ComboBoxPopUpSelected_SelectedIndexChanged;
            // 
            // materialButton20
            // 
            materialButton20.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton20.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton20.Depth = 0;
            materialButton20.Dock = DockStyle.Fill;
            materialButton20.Enabled = false;
            materialButton20.HighEmphasis = true;
            materialButton20.Icon = null;
            materialButton20.Location = new Point(681, 5);
            materialButton20.Margin = new Padding(5);
            materialButton20.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton20.Name = "materialButton20";
            materialButton20.NoAccentTextColor = Color.Empty;
            materialButton20.Size = new Size(70, 41);
            materialButton20.TabIndex = 0;
            materialButton20.Text = "Stop";
            materialButton20.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton20.UseAccentColor = false;
            materialButton20.UseVisualStyleBackColor = true;
            materialButton20.Click += simulateStop_Click;
            // 
            // materialButton21
            // 
            materialButton21.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton21.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton21.Depth = 0;
            materialButton21.Dock = DockStyle.Fill;
            materialButton21.Enabled = false;
            materialButton21.HighEmphasis = true;
            materialButton21.Icon = null;
            materialButton21.Location = new Point(601, 5);
            materialButton21.Margin = new Padding(5);
            materialButton21.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton21.Name = "materialButton21";
            materialButton21.NoAccentTextColor = Color.Empty;
            materialButton21.Size = new Size(70, 41);
            materialButton21.TabIndex = 1;
            materialButton21.Text = "Start";
            materialButton21.TextAlign = ContentAlignment.MiddleRight;
            materialButton21.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton21.UseAccentColor = false;
            materialButton21.UseVisualStyleBackColor = true;
            materialButton21.Click += simulateStart_Click;
            // 
            // materialButton22
            // 
            materialButton22.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton22.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton22.Depth = 0;
            materialButton22.Dock = DockStyle.Fill;
            materialButton22.HighEmphasis = true;
            materialButton22.Icon = null;
            materialButton22.Location = new Point(521, 5);
            materialButton22.Margin = new Padding(5);
            materialButton22.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton22.Name = "materialButton22";
            materialButton22.NoAccentTextColor = Color.Empty;
            materialButton22.Size = new Size(70, 41);
            materialButton22.TabIndex = 4;
            materialButton22.Text = "Load";
            materialButton22.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton22.UseAccentColor = false;
            materialButton22.UseVisualStyleBackColor = true;
            materialButton22.Click += SimulateLoad_ClickAsync;
            // 
            // button1
            // 
            button1.Anchor = AnchorStyles.Left;
            button1.Font = new Font("Wingdings 3", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            button1.Location = new Point(203, 10);
            button1.Name = "button1";
            button1.Size = new Size(30, 30);
            button1.TabIndex = 10;
            button1.Text = "Q";
            button1.UseVisualStyleBackColor = true;
            button1.Click += buttonReload_Click;
            // 
            // tableLayoutPanel78
            // 
            tableLayoutPanel78.ColumnCount = 1;
            tableLayoutPanel78.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel78.Controls.Add(materialCard41, 0, 0);
            tableLayoutPanel78.Controls.Add(tableLayoutPanel79, 0, 1);
            tableLayoutPanel78.Dock = DockStyle.Fill;
            tableLayoutPanel78.Location = new Point(3, 3);
            tableLayoutPanel78.Name = "tableLayoutPanel78";
            tableLayoutPanel78.RowCount = 2;
            tableLayoutPanel78.RowStyles.Add(new RowStyle(SizeType.Absolute, 75F));
            tableLayoutPanel78.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel78.Size = new Size(780, 491);
            tableLayoutPanel78.TabIndex = 3;
            // 
            // tableLayoutPanel79
            // 
            tableLayoutPanel79.ColumnCount = 1;
            tableLayoutPanel79.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel79.Controls.Add(statusStrip3, 0, 1);
            tableLayoutPanel79.Controls.Add(materialCard42, 0, 0);
            tableLayoutPanel79.Dock = DockStyle.Fill;
            tableLayoutPanel79.Location = new Point(3, 78);
            tableLayoutPanel79.Name = "tableLayoutPanel79";
            tableLayoutPanel79.RowCount = 2;
            tableLayoutPanel79.RowStyles.Add(new RowStyle(SizeType.Percent, 95F));
            tableLayoutPanel79.RowStyles.Add(new RowStyle(SizeType.Percent, 5F));
            tableLayoutPanel79.Size = new Size(774, 410);
            tableLayoutPanel79.TabIndex = 1;
            // 
            // statusStrip3
            // 
            statusStrip3.Dock = DockStyle.Fill;
            statusStrip3.ImageScalingSize = new Size(20, 20);
            statusStrip3.Items.AddRange(new ToolStripItem[] { toolStripStatusLabel1, toolStripStatusLabel2 });
            statusStrip3.Location = new Point(0, 389);
            statusStrip3.Name = "statusStrip3";
            statusStrip3.Size = new Size(774, 21);
            statusStrip3.TabIndex = 3;
            statusStrip3.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            toolStripStatusLabel1.Font = new Font("Segoe UI", 10.2F, FontStyle.Bold, GraphicsUnit.Point);
            toolStripStatusLabel1.ForeColor = Color.Green;
            toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            toolStripStatusLabel1.Size = new Size(70, 15);
            toolStripStatusLabel1.Text = "Status :";
            // 
            // toolStripStatusLabel2
            // 
            toolStripStatusLabel2.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            toolStripStatusLabel2.ForeColor = Color.Green;
            toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            toolStripStatusLabel2.Size = new Size(52, 15);
            toolStripStatusLabel2.Text = "Ready";
            // 
            // materialCard42
            // 
            materialCard42.BackColor = Color.FromArgb(255, 255, 255);
            materialCard42.Controls.Add(tableLayoutPanel80);
            materialCard42.Depth = 0;
            materialCard42.Dock = DockStyle.Fill;
            materialCard42.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard42.Location = new Point(5, 5);
            materialCard42.Margin = new Padding(5);
            materialCard42.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard42.Name = "materialCard42";
            materialCard42.Padding = new Padding(5);
            materialCard42.Size = new Size(764, 379);
            materialCard42.TabIndex = 4;
            // 
            // tableLayoutPanel80
            // 
            tableLayoutPanel80.ColumnCount = 1;
            tableLayoutPanel80.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel80.Controls.Add(dataGridView7, 0, 2);
            tableLayoutPanel80.Controls.Add(panel43, 0, 1);
            tableLayoutPanel80.Controls.Add(materialButton23, 0, 0);
            tableLayoutPanel80.Dock = DockStyle.Fill;
            tableLayoutPanel80.Location = new Point(5, 5);
            tableLayoutPanel80.Name = "tableLayoutPanel80";
            tableLayoutPanel80.RowCount = 3;
            tableLayoutPanel80.RowStyles.Add(new RowStyle());
            tableLayoutPanel80.RowStyles.Add(new RowStyle());
            tableLayoutPanel80.RowStyles.Add(new RowStyle());
            tableLayoutPanel80.Size = new Size(754, 369);
            tableLayoutPanel80.TabIndex = 0;
            // 
            // dataGridView7
            // 
            dataGridView7.AllowUserToAddRows = false;
            dataGridView7.AllowUserToDeleteRows = false;
            dataGridView7.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView7.BackgroundColor = SystemColors.ButtonHighlight;
            dataGridView7.BorderStyle = BorderStyle.None;
            dataGridViewCellStyle19.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = SystemColors.Control;
            dataGridViewCellStyle19.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle19.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = Color.Orange;
            dataGridViewCellStyle19.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = DataGridViewTriState.True;
            dataGridView7.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            dataGridView7.ColumnHeadersHeight = 40;
            dataGridView7.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridView7.Columns.AddRange(new DataGridViewColumn[] { dataGridViewTextBoxColumn24, dataGridViewTextBoxColumn25, dataGridViewTextBoxColumn26, dataGridViewTextBoxColumn27 });
            dataGridView7.Cursor = Cursors.Hand;
            dataGridViewCellStyle20.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = SystemColors.Window;
            dataGridViewCellStyle20.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle20.ForeColor = Color.FromArgb(222, 0, 0, 0);
            dataGridViewCellStyle20.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle20.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = DataGridViewTriState.False;
            dataGridView7.DefaultCellStyle = dataGridViewCellStyle20;
            dataGridView7.Dock = DockStyle.Fill;
            dataGridView7.Location = new Point(3, 58);
            dataGridView7.Name = "dataGridView7";
            dataGridView7.ReadOnly = true;
            dataGridViewCellStyle21.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = SystemColors.Control;
            dataGridViewCellStyle21.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle21.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle21.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle21.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = DataGridViewTriState.True;
            dataGridView7.RowHeadersDefaultCellStyle = dataGridViewCellStyle21;
            dataGridView7.RowHeadersVisible = false;
            dataGridView7.RowHeadersWidth = 50;
            dataGridView7.RowTemplate.Height = 42;
            dataGridView7.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView7.Size = new Size(748, 765);
            dataGridView7.TabIndex = 8;
            dataGridView7.CellClick += Simu_contentClick;
            // 
            // dataGridViewTextBoxColumn24
            // 
            dataGridViewTextBoxColumn24.DataPropertyName = "profile_id";
            dataGridViewTextBoxColumn24.FillWeight = 109.817F;
            dataGridViewTextBoxColumn24.HeaderText = "ID";
            dataGridViewTextBoxColumn24.MinimumWidth = 6;
            dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            dataGridViewTextBoxColumn24.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn25
            // 
            dataGridViewTextBoxColumn25.DataPropertyName = "profile_status";
            dataGridViewTextBoxColumn25.FillWeight = 42.66524F;
            dataGridViewTextBoxColumn25.HeaderText = "Status";
            dataGridViewTextBoxColumn25.MinimumWidth = 6;
            dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            dataGridViewTextBoxColumn25.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn26
            // 
            dataGridViewTextBoxColumn26.DataPropertyName = "profile_last_update";
            dataGridViewTextBoxColumn26.HeaderText = "Last update";
            dataGridViewTextBoxColumn26.MinimumWidth = 6;
            dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            dataGridViewTextBoxColumn26.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn27
            // 
            dataGridViewTextBoxColumn27.DataPropertyName = "profile_proxy";
            dataGridViewTextBoxColumn27.HeaderText = "Proxy";
            dataGridViewTextBoxColumn27.MinimumWidth = 6;
            dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            dataGridViewTextBoxColumn27.ReadOnly = true;
            // 
            // panel43
            // 
            panel43.BackColor = Color.White;
            panel43.Controls.Add(tableLayoutPanel81);
            panel43.Dock = DockStyle.Fill;
            panel43.Location = new Point(3, 51);
            panel43.Name = "panel43";
            panel43.Size = new Size(748, 1);
            panel43.TabIndex = 9;
            // 
            // tableLayoutPanel81
            // 
            tableLayoutPanel81.ColumnCount = 2;
            tableLayoutPanel81.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel81.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel81.Controls.Add(tableLayoutPanel82, 0, 1);
            tableLayoutPanel81.Controls.Add(tableLayoutPanel83, 1, 0);
            tableLayoutPanel81.Controls.Add(tableLayoutPanel84, 1, 1);
            tableLayoutPanel81.Controls.Add(tableLayoutPanel85, 0, 0);
            tableLayoutPanel81.Dock = DockStyle.Fill;
            tableLayoutPanel81.Location = new Point(0, 0);
            tableLayoutPanel81.Name = "tableLayoutPanel81";
            tableLayoutPanel81.RowCount = 2;
            tableLayoutPanel81.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel81.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel81.Size = new Size(748, 1);
            tableLayoutPanel81.TabIndex = 2;
            // 
            // tableLayoutPanel82
            // 
            tableLayoutPanel82.ColumnCount = 2;
            tableLayoutPanel82.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanel82.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            tableLayoutPanel82.Controls.Add(label14, 0, 0);
            tableLayoutPanel82.Controls.Add(materialTextBox27, 1, 0);
            tableLayoutPanel82.Dock = DockStyle.Fill;
            tableLayoutPanel82.Location = new Point(3, 3);
            tableLayoutPanel82.Name = "tableLayoutPanel82";
            tableLayoutPanel82.RowCount = 1;
            tableLayoutPanel82.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel82.Size = new Size(368, 1);
            tableLayoutPanel82.TabIndex = 1;
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Dock = DockStyle.Fill;
            label14.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label14.Location = new Point(10, 10);
            label14.Margin = new Padding(10);
            label14.Name = "label14";
            label14.Size = new Size(53, 1);
            label14.TabIndex = 0;
            label14.Text = "Channel";
            label14.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialTextBox27
            // 
            materialTextBox27.AnimateReadOnly = false;
            materialTextBox27.BackgroundImageLayout = ImageLayout.None;
            materialTextBox27.CharacterCasing = CharacterCasing.Normal;
            materialTextBox27.Depth = 0;
            materialTextBox27.Dock = DockStyle.Fill;
            materialTextBox27.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialTextBox27.HideSelection = true;
            materialTextBox27.Hint = "vnm, vt, ..";
            materialTextBox27.LeadingIcon = null;
            materialTextBox27.Location = new Point(83, 10);
            materialTextBox27.Margin = new Padding(10);
            materialTextBox27.MaxLength = 32767;
            materialTextBox27.MouseState = MaterialSkin.MouseState.OUT;
            materialTextBox27.Name = "materialTextBox27";
            materialTextBox27.PasswordChar = '\0';
            materialTextBox27.PrefixSuffixText = null;
            materialTextBox27.ReadOnly = false;
            materialTextBox27.RightToLeft = RightToLeft.No;
            materialTextBox27.SelectedText = "";
            materialTextBox27.SelectionLength = 0;
            materialTextBox27.SelectionStart = 0;
            materialTextBox27.ShortcutsEnabled = true;
            materialTextBox27.Size = new Size(275, 48);
            materialTextBox27.TabIndex = 1;
            materialTextBox27.TabStop = false;
            materialTextBox27.Text = "chung_duck";
            materialTextBox27.TextAlign = HorizontalAlignment.Left;
            materialTextBox27.TrailingIcon = null;
            materialTextBox27.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel83
            // 
            tableLayoutPanel83.ColumnCount = 2;
            tableLayoutPanel83.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanel83.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            tableLayoutPanel83.Controls.Add(label15, 0, 0);
            tableLayoutPanel83.Controls.Add(materialTextBox28, 1, 0);
            tableLayoutPanel83.Dock = DockStyle.Fill;
            tableLayoutPanel83.Location = new Point(377, 3);
            tableLayoutPanel83.Name = "tableLayoutPanel83";
            tableLayoutPanel83.RowCount = 1;
            tableLayoutPanel83.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel83.Size = new Size(368, 1);
            tableLayoutPanel83.TabIndex = 2;
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Dock = DockStyle.Fill;
            label15.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label15.Location = new Point(10, 10);
            label15.Margin = new Padding(10);
            label15.Name = "label15";
            label15.Size = new Size(53, 1);
            label15.TabIndex = 0;
            label15.Text = "Brand";
            label15.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialTextBox28
            // 
            materialTextBox28.AnimateReadOnly = false;
            materialTextBox28.BackgroundImageLayout = ImageLayout.None;
            materialTextBox28.CharacterCasing = CharacterCasing.Normal;
            materialTextBox28.Depth = 0;
            materialTextBox28.Dock = DockStyle.Fill;
            materialTextBox28.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialTextBox28.HideSelection = true;
            materialTextBox28.Hint = "facebook,..";
            materialTextBox28.LeadingIcon = null;
            materialTextBox28.Location = new Point(83, 10);
            materialTextBox28.Margin = new Padding(10);
            materialTextBox28.MaxLength = 32767;
            materialTextBox28.MouseState = MaterialSkin.MouseState.OUT;
            materialTextBox28.Name = "materialTextBox28";
            materialTextBox28.PasswordChar = '\0';
            materialTextBox28.PrefixSuffixText = null;
            materialTextBox28.ReadOnly = false;
            materialTextBox28.RightToLeft = RightToLeft.No;
            materialTextBox28.SelectedText = "";
            materialTextBox28.SelectionLength = 0;
            materialTextBox28.SelectionStart = 0;
            materialTextBox28.ShortcutsEnabled = true;
            materialTextBox28.Size = new Size(275, 48);
            materialTextBox28.TabIndex = 1;
            materialTextBox28.TabStop = false;
            materialTextBox28.TextAlign = HorizontalAlignment.Left;
            materialTextBox28.TrailingIcon = null;
            materialTextBox28.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel84
            // 
            tableLayoutPanel84.ColumnCount = 2;
            tableLayoutPanel84.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanel84.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            tableLayoutPanel84.Controls.Add(label16, 0, 0);
            tableLayoutPanel84.Controls.Add(materialTextBox3, 1, 0);
            tableLayoutPanel84.Dock = DockStyle.Fill;
            tableLayoutPanel84.Location = new Point(377, 3);
            tableLayoutPanel84.Name = "tableLayoutPanel84";
            tableLayoutPanel84.RowCount = 1;
            tableLayoutPanel84.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel84.Size = new Size(368, 1);
            tableLayoutPanel84.TabIndex = 3;
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Dock = DockStyle.Fill;
            label16.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label16.Location = new Point(10, 10);
            label16.Margin = new Padding(10);
            label16.Name = "label16";
            label16.Size = new Size(53, 1);
            label16.TabIndex = 0;
            label16.Text = "Table Saving";
            label16.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialTextBox3
            // 
            materialTextBox3.AnimateReadOnly = false;
            materialTextBox3.BorderStyle = BorderStyle.None;
            materialTextBox3.Depth = 0;
            materialTextBox3.Dock = DockStyle.Fill;
            materialTextBox3.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialTextBox3.Hint = "database table";
            materialTextBox3.LeadingIcon = null;
            materialTextBox3.Location = new Point(83, 10);
            materialTextBox3.Margin = new Padding(10);
            materialTextBox3.MaxLength = 50;
            materialTextBox3.MouseState = MaterialSkin.MouseState.OUT;
            materialTextBox3.Multiline = false;
            materialTextBox3.Name = "materialTextBox3";
            materialTextBox3.Size = new Size(275, 50);
            materialTextBox3.TabIndex = 1;
            materialTextBox3.Text = "";
            materialTextBox3.TrailingIcon = null;
            // 
            // tableLayoutPanel85
            // 
            tableLayoutPanel85.ColumnCount = 2;
            tableLayoutPanel85.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20F));
            tableLayoutPanel85.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 80F));
            tableLayoutPanel85.Controls.Add(label17, 0, 0);
            tableLayoutPanel85.Controls.Add(materialTextBox29, 1, 0);
            tableLayoutPanel85.Dock = DockStyle.Fill;
            tableLayoutPanel85.Location = new Point(3, 3);
            tableLayoutPanel85.Name = "tableLayoutPanel85";
            tableLayoutPanel85.RowCount = 1;
            tableLayoutPanel85.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel85.Size = new Size(368, 1);
            tableLayoutPanel85.TabIndex = 4;
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Dock = DockStyle.Fill;
            label17.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label17.Location = new Point(10, 10);
            label17.Margin = new Padding(10);
            label17.Name = "label17";
            label17.Size = new Size(53, 1);
            label17.TabIndex = 0;
            label17.Text = "API";
            label17.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialTextBox29
            // 
            materialTextBox29.AnimateReadOnly = false;
            materialTextBox29.BackgroundImageLayout = ImageLayout.None;
            materialTextBox29.CharacterCasing = CharacterCasing.Normal;
            materialTextBox29.Cursor = Cursors.IBeam;
            materialTextBox29.Depth = 0;
            materialTextBox29.Dock = DockStyle.Fill;
            materialTextBox29.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialTextBox29.HideSelection = false;
            materialTextBox29.Hint = "abc/xzy";
            materialTextBox29.LeadingIcon = null;
            materialTextBox29.Location = new Point(83, 10);
            materialTextBox29.Margin = new Padding(10);
            materialTextBox29.MaxLength = 32767;
            materialTextBox29.MouseState = MaterialSkin.MouseState.OUT;
            materialTextBox29.Name = "materialTextBox29";
            materialTextBox29.Padding = new Padding(1);
            materialTextBox29.PasswordChar = '\0';
            materialTextBox29.PrefixSuffixText = null;
            materialTextBox29.ReadOnly = false;
            materialTextBox29.RightToLeft = RightToLeft.No;
            materialTextBox29.SelectedText = "";
            materialTextBox29.SelectionLength = 0;
            materialTextBox29.SelectionStart = 0;
            materialTextBox29.ShortcutsEnabled = true;
            materialTextBox29.Size = new Size(275, 48);
            materialTextBox29.TabIndex = 1;
            materialTextBox29.TabStop = false;
            materialTextBox29.Text = "http://cbp.hta2p.xyz";
            materialTextBox29.TextAlign = HorizontalAlignment.Left;
            materialTextBox29.TrailingIcon = null;
            materialTextBox29.UseSystemPasswordChar = false;
            // 
            // materialButton23
            // 
            materialButton23.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton23.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton23.Depth = 0;
            materialButton23.HighEmphasis = true;
            materialButton23.Icon = null;
            materialButton23.Location = new Point(4, 6);
            materialButton23.Margin = new Padding(4, 6, 4, 6);
            materialButton23.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton23.Name = "materialButton23";
            materialButton23.NoAccentTextColor = Color.Empty;
            materialButton23.Size = new Size(139, 36);
            materialButton23.TabIndex = 10;
            materialButton23.Text = "Modify params";
            materialButton23.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton23.UseAccentColor = false;
            materialButton23.UseVisualStyleBackColor = true;
            materialButton23.Click += ButtonCollapsed_Click;
            // 
            // tabPage6
            // 
            tabPage6.Controls.Add(tableLayoutPanel78);
            tabPage6.ImageKey = "sensor.png";
            tabPage6.Location = new Point(4, 32);
            tabPage6.Name = "tabPage6";
            tabPage6.Padding = new Padding(3);
            tabPage6.Size = new Size(786, 497);
            tabPage6.TabIndex = 4;
            tabPage6.Text = "Simulate";
            tabPage6.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTextBoxColumn28
            // 
            dataGridViewTextBoxColumn28.DataPropertyName = "profile_last_update";
            dataGridViewTextBoxColumn28.FillWeight = 78.28705F;
            dataGridViewTextBoxColumn28.HeaderText = "Last update";
            dataGridViewTextBoxColumn28.MinimumWidth = 6;
            dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            // 
            // dataGridViewTextBoxColumn29
            // 
            dataGridViewTextBoxColumn29.DataPropertyName = "profile_proxy";
            dataGridViewTextBoxColumn29.FillWeight = 78.28705F;
            dataGridViewTextBoxColumn29.HeaderText = "Proxy";
            dataGridViewTextBoxColumn29.MinimumWidth = 6;
            dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            // 
            // dataGridViewTextBoxColumn30
            // 
            dataGridViewTextBoxColumn30.DataPropertyName = "profile_status";
            dataGridViewTextBoxColumn30.FillWeight = 42.66524F;
            dataGridViewTextBoxColumn30.HeaderText = "Status";
            dataGridViewTextBoxColumn30.MinimumWidth = 6;
            dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            // 
            // dataGridView8
            // 
            dataGridView8.AllowUserToAddRows = false;
            dataGridView8.AllowUserToDeleteRows = false;
            dataGridView8.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView8.BackgroundColor = SystemColors.ButtonHighlight;
            dataGridView8.BorderStyle = BorderStyle.None;
            dataGridViewCellStyle22.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = SystemColors.Control;
            dataGridViewCellStyle22.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle22.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = Color.Orange;
            dataGridViewCellStyle22.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = DataGridViewTriState.True;
            dataGridView8.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            dataGridView8.ColumnHeadersHeight = 40;
            dataGridView8.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridView8.Columns.AddRange(new DataGridViewColumn[] { dataGridViewTextBoxColumn23, dataGridViewTextBoxColumn30, dataGridViewTextBoxColumn29, dataGridViewTextBoxColumn28 });
            dataGridView8.Cursor = Cursors.Hand;
            dataGridViewCellStyle23.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = SystemColors.Window;
            dataGridViewCellStyle23.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle23.ForeColor = Color.FromArgb(222, 0, 0, 0);
            dataGridViewCellStyle23.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle23.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = DataGridViewTriState.False;
            dataGridView8.DefaultCellStyle = dataGridViewCellStyle23;
            dataGridView8.Dock = DockStyle.Fill;
            dataGridView8.Location = new Point(14, 14);
            dataGridView8.Name = "dataGridView8";
            dataGridViewCellStyle24.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = SystemColors.Control;
            dataGridViewCellStyle24.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle24.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle24.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = DataGridViewTriState.True;
            dataGridView8.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            dataGridView8.RowHeadersVisible = false;
            dataGridView8.RowHeadersWidth = 50;
            dataGridView8.RowTemplate.Height = 42;
            dataGridView8.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView8.Size = new Size(1422, 553);
            dataGridView8.TabIndex = 0;
            dataGridView8.CellClick += ListProfiles_CellClick;
            // 
            // materialCard43
            // 
            materialCard43.BackColor = Color.FromArgb(255, 255, 255);
            materialCard43.Controls.Add(dataGridView8);
            materialCard43.Depth = 0;
            materialCard43.Dock = DockStyle.Fill;
            materialCard43.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard43.Location = new Point(14, 2);
            materialCard43.Margin = new Padding(14, 2, 2, 14);
            materialCard43.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard43.Name = "materialCard43";
            materialCard43.Padding = new Padding(14);
            materialCard43.Size = new Size(1450, 581);
            materialCard43.TabIndex = 2;
            // 
            // tableLayoutPanel86
            // 
            tableLayoutPanel86.ColumnCount = 2;
            tableLayoutPanel86.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 70F));
            tableLayoutPanel86.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel86.Controls.Add(materialLabel121, 1, 3);
            tableLayoutPanel86.Controls.Add(materialLabel122, 0, 3);
            tableLayoutPanel86.Controls.Add(materialLabel123, 1, 2);
            tableLayoutPanel86.Controls.Add(materialLabel124, 0, 2);
            tableLayoutPanel86.Controls.Add(materialLabel125, 1, 1);
            tableLayoutPanel86.Controls.Add(materialLabel126, 0, 1);
            tableLayoutPanel86.Controls.Add(materialLabel127, 1, 0);
            tableLayoutPanel86.Controls.Add(materialLabel128, 0, 0);
            tableLayoutPanel86.Controls.Add(materialLabel129, 0, 4);
            tableLayoutPanel86.Controls.Add(materialLabel130, 1, 4);
            tableLayoutPanel86.Controls.Add(materialLabel131, 0, 5);
            tableLayoutPanel86.Controls.Add(materialLabel132, 1, 5);
            tableLayoutPanel86.Dock = DockStyle.Top;
            tableLayoutPanel86.Location = new Point(0, 0);
            tableLayoutPanel86.Name = "tableLayoutPanel86";
            tableLayoutPanel86.RowCount = 7;
            tableLayoutPanel86.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel86.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel86.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel86.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel86.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel86.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel86.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel86.Size = new Size(244, 352);
            tableLayoutPanel86.TabIndex = 0;
            // 
            // materialLabel121
            // 
            materialLabel121.AutoSize = true;
            materialLabel121.Depth = 0;
            materialLabel121.Dock = DockStyle.Fill;
            materialLabel121.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel121.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel121.Location = new Point(73, 160);
            materialLabel121.Margin = new Padding(3, 10, 3, 10);
            materialLabel121.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel121.Name = "materialLabel121";
            materialLabel121.Size = new Size(168, 30);
            materialLabel121.TabIndex = 7;
            materialLabel121.Text = "...";
            // 
            // materialLabel122
            // 
            materialLabel122.Depth = 0;
            materialLabel122.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel122.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel122.Location = new Point(3, 160);
            materialLabel122.Margin = new Padding(3, 10, 3, 10);
            materialLabel122.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel122.Name = "materialLabel122";
            materialLabel122.Size = new Size(60, 30);
            materialLabel122.TabIndex = 6;
            materialLabel122.Text = "Screen Resolution";
            // 
            // materialLabel123
            // 
            materialLabel123.AutoSize = true;
            materialLabel123.Depth = 0;
            materialLabel123.Dock = DockStyle.Fill;
            materialLabel123.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel123.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel123.Location = new Point(73, 110);
            materialLabel123.Margin = new Padding(3, 10, 3, 10);
            materialLabel123.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel123.Name = "materialLabel123";
            materialLabel123.Size = new Size(168, 30);
            materialLabel123.TabIndex = 5;
            materialLabel123.Text = "...";
            // 
            // materialLabel124
            // 
            materialLabel124.AutoSize = true;
            materialLabel124.Depth = 0;
            materialLabel124.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel124.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel124.Location = new Point(3, 110);
            materialLabel124.Margin = new Padding(3, 10, 3, 10);
            materialLabel124.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel124.Name = "materialLabel124";
            materialLabel124.Size = new Size(32, 14);
            materialLabel124.TabIndex = 4;
            materialLabel124.Text = "Proxy";
            // 
            // materialLabel125
            // 
            materialLabel125.AutoSize = true;
            materialLabel125.Depth = 0;
            materialLabel125.Dock = DockStyle.Fill;
            materialLabel125.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel125.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel125.Location = new Point(73, 60);
            materialLabel125.Margin = new Padding(3, 10, 3, 10);
            materialLabel125.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel125.Name = "materialLabel125";
            materialLabel125.Size = new Size(168, 30);
            materialLabel125.TabIndex = 3;
            materialLabel125.Text = "...";
            // 
            // materialLabel126
            // 
            materialLabel126.AutoSize = true;
            materialLabel126.Depth = 0;
            materialLabel126.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel126.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel126.Location = new Point(3, 60);
            materialLabel126.Margin = new Padding(3, 10, 3, 10);
            materialLabel126.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel126.Name = "materialLabel126";
            materialLabel126.Size = new Size(60, 14);
            materialLabel126.TabIndex = 2;
            materialLabel126.Text = "User Agent";
            // 
            // materialLabel127
            // 
            materialLabel127.Depth = 0;
            materialLabel127.Dock = DockStyle.Fill;
            materialLabel127.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel127.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel127.Location = new Point(73, 10);
            materialLabel127.Margin = new Padding(3, 10, 3, 10);
            materialLabel127.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel127.Name = "materialLabel127";
            materialLabel127.Size = new Size(168, 30);
            materialLabel127.TabIndex = 1;
            materialLabel127.Text = "...";
            // 
            // materialLabel128
            // 
            materialLabel128.AutoSize = true;
            materialLabel128.Depth = 0;
            materialLabel128.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel128.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel128.Location = new Point(3, 10);
            materialLabel128.Margin = new Padding(3, 10, 3, 10);
            materialLabel128.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel128.Name = "materialLabel128";
            materialLabel128.Size = new Size(50, 14);
            materialLabel128.TabIndex = 0;
            materialLabel128.Text = "Profile ID";
            // 
            // materialLabel129
            // 
            materialLabel129.AutoSize = true;
            materialLabel129.Depth = 0;
            materialLabel129.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel129.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel129.Location = new Point(3, 210);
            materialLabel129.Margin = new Padding(3, 10, 3, 10);
            materialLabel129.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel129.Name = "materialLabel129";
            materialLabel129.Size = new Size(54, 14);
            materialLabel129.TabIndex = 8;
            materialLabel129.Text = "Facebook";
            // 
            // materialLabel130
            // 
            materialLabel130.AutoSize = true;
            materialLabel130.Depth = 0;
            materialLabel130.Dock = DockStyle.Fill;
            materialLabel130.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel130.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel130.Location = new Point(73, 210);
            materialLabel130.Margin = new Padding(3, 10, 3, 10);
            materialLabel130.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel130.Name = "materialLabel130";
            materialLabel130.Size = new Size(168, 30);
            materialLabel130.TabIndex = 9;
            materialLabel130.Text = "...";
            // 
            // materialLabel131
            // 
            materialLabel131.AutoSize = true;
            materialLabel131.Depth = 0;
            materialLabel131.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel131.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel131.Location = new Point(3, 260);
            materialLabel131.Margin = new Padding(3, 10, 3, 10);
            materialLabel131.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel131.Name = "materialLabel131";
            materialLabel131.Size = new Size(39, 14);
            materialLabel131.TabIndex = 10;
            materialLabel131.Text = "Google";
            // 
            // materialLabel132
            // 
            materialLabel132.AutoSize = true;
            materialLabel132.Depth = 0;
            materialLabel132.Dock = DockStyle.Fill;
            materialLabel132.Font = new Font("Roboto", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel132.FontType = MaterialSkin.MaterialSkinManager.fontType.Caption;
            materialLabel132.Location = new Point(73, 260);
            materialLabel132.Margin = new Padding(3, 10, 3, 10);
            materialLabel132.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel132.Name = "materialLabel132";
            materialLabel132.Size = new Size(168, 30);
            materialLabel132.TabIndex = 11;
            materialLabel132.Text = "...";
            // 
            // panel44
            // 
            panel44.AutoScroll = true;
            panel44.Controls.Add(tableLayoutPanel86);
            panel44.Dock = DockStyle.Fill;
            panel44.Location = new Point(0, 52);
            panel44.Margin = new Padding(0, 12, 0, 0);
            panel44.Name = "panel44";
            panel44.Size = new Size(244, 800);
            panel44.TabIndex = 1;
            // 
            // materialLabel133
            // 
            materialLabel133.Depth = 0;
            materialLabel133.Dock = DockStyle.Fill;
            materialLabel133.Font = new Font("Roboto", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel133.FontType = MaterialSkin.MaterialSkinManager.fontType.Button;
            materialLabel133.Location = new Point(53, 5);
            materialLabel133.Margin = new Padding(5);
            materialLabel133.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel133.Name = "materialLabel133";
            materialLabel133.Size = new Size(186, 30);
            materialLabel133.TabIndex = 1;
            materialLabel133.Text = "Profile Info";
            materialLabel133.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialCard44
            // 
            materialCard44.BackColor = Color.FromArgb(255, 255, 255);
            materialCard44.Controls.Add(tableLayoutPanel87);
            materialCard44.Depth = 0;
            materialCard44.Dock = DockStyle.Fill;
            materialCard44.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard44.Location = new Point(10, 10);
            materialCard44.Margin = new Padding(10);
            materialCard44.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard44.Name = "materialCard44";
            materialCard44.Padding = new Padding(5);
            materialCard44.Size = new Size(1746, 55);
            materialCard44.TabIndex = 0;
            // 
            // tableLayoutPanel87
            // 
            tableLayoutPanel87.ColumnCount = 4;
            tableLayoutPanel87.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 240F));
            tableLayoutPanel87.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel87.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 120F));
            tableLayoutPanel87.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 130F));
            tableLayoutPanel87.Controls.Add(materialButton24, 3, 0);
            tableLayoutPanel87.Controls.Add(materialButton25, 2, 0);
            tableLayoutPanel87.Controls.Add(materialComboBox4, 0, 0);
            tableLayoutPanel87.Dock = DockStyle.Fill;
            tableLayoutPanel87.Location = new Point(5, 5);
            tableLayoutPanel87.Margin = new Padding(0);
            tableLayoutPanel87.Name = "tableLayoutPanel87";
            tableLayoutPanel87.RowCount = 1;
            tableLayoutPanel87.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel87.Size = new Size(1736, 45);
            tableLayoutPanel87.TabIndex = 0;
            // 
            // materialButton24
            // 
            materialButton24.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton24.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton24.Depth = 0;
            materialButton24.Dock = DockStyle.Fill;
            materialButton24.HighEmphasis = true;
            materialButton24.Icon = null;
            materialButton24.Location = new Point(1611, 5);
            materialButton24.Margin = new Padding(5);
            materialButton24.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton24.Name = "materialButton24";
            materialButton24.NoAccentTextColor = Color.Empty;
            materialButton24.Size = new Size(120, 35);
            materialButton24.TabIndex = 0;
            materialButton24.Text = "Create Profiles";
            materialButton24.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton24.UseAccentColor = false;
            materialButton24.UseVisualStyleBackColor = true;
            materialButton24.Click += ButtonCreateProfiles_Click;
            // 
            // materialButton25
            // 
            materialButton25.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton25.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton25.Depth = 0;
            materialButton25.Dock = DockStyle.Fill;
            materialButton25.HighEmphasis = true;
            materialButton25.Icon = null;
            materialButton25.Location = new Point(1491, 5);
            materialButton25.Margin = new Padding(5);
            materialButton25.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton25.Name = "materialButton25";
            materialButton25.NoAccentTextColor = Color.Empty;
            materialButton25.Size = new Size(110, 35);
            materialButton25.TabIndex = 1;
            materialButton25.Text = "Create Group";
            materialButton25.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton25.UseAccentColor = false;
            materialButton25.UseVisualStyleBackColor = true;
            materialButton25.Click += ButtonCreateGroup_Click;
            // 
            // materialComboBox4
            // 
            materialComboBox4.AutoResize = false;
            materialComboBox4.BackColor = Color.FromArgb(255, 255, 255);
            materialComboBox4.Depth = 0;
            materialComboBox4.Dock = DockStyle.Fill;
            materialComboBox4.DrawMode = DrawMode.OwnerDrawVariable;
            materialComboBox4.DropDownHeight = 174;
            materialComboBox4.DropDownStyle = ComboBoxStyle.DropDownList;
            materialComboBox4.DropDownWidth = 121;
            materialComboBox4.Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialComboBox4.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialComboBox4.FormattingEnabled = true;
            materialComboBox4.IntegralHeight = false;
            materialComboBox4.ItemHeight = 43;
            materialComboBox4.Location = new Point(3, 3);
            materialComboBox4.MaxDropDownItems = 4;
            materialComboBox4.MouseState = MaterialSkin.MouseState.OUT;
            materialComboBox4.Name = "materialComboBox4";
            materialComboBox4.Size = new Size(234, 49);
            materialComboBox4.StartIndex = 0;
            materialComboBox4.TabIndex = 2;
            materialComboBox4.SelectedIndexChanged += filterByGroup_SelectedIndexChanged;
            // 
            // panel45
            // 
            panel45.BackgroundImage = Properties.Resources.fingerprint;
            panel45.BackgroundImageLayout = ImageLayout.Zoom;
            panel45.Location = new Point(6, 6);
            panel45.Margin = new Padding(6);
            panel45.Name = "panel45";
            panel45.Size = new Size(36, 28);
            panel45.TabIndex = 0;
            // 
            // tableLayoutPanel88
            // 
            tableLayoutPanel88.ColumnCount = 2;
            tableLayoutPanel88.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 48F));
            tableLayoutPanel88.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel88.Controls.Add(panel45, 0, 0);
            tableLayoutPanel88.Controls.Add(materialLabel133, 1, 0);
            tableLayoutPanel88.Dock = DockStyle.Fill;
            tableLayoutPanel88.Location = new Point(0, 0);
            tableLayoutPanel88.Margin = new Padding(0);
            tableLayoutPanel88.Name = "tableLayoutPanel88";
            tableLayoutPanel88.RowCount = 1;
            tableLayoutPanel88.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel88.Size = new Size(244, 40);
            tableLayoutPanel88.TabIndex = 0;
            // 
            // tableLayoutPanel89
            // 
            tableLayoutPanel89.ColumnCount = 1;
            tableLayoutPanel89.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel89.Controls.Add(tableLayoutPanel88, 0, 0);
            tableLayoutPanel89.Controls.Add(panel44, 0, 1);
            tableLayoutPanel89.Dock = DockStyle.Fill;
            tableLayoutPanel89.Location = new Point(14, 14);
            tableLayoutPanel89.Margin = new Padding(0);
            tableLayoutPanel89.Name = "tableLayoutPanel89";
            tableLayoutPanel89.RowCount = 2;
            tableLayoutPanel89.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel89.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel89.Size = new Size(244, 852);
            tableLayoutPanel89.TabIndex = 0;
            // 
            // materialCard45
            // 
            materialCard45.BackColor = Color.FromArgb(255, 255, 255);
            materialCard45.Controls.Add(tableLayoutPanel89);
            materialCard45.Depth = 0;
            materialCard45.Dock = DockStyle.Fill;
            materialCard45.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard45.Location = new Point(1480, 2);
            materialCard45.Margin = new Padding(14, 2, 14, 14);
            materialCard45.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard45.Name = "materialCard45";
            materialCard45.Padding = new Padding(14);
            materialCard45.Size = new Size(272, 880);
            materialCard45.TabIndex = 2;
            // 
            // tableLayoutPanel90
            // 
            tableLayoutPanel90.ColumnCount = 2;
            tableLayoutPanel90.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel90.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 300F));
            tableLayoutPanel90.Controls.Add(materialCard45, 1, 0);
            tableLayoutPanel90.Controls.Add(tableLayoutPanel91, 0, 0);
            tableLayoutPanel90.Dock = DockStyle.Fill;
            tableLayoutPanel90.Location = new Point(0, 75);
            tableLayoutPanel90.Margin = new Padding(0);
            tableLayoutPanel90.Name = "tableLayoutPanel90";
            tableLayoutPanel90.RowCount = 1;
            tableLayoutPanel90.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel90.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel90.Size = new Size(1766, 896);
            tableLayoutPanel90.TabIndex = 1;
            // 
            // tableLayoutPanel91
            // 
            tableLayoutPanel91.ColumnCount = 1;
            tableLayoutPanel91.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel91.Controls.Add(materialCard46, 0, 1);
            tableLayoutPanel91.Controls.Add(materialCard43, 0, 0);
            tableLayoutPanel91.Dock = DockStyle.Fill;
            tableLayoutPanel91.Location = new Point(0, 0);
            tableLayoutPanel91.Margin = new Padding(0);
            tableLayoutPanel91.Name = "tableLayoutPanel91";
            tableLayoutPanel91.RowCount = 2;
            tableLayoutPanel91.RowStyles.Add(new RowStyle(SizeType.Percent, 66.7263F));
            tableLayoutPanel91.RowStyles.Add(new RowStyle(SizeType.Percent, 33.2737F));
            tableLayoutPanel91.Size = new Size(1466, 896);
            tableLayoutPanel91.TabIndex = 3;
            // 
            // materialCard46
            // 
            materialCard46.BackColor = Color.FromArgb(255, 255, 255);
            materialCard46.Controls.Add(dataGridView9);
            materialCard46.Depth = 0;
            materialCard46.Dock = DockStyle.Fill;
            materialCard46.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard46.Location = new Point(14, 599);
            materialCard46.Margin = new Padding(14, 2, 2, 14);
            materialCard46.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard46.Name = "materialCard46";
            materialCard46.Padding = new Padding(14);
            materialCard46.Size = new Size(1450, 283);
            materialCard46.TabIndex = 3;
            // 
            // dataGridView9
            // 
            dataGridView9.AllowUserToAddRows = false;
            dataGridView9.AllowUserToDeleteRows = false;
            dataGridView9.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView9.BackgroundColor = SystemColors.ButtonHighlight;
            dataGridView9.BorderStyle = BorderStyle.None;
            dataGridViewCellStyle25.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = SystemColors.Control;
            dataGridViewCellStyle25.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle25.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle25.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle25.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle25.WrapMode = DataGridViewTriState.True;
            dataGridView9.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            dataGridView9.ColumnHeadersHeight = 40;
            dataGridView9.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridView9.Columns.AddRange(new DataGridViewColumn[] { dataGridViewTextBoxColumn31, dataGridViewTextBoxColumn32 });
            dataGridView9.Cursor = Cursors.Hand;
            dataGridViewCellStyle26.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = SystemColors.Window;
            dataGridViewCellStyle26.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle26.ForeColor = Color.FromArgb(222, 0, 0, 0);
            dataGridViewCellStyle26.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle26.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = DataGridViewTriState.False;
            dataGridView9.DefaultCellStyle = dataGridViewCellStyle26;
            dataGridView9.Dock = DockStyle.Fill;
            dataGridView9.Location = new Point(14, 14);
            dataGridView9.Name = "dataGridView9";
            dataGridViewCellStyle27.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = SystemColors.Control;
            dataGridViewCellStyle27.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle27.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle27.SelectionBackColor = Color.RoyalBlue;
            dataGridViewCellStyle27.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle27.WrapMode = DataGridViewTriState.True;
            dataGridView9.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            dataGridView9.RowHeadersVisible = false;
            dataGridView9.RowHeadersWidth = 50;
            dataGridView9.RowTemplate.Height = 42;
            dataGridView9.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView9.Size = new Size(1422, 255);
            dataGridView9.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn31
            // 
            dataGridViewTextBoxColumn31.DataPropertyName = "group_name";
            dataGridViewTextBoxColumn31.FillWeight = 109.817F;
            dataGridViewTextBoxColumn31.HeaderText = "Group Name";
            dataGridViewTextBoxColumn31.MinimumWidth = 6;
            dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            // 
            // dataGridViewTextBoxColumn32
            // 
            dataGridViewTextBoxColumn32.DataPropertyName = "group_total";
            dataGridViewTextBoxColumn32.FillWeight = 42.66524F;
            dataGridViewTextBoxColumn32.HeaderText = "Total Profiles";
            dataGridViewTextBoxColumn32.MinimumWidth = 6;
            dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            // 
            // tableLayoutPanel92
            // 
            tableLayoutPanel92.ColumnCount = 1;
            tableLayoutPanel92.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel92.Controls.Add(materialCard44, 0, 0);
            tableLayoutPanel92.Controls.Add(tableLayoutPanel90, 0, 1);
            tableLayoutPanel92.Dock = DockStyle.Fill;
            tableLayoutPanel92.Location = new Point(3, 3);
            tableLayoutPanel92.Name = "tableLayoutPanel92";
            tableLayoutPanel92.RowCount = 2;
            tableLayoutPanel92.RowStyles.Add(new RowStyle(SizeType.Absolute, 75F));
            tableLayoutPanel92.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel92.Size = new Size(1766, 971);
            tableLayoutPanel92.TabIndex = 1;
            // 
            // tabPage7
            // 
            tabPage7.Controls.Add(tableLayoutPanel92);
            tabPage7.ImageKey = "browser.png";
            tabPage7.Location = new Point(4, 32);
            tabPage7.Name = "tabPage7";
            tabPage7.Padding = new Padding(3);
            tabPage7.Size = new Size(1772, 977);
            tabPage7.TabIndex = 1;
            tabPage7.Text = "Profiles";
            tabPage7.UseVisualStyleBackColor = true;
            // 
            // materialTabControl4
            // 
            materialTabControl4.Appearance = TabAppearance.Buttons;
            materialTabControl4.Controls.Add(tabPage7);
            materialTabControl4.Controls.Add(tabPage6);
            materialTabControl4.Controls.Add(tabPage8);
            materialTabControl4.Controls.Add(tabPage4);
            materialTabControl4.Controls.Add(tabPage9);
            materialTabControl4.Depth = 0;
            materialTabControl4.Dock = DockStyle.Fill;
            materialTabControl4.ImageList = imageList1;
            materialTabControl4.Location = new Point(3, 64);
            materialTabControl4.MouseState = MaterialSkin.MouseState.HOVER;
            materialTabControl4.Multiline = true;
            materialTabControl4.Name = "materialTabControl4";
            materialTabControl4.SelectedIndex = 0;
            materialTabControl4.Size = new Size(794, 533);
            materialTabControl4.TabIndex = 1;
            // 
            // tabPage8
            // 
            tabPage8.Controls.Add(tableLayoutPanel93);
            tabPage8.ImageKey = "network-cloud.png";
            tabPage8.Location = new Point(4, 32);
            tabPage8.Name = "tabPage8";
            tabPage8.Size = new Size(1772, 977);
            tabPage8.TabIndex = 5;
            tabPage8.Text = "Recorded ";
            tabPage8.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel93
            // 
            tableLayoutPanel93.ColumnCount = 1;
            tableLayoutPanel93.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel93.Controls.Add(materialCard47, 0, 0);
            tableLayoutPanel93.Controls.Add(materialCard48, 0, 1);
            tableLayoutPanel93.Dock = DockStyle.Fill;
            tableLayoutPanel93.Location = new Point(0, 0);
            tableLayoutPanel93.Name = "tableLayoutPanel93";
            tableLayoutPanel93.RowCount = 3;
            tableLayoutPanel93.RowStyles.Add(new RowStyle(SizeType.Absolute, 75F));
            tableLayoutPanel93.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel93.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel93.Size = new Size(1772, 977);
            tableLayoutPanel93.TabIndex = 0;
            // 
            // materialCard47
            // 
            materialCard47.BackColor = Color.FromArgb(255, 255, 255);
            materialCard47.Controls.Add(tableLayoutPanel94);
            materialCard47.Depth = 0;
            materialCard47.Dock = DockStyle.Fill;
            materialCard47.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard47.Location = new Point(8, 8);
            materialCard47.Margin = new Padding(8);
            materialCard47.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard47.Name = "materialCard47";
            materialCard47.Padding = new Padding(4);
            materialCard47.Size = new Size(1756, 59);
            materialCard47.TabIndex = 0;
            // 
            // tableLayoutPanel94
            // 
            tableLayoutPanel94.ColumnCount = 4;
            tableLayoutPanel94.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel94.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel94.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel94.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 80F));
            tableLayoutPanel94.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
            tableLayoutPanel94.Controls.Add(materialButton26, 3, 0);
            tableLayoutPanel94.Controls.Add(materialButton27, 2, 0);
            tableLayoutPanel94.Controls.Add(materialButton28, 1, 0);
            tableLayoutPanel94.Dock = DockStyle.Fill;
            tableLayoutPanel94.Location = new Point(4, 4);
            tableLayoutPanel94.Margin = new Padding(2);
            tableLayoutPanel94.Name = "tableLayoutPanel94";
            tableLayoutPanel94.RowCount = 1;
            tableLayoutPanel94.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel94.Size = new Size(1748, 51);
            tableLayoutPanel94.TabIndex = 0;
            // 
            // materialButton26
            // 
            materialButton26.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton26.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton26.Depth = 0;
            materialButton26.Dock = DockStyle.Fill;
            materialButton26.Enabled = false;
            materialButton26.HighEmphasis = true;
            materialButton26.Icon = null;
            materialButton26.Location = new Point(1673, 5);
            materialButton26.Margin = new Padding(5);
            materialButton26.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton26.Name = "materialButton26";
            materialButton26.NoAccentTextColor = Color.Empty;
            materialButton26.Size = new Size(70, 41);
            materialButton26.TabIndex = 1;
            materialButton26.Text = "Stop";
            materialButton26.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton26.UseAccentColor = false;
            materialButton26.UseVisualStyleBackColor = true;
            materialButton26.Click += stopRecord_Click;
            // 
            // materialButton27
            // 
            materialButton27.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton27.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton27.Depth = 0;
            materialButton27.Dock = DockStyle.Fill;
            materialButton27.Enabled = false;
            materialButton27.HighEmphasis = true;
            materialButton27.Icon = null;
            materialButton27.Location = new Point(1593, 5);
            materialButton27.Margin = new Padding(5);
            materialButton27.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton27.Name = "materialButton27";
            materialButton27.NoAccentTextColor = Color.Empty;
            materialButton27.Size = new Size(70, 41);
            materialButton27.TabIndex = 2;
            materialButton27.Text = "Pause";
            materialButton27.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton27.UseAccentColor = false;
            materialButton27.UseVisualStyleBackColor = true;
            materialButton27.Click += pauseRecord_Click;
            // 
            // materialButton28
            // 
            materialButton28.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton28.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton28.Depth = 0;
            materialButton28.Dock = DockStyle.Fill;
            materialButton28.HighEmphasis = true;
            materialButton28.Icon = null;
            materialButton28.Location = new Point(1513, 5);
            materialButton28.Margin = new Padding(5);
            materialButton28.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton28.Name = "materialButton28";
            materialButton28.NoAccentTextColor = Color.Empty;
            materialButton28.Size = new Size(70, 41);
            materialButton28.TabIndex = 0;
            materialButton28.Text = "Record";
            materialButton28.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton28.UseAccentColor = false;
            materialButton28.UseVisualStyleBackColor = true;
            materialButton28.Click += startRecord_Click;
            // 
            // materialCard48
            // 
            materialCard48.BackColor = Color.FromArgb(255, 255, 255);
            materialCard48.Controls.Add(tableLayoutPanel95);
            materialCard48.Depth = 0;
            materialCard48.Dock = DockStyle.Fill;
            materialCard48.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard48.Location = new Point(14, 89);
            materialCard48.Margin = new Padding(14);
            materialCard48.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard48.Name = "materialCard48";
            materialCard48.Padding = new Padding(14);
            materialCard48.Size = new Size(1744, 824);
            materialCard48.TabIndex = 2;
            // 
            // tableLayoutPanel95
            // 
            tableLayoutPanel95.ColumnCount = 1;
            tableLayoutPanel95.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel95.Controls.Add(materialButton29, 0, 0);
            tableLayoutPanel95.Controls.Add(dataGridView10, 0, 2);
            tableLayoutPanel95.Controls.Add(panel46, 0, 1);
            tableLayoutPanel95.Dock = DockStyle.Fill;
            tableLayoutPanel95.Location = new Point(14, 14);
            tableLayoutPanel95.Name = "tableLayoutPanel95";
            tableLayoutPanel95.RowCount = 3;
            tableLayoutPanel95.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel95.RowStyles.Add(new RowStyle());
            tableLayoutPanel95.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel95.Size = new Size(1716, 796);
            tableLayoutPanel95.TabIndex = 0;
            // 
            // materialButton29
            // 
            materialButton29.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton29.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton29.Depth = 0;
            materialButton29.HighEmphasis = true;
            materialButton29.Icon = null;
            materialButton29.Location = new Point(4, 6);
            materialButton29.Margin = new Padding(4, 6, 4, 6);
            materialButton29.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton29.Name = "materialButton29";
            materialButton29.NoAccentTextColor = Color.Empty;
            materialButton29.Size = new Size(81, 36);
            materialButton29.TabIndex = 0;
            materialButton29.Text = "Setting";
            materialButton29.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton29.UseAccentColor = false;
            materialButton29.UseVisualStyleBackColor = true;
            materialButton29.Click += BtnSettingRecord_Click;
            // 
            // dataGridView10
            // 
            dataGridView10.AllowUserToAddRows = false;
            dataGridView10.AllowUserToDeleteRows = false;
            dataGridView10.AllowUserToResizeRows = false;
            dataGridView10.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView10.BackgroundColor = SystemColors.ButtonHighlight;
            dataGridView10.BorderStyle = BorderStyle.None;
            dataGridView10.ColumnHeadersHeight = 40;
            dataGridView10.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridView10.Columns.AddRange(new DataGridViewColumn[] { dataGridViewTextBoxColumn33, dataGridViewTextBoxColumn34, dataGridViewTextBoxColumn35, dataGridViewTextBoxColumn36 });
            dataGridView10.Cursor = Cursors.Hand;
            dataGridView10.Dock = DockStyle.Fill;
            dataGridView10.Location = new Point(3, 58);
            dataGridView10.Name = "dataGridView10";
            dataGridView10.RowHeadersVisible = false;
            dataGridView10.RowHeadersWidth = 51;
            dataGridView10.RowTemplate.Height = 40;
            dataGridView10.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView10.Size = new Size(1710, 735);
            dataGridView10.TabIndex = 0;
            dataGridView10.CellEndEdit += CellContentedEditFInish;
            // 
            // dataGridViewTextBoxColumn33
            // 
            dataGridViewTextBoxColumn33.DataPropertyName = "record_name";
            dataGridViewTextBoxColumn33.HeaderText = "Name";
            dataGridViewTextBoxColumn33.MinimumWidth = 6;
            dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            // 
            // dataGridViewTextBoxColumn34
            // 
            dataGridViewTextBoxColumn34.DataPropertyName = "record_time";
            dataGridViewTextBoxColumn34.HeaderText = "Created time";
            dataGridViewTextBoxColumn34.MinimumWidth = 6;
            dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            dataGridViewTextBoxColumn34.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn35
            // 
            dataGridViewTextBoxColumn35.DataPropertyName = "record_last_modify";
            dataGridViewTextBoxColumn35.HeaderText = "Last modify";
            dataGridViewTextBoxColumn35.MinimumWidth = 6;
            dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            dataGridViewTextBoxColumn35.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn36
            // 
            dataGridViewTextBoxColumn36.DataPropertyName = "record_link";
            dataGridViewTextBoxColumn36.HeaderText = "Link";
            dataGridViewTextBoxColumn36.MinimumWidth = 6;
            dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            dataGridViewTextBoxColumn36.ReadOnly = true;
            // 
            // panel46
            // 
            panel46.BackColor = Color.WhiteSmoke;
            panel46.Controls.Add(materialLabel134);
            panel46.Controls.Add(textBox1);
            panel46.Controls.Add(materialLabel135);
            panel46.Controls.Add(label18);
            panel46.Controls.Add(numericUpDown1);
            panel46.Controls.Add(materialRadioButton1);
            panel46.Controls.Add(materialRadioButton2);
            panel46.Controls.Add(materialLabel136);
            panel46.Dock = DockStyle.Fill;
            panel46.Location = new Point(2, 52);
            panel46.Margin = new Padding(2);
            panel46.Name = "panel46";
            panel46.Size = new Size(1712, 1);
            panel46.TabIndex = 1;
            // 
            // materialLabel134
            // 
            materialLabel134.AutoSize = true;
            materialLabel134.Depth = 0;
            materialLabel134.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel134.Location = new Point(194, 82);
            materialLabel134.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel134.Name = "materialLabel134";
            materialLabel134.Size = new Size(25, 19);
            materialLabel134.TabIndex = 8;
            materialLabel134.Text = "sec";
            // 
            // textBox1
            // 
            textBox1.Location = new Point(132, 77);
            textBox1.Name = "textBox1";
            textBox1.Size = new Size(56, 27);
            textBox1.TabIndex = 7;
            textBox1.Text = "0";
            textBox1.KeyPress += onlyNumber_KeyPress;
            // 
            // materialLabel135
            // 
            materialLabel135.AutoSize = true;
            materialLabel135.Depth = 0;
            materialLabel135.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel135.Location = new Point(14, 82);
            materialLabel135.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel135.Name = "materialLabel135";
            materialLabel135.Size = new Size(101, 19);
            materialLabel135.TabIndex = 6;
            materialLabel135.Text = "Loop interval :";
            // 
            // label18
            // 
            label18.Anchor = AnchorStyles.Left;
            label18.AutoSize = true;
            label18.Location = new Point(259, -156);
            label18.Name = "label18";
            label18.Size = new Size(45, 20);
            label18.TabIndex = 5;
            label18.Text = "times";
            // 
            // numericUpDown1
            // 
            numericUpDown1.Location = new Point(205, 12);
            numericUpDown1.Maximum = new decimal(new int[] { 50, 0, 0, 0 });
            numericUpDown1.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            numericUpDown1.Name = "numericUpDown1";
            numericUpDown1.Size = new Size(50, 27);
            numericUpDown1.TabIndex = 4;
            numericUpDown1.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // materialRadioButton1
            // 
            materialRadioButton1.AutoSize = true;
            materialRadioButton1.Depth = 0;
            materialRadioButton1.Location = new Point(124, 35);
            materialRadioButton1.Margin = new Padding(0);
            materialRadioButton1.MouseLocation = new Point(-1, -1);
            materialRadioButton1.MouseState = MaterialSkin.MouseState.HOVER;
            materialRadioButton1.Name = "materialRadioButton1";
            materialRadioButton1.Ripple = true;
            materialRadioButton1.Size = new Size(102, 37);
            materialRadioButton1.TabIndex = 2;
            materialRadioButton1.TabStop = true;
            materialRadioButton1.Text = "Until stop";
            materialRadioButton1.UseVisualStyleBackColor = true;
            // 
            // materialRadioButton2
            // 
            materialRadioButton2.AutoSize = true;
            materialRadioButton2.Checked = true;
            materialRadioButton2.Depth = 0;
            materialRadioButton2.Location = new Point(124, 6);
            materialRadioButton2.Margin = new Padding(0);
            materialRadioButton2.MouseLocation = new Point(-1, -1);
            materialRadioButton2.MouseState = MaterialSkin.MouseState.HOVER;
            materialRadioButton2.Name = "materialRadioButton2";
            materialRadioButton2.Ripple = true;
            materialRadioButton2.Size = new Size(71, 37);
            materialRadioButton2.TabIndex = 1;
            materialRadioButton2.TabStop = true;
            materialRadioButton2.Text = "Loop";
            materialRadioButton2.UseVisualStyleBackColor = true;
            // 
            // materialLabel136
            // 
            materialLabel136.AutoSize = true;
            materialLabel136.Depth = 0;
            materialLabel136.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel136.Location = new Point(14, 15);
            materialLabel136.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel136.Name = "materialLabel136";
            materialLabel136.Size = new Size(58, 19);
            materialLabel136.TabIndex = 0;
            materialLabel136.Text = "Repeat :";
            // 
            // tabPage9
            // 
            tabPage9.Controls.Add(tableLayoutPanel96);
            tabPage9.ImageKey = "settings-sliders.png";
            tabPage9.Location = new Point(4, 32);
            tabPage9.Name = "tabPage9";
            tabPage9.Size = new Size(1772, 977);
            tabPage9.TabIndex = 3;
            tabPage9.Text = "Setting";
            tabPage9.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel96
            // 
            tableLayoutPanel96.ColumnCount = 1;
            tableLayoutPanel96.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel96.Controls.Add(materialCard49, 0, 0);
            tableLayoutPanel96.Controls.Add(flowLayoutPanel4, 0, 1);
            tableLayoutPanel96.Dock = DockStyle.Fill;
            tableLayoutPanel96.Location = new Point(0, 0);
            tableLayoutPanel96.Name = "tableLayoutPanel96";
            tableLayoutPanel96.RowCount = 2;
            tableLayoutPanel96.RowStyles.Add(new RowStyle(SizeType.Absolute, 79F));
            tableLayoutPanel96.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel96.Size = new Size(1772, 977);
            tableLayoutPanel96.TabIndex = 1;
            // 
            // materialCard49
            // 
            materialCard49.BackColor = Color.FromArgb(255, 255, 255);
            materialCard49.Controls.Add(tableLayoutPanel97);
            materialCard49.Depth = 0;
            materialCard49.Dock = DockStyle.Fill;
            materialCard49.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard49.Location = new Point(14, 14);
            materialCard49.Margin = new Padding(14);
            materialCard49.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard49.Name = "materialCard49";
            materialCard49.Padding = new Padding(4);
            materialCard49.Size = new Size(1744, 51);
            materialCard49.TabIndex = 0;
            // 
            // tableLayoutPanel97
            // 
            tableLayoutPanel97.ColumnCount = 2;
            tableLayoutPanel97.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel97.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 150F));
            tableLayoutPanel97.Controls.Add(materialButton30, 1, 0);
            tableLayoutPanel97.Dock = DockStyle.Fill;
            tableLayoutPanel97.Location = new Point(4, 4);
            tableLayoutPanel97.Margin = new Padding(0);
            tableLayoutPanel97.Name = "tableLayoutPanel97";
            tableLayoutPanel97.RowCount = 1;
            tableLayoutPanel97.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel97.Size = new Size(1736, 43);
            tableLayoutPanel97.TabIndex = 1;
            // 
            // materialButton30
            // 
            materialButton30.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton30.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton30.Depth = 0;
            materialButton30.Dock = DockStyle.Fill;
            materialButton30.HighEmphasis = true;
            materialButton30.Icon = null;
            materialButton30.Location = new Point(1586, 0);
            materialButton30.Margin = new Padding(0);
            materialButton30.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton30.Name = "materialButton30";
            materialButton30.NoAccentTextColor = Color.Empty;
            materialButton30.Size = new Size(150, 43);
            materialButton30.TabIndex = 0;
            materialButton30.Text = "Save Seting";
            materialButton30.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton30.UseAccentColor = false;
            materialButton30.UseVisualStyleBackColor = true;
            materialButton30.Click += SaveSetting_Click;
            // 
            // flowLayoutPanel4
            // 
            flowLayoutPanel4.AutoScroll = true;
            flowLayoutPanel4.Controls.Add(materialCard50);
            flowLayoutPanel4.Controls.Add(materialCard29);
            flowLayoutPanel4.Controls.Add(materialCard34);
            flowLayoutPanel4.Controls.Add(materialCard35);
            flowLayoutPanel4.Dock = DockStyle.Fill;
            flowLayoutPanel4.Location = new Point(3, 82);
            flowLayoutPanel4.Name = "flowLayoutPanel4";
            flowLayoutPanel4.Size = new Size(1766, 892);
            flowLayoutPanel4.TabIndex = 1;
            // 
            // materialCard50
            // 
            materialCard50.BackColor = Color.FromArgb(255, 255, 255);
            materialCard50.Controls.Add(tableLayoutPanel98);
            materialCard50.Depth = 0;
            materialCard50.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard50.Location = new Point(14, 14);
            materialCard50.Margin = new Padding(14);
            materialCard50.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard50.Name = "materialCard50";
            materialCard50.Padding = new Padding(14);
            materialCard50.Size = new Size(879, 118);
            materialCard50.TabIndex = 3;
            // 
            // tableLayoutPanel98
            // 
            tableLayoutPanel98.ColumnCount = 1;
            tableLayoutPanel98.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel98.Controls.Add(panel47, 0, 0);
            tableLayoutPanel98.Controls.Add(panel48, 0, 1);
            tableLayoutPanel98.Dock = DockStyle.Fill;
            tableLayoutPanel98.Location = new Point(14, 14);
            tableLayoutPanel98.Margin = new Padding(0);
            tableLayoutPanel98.Name = "tableLayoutPanel98";
            tableLayoutPanel98.RowCount = 2;
            tableLayoutPanel98.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel98.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel98.Size = new Size(851, 90);
            tableLayoutPanel98.TabIndex = 0;
            // 
            // panel47
            // 
            panel47.Controls.Add(materialLabel137);
            panel47.Controls.Add(materialButton31);
            panel47.Dock = DockStyle.Fill;
            panel47.Location = new Point(0, 0);
            panel47.Margin = new Padding(0);
            panel47.Name = "panel47";
            panel47.Size = new Size(851, 40);
            panel47.TabIndex = 1;
            // 
            // materialLabel137
            // 
            materialLabel137.Depth = 0;
            materialLabel137.Dock = DockStyle.Left;
            materialLabel137.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel137.Location = new Point(0, 0);
            materialLabel137.Margin = new Padding(12, 0, 12, 0);
            materialLabel137.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel137.Name = "materialLabel137";
            materialLabel137.Padding = new Padding(12);
            materialLabel137.Size = new Size(163, 40);
            materialLabel137.TabIndex = 0;
            materialLabel137.Text = "Folder Profiles";
            materialLabel137.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialButton31
            // 
            materialButton31.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton31.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton31.Depth = 0;
            materialButton31.Dock = DockStyle.Right;
            materialButton31.HighEmphasis = true;
            materialButton31.Icon = null;
            materialButton31.Location = new Point(745, 0);
            materialButton31.Margin = new Padding(4, 6, 4, 6);
            materialButton31.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton31.Name = "materialButton31";
            materialButton31.NoAccentTextColor = Color.Empty;
            materialButton31.Size = new Size(106, 40);
            materialButton31.TabIndex = 2;
            materialButton31.Text = "Select File";
            materialButton31.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton31.UseAccentColor = false;
            materialButton31.UseVisualStyleBackColor = true;
            materialButton31.Click += SelectFolderProfiles_Click;
            // 
            // panel48
            // 
            panel48.Controls.Add(materialLabel120);
            panel48.Controls.Add(materialLabel138);
            panel48.Dock = DockStyle.Fill;
            panel48.Location = new Point(0, 40);
            panel48.Margin = new Padding(0);
            panel48.Name = "panel48";
            panel48.Size = new Size(851, 50);
            panel48.TabIndex = 2;
            // 
            // materialLabel138
            // 
            materialLabel138.Depth = 0;
            materialLabel138.Dock = DockStyle.Top;
            materialLabel138.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            materialLabel138.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            materialLabel138.Location = new Point(0, 0);
            materialLabel138.Margin = new Padding(0);
            materialLabel138.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel138.Name = "materialLabel138";
            materialLabel138.Size = new Size(851, 35);
            materialLabel138.TabIndex = 0;
            materialLabel138.Text = "Path File...";
            materialLabel138.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            AutoScaleMode = AutoScaleMode.None;
            BackColor = SystemColors.ActiveBorder;
            ClientSize = new Size(1718, 1070);
            Controls.Add(materialTabControl1);
            Controls.Add(tableLayoutPanel53);
            Controls.Add(tableLayoutPanel55);
            Controls.Add(panel26);
            Controls.Add(materialTextBox24);
            Controls.Add(tableLayoutPanel66);
            Controls.Add(tableLayoutPanel69);
            Controls.Add(flowLayoutPanel3);
            Controls.Add(tableLayoutPanel76);
            DrawerShowIconsWhenHidden = true;
            DrawerTabControl = materialTabControl1;
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximumSize = new Size(1910, 1070);
            MinimumSize = new Size(800, 600);
            Name = "MainForm";
            StartPosition = FormStartPosition.CenterParent;
            Text = "Manager - SProfile";
            FormClosing += MainForm_FormClosing;
            materialTabControl1.ResumeLayout(false);
            tabProfiles.ResumeLayout(false);
            tableLayoutPanel2.ResumeLayout(false);
            materialCard2.ResumeLayout(false);
            tableLayoutPanel7.ResumeLayout(false);
            tableLayoutPanel7.PerformLayout();
            tableLayoutPanel5.ResumeLayout(false);
            materialCard6.ResumeLayout(false);
            tableLayoutPanel12.ResumeLayout(false);
            tableLayoutPanel13.ResumeLayout(false);
            panel10.ResumeLayout(false);
            tableLayoutPanel14.ResumeLayout(false);
            tableLayoutPanel14.PerformLayout();
            tableLayoutPanel1.ResumeLayout(false);
            materialCard1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)ListGroups).EndInit();
            MenuGroups.ResumeLayout(false);
            materialCard5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)ListProfiles).EndInit();
            MenuProfiles.ResumeLayout(false);
            tabSimulate.ResumeLayout(false);
            tableLayoutPanel34.ResumeLayout(false);
            materialCard22.ResumeLayout(false);
            tableLayoutPanel35.ResumeLayout(false);
            tableLayoutPanel35.PerformLayout();
            tableLayoutPanel32.ResumeLayout(false);
            tableLayoutPanel32.PerformLayout();
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            materialCard25.ResumeLayout(false);
            tableLayoutPanel37.ResumeLayout(false);
            tableLayoutPanel37.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dataSimulator).EndInit();
            MenuSimu.ResumeLayout(false);
            panelCollapsed.ResumeLayout(false);
            tableLayoutPanel38.ResumeLayout(false);
            tableLayoutPanel40.ResumeLayout(false);
            tableLayoutPanel40.PerformLayout();
            tableLayoutPanel41.ResumeLayout(false);
            tableLayoutPanel41.PerformLayout();
            tableLayoutPanel42.ResumeLayout(false);
            tableLayoutPanel42.PerformLayout();
            tableLayoutPanel39.ResumeLayout(false);
            tableLayoutPanel39.PerformLayout();
            tabRecord.ResumeLayout(false);
            tableLayoutPanel36.ResumeLayout(false);
            materialCard23.ResumeLayout(false);
            tableLayoutPanel43.ResumeLayout(false);
            tableLayoutPanel43.PerformLayout();
            materialCard26.ResumeLayout(false);
            tableLayoutPanel49.ResumeLayout(false);
            tableLayoutPanel49.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dataRecord).EndInit();
            MenuRecord.ResumeLayout(false);
            panelSettingRecord.ResumeLayout(false);
            panelSettingRecord.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)numericTimes).EndInit();
            tabRunRecord.ResumeLayout(false);
            tableLayoutPanel57.ResumeLayout(false);
            tableLayoutPanel57.PerformLayout();
            statusStrip2.ResumeLayout(false);
            statusStrip2.PerformLayout();
            materialCard30.ResumeLayout(false);
            tableLayoutPanel58.ResumeLayout(false);
            tableLayoutPanel58.PerformLayout();
            materialCard31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)RunRecordTable).EndInit();
            tabSetting.ResumeLayout(false);
            tableLayoutPanel4.ResumeLayout(false);
            materialCard4.ResumeLayout(false);
            tableLayoutPanel9.ResumeLayout(false);
            tableLayoutPanel9.PerformLayout();
            flowLayoutPanel1.ResumeLayout(false);
            materialCard10.ResumeLayout(false);
            tableLayoutPanel11.ResumeLayout(false);
            panel7.ResumeLayout(false);
            panel7.PerformLayout();
            panel8.ResumeLayout(false);
            materialCard7.ResumeLayout(false);
            tableLayoutPanel6.ResumeLayout(false);
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            panel2.ResumeLayout(false);
            materialCard8.ResumeLayout(false);
            tableLayoutPanel8.ResumeLayout(false);
            panel3.ResumeLayout(false);
            panel3.PerformLayout();
            panel4.ResumeLayout(false);
            materialCard21.ResumeLayout(false);
            tableLayoutPanel33.ResumeLayout(false);
            panel21.ResumeLayout(false);
            panel21.PerformLayout();
            panel22.ResumeLayout(false);
            panel23.ResumeLayout(false);
            tableLayoutPanel10.ResumeLayout(false);
            panel5.ResumeLayout(false);
            panel5.PerformLayout();
            panel6.ResumeLayout(false);
            tableLayoutPanel15.ResumeLayout(false);
            materialCard11.ResumeLayout(false);
            tableLayoutPanel16.ResumeLayout(false);
            tableLayoutPanel16.PerformLayout();
            tableLayoutPanel17.ResumeLayout(false);
            materialCard12.ResumeLayout(false);
            tableLayoutPanel18.ResumeLayout(false);
            tableLayoutPanel19.ResumeLayout(false);
            panel12.ResumeLayout(false);
            tableLayoutPanel20.ResumeLayout(false);
            tableLayoutPanel20.PerformLayout();
            materialCard13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            tabPage1.ResumeLayout(false);
            tableLayoutPanel3.ResumeLayout(false);
            materialCard3.ResumeLayout(false);
            tableLayoutPanel21.ResumeLayout(false);
            tableLayoutPanel21.PerformLayout();
            tableLayoutPanel22.ResumeLayout(false);
            materialCard14.ResumeLayout(false);
            tableLayoutPanel23.ResumeLayout(false);
            tableLayoutPanel24.ResumeLayout(false);
            panel14.ResumeLayout(false);
            tableLayoutPanel25.ResumeLayout(false);
            tableLayoutPanel25.PerformLayout();
            tableLayoutPanel26.ResumeLayout(false);
            materialCard15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dataGridView2).EndInit();
            materialCard16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dataGridView3).EndInit();
            tableLayoutPanel27.ResumeLayout(false);
            materialCard17.ResumeLayout(false);
            tableLayoutPanel28.ResumeLayout(false);
            tableLayoutPanel28.PerformLayout();
            flowLayoutPanel2.ResumeLayout(false);
            materialCard18.ResumeLayout(false);
            tableLayoutPanel29.ResumeLayout(false);
            panel15.ResumeLayout(false);
            panel15.PerformLayout();
            panel16.ResumeLayout(false);
            materialCard19.ResumeLayout(false);
            tableLayoutPanel30.ResumeLayout(false);
            panel17.ResumeLayout(false);
            panel17.PerformLayout();
            panel18.ResumeLayout(false);
            materialCard20.ResumeLayout(false);
            tableLayoutPanel31.ResumeLayout(false);
            panel19.ResumeLayout(false);
            panel19.PerformLayout();
            panel20.ResumeLayout(false);
            materialTabControl2.ResumeLayout(false);
            materialExpansionPanel2.ResumeLayout(false);
            materialExpansionPanel2.PerformLayout();
            tableLayoutPanel44.ResumeLayout(false);
            tableLayoutPanel45.ResumeLayout(false);
            tableLayoutPanel45.PerformLayout();
            tableLayoutPanel46.ResumeLayout(false);
            tableLayoutPanel46.PerformLayout();
            tableLayoutPanel47.ResumeLayout(false);
            tableLayoutPanel47.PerformLayout();
            tableLayoutPanel48.ResumeLayout(false);
            tableLayoutPanel48.PerformLayout();
            tableLayoutPanel50.ResumeLayout(false);
            tableLayoutPanel50.PerformLayout();
            tableLayoutPanel51.ResumeLayout(false);
            materialCard24.ResumeLayout(false);
            tableLayoutPanel52.ResumeLayout(false);
            panel25.ResumeLayout(false);
            tableLayoutPanel53.ResumeLayout(false);
            materialCard27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dataGridView4).EndInit();
            tableLayoutPanel54.ResumeLayout(false);
            tableLayoutPanel54.PerformLayout();
            materialCard28.ResumeLayout(false);
            tableLayoutPanel55.ResumeLayout(false);
            panel26.ResumeLayout(false);
            materialCard29.ResumeLayout(false);
            tableLayoutPanel56.ResumeLayout(false);
            panel27.ResumeLayout(false);
            panel27.PerformLayout();
            panel28.ResumeLayout(false);
            panel31.ResumeLayout(false);
            panel32.ResumeLayout(false);
            panel32.PerformLayout();
            tableLayoutPanel64.ResumeLayout(false);
            materialCard34.ResumeLayout(false);
            materialCard35.ResumeLayout(false);
            tableLayoutPanel65.ResumeLayout(false);
            panel33.ResumeLayout(false);
            panel33.PerformLayout();
            panel34.ResumeLayout(false);
            panel35.ResumeLayout(false);
            panel36.ResumeLayout(false);
            panel36.PerformLayout();
            tableLayoutPanel66.ResumeLayout(false);
            panel37.ResumeLayout(false);
            panel38.ResumeLayout(false);
            panel38.PerformLayout();
            tableLayoutPanel67.ResumeLayout(false);
            materialCard37.ResumeLayout(false);
            panel39.ResumeLayout(false);
            panel39.PerformLayout();
            panel40.ResumeLayout(false);
            tableLayoutPanel68.ResumeLayout(false);
            materialCard38.ResumeLayout(false);
            tableLayoutPanel69.ResumeLayout(false);
            tableLayoutPanel69.PerformLayout();
            panel41.ResumeLayout(false);
            tableLayoutPanel74.ResumeLayout(false);
            panel42.ResumeLayout(false);
            panel42.PerformLayout();
            materialCard39.ResumeLayout(false);
            flowLayoutPanel3.ResumeLayout(false);
            tableLayoutPanel75.ResumeLayout(false);
            tableLayoutPanel75.PerformLayout();
            materialCard40.ResumeLayout(false);
            tableLayoutPanel76.ResumeLayout(false);
            materialCard41.ResumeLayout(false);
            tableLayoutPanel77.ResumeLayout(false);
            tableLayoutPanel77.PerformLayout();
            tableLayoutPanel78.ResumeLayout(false);
            tableLayoutPanel79.ResumeLayout(false);
            tableLayoutPanel79.PerformLayout();
            statusStrip3.ResumeLayout(false);
            statusStrip3.PerformLayout();
            materialCard42.ResumeLayout(false);
            tableLayoutPanel80.ResumeLayout(false);
            tableLayoutPanel80.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView7).EndInit();
            panel43.ResumeLayout(false);
            tableLayoutPanel81.ResumeLayout(false);
            tableLayoutPanel82.ResumeLayout(false);
            tableLayoutPanel82.PerformLayout();
            tableLayoutPanel83.ResumeLayout(false);
            tableLayoutPanel83.PerformLayout();
            tableLayoutPanel84.ResumeLayout(false);
            tableLayoutPanel84.PerformLayout();
            tableLayoutPanel85.ResumeLayout(false);
            tableLayoutPanel85.PerformLayout();
            tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dataGridView8).EndInit();
            materialCard43.ResumeLayout(false);
            tableLayoutPanel86.ResumeLayout(false);
            tableLayoutPanel86.PerformLayout();
            panel44.ResumeLayout(false);
            materialCard44.ResumeLayout(false);
            tableLayoutPanel87.ResumeLayout(false);
            tableLayoutPanel87.PerformLayout();
            tableLayoutPanel88.ResumeLayout(false);
            tableLayoutPanel89.ResumeLayout(false);
            materialCard45.ResumeLayout(false);
            tableLayoutPanel90.ResumeLayout(false);
            tableLayoutPanel91.ResumeLayout(false);
            materialCard46.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dataGridView9).EndInit();
            tableLayoutPanel92.ResumeLayout(false);
            tabPage7.ResumeLayout(false);
            materialTabControl4.ResumeLayout(false);
            tabPage8.ResumeLayout(false);
            tableLayoutPanel93.ResumeLayout(false);
            materialCard47.ResumeLayout(false);
            tableLayoutPanel94.ResumeLayout(false);
            tableLayoutPanel94.PerformLayout();
            materialCard48.ResumeLayout(false);
            tableLayoutPanel95.ResumeLayout(false);
            tableLayoutPanel95.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView10).EndInit();
            panel46.ResumeLayout(false);
            panel46.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDown1).EndInit();
            tabPage9.ResumeLayout(false);
            tableLayoutPanel96.ResumeLayout(false);
            materialCard49.ResumeLayout(false);
            tableLayoutPanel97.ResumeLayout(false);
            tableLayoutPanel97.PerformLayout();
            flowLayoutPanel4.ResumeLayout(false);
            materialCard50.ResumeLayout(false);
            tableLayoutPanel98.ResumeLayout(false);
            panel47.ResumeLayout(false);
            panel47.PerformLayout();
            panel48.ResumeLayout(false);
            ResumeLayout(false);
        }

        private MaterialSkin.Controls.MaterialTabControl materialTabControl1;
        private ImageList IconSidebar;
        private TabPage tabProfiles;
        private TableLayoutPanel tableLayoutPanel2;
        private MaterialSkin.Controls.MaterialCard materialCard2;
        private TabPage tabSetting;
        private TableLayoutPanel tableLayoutPanel4;
        private MaterialSkin.Controls.MaterialCard materialCard4;
        private TableLayoutPanel tableLayoutPanel5;
        private DataGridViewTextBoxColumn Status;
        private DataGridViewTextBoxColumn Proxy;
        private MaterialSkin.Controls.MaterialCard materialCard6;
        private FlowLayoutPanel flowLayoutPanel1;
        private MaterialSkin.Controls.MaterialCard materialCard7;
        private TableLayoutPanel tableLayoutPanel6;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private TableLayoutPanel tableLayoutPanel7;
        private MaterialSkin.Controls.MaterialButton ButtonCreateProfiles;
        private ContextMenuStrip MenuProfiles;
        private ToolStripMenuItem EditProfile;
        private ToolStripMenuItem CloneProfile;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem DeleteProfile;
        private ToolStripMenuItem StartProfile;
        private Panel panel1;
        private MaterialSkin.Controls.MaterialButton SelectFileProxy;
        private Panel panel2;
        private MaterialSkin.Controls.MaterialLabel tFileProxy;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialCard materialCard8;
        private TableLayoutPanel tableLayoutPanel8;
        private Panel panel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialButton SelectFileName;
        private Panel panel4;
        private MaterialSkin.Controls.MaterialLabel materialLabel10;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialLabel tFileName;
        private TableLayoutPanel tableLayoutPanel9;
        private MaterialSkin.Controls.MaterialButton SaveSetting;
        private MaterialSkin.Controls.MaterialCard materialCard10;
        private TableLayoutPanel tableLayoutPanel11;
        private Panel panel7;
        private MaterialSkin.Controls.MaterialLabel materialLabel13;
        private MaterialSkin.Controls.MaterialButton SelectFolderProfiles;
        private Panel panel8;
        private MaterialSkin.Controls.MaterialLabel materialLabel15;
        private MaterialSkin.Controls.MaterialLabel tFolderProfiles;
        private MaterialSkin.Controls.MaterialCard materialCard9;
        private TableLayoutPanel tableLayoutPanel10;
        private Panel panel5;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialButton materialButton1;
        private Panel panel6;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialLabel materialLabel11;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private TableLayoutPanel tableLayoutPanel12;
        private TableLayoutPanel tableLayoutPanel13;
        private Panel panel9;
        private MaterialSkin.Controls.MaterialLabel materialLabel14;
        private Panel panel10;
        private TableLayoutPanel tableLayoutPanel14;
        private MaterialSkin.Controls.MaterialLabel iScreen;
        private MaterialSkin.Controls.MaterialLabel materialLabel22;
        private MaterialSkin.Controls.MaterialLabel iProxy;
        private MaterialSkin.Controls.MaterialLabel materialLabel20;
        private MaterialSkin.Controls.MaterialLabel iUserAgent;
        private MaterialSkin.Controls.MaterialLabel materialLabel18;
        private MaterialSkin.Controls.MaterialLabel iProfileID;
        private MaterialSkin.Controls.MaterialLabel materialLabel16;
        private MaterialSkin.Controls.MaterialLabel materialLabel24;
        private MaterialSkin.Controls.MaterialLabel iFacebook;
        private MaterialSkin.Controls.MaterialLabel materialLabel26;
        private MaterialSkin.Controls.MaterialLabel iGoogle;
        private TableLayoutPanel tableLayoutPanel15;
        private MaterialSkin.Controls.MaterialCard materialCard11;
        private TableLayoutPanel tableLayoutPanel16;
        private MaterialSkin.Controls.MaterialButton materialButton2;
        private TableLayoutPanel tableLayoutPanel17;
        private MaterialSkin.Controls.MaterialCard materialCard12;
        private TableLayoutPanel tableLayoutPanel18;
        private TableLayoutPanel tableLayoutPanel19;
        private Panel panel11;
        private MaterialSkin.Controls.MaterialLabel materialLabel17;
        private Panel panel12;
        private TableLayoutPanel tableLayoutPanel20;
        private MaterialSkin.Controls.MaterialLabel materialLabel19;
        private MaterialSkin.Controls.MaterialLabel materialLabel21;
        private MaterialSkin.Controls.MaterialLabel materialLabel23;
        private MaterialSkin.Controls.MaterialLabel materialLabel25;
        private MaterialSkin.Controls.MaterialLabel materialLabel27;
        private MaterialSkin.Controls.MaterialLabel materialLabel28;
        private MaterialSkin.Controls.MaterialLabel materialLabel29;
        private MaterialSkin.Controls.MaterialLabel materialLabel30;
        private MaterialSkin.Controls.MaterialLabel materialLabel31;
        private MaterialSkin.Controls.MaterialLabel materialLabel32;
        private MaterialSkin.Controls.MaterialLabel materialLabel33;
        private MaterialSkin.Controls.MaterialLabel materialLabel34;
        private MaterialSkin.Controls.MaterialCard materialCard13;
        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private MaterialSkin.Controls.MaterialButton ButtonCreateGroup;
        private TableLayoutPanel tableLayoutPanel1;
        private MaterialSkin.Controls.MaterialCard materialCard1;
        private DataGridView ListGroups;
        private MaterialSkin.Controls.MaterialCard materialCard5;
        private DataGridView ListProfiles;
        private DataGridViewTextBoxColumn profile_id;
        private DataGridViewTextBoxColumn profile_status;
        private DataGridViewTextBoxColumn profile_proxy;
        private DataGridViewTextBoxColumn profile_last_update;
        private DataGridViewTextBoxColumn group_name;
        private DataGridViewTextBoxColumn group_total;
        private MaterialSkin.Controls.MaterialComboBox filterByGroup;
        private TabPage tabSimulate;
        private TabPage tabPage1;
        private TableLayoutPanel tableLayoutPanel3;
        private MaterialSkin.Controls.MaterialCard materialCard3;
        private TableLayoutPanel tableLayoutPanel21;
        private MaterialSkin.Controls.MaterialButton materialButton3;
        private MaterialSkin.Controls.MaterialButton materialButton4;
        private MaterialSkin.Controls.MaterialComboBox materialComboBox1;
        private TableLayoutPanel tableLayoutPanel22;
        private MaterialSkin.Controls.MaterialCard materialCard14;
        private TableLayoutPanel tableLayoutPanel23;
        private TableLayoutPanel tableLayoutPanel24;
        private Panel panel13;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private Panel panel14;
        private TableLayoutPanel tableLayoutPanel25;
        private MaterialSkin.Controls.MaterialLabel materialLabel35;
        private MaterialSkin.Controls.MaterialLabel materialLabel36;
        private MaterialSkin.Controls.MaterialLabel materialLabel37;
        private MaterialSkin.Controls.MaterialLabel materialLabel38;
        private MaterialSkin.Controls.MaterialLabel materialLabel39;
        private MaterialSkin.Controls.MaterialLabel materialLabel40;
        private MaterialSkin.Controls.MaterialLabel materialLabel41;
        private MaterialSkin.Controls.MaterialLabel materialLabel42;
        private MaterialSkin.Controls.MaterialLabel materialLabel43;
        private MaterialSkin.Controls.MaterialLabel materialLabel44;
        private MaterialSkin.Controls.MaterialLabel materialLabel45;
        private MaterialSkin.Controls.MaterialLabel materialLabel46;
        private TableLayoutPanel tableLayoutPanel26;
        private MaterialSkin.Controls.MaterialCard materialCard15;
        private DataGridView dataGridView2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private MaterialSkin.Controls.MaterialCard materialCard16;
        private DataGridView dataGridView3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private TabPage tabPage2;
        private TableLayoutPanel tableLayoutPanel27;
        private MaterialSkin.Controls.MaterialCard materialCard17;
        private TableLayoutPanel tableLayoutPanel28;
        private MaterialSkin.Controls.MaterialButton materialButton5;
        private FlowLayoutPanel flowLayoutPanel2;
        private MaterialSkin.Controls.MaterialCard materialCard18;
        private TableLayoutPanel tableLayoutPanel29;
        private Panel panel15;
        private MaterialSkin.Controls.MaterialLabel materialLabel47;
        private MaterialSkin.Controls.MaterialButton materialButton6;
        private Panel panel16;
        private MaterialSkin.Controls.MaterialLabel materialLabel48;
        private MaterialSkin.Controls.MaterialLabel materialLabel49;
        private MaterialSkin.Controls.MaterialCard materialCard19;
        private TableLayoutPanel tableLayoutPanel30;
        private Panel panel17;
        private MaterialSkin.Controls.MaterialLabel materialLabel50;
        private MaterialSkin.Controls.MaterialButton materialButton7;
        private Panel panel18;
        private MaterialSkin.Controls.MaterialLabel materialLabel51;
        private MaterialSkin.Controls.MaterialLabel materialLabel52;
        private MaterialSkin.Controls.MaterialLabel materialLabel53;
        private MaterialSkin.Controls.MaterialCard materialCard20;
        private TableLayoutPanel tableLayoutPanel31;
        private Panel panel19;
        private MaterialSkin.Controls.MaterialLabel materialLabel54;
        private MaterialSkin.Controls.MaterialButton materialButton8;
        private Panel panel20;
        private MaterialSkin.Controls.MaterialLabel materialLabel55;
        private MaterialSkin.Controls.MaterialLabel materialLabel56;
        private MaterialSkin.Controls.MaterialLabel materialLabel57;
        private MaterialSkin.Controls.MaterialLabel materialLabel58;
        private MaterialSkin.Controls.MaterialTabControl materialTabControl2;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripMenuItem SimulateProfile;
        private TableLayoutPanel tableLayoutPanel34;
        private MaterialSkin.Controls.MaterialCard materialCard22;
        private TableLayoutPanel tableLayoutPanel35;
        private MaterialSkin.Controls.MaterialButton simulateLoad;
        private MaterialSkin.Controls.MaterialButton simulateStart;
        private MaterialSkin.Controls.MaterialButton simulateStop;
        private MaterialSkin.Controls.MaterialComboBox chooseProfileMain;
        private ToolStripMenuItem moveGroup;
        private TableLayoutPanel tableLayoutPanel32;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel StatusLabel;
        private ToolStripMenuItem updateProxyToolStripMenuItem;
        private ToolStripStatusLabel LightLabel;
        private MaterialSkin.Controls.MaterialCard materialCard21;
        private TableLayoutPanel tableLayoutPanel33;
        private Panel panel21;
        private MaterialSkin.Controls.MaterialLabel materialLabel59;
        private MaterialSkin.Controls.MaterialButton SelectFileExt;
        private Panel panel22;
        private MaterialSkin.Controls.MaterialLabel materialLabel60;
        private MaterialSkin.Controls.MaterialLabel materialLabel61;
        private MaterialSkin.Controls.MaterialLabel tFileExt;
        private MaterialSkin.Controls.MaterialMultiLineTextBox2 multiLineTextExt;
        private Panel panel23;
        private MaterialSkin.Controls.MaterialCard materialCard25;
        private MaterialSkin.Controls.MaterialTextBox2 materialTextBox22;
        private MaterialSkin.Controls.MaterialTextBox2 materialTextBox23;
        private MaterialSkin.Controls.MaterialTextBox2 materialTextBox21;
        private ContextMenuStrip MenuGroups;
        private ToolStripMenuItem EditGroup;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem DeleteGroup;
        private ContextMenuStrip MenuSimu;
        private ToolStripMenuItem getTaskToolStripMenuItem;
        private ToolStripMenuItem getOTPToolStripMenuItem;
        private ToolStripMenuItem nameToolStripMenuItem;
        private ToolStripMenuItem phoneToolStripMenuItem;
        private ToolStripMenuItem getName;
        private ToolStripMenuItem getPhone;
        private ToolStripMenuItem phone2ToolStripMenuItem;
        private ToolStripMenuItem userNameToolStripMenuItem;
        private ToolStripMenuItem emailToolStripMenuItem;
        private ToolStripMenuItem passwordToolStripMenuItem;
        private ToolStripProgressBar ProgressBar;
        private ToolStripMenuItem saveAccoutToolStripMenuItem;
        private ToolStripStatusLabel StatusMess;
        private ToolStripMenuItem firstNameToolStripMenuItem;
        private ToolStripMenuItem lastnameToolStripMenuItem;
        private ToolStripMenuItem username2ToolStripMenuItem;
        private ToolStripMenuItem password2ToolStripMenuItem;
        private ToolStripMenuItem birthDayToolStripMenuItem;
        private ToolStripMenuItem getYahooMailToolStripMenuItem;
        private ToolStripMenuItem getContentMailToolStripMenuItem;
        private ToolStripMenuItem yahooMailToolStripMenuItem;
        private ToolStripMenuItem passwordToolStripMenuItem1;
        private ToolStripMenuItem oTPToolStripMenuItem;
        private ToolStripMenuItem linkToolStripMenuItem;
        private ToolStripMenuItem viewRawToolStripMenuItem;
        private ToolStripMenuItem dodToolStripMenuItem;
        private ToolStripMenuItem domToolStripMenuItem;
        private ToolStripMenuItem doyToolStripMenuItem;
        private ToolStripMenuItem copyToolStripMenuItem;
        private ToolStripMenuItem pasteToolStripMenuItem;
        private TabPage tabRecord;
        private TableLayoutPanel tableLayoutPanel36;
        private MaterialSkin.Controls.MaterialCard materialCard23;
        private TableLayoutPanel tableLayoutPanel43;
        private MaterialSkin.Controls.MaterialButton startRecord;
        private MaterialSkin.Controls.MaterialButton stopRecord;
        private MaterialSkin.Controls.MaterialButton pauseRecord;
        private DataGridView dataRecord;
        private MaterialSkin.Controls.MaterialExpansionPanel materialExpansionPanel2;
        private TableLayoutPanel tableLayoutPanel44;
        private TableLayoutPanel tableLayoutPanel45;
        private Label label5;
        private TableLayoutPanel tableLayoutPanel46;
        private Label label6;
        private TableLayoutPanel tableLayoutPanel47;
        private Label label7;
        private MaterialSkin.Controls.MaterialTextBox materialTextBox1;
        private TableLayoutPanel tableLayoutPanel48;
        private Label label8;
        private TableLayoutPanel tableLayoutPanel38;
        private TableLayoutPanel tableLayoutPanel40;
        private Label label2;
        private MaterialSkin.Controls.MaterialTextBox2 channelTxt;
        private TableLayoutPanel tableLayoutPanel41;
        private Label label3;
        private MaterialSkin.Controls.MaterialTextBox2 brandTxt;
        private TableLayoutPanel tableLayoutPanel42;
        private Label label4;
        private MaterialSkin.Controls.MaterialTextBox sqlTabtxt;
        private TableLayoutPanel tableLayoutPanel39;
        private Label label1;
        private MaterialSkin.Controls.MaterialTextBox2 APITxt;
        private Panel panelExp;
        private TableLayoutPanel tableLayoutPanel51;
        private MaterialSkin.Controls.MaterialButton BtnSettingRecord;
        private TableLayoutPanel tableLayoutPanel37;
        private Panel panel25;
        private Panel panelCollapsed;
        private MaterialSkin.Controls.MaterialButton ButtonCollapsed;
        private DataGridViewTextBoxColumn record_name;
        private DataGridViewTextBoxColumn record_time;
        private DataGridViewTextBoxColumn record_last_modify;
        private DataGridViewTextBoxColumn record_link;
        private DataGridView dataSimulator;
        private DataGridViewTextBoxColumn simu_id;
        private DataGridViewTextBoxColumn simu_status;
        private DataGridViewTextBoxColumn update_time;
        private DataGridViewTextBoxColumn profile_simu_proxy;
        private MaterialSkin.Controls.MaterialComboBox ComboBoxPopUpSelected;
        private Button buttonReload;
        private ContextMenuStrip MenuRecord;
        private ToolStripMenuItem playToolStripMenuItem;
        private ToolStripMenuItem deleteToolStripMenuItem;
        private ToolStripMenuItem recordToolStripMenuItem1;
        private MaterialSkin.Controls.MaterialCard materialCard26;
        private TableLayoutPanel tableLayoutPanel49;
        private Panel panelSettingRecord;
        private MaterialSkin.Controls.MaterialRadioButton RadioBtnLoop4Ever;
        private MaterialSkin.Controls.MaterialRadioButton RadioBtnLoop;
        private MaterialSkin.Controls.MaterialLabel materialLabel62;
        private Label label9;
        private NumericUpDown numericTimes;
        private TextBox LoopIntervalTxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel63;
        private MaterialSkin.Controls.MaterialLabel materialLabel64;
        private ToolStripMenuItem playWithNewProfileToolStripMenuItem;
        private TabPage tabRunRecord;
        private MaterialSkin.Controls.MaterialLabel materialLabel65;
        private MaterialSkin.Controls.MaterialLabel materialLabel66;
        private MaterialSkin.Controls.MaterialLabel materialLabel67;
        private MaterialSkin.Controls.MaterialLabel materialLabel68;
        private TableLayoutPanel tableLayoutPanel50;
        private MaterialSkin.Controls.MaterialLabel materialLabel69;
        private MaterialSkin.Controls.MaterialLabel materialLabel70;
        private MaterialSkin.Controls.MaterialLabel materialLabel71;
        private MaterialSkin.Controls.MaterialLabel materialLabel72;
        private MaterialSkin.Controls.MaterialLabel materialLabel73;
        private MaterialSkin.Controls.MaterialLabel materialLabel74;
        private MaterialSkin.Controls.MaterialLabel materialLabel75;
        private MaterialSkin.Controls.MaterialLabel materialLabel76;
        private Panel panel24;
        private MaterialSkin.Controls.MaterialLabel materialLabel77;
        private MaterialSkin.Controls.MaterialCard materialCard24;
        private TableLayoutPanel tableLayoutPanel52;
        private TableLayoutPanel tableLayoutPanel53;
        private MaterialSkin.Controls.MaterialCard materialCard27;
        private DataGridView dataGridView4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripMenuItem toolStripMenuItem2;
        private ToolStripMenuItem toolStripMenuItem3;
        private ToolStripMenuItem toolStripMenuItem4;
        private ToolStripMenuItem toolStripMenuItem5;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripMenuItem toolStripMenuItem6;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripMenuItem toolStripMenuItem7;
        private MaterialSkin.Controls.MaterialButton materialButton9;
        private TableLayoutPanel tableLayoutPanel54;
        private MaterialSkin.Controls.MaterialCard materialCard28;
        private TableLayoutPanel tableLayoutPanel55;
        private Panel panel26;
        private MaterialSkin.Controls.MaterialLabel materialLabel78;
        private MaterialSkin.Controls.MaterialLabel materialLabel79;
        private MaterialSkin.Controls.MaterialLabel materialLabel80;
        private MaterialSkin.Controls.MaterialCard materialCard29;
        private TableLayoutPanel tableLayoutPanel56;
        private Panel panel27;
        private MaterialSkin.Controls.MaterialLabel materialLabel81;
        private MaterialSkin.Controls.MaterialButton materialButton10;
        private Panel panel28;
        private MaterialSkin.Controls.MaterialLabel materialLabel82;
        private MaterialSkin.Controls.MaterialLabel materialLabel83;
        private MaterialSkin.Controls.MaterialLabel materialLabel84;
        private ContextMenuStrip contextMenuStrip2;
        private ToolStripMenuItem toolStripMenuItem8;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripMenuItem toolStripMenuItem9;
        private MaterialSkin.Controls.MaterialLabel materialLabel98;
        private MaterialSkin.Controls.MaterialLabel materialLabel99;
        private MaterialSkin.Controls.MaterialLabel materialLabel100;
        private Panel panel31;
        private MaterialSkin.Controls.MaterialLabel materialLabel101;
        private MaterialSkin.Controls.MaterialLabel materialLabel102;
        private MaterialSkin.Controls.MaterialButton materialButton13;
        private Panel panel32;
        private TableLayoutPanel tableLayoutPanel64;
        private MaterialSkin.Controls.MaterialCard materialCard34;
        private MaterialSkin.Controls.MaterialTextBox2 materialTextBox24;
        private MaterialSkin.Controls.MaterialCard materialCard35;
        private TableLayoutPanel tableLayoutPanel65;
        private Panel panel33;
        private MaterialSkin.Controls.MaterialLabel materialLabel103;
        private MaterialSkin.Controls.MaterialButton materialButton14;
        private Panel panel34;
        private Panel panel35;
        private MaterialSkin.Controls.MaterialMultiLineTextBox2 materialMultiLineTextBox21;
        private MaterialSkin.Controls.MaterialLabel materialLabel104;
        private MaterialSkin.Controls.MaterialLabel materialLabel105;
        private MaterialSkin.Controls.MaterialLabel materialLabel106;
        private MaterialSkin.Controls.MaterialLabel materialLabel107;
        private Panel panel36;
        private MaterialSkin.Controls.MaterialButton materialButton15;
        private TableLayoutPanel tableLayoutPanel66;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private ImageList imageList1;
        private Panel panel37;
        private MaterialSkin.Controls.MaterialLabel materialLabel108;
        private MaterialSkin.Controls.MaterialLabel materialLabel109;
        private MaterialSkin.Controls.MaterialLabel materialLabel110;
        private MaterialSkin.Controls.MaterialLabel materialLabel111;
        private MaterialSkin.Controls.MaterialLabel materialLabel112;
        private Panel panel38;
        private MaterialSkin.Controls.MaterialButton materialButton16;
        private TableLayoutPanel tableLayoutPanel67;
        private MaterialSkin.Controls.MaterialCard materialCard37;
        private Panel panel39;
        private MaterialSkin.Controls.MaterialLabel materialLabel113;
        private MaterialSkin.Controls.MaterialButton materialButton17;
        private MaterialSkin.Controls.MaterialLabel materialLabel114;
        private MaterialSkin.Controls.MaterialLabel materialLabel115;
        private MaterialSkin.Controls.MaterialLabel materialLabel116;
        private Panel panel40;
        private TableLayoutPanel tableLayoutPanel68;
        private MaterialSkin.Controls.MaterialCard materialCard38;
        private MaterialSkin.Controls.MaterialLabel materialLabel117;
        private MaterialSkin.Controls.MaterialLabel materialLabel118;
        private TabPage tabPage4;
        private Label label10;
        private TableLayoutPanel tableLayoutPanel69;
        private Panel panel41;
        private MaterialSkin.Controls.MaterialLabel materialLabel119;
        private TableLayoutPanel tableLayoutPanel74;
        private Panel panel42;
        private MaterialSkin.Controls.MaterialButton materialButton18;
        private MaterialSkin.Controls.MaterialCard materialCard39;
        private FlowLayoutPanel flowLayoutPanel3;
        private MaterialSkin.Controls.MaterialButton materialButton19;
        private TableLayoutPanel tableLayoutPanel75;
        private MaterialSkin.Controls.MaterialCard materialCard40;
        private TableLayoutPanel tableLayoutPanel76;
        private MaterialSkin.Controls.MaterialLabel materialLabel120;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private MaterialSkin.Controls.MaterialCard materialCard41;
        private TableLayoutPanel tableLayoutPanel77;
        private MaterialSkin.Controls.MaterialComboBox materialComboBox3;
        private MaterialSkin.Controls.MaterialButton materialButton20;
        private MaterialSkin.Controls.MaterialButton materialButton21;
        private MaterialSkin.Controls.MaterialButton materialButton22;
        private Button button1;
        private TableLayoutPanel tableLayoutPanel78;
        private TableLayoutPanel tableLayoutPanel79;
        private StatusStrip statusStrip3;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private ToolStripStatusLabel toolStripStatusLabel2;
        private MaterialSkin.Controls.MaterialCard materialCard42;
        private TableLayoutPanel tableLayoutPanel80;
        private DataGridView dataGridView7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private ContextMenuStrip contextMenuStrip3;
        private ToolStripMenuItem toolStripMenuItem10;
        private ToolStripMenuItem toolStripMenuItem11;
        private ToolStripMenuItem toolStripMenuItem12;
        private ToolStripMenuItem toolStripMenuItem13;
        private ToolStripMenuItem toolStripMenuItem14;
        private ToolStripMenuItem toolStripMenuItem15;
        private ToolStripMenuItem toolStripMenuItem16;
        private ToolStripMenuItem toolStripMenuItem17;
        private ToolStripMenuItem toolStripMenuItem18;
        private ToolStripMenuItem toolStripMenuItem19;
        private ToolStripMenuItem toolStripMenuItem20;
        private ToolStripMenuItem toolStripMenuItem21;
        private ToolStripMenuItem toolStripMenuItem22;
        private ToolStripMenuItem toolStripMenuItem23;
        private ToolStripMenuItem toolStripMenuItem24;
        private ToolStripMenuItem toolStripMenuItem25;
        private ToolStripMenuItem toolStripMenuItem26;
        private ToolStripMenuItem toolStripMenuItem27;
        private ToolStripMenuItem toolStripMenuItem28;
        private ToolStripMenuItem toolStripMenuItem29;
        private ToolStripMenuItem toolStripMenuItem30;
        private ToolStripMenuItem toolStripMenuItem31;
        private ToolStripMenuItem toolStripMenuItem32;
        private ToolStripMenuItem toolStripMenuItem33;
        private ToolStripMenuItem toolStripMenuItem34;
        private ToolStripMenuItem toolStripMenuItem35;
        private ToolStripMenuItem toolStripMenuItem36;
        private Panel panel43;
        private TableLayoutPanel tableLayoutPanel81;
        private TableLayoutPanel tableLayoutPanel82;
        private Label label14;
        private MaterialSkin.Controls.MaterialTextBox2 materialTextBox27;
        private TableLayoutPanel tableLayoutPanel83;
        private Label label15;
        private MaterialSkin.Controls.MaterialTextBox2 materialTextBox28;
        private TableLayoutPanel tableLayoutPanel84;
        private Label label16;
        private MaterialSkin.Controls.MaterialTextBox materialTextBox3;
        private TableLayoutPanel tableLayoutPanel85;
        private Label label17;
        private MaterialSkin.Controls.MaterialTextBox2 materialTextBox29;
        private MaterialSkin.Controls.MaterialButton materialButton23;
        private TabPage tabPage6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private DataGridView dataGridView8;
        private MaterialSkin.Controls.MaterialCard materialCard43;
        private TableLayoutPanel tableLayoutPanel86;
        private MaterialSkin.Controls.MaterialLabel materialLabel121;
        private MaterialSkin.Controls.MaterialLabel materialLabel122;
        private MaterialSkin.Controls.MaterialLabel materialLabel123;
        private MaterialSkin.Controls.MaterialLabel materialLabel124;
        private MaterialSkin.Controls.MaterialLabel materialLabel125;
        private MaterialSkin.Controls.MaterialLabel materialLabel126;
        private MaterialSkin.Controls.MaterialLabel materialLabel127;
        private MaterialSkin.Controls.MaterialLabel materialLabel128;
        private MaterialSkin.Controls.MaterialLabel materialLabel129;
        private MaterialSkin.Controls.MaterialLabel materialLabel130;
        private MaterialSkin.Controls.MaterialLabel materialLabel131;
        private MaterialSkin.Controls.MaterialLabel materialLabel132;
        private Panel panel44;
        private MaterialSkin.Controls.MaterialLabel materialLabel133;
        private MaterialSkin.Controls.MaterialCard materialCard44;
        private TableLayoutPanel tableLayoutPanel87;
        private MaterialSkin.Controls.MaterialButton materialButton24;
        private MaterialSkin.Controls.MaterialButton materialButton25;
        private MaterialSkin.Controls.MaterialComboBox materialComboBox4;
        private Panel panel45;
        private TableLayoutPanel tableLayoutPanel88;
        private TableLayoutPanel tableLayoutPanel89;
        private MaterialSkin.Controls.MaterialCard materialCard45;
        private TableLayoutPanel tableLayoutPanel90;
        private TableLayoutPanel tableLayoutPanel91;
        private MaterialSkin.Controls.MaterialCard materialCard46;
        private DataGridView dataGridView9;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private TableLayoutPanel tableLayoutPanel92;
        private TabPage tabPage7;
        private MaterialSkin.Controls.MaterialTabControl materialTabControl4;
        private TabPage tabPage8;
        private TableLayoutPanel tableLayoutPanel93;
        private MaterialSkin.Controls.MaterialCard materialCard47;
        private TableLayoutPanel tableLayoutPanel94;
        private MaterialSkin.Controls.MaterialButton materialButton26;
        private MaterialSkin.Controls.MaterialButton materialButton27;
        private MaterialSkin.Controls.MaterialButton materialButton28;
        private MaterialSkin.Controls.MaterialCard materialCard48;
        private TableLayoutPanel tableLayoutPanel95;
        private MaterialSkin.Controls.MaterialButton materialButton29;
        private DataGridView dataGridView10;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private ContextMenuStrip contextMenuStrip4;
        private Panel panel46;
        private MaterialSkin.Controls.MaterialLabel materialLabel134;
        private TextBox textBox1;
        private MaterialSkin.Controls.MaterialLabel materialLabel135;
        private Label label18;
        private NumericUpDown numericUpDown1;
        private MaterialSkin.Controls.MaterialRadioButton materialRadioButton1;
        private MaterialSkin.Controls.MaterialRadioButton materialRadioButton2;
        private MaterialSkin.Controls.MaterialLabel materialLabel136;
        private StatusStrip statusStrip4;
        private ToolStripStatusLabel toolStripStatusLabel3;
        private ToolStripStatusLabel toolStripStatusLabel4;
        private TabPage tabPage9;
        private TableLayoutPanel tableLayoutPanel96;
        private MaterialSkin.Controls.MaterialCard materialCard49;
        private TableLayoutPanel tableLayoutPanel97;
        private MaterialSkin.Controls.MaterialButton materialButton30;
        private FlowLayoutPanel flowLayoutPanel4;
        private MaterialSkin.Controls.MaterialCard materialCard50;
        private TableLayoutPanel tableLayoutPanel98;
        private Panel panel47;
        private MaterialSkin.Controls.MaterialLabel materialLabel137;
        private MaterialSkin.Controls.MaterialButton materialButton31;
        private Panel panel48;
        private MaterialSkin.Controls.MaterialLabel materialLabel138;
        private TableLayoutPanel tableLayoutPanel57;
        private MaterialSkin.Controls.MaterialCard materialCard30;
        private TableLayoutPanel tableLayoutPanel58;
        private MaterialSkin.Controls.MaterialComboBox ComboBoxRecord;
        private MaterialSkin.Controls.MaterialButton RunBtn;
        private MaterialSkin.Controls.MaterialButton RunAsNewBtn;
        private MaterialSkin.Controls.MaterialCard materialCard31;
        private StatusStrip statusStrip2;
        private ToolStripStatusLabel StatusTxt;
        private ToolStripStatusLabel StatusRecord;
        private MaterialSkin.Controls.MaterialButton StopRunScripsBtn;
        private DataGridView RunRecordTable;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private DataGridViewTextBoxColumn record_profile_id;
        private DataGridViewTextBoxColumn run_record_status;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private ToolStripMenuItem runWithRecordToolStripMenuItem;
    }
}