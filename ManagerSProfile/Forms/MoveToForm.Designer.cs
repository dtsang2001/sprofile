﻿namespace ManagerSProfile.Forms
{
    partial class MoveToForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            materialCard2 = new MaterialSkin.Controls.MaterialCard();
            moveBtn = new MaterialSkin.Controls.MaterialButton();
            tableLayoutPanel1 = new TableLayoutPanel();
            materialCard1 = new MaterialSkin.Controls.MaterialCard();
            filterByGroup = new MaterialSkin.Controls.MaterialComboBox();
            materialCard2.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            materialCard1.SuspendLayout();
            SuspendLayout();
            // 
            // materialCard2
            // 
            materialCard2.BackColor = Color.FromArgb(255, 255, 255);
            materialCard2.Controls.Add(moveBtn);
            materialCard2.Depth = 0;
            materialCard2.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard2.Location = new Point(6, 130);
            materialCard2.Margin = new Padding(6);
            materialCard2.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard2.Name = "materialCard2";
            materialCard2.Padding = new Padding(14);
            materialCard2.Size = new Size(518, 67);
            materialCard2.TabIndex = 1;
            // 
            // moveBtn
            // 
            moveBtn.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            moveBtn.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            moveBtn.Depth = 0;
            moveBtn.Dock = DockStyle.Right;
            moveBtn.HighEmphasis = true;
            moveBtn.Icon = null;
            moveBtn.Location = new Point(418, 14);
            moveBtn.Margin = new Padding(2);
            moveBtn.MouseState = MaterialSkin.MouseState.HOVER;
            moveBtn.Name = "moveBtn";
            moveBtn.NoAccentTextColor = Color.Empty;
            moveBtn.Padding = new Padding(5);
            moveBtn.Size = new Size(86, 39);
            moveBtn.TabIndex = 0;
            moveBtn.Text = "Confirm";
            moveBtn.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            moveBtn.UseAccentColor = false;
            moveBtn.UseVisualStyleBackColor = true;
            moveBtn.Click += moveBtn_Click;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel1.Controls.Add(materialCard1, 0, 0);
            tableLayoutPanel1.Controls.Add(materialCard2, 0, 1);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(3, 64);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 60F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 40F));
            tableLayoutPanel1.Size = new Size(532, 207);
            tableLayoutPanel1.TabIndex = 1;
            // 
            // materialCard1
            // 
            materialCard1.BackColor = Color.FromArgb(255, 255, 255);
            materialCard1.Controls.Add(filterByGroup);
            materialCard1.Depth = 0;
            materialCard1.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard1.Location = new Point(6, 6);
            materialCard1.Margin = new Padding(6);
            materialCard1.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard1.Name = "materialCard1";
            materialCard1.Padding = new Padding(25);
            materialCard1.Size = new Size(518, 106);
            materialCard1.TabIndex = 0;
            // 
            // filterByGroup
            // 
            filterByGroup.AutoResize = false;
            filterByGroup.BackColor = Color.FromArgb(255, 255, 255);
            filterByGroup.Depth = 0;
            filterByGroup.Dock = DockStyle.Fill;
            filterByGroup.DrawMode = DrawMode.OwnerDrawVariable;
            filterByGroup.DropDownHeight = 174;
            filterByGroup.DropDownStyle = ComboBoxStyle.DropDownList;
            filterByGroup.DropDownWidth = 121;
            filterByGroup.Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            filterByGroup.ForeColor = Color.FromArgb(222, 0, 0, 0);
            filterByGroup.FormattingEnabled = true;
            filterByGroup.Hint = "Group";
            filterByGroup.IntegralHeight = false;
            filterByGroup.ItemHeight = 43;
            filterByGroup.Location = new Point(25, 25);
            filterByGroup.MaxDropDownItems = 4;
            filterByGroup.MouseState = MaterialSkin.MouseState.OUT;
            filterByGroup.Name = "filterByGroup";
            filterByGroup.Size = new Size(468, 49);
            filterByGroup.StartIndex = 0;
            filterByGroup.TabIndex = 3;
            // 
            // MoveToForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(538, 274);
            Controls.Add(tableLayoutPanel1);
            Name = "MoveToForm";
            Text = "Update Group";
            Load += MoveToForm_Load;
            materialCard2.ResumeLayout(false);
            materialCard2.PerformLayout();
            tableLayoutPanel1.ResumeLayout(false);
            materialCard1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private MaterialSkin.Controls.MaterialCard materialCard2;
        private MaterialSkin.Controls.MaterialButton moveBtn;
        private TableLayoutPanel tableLayoutPanel1;
        private MaterialSkin.Controls.MaterialCard materialCard1;
        private MaterialSkin.Controls.MaterialComboBox filterByGroup;
    }
}