﻿namespace ManagerSProfile.Forms
{
    partial class RecordAsNewProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            materialCard1 = new MaterialSkin.Controls.MaterialCard();
            failTxt = new TextBox();
            materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            successTxt = new TextBox();
            materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            confirmBtn = new MaterialSkin.Controls.MaterialButton();
            proxyRadio = new MaterialSkin.Controls.MaterialSwitch();
            materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            zoomtxt = new MaterialSkin.Controls.MaterialComboBox();
            materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            hTxt = new MaterialSkin.Controls.MaterialTextBox();
            materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            widTxt = new MaterialSkin.Controls.MaterialTextBox();
            materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            ThreadTxt = new MaterialSkin.Controls.MaterialTextBox2();
            materialCard1.SuspendLayout();
            SuspendLayout();
            // 
            // materialCard1
            // 
            materialCard1.BackColor = Color.FromArgb(255, 255, 255);
            materialCard1.Controls.Add(failTxt);
            materialCard1.Controls.Add(materialLabel8);
            materialCard1.Controls.Add(successTxt);
            materialCard1.Controls.Add(materialLabel7);
            materialCard1.Controls.Add(materialLabel6);
            materialCard1.Controls.Add(materialLabel4);
            materialCard1.Controls.Add(confirmBtn);
            materialCard1.Controls.Add(proxyRadio);
            materialCard1.Controls.Add(materialLabel3);
            materialCard1.Controls.Add(zoomtxt);
            materialCard1.Controls.Add(materialLabel2);
            materialCard1.Controls.Add(hTxt);
            materialCard1.Controls.Add(materialLabel5);
            materialCard1.Controls.Add(widTxt);
            materialCard1.Controls.Add(materialLabel1);
            materialCard1.Controls.Add(ThreadTxt);
            materialCard1.Depth = 0;
            materialCard1.Dock = DockStyle.Fill;
            materialCard1.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard1.Location = new Point(3, 64);
            materialCard1.Margin = new Padding(14);
            materialCard1.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard1.Name = "materialCard1";
            materialCard1.Padding = new Padding(14);
            materialCard1.Size = new Size(310, 434);
            materialCard1.TabIndex = 0;
            // 
            // failTxt
            // 
            failTxt.Enabled = false;
            failTxt.Location = new Point(81, 347);
            failTxt.Name = "failTxt";
            failTxt.Size = new Size(43, 27);
            failTxt.TabIndex = 21;
            failTxt.Text = "10";
            // 
            // materialLabel8
            // 
            materialLabel8.AutoSize = true;
            materialLabel8.Depth = 0;
            materialLabel8.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel8.Location = new Point(130, 351);
            materialLabel8.Margin = new Padding(3, 5, 3, 0);
            materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel8.Name = "materialLabel8";
            materialLabel8.Padding = new Padding(0, 5, 0, 0);
            materialLabel8.Size = new Size(24, 19);
            materialLabel8.TabIndex = 20;
            materialLabel8.Text = "fail";
            materialLabel8.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // successTxt
            // 
            successTxt.Enabled = false;
            successTxt.Location = new Point(81, 309);
            successTxt.Name = "successTxt";
            successTxt.Size = new Size(43, 27);
            successTxt.TabIndex = 19;
            successTxt.Text = "10";
            // 
            // materialLabel7
            // 
            materialLabel7.AutoSize = true;
            materialLabel7.Depth = 0;
            materialLabel7.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel7.Location = new Point(130, 313);
            materialLabel7.Margin = new Padding(3, 5, 3, 0);
            materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel7.Name = "materialLabel7";
            materialLabel7.Padding = new Padding(0, 5, 0, 0);
            materialLabel7.Size = new Size(76, 19);
            materialLabel7.TabIndex = 18;
            materialLabel7.Text = "success or";
            materialLabel7.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialLabel6
            // 
            materialLabel6.AutoSize = true;
            materialLabel6.Depth = 0;
            materialLabel6.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel6.Location = new Point(17, 312);
            materialLabel6.Margin = new Padding(3, 5, 3, 0);
            materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel6.Name = "materialLabel6";
            materialLabel6.Padding = new Padding(0, 5, 0, 0);
            materialLabel6.Size = new Size(56, 19);
            materialLabel6.TabIndex = 17;
            materialLabel6.Text = "Stop at:";
            materialLabel6.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialLabel4
            // 
            materialLabel4.AutoSize = true;
            materialLabel4.Depth = 0;
            materialLabel4.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel4.Location = new Point(17, 25);
            materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel4.Name = "materialLabel4";
            materialLabel4.Size = new Size(141, 19);
            materialLabel4.TabIndex = 16;
            materialLabel4.Text = "Number of threads :";
            // 
            // confirmBtn
            // 
            confirmBtn.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            confirmBtn.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            confirmBtn.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            confirmBtn.Depth = 0;
            confirmBtn.HighEmphasis = true;
            confirmBtn.Icon = null;
            confirmBtn.Location = new Point(206, 378);
            confirmBtn.Margin = new Padding(4, 6, 4, 6);
            confirmBtn.MouseState = MaterialSkin.MouseState.HOVER;
            confirmBtn.Name = "confirmBtn";
            confirmBtn.NoAccentTextColor = Color.Empty;
            confirmBtn.Size = new Size(86, 36);
            confirmBtn.TabIndex = 15;
            confirmBtn.Text = "Confirm";
            confirmBtn.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            confirmBtn.UseAccentColor = false;
            confirmBtn.UseVisualStyleBackColor = true;
            confirmBtn.Click += confirmBtn_Click;
            // 
            // proxyRadio
            // 
            proxyRadio.AutoSize = true;
            proxyRadio.Depth = 0;
            proxyRadio.Location = new Point(95, 261);
            proxyRadio.Margin = new Padding(0);
            proxyRadio.MouseLocation = new Point(-1, -1);
            proxyRadio.MouseState = MaterialSkin.MouseState.HOVER;
            proxyRadio.Name = "proxyRadio";
            proxyRadio.Ripple = true;
            proxyRadio.Size = new Size(58, 37);
            proxyRadio.TabIndex = 12;
            proxyRadio.TextAlign = ContentAlignment.MiddleRight;
            proxyRadio.UseVisualStyleBackColor = true;
            // 
            // materialLabel3
            // 
            materialLabel3.AutoSize = true;
            materialLabel3.Depth = 0;
            materialLabel3.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel3.Location = new Point(19, 270);
            materialLabel3.Margin = new Padding(3, 5, 3, 0);
            materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel3.Name = "materialLabel3";
            materialLabel3.Padding = new Padding(0, 5, 0, 0);
            materialLabel3.Size = new Size(41, 19);
            materialLabel3.TabIndex = 7;
            materialLabel3.Text = "Proxy";
            materialLabel3.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // zoomtxt
            // 
            zoomtxt.AutoResize = false;
            zoomtxt.BackColor = Color.FromArgb(255, 255, 255);
            zoomtxt.Depth = 0;
            zoomtxt.DrawMode = DrawMode.OwnerDrawVariable;
            zoomtxt.DropDownHeight = 174;
            zoomtxt.DropDownStyle = ComboBoxStyle.DropDownList;
            zoomtxt.DropDownWidth = 121;
            zoomtxt.Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            zoomtxt.ForeColor = Color.FromArgb(222, 0, 0, 0);
            zoomtxt.FormattingEnabled = true;
            zoomtxt.IntegralHeight = false;
            zoomtxt.ItemHeight = 43;
            zoomtxt.Items.AddRange(new object[] { "100", "90", "80", "70", "60", "50" });
            zoomtxt.Location = new Point(17, 209);
            zoomtxt.MaxDropDownItems = 4;
            zoomtxt.MouseState = MaterialSkin.MouseState.OUT;
            zoomtxt.Name = "zoomtxt";
            zoomtxt.Size = new Size(279, 49);
            zoomtxt.StartIndex = 0;
            zoomtxt.TabIndex = 13;
            // 
            // materialLabel2
            // 
            materialLabel2.AutoSize = true;
            materialLabel2.Depth = 0;
            materialLabel2.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel2.Location = new Point(17, 183);
            materialLabel2.Margin = new Padding(3, 5, 3, 0);
            materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel2.Name = "materialLabel2";
            materialLabel2.Padding = new Padding(0, 5, 0, 0);
            materialLabel2.Size = new Size(43, 19);
            materialLabel2.TabIndex = 8;
            materialLabel2.Text = "Zoom";
            materialLabel2.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // hTxt
            // 
            hTxt.AnimateReadOnly = false;
            hTxt.BorderStyle = BorderStyle.None;
            hTxt.Depth = 0;
            hTxt.Font = new Font("Roboto", 9.6F, FontStyle.Regular, GraphicsUnit.Point);
            hTxt.LeadingIcon = null;
            hTxt.Location = new Point(159, 123);
            hTxt.MaxLength = 50;
            hTxt.MouseState = MaterialSkin.MouseState.OUT;
            hTxt.Multiline = false;
            hTxt.Name = "hTxt";
            hTxt.Size = new Size(137, 50);
            hTxt.TabIndex = 14;
            hTxt.Text = "480";
            hTxt.TrailingIcon = null;
            // 
            // materialLabel5
            // 
            materialLabel5.AutoSize = true;
            materialLabel5.Depth = 0;
            materialLabel5.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel5.Location = new Point(159, 99);
            materialLabel5.Margin = new Padding(3, 5, 3, 0);
            materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel5.Name = "materialLabel5";
            materialLabel5.Padding = new Padding(0, 5, 0, 0);
            materialLabel5.Size = new Size(47, 19);
            materialLabel5.TabIndex = 9;
            materialLabel5.Text = "Height";
            materialLabel5.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // widTxt
            // 
            widTxt.AnimateReadOnly = false;
            widTxt.BorderStyle = BorderStyle.None;
            widTxt.Depth = 0;
            widTxt.Font = new Font("Roboto", 9.6F, FontStyle.Regular, GraphicsUnit.Point);
            widTxt.LeadingIcon = null;
            widTxt.Location = new Point(17, 123);
            widTxt.MaxLength = 50;
            widTxt.MouseState = MaterialSkin.MouseState.OUT;
            widTxt.Multiline = false;
            widTxt.Name = "widTxt";
            widTxt.Size = new Size(136, 50);
            widTxt.TabIndex = 11;
            widTxt.Text = "640";
            widTxt.TrailingIcon = null;
            // 
            // materialLabel1
            // 
            materialLabel1.AutoSize = true;
            materialLabel1.Depth = 0;
            materialLabel1.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel1.Location = new Point(17, 99);
            materialLabel1.Margin = new Padding(3, 5, 3, 0);
            materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel1.Name = "materialLabel1";
            materialLabel1.Padding = new Padding(0, 5, 0, 0);
            materialLabel1.Size = new Size(42, 19);
            materialLabel1.TabIndex = 10;
            materialLabel1.Text = "Width";
            materialLabel1.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // ThreadTxt
            // 
            ThreadTxt.AnimateReadOnly = false;
            ThreadTxt.BackgroundImageLayout = ImageLayout.None;
            ThreadTxt.CharacterCasing = CharacterCasing.Normal;
            ThreadTxt.Depth = 0;
            ThreadTxt.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            ThreadTxt.HideSelection = true;
            ThreadTxt.LeadingIcon = null;
            ThreadTxt.Location = new Point(17, 47);
            ThreadTxt.MaxLength = 32767;
            ThreadTxt.MouseState = MaterialSkin.MouseState.OUT;
            ThreadTxt.Name = "ThreadTxt";
            ThreadTxt.PasswordChar = '\0';
            ThreadTxt.PrefixSuffixText = null;
            ThreadTxt.ReadOnly = false;
            ThreadTxt.RightToLeft = RightToLeft.No;
            ThreadTxt.SelectedText = "";
            ThreadTxt.SelectionLength = 0;
            ThreadTxt.SelectionStart = 0;
            ThreadTxt.ShortcutsEnabled = true;
            ThreadTxt.Size = new Size(279, 48);
            ThreadTxt.TabIndex = 1;
            ThreadTxt.TabStop = false;
            ThreadTxt.TextAlign = HorizontalAlignment.Left;
            ThreadTxt.TrailingIcon = null;
            ThreadTxt.UseSystemPasswordChar = false;
            // 
            // RecordAsNewProfile
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(316, 501);
            Controls.Add(materialCard1);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "RecordAsNewProfile";
            ShowIcon = false;
            ShowInTaskbar = false;
            Sizable = false;
            Text = "Play record with new profile:";
            Load += RecordAsNewProfile_Load;
            materialCard1.ResumeLayout(false);
            materialCard1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private MaterialSkin.Controls.MaterialCard materialCard1;
        private MaterialSkin.Controls.MaterialTextBox2 ThreadTxt;
        private MaterialSkin.Controls.MaterialButton confirmBtn;
        private MaterialSkin.Controls.MaterialSwitch proxyRadio;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialComboBox zoomtxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialTextBox hTxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialTextBox widTxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private TextBox successTxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private TextBox failTxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
    }
}