﻿using CoreSProfile;
using CoreSProfile.Entity;
using KAutoHelper;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebSocketSharp;
using static System.Data.Entity.Infrastructure.Design.Executor;

namespace ManagerSProfile.Forms
{
    public partial class AddProfileForm : MaterialForm
    {
        public AddProfileForm()
        {
            InitializeComponent();
            initSkin();
        }

        public string Group_id { get; internal set; }

        private void AddProfileForm_Load(object sender, EventArgs e)
        {
            LoadComboboxGroup();
            Group_id = "0";
        }

        private void initSkin()
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Blue600,
                Primary.Blue800,
                Primary.Blue300,
                Accent.Blue100,
                TextShade.WHITE
            );
        }

        private void createBtn_Click(object sender, EventArgs e)
        {
            if (numOfProf.Text.IsNullOrEmpty())
            {
                numOfProf.SelectAll();
                return;
            }
            string n = this.numOfProf.Text.Trim();
            ComboBoxEntity? comboBox = eGroups.SelectedItem as ComboBoxEntity;

            int check = 0;
            for (int i = 0; i < Int32.Parse(n); i++)
            {
                string fileProxyPath = (tFileProxy.Text != "Path File...") ? tFileProxy.Text : Config.fileProxy;

                ProfileEntity profile = GoLogin.CreateProfile(Config.folderProfile, Config.fileName, fileProxyPath);

                profile.group_id = (comboBox?.id.ToString() ?? Group_id);

                if (string.IsNullOrEmpty(profile.folders))
                {
                    return;
                }
                check++;

                SQLite.InsertProfile(profile);
            }
            DialogResult dialogResult = MessageBox.Show("Thêm thành công " + check.ToString() + " profile", "Profile", MessageBoxButtons.OK);
            this.DialogResult = DialogResult.OK;
        }

        private void LoadComboboxGroup()
        {
            List<ComboBoxEntity> l = new List<ComboBoxEntity>
            {
                new ComboBoxEntity() { id = 0, name = "Select Group" }
            };

            List<GroupEntity> groups = SQLite.GetGroupsCountProfiles();

            int i = 0;
            foreach (GroupEntity item in groups)
            {
                i++;
                l.Add(new ComboBoxEntity() { id = item.id, name = item.name });
            }

            this.Invoke((Action)(() =>
            {
                this.eGroups.DataSource = l;
                this.eGroups.DisplayMember = "name";
                this.eGroups.ValueMember = "id";
                this.eGroups.SelectedIndex = 0;
            }));
        }

        private void SelectFileProxy_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "TXT Files|*.txt";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.tFileProxy.Text = openFileDialog.FileName;
            }
        }
    }
}