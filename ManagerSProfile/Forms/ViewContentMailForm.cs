﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManagerSProfile.Forms
{
    public partial class ViewContentMailForm : MaterialForm
    {
        public string ListLink;
        public string MainContent;

        public ViewContentMailForm()
        {
            InitializeComponent();
        }

        private void ViewContentMailForm_Load(object sender, EventArgs e)
        {

            //string content = "Hi Sang,\r\n\r\nGet ready to dive into a day packed with code-centric demonstrations, workshops, and intensive training sessions that will help you improve your developer skills and experience the very latest in Twilio innovations.\r\n\r\nJoin your fellow developers at SIGNAL Singapore Developer Training on November 15th in Fairmont Singapore for an opportunity to learn from top Twilio tech experts and gain hands-on experience that will take your skills to the next level.\r\n\r\nSeats are filling up fast, so secure your FREE seat now. <https://u3528416.ct.sendgrid.net/ls/click?upn=OHV4-2FqkTPBIi8pj9zljUkGvaabq0ecKgjN5WVH8ZDzf9L2e5lzgUfaj8K-2B2VpbaMh7hADzInaZCI3zAVUnQlDkireSCnIAEH9dQZTHS11QhhFwLsU2m7WpzorHwiOnldd20kX1k7X4-2Bxnc-2Feeo9zYrxhVrVAc6JIoLRTyydJVx8-3Dm9Ar_4SpANnj8cL407ZSjnS-2FECftJK4r6k5Wdj1MtYIaJnGO-2BnztCwsHk-2FVrWZw2lnk8Twi-2BGbcgtMpWPEZMrx41RAcj3lZo1dOi20U93f0AIK-2BbG0ZNnaj-2BY5RpjYcmaQ-2BSdryXNOOpTKg4I3pJwAU7Lsh-2BYdcMGp439x4z2tVE8f12oyPhOfSUd9POZCjDS9Phpxa0VWGw3GEpSIuVDV-2FQ8iA-3D-3D>\r\n\r\n\t- Enhance Your Developer Proficiency: See, learn, interact and experience the latest in Twilio advancements that’s \r\n          revolutionizing the future of customer engagement.\r\n\t- Learn from the Best: Take a deep dive into Twilio products from experienced Twilio developers. Leave with ready- \r\n          to-use apps that you can further develop and customize for your specific needs.\r\n\t- Put Your Skills to the Test: Apply the skills you have learned through practical exercises while getting the \r\n          guidance you need.\r\n\r\nUpgrade your developer journey and future-proof your skills by taking advantage of this opportunity. Join us for a day packed with learning, exploring and building.\r\n\r\nSecure your FREE seat now. <https://u3528416.ct.sendgrid.net/ls/click?upn=OHV4-2FqkTPBIi8pj9zljUkGvaabq0ecKgjN5WVH8ZDzf9L2e5lzgUfaj8K-2B2VpbaMh7hADzInaZCI3zAVUnQlDkireSCnIAEH9dQZTHS11QhhFwLsU2m7WpzorHwiOnldd20kX1k7X4-2Bxnc-2Feeo9zYrxhVrVAc6JIoLRTyydJVx8-3DTSQn_4SpANnj8cL407ZSjnS-2FECftJK4r6k5Wdj1MtYIaJnGO-2BnztCwsHk-2FVrWZw2lnk8Twi-2BGbcgtMpWPEZMrx41RAdlXbEbwlGnn-2Fv0PD5KZKjn8ZjAB2E7nCPbU80JmwjaOvVl8OrTBtW1-2FO5AL9CwJocLDR6bnS4k9OymS86bkVOJ5tFUexjg8D8Df3FijySaGhmBbK190xtXG7Q7AXc-2B2Sw-3D-3D>\r\n\r\nSee you there!\r\n\r\nNathaniel Okenwa\r\nStaff, Developer Evangelist\r\n\r\nThis email was sent to daohongnh6631@yahoo.com. If you no longer wish to receive these emails you may unsubscribe or update your preferences here: https://u3528416.ct.sendgrid.net/ls/click?upn=OHV4-2FqkTPBIi8pj9zljUkKNr39pANyFQVtCsZDf64ZKC6-2BU1M8HUmidvI2iADh0Qlb-2BVIpG2JIgHkeI7hIdP9vVGQzWi37ujDhIgxy7F0Fs-3DauDo_4SpANnj8cL407ZSjnS-2FECftJK4r6k5Wdj1MtYIaJnGO-2BnztCwsHk-2FVrWZw2lnk8Twi-2BGbcgtMpWPEZMrx41RAWexGzNBwMR93j-2BaV7uzSMA1a1oaYXsq1-2FlcGnsgkWj-2FxTvw0dP0v1KCeart9wa8yVF2tzrOA-2FqPkEX-2FR02RW7Odu8IhGdSy1EXUhl0SzJgDI-2BF-2FkV9Lqt4TRDncfzDhHg-3D-3D\n";
            string content = MainContent;
            List<string> lineCs = GetLinesContent(content);

            foreach (string line in lineCs)
            {
                if (line.Trim() == "") { continue; }
                ListBoxMainContent.Items.Add(new MaterialSkin.MaterialListBoxItem(line.Trim()));
            }
            //string link = "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd,http://www.w3.org/1999/xhtml,https://u3528416.ct.sendgrid.net/ls/click?upn=OHV4-2FqkTPBIi8pj9zljUkGvaabq0ecKgjN5WVH8ZDzf9L2e5lzgUfaj8K-2B2VpbaM4ohIR6SHoFYqcMqR7fzTh6anwZJ5Qpg9hcv3x-2BP4KnGeMVLfjn12je1kXmcHnFwgwEScUYMOiGQpURJx26vV06rYSLbydMt1AOWOAsg-2Bi-2Fo-3DELkt_4SpANnj8cL407ZSjnS-2FECftJK4r6k5Wdj1MtYIaJnGO-2BnztCwsHk-2FVrWZw2lnk8TpQ69LHno4CoghPYFD3AJ5UnnAND1WZ-2BcpxldmknpQ0regpcFWe9-2FOhu9KUKjfGvSCBU85J6kXHrxfx6b9jnJwNeinsSYBPdDVaB-2FSStmF00KeSEcTVxDP1MPSZtdbvi72ET7j-2BmrI-2BWePJKi3UfBfQ-3D-3D";
            string link = ListLink;
            List<string> linesL = GetLinesLink(link);
            foreach (string line in linesL)
            {
                if (line.Trim() == "") { continue; }
                ListBoxLink.Items.Add(new MaterialSkin.MaterialListBoxItem(line.Trim()));
            }
        }


        private static List<string> GetLinesContent(string text)
        {

            string[] linesArray = text.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
            List<string> linesList = new List<string>(linesArray);
            return linesList;
        }
        private static List<string> GetLinesLink(string text)
        {
            string[] linesArray = text.Split(",");
            List<string> linesList = new List<string>(linesArray);
            return linesList;
        }

        private void onClickItemContent(object sender, MaterialSkin.MaterialListBoxItem selectedItem)
        {
            this.TextBoxShow.Text = ListBoxMainContent.SelectedItem.Text;
        }

        private void onClickItemLink(object sender, MaterialSkin.MaterialListBoxItem selectedItem)
        {
            this.TextBoxShow.Text = ListBoxLink.SelectedItem.Text;
        }
    }
}