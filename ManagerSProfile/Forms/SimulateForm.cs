﻿using MaterialSkin;
using MaterialSkin.Controls;
using CoreSProfile.Entity;
using CoreSProfile.Helper;
using WebSocketSharp.Server;
using WebSocketSharp;
using CoreSProfile;

namespace ManagerSProfile.Forms
{
    public partial class SimulateForm : MaterialForm
    {
        private string extSimulateAction = Application.StartupPath + "Data\\SimulateAction";

        private string extSimulateRecord = Application.StartupPath + "Data\\SimulateRecord";
        public List<ProfileEntity> Profiles { get; set; }

        private WebSocketServer websocketServer;

        public SimulateForm()
        {
            InitializeComponent();
            initSkin();
        }

        private void StartWebSocketServer()
        {
            websocketServer = new WebSocketServer(9797);
            websocketServer.AddWebSocketService<MyWebSocketBehavior>("/myendpoint");
            websocketServer.Start();
        }

        private void initSkin()
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Blue600, // Primary color
                Primary.Blue800, // Darker primary color
                Primary.Blue300, // Lighter primary color
                Accent.Blue100,  // Accent color
                TextShade.WHITE
            );
        }


        private void SimulateForm_Load(object sender, EventArgs e)
        {
            StartWebSocketServer();

            IntPtr intLead = this.BlockRecordAction.Handle;

            new Task(() =>
            {
                ProfileEntity lead = Profiles[0];
                ProfileEntity two = Profiles[1];

                new Browser
                {
                    baseProfilePath = Config.folderProfile,
                    extension = extSimulateRecord
                }.Open(lead, 0, intLead);

                new Browser
                {
                    baseProfilePath = Config.folderProfile,
                    extension = extSimulateAction
                }.Open(two, 0);

            }).Start();
        }
    }

    public class MyWebSocketBehavior : WebSocketBehavior
    {
        private static List<IWebSocketSession> connectedClients = new List<IWebSocketSession>();

        protected override void OnOpen()
        {
            connectedClients.Add(this);
            Utils.Logs("Socket Client Connected", ID);
            Console.WriteLine($"Client connected: {ID}");
        }

        protected override void OnClose(CloseEventArgs e)
        {
            connectedClients.Remove(this);
            Console.WriteLine($"Client disconnected: {ID}");
        }
        protected override void OnMessage(MessageEventArgs e)
        {
            string receivedMessage = e.Data;
            Console.WriteLine("Received message: " + receivedMessage);

            //Utils.Logs("Socket", e.Data);

            // Gửi tin nhắn cho tất cả các client đã kết nối
            foreach (var client in connectedClients)
            {
                if (client != this)
                {
                    client.Context.WebSocket.Send(e.Data);
                }
            }
        }
    }
}
