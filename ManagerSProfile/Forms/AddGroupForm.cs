﻿using CoreSProfile;
using CoreSProfile.Entity;
using MaterialSkin;
using MaterialSkin.Controls;

namespace ManagerSProfile.Forms
{
    public partial class AddGroupForm : MaterialForm
    {
        public bool isEdit { get; set; } = false;

        public GroupEntity group { get; set; }

        public AddGroupForm()
        {
            InitializeComponent();
            initSkin();
        }

        private void initSkin()
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Blue600, // Primary color
                Primary.Blue800, // Darker primary color
                Primary.Blue300, // Lighter primary color
                Accent.Blue100,  // Accent color
                TextShade.WHITE
            );
        }


        private void CreateNewGroup()
        {
            string name = this.NewGroupName.Text.Trim();

            if (!String.IsNullOrEmpty(name))
            {
                GroupEntity g = new GroupEntity();

                if (isEdit && group != null)
                {
                    g.name = group.name;
                    g.group_name = name;
                    SQLite.UpdateGroup(g);
                }
                else
                {
                    g.name = name;
                    SQLite.InsertGroup(g);
                }
            }
            this.DialogResult = DialogResult.OK;
        }

        private void NewGroupName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CreateNewGroup();
            }
        }

        private void CreateGroup_Click(object sender, EventArgs e)
        {
            CreateNewGroup();
        }

        private void AddGroupForm_Load(object sender, EventArgs e)
        {
            if (isEdit && group != null)
            {
                this.CreateGroup.Text = "Update";
                this.NewGroupName.Text = this.group.name;
            }
        }
    }
}