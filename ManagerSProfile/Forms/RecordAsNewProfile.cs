﻿using CoreSProfile.Entity;
using CoreSProfile;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using CoreSProfile.Helper;
using KAutoHelper;

namespace ManagerSProfile.Forms
{
    public partial class RecordAsNewProfile : MaterialForm
    {
        public bool RunAsNew = false;
        public bool using_proxy { get; private set; }
        public string thread { get; private set; }
        public Point size { get; private set; }
        public int zoom { get; private set; }
        public int success { get; private set; }
        public int fail { get; private set; }

        public List<Point> lstPos { get; private set; }

        public RecordAsNewProfile()
        {
            InitializeComponent();
        }

        private void confirmBtn_Click(object sender, EventArgs e)
        {
            // params
            thread = this.ThreadTxt.Text.Trim();
            int width = int.Parse(widTxt.Text.Trim());
            int height = int.Parse(hTxt.Text.Trim());
            int screenWidth = Screen.PrimaryScreen.Bounds.Width;
            int screenHeight = Screen.PrimaryScreen.Bounds.Height;


            success = int.Parse(successTxt.Text.Trim());
            fail = int.Parse(failTxt.Text.Trim());
            size = new Point(width, height);
            zoom = int.Parse(zoomtxt.SelectedItem?.ToString());

            if (proxyRadio.CheckState == CheckState.Unchecked) { using_proxy = false; }

            lstPos = CalculateWindowPositions(screenWidth, screenHeight, width * zoom / 100, height * zoom / 100, int.Parse(thread));
            this.Close();

            this.DialogResult = DialogResult.OK;
        }

        public List<Point> CalculateWindowPositions(int screenWidth, int screenHeight, int windowWidth, int windowHeight, int numWindows)
        {
            var listPositions = new List<Point> { };
            int x = 0;
            int y = 0;
            int index = 0;

            while (index < numWindows)
            {
                listPositions.Add(new Point(x, y));
                x += windowWidth;
                index++;

                if (x + windowWidth > screenWidth) // out range?
                {
                    x = 0; // new row
                    y += windowHeight; // next row
                    // when want limit u browser
                    //if (y + windowHeight > screenHeight)
                    //{
                    //    y = 0;
                    //}
                }
            }

            return listPositions;
        }

        private void RecordAsNewProfile_Load(object sender, EventArgs e)
        {
            if (RunAsNew)
            {
                successTxt.Enabled = true; failTxt.Enabled = true;
            }
        }
    }
}