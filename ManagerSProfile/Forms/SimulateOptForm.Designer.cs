﻿namespace ManagerSProfile.Forms
{
    partial class SimulateOptForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            materialButton1 = new MaterialSkin.Controls.MaterialButton();
            materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            ratioProxy = new MaterialSkin.Controls.MaterialSwitch();
            materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            zoomtxt = new MaterialSkin.Controls.MaterialComboBox();
            materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            hTxt = new MaterialSkin.Controls.MaterialTextBox();
            materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            widTxt = new MaterialSkin.Controls.MaterialTextBox();
            materialCard9 = new MaterialSkin.Controls.MaterialCard();
            materialCard9.SuspendLayout();
            SuspendLayout();
            // 
            // materialButton1
            // 
            materialButton1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            materialButton1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton1.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton1.Depth = 0;
            materialButton1.HighEmphasis = true;
            materialButton1.Icon = null;
            materialButton1.Location = new Point(222, 229);
            materialButton1.Margin = new Padding(4, 6, 4, 6);
            materialButton1.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton1.Name = "materialButton1";
            materialButton1.NoAccentTextColor = Color.Empty;
            materialButton1.Size = new Size(86, 36);
            materialButton1.TabIndex = 6;
            materialButton1.Text = "Confirm";
            materialButton1.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton1.UseAccentColor = false;
            materialButton1.UseVisualStyleBackColor = true;
            materialButton1.Click += materialButton1_Click;
            // 
            // materialLabel3
            // 
            materialLabel3.AutoSize = true;
            materialLabel3.Depth = 0;
            materialLabel3.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel3.Location = new Point(30, 200);
            materialLabel3.Margin = new Padding(3, 5, 3, 0);
            materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel3.Name = "materialLabel3";
            materialLabel3.Padding = new Padding(0, 5, 0, 0);
            materialLabel3.Size = new Size(41, 19);
            materialLabel3.TabIndex = 0;
            materialLabel3.Text = "Proxy";
            materialLabel3.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // ratioProxy
            // 
            ratioProxy.AutoSize = true;
            ratioProxy.Depth = 0;
            ratioProxy.Location = new Point(250, 186);
            ratioProxy.Margin = new Padding(0);
            ratioProxy.MouseLocation = new Point(-1, -1);
            ratioProxy.MouseState = MaterialSkin.MouseState.HOVER;
            ratioProxy.Name = "ratioProxy";
            ratioProxy.Ripple = true;
            ratioProxy.Size = new Size(58, 37);
            ratioProxy.TabIndex = 2;
            ratioProxy.TextAlign = ContentAlignment.MiddleRight;
            ratioProxy.UseVisualStyleBackColor = true;
            // 
            // materialLabel2
            // 
            materialLabel2.AutoSize = true;
            materialLabel2.Depth = 0;
            materialLabel2.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel2.Location = new Point(30, 130);
            materialLabel2.Margin = new Padding(3, 5, 3, 0);
            materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel2.Name = "materialLabel2";
            materialLabel2.Padding = new Padding(0, 5, 0, 0);
            materialLabel2.Size = new Size(43, 19);
            materialLabel2.TabIndex = 0;
            materialLabel2.Text = "Zoom";
            materialLabel2.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // zoomtxt
            // 
            zoomtxt.AutoResize = false;
            zoomtxt.BackColor = Color.FromArgb(255, 255, 255);
            zoomtxt.Depth = 0;
            zoomtxt.DrawMode = DrawMode.OwnerDrawVariable;
            zoomtxt.DropDownHeight = 174;
            zoomtxt.DropDownStyle = ComboBoxStyle.DropDownList;
            zoomtxt.DropDownWidth = 121;
            zoomtxt.Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            zoomtxt.ForeColor = Color.FromArgb(222, 0, 0, 0);
            zoomtxt.FormattingEnabled = true;
            zoomtxt.IntegralHeight = false;
            zoomtxt.ItemHeight = 43;
            zoomtxt.Items.AddRange(new object[] { "100", "90", "80", "70", "60", "50" });
            zoomtxt.Location = new Point(110, 121);
            zoomtxt.MaxDropDownItems = 4;
            zoomtxt.MouseState = MaterialSkin.MouseState.OUT;
            zoomtxt.Name = "zoomtxt";
            zoomtxt.Size = new Size(198, 49);
            zoomtxt.StartIndex = 0;
            zoomtxt.TabIndex = 2;
            // 
            // materialLabel5
            // 
            materialLabel5.AutoSize = true;
            materialLabel5.Depth = 0;
            materialLabel5.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel5.Location = new Point(30, 84);
            materialLabel5.Margin = new Padding(3, 5, 3, 0);
            materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel5.Name = "materialLabel5";
            materialLabel5.Padding = new Padding(0, 5, 0, 0);
            materialLabel5.Size = new Size(47, 19);
            materialLabel5.TabIndex = 0;
            materialLabel5.Text = "Height";
            materialLabel5.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // hTxt
            // 
            hTxt.AnimateReadOnly = false;
            hTxt.BorderStyle = BorderStyle.None;
            hTxt.Depth = 0;
            hTxt.Font = new Font("Roboto", 9.6F, FontStyle.Regular, GraphicsUnit.Point);
            hTxt.LeadingIcon = null;
            hTxt.Location = new Point(110, 65);
            hTxt.MaxLength = 50;
            hTxt.MouseState = MaterialSkin.MouseState.OUT;
            hTxt.Multiline = false;
            hTxt.Name = "hTxt";
            hTxt.Size = new Size(198, 50);
            hTxt.TabIndex = 2;
            hTxt.Text = "480";
            hTxt.TrailingIcon = null;
            // 
            // materialLabel1
            // 
            materialLabel1.AutoSize = true;
            materialLabel1.Depth = 0;
            materialLabel1.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel1.Location = new Point(30, 26);
            materialLabel1.Margin = new Padding(3, 5, 3, 0);
            materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel1.Name = "materialLabel1";
            materialLabel1.Padding = new Padding(0, 5, 0, 0);
            materialLabel1.Size = new Size(42, 19);
            materialLabel1.TabIndex = 0;
            materialLabel1.Text = "Width";
            materialLabel1.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // widTxt
            // 
            widTxt.AnimateReadOnly = false;
            widTxt.BorderStyle = BorderStyle.None;
            widTxt.Depth = 0;
            widTxt.Font = new Font("Roboto", 9.6F, FontStyle.Regular, GraphicsUnit.Point);
            widTxt.LeadingIcon = null;
            widTxt.Location = new Point(110, 9);
            widTxt.MaxLength = 50;
            widTxt.MouseState = MaterialSkin.MouseState.OUT;
            widTxt.Multiline = false;
            widTxt.Name = "widTxt";
            widTxt.Size = new Size(198, 50);
            widTxt.TabIndex = 1;
            widTxt.Text = "640";
            widTxt.TrailingIcon = null;
            // 
            // materialCard9
            // 
            materialCard9.BackColor = Color.FromArgb(255, 255, 255);
            materialCard9.Controls.Add(materialButton1);
            materialCard9.Controls.Add(ratioProxy);
            materialCard9.Controls.Add(materialLabel3);
            materialCard9.Controls.Add(zoomtxt);
            materialCard9.Controls.Add(materialLabel2);
            materialCard9.Controls.Add(hTxt);
            materialCard9.Controls.Add(materialLabel5);
            materialCard9.Controls.Add(widTxt);
            materialCard9.Controls.Add(materialLabel1);
            materialCard9.Depth = 0;
            materialCard9.Dock = DockStyle.Fill;
            materialCard9.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard9.Location = new Point(3, 24);
            materialCard9.Margin = new Padding(14);
            materialCard9.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard9.Name = "materialCard9";
            materialCard9.Padding = new Padding(14);
            materialCard9.Size = new Size(329, 285);
            materialCard9.TabIndex = 1;
            // 
            // SimulateOptForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoSize = true;
            ClientSize = new Size(335, 312);
            Controls.Add(materialCard9);
            FormStyle = FormStyles.ActionBar_None;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "SimulateOptForm";
            Padding = new Padding(3, 24, 3, 3);
            ShowIcon = false;
            ShowInTaskbar = false;
            Sizable = false;
            Text = "SimulateOptForm";
            
            materialCard9.ResumeLayout(false);
            materialCard9.PerformLayout();
            ResumeLayout(false);
        }

        #endregion
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialButton materialButton1;
        private MaterialSkin.Controls.MaterialTextBox widTxt;
        private MaterialSkin.Controls.MaterialTextBox hTxt;
        private MaterialSkin.Controls.MaterialComboBox zoomtxt;
        private MaterialSkin.Controls.MaterialSwitch ratioProxy;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialCard materialCard9;
        private MaterialSkin.Controls.MaterialSlider materialSlider1;
        private MaterialSkin.Controls.MaterialSlider materialSlider2;
        public MaterialSkin.Controls.MaterialTabSelector materialTabSelector1;
    }
}