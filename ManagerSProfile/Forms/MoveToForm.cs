﻿using CoreSProfile;
using CoreSProfile.Entity;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManagerSProfile.Forms
{
    public partial class MoveToForm : MaterialForm
    {
      
        public MoveToForm()
        {
            InitializeComponent();
        }

        public List<ProfileEntity> Data { get; internal set; }
        public int Group_id { get; internal set; }

        private void LoadComboBoxGroup()
        {
            List<ComboBoxEntity> l = new List<ComboBoxEntity>
            {
                new ComboBoxEntity() { id = 0, name = "Select Group" }
            };
            List<GroupEntity> groups = SQLite.GetGroupsCountProfiles();
            foreach (GroupEntity item in groups)
            {
                l.Add(new ComboBoxEntity() { id = item.id, name = item.name });
            }

            Thread.Sleep(1000);
            this.Invoke((Action)(() =>
            {
                this.filterByGroup.DataSource = l;
                this.filterByGroup.DisplayMember = "name";
                this.filterByGroup.ValueMember = "id";
                this.filterByGroup.SelectedIndex = Group_id;
            }));
        }

        private void MoveToForm_Load(object sender, EventArgs e)
        {
            LoadComboBoxGroup();
        }

        private void moveBtn_Click(object sender, EventArgs e)
        {
            List<Task> tasks = new();
            ComboBoxEntity? comboBox = filterByGroup.SelectedItem as ComboBoxEntity;
            string group_id = comboBox.id.ToString();

            foreach (ProfileEntity item in Data)
            {
                tasks.Add(Task.Run(() =>
                {
                    item.group_id = group_id;
                    GoLogin.MoveProfileToGroup(item);
                }));
            }

            Task t = Task.WhenAll(tasks);
            t.Wait();
            this.DialogResult = DialogResult.OK;
        }
    }
}