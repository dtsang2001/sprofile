﻿using MaterialSkin;
using MaterialSkin.Controls;
using CoreSProfile;
using CoreSProfile.Entity;
using CoreSProfile.Helper;
using System.Text.RegularExpressions;
using OpenQA.Selenium;

namespace ManagerSProfile.Forms
{
    public partial class EditProfileForm : MaterialForm
    {
        public string ProfileID { get; set; }

        public string BaseProfilePath { get; set; }

        private string pathProxy = Application.StartupPath + "\\Data\\proxy.txt";

        private ProfileEntity profile;

        public EditProfileForm()
        {
            InitializeComponent();
            initSkin();
        }

        // Init Loading Form
        private void EditProfileForm_Load(object sender, EventArgs e)
        {
            profile = SQLite.SelectProfile(ProfileID);

            LoadComboboxGroup();

            this.eProfileID.Text = profile.profile_id;
            this.eUserAgent.Text = profile.ua;
            this.eProxy.Text = profile.proxy;
            this.eStartUrl.Text = profile.start_url;
        }

        private void initSkin()
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Blue600, // Primary color
                Primary.Blue800, // Darker primary color
                Primary.Blue300, // Lighter primary color
                Accent.Blue100,  // Accent color
                TextShade.WHITE
            );
        }


        private void eSave_Click(object sender, EventArgs e)
        {
            string Preferences = File.ReadAllText($"{BaseProfilePath}\\{profile.profile_id}\\Default\\Preferences");
            bool isUpdate = false;
            ComboBoxEntity? comboBox = eGroups.SelectedItem as ComboBoxEntity;

            if ( comboBox != null && comboBox.id.ToString() != profile.group_id)
            {
                isUpdate = true;
                profile.group_id = comboBox.id.ToString();
            }

            if (!String.IsNullOrEmpty(this.eProxy.Text) && this.eProxy.Text != profile.proxy)
            {
                ProxyEntity proxy = new ProxyEntity(this.eProxy.Text);

                string pUserProxy = Regex.Match(Preferences, "\"username\": \"(.*?)\"").Groups[1].ToString();
                string pPassProxy = Regex.Match(Preferences, "\"password\": \"(.*?)\"").Groups[1].ToString();
                string pPublicip = Regex.Match(Preferences, "\"public_ip\": \"(.*?)\"").Groups[1].ToString();

                if (String.IsNullOrEmpty(pUserProxy))
                {
                    Preferences = Preferences.Replace("\"password\": \"\"", $"\"password\": \"{proxy.pass}\"");
                    Preferences = Preferences.Replace("\"username\": \"\"", $"\"username\": \"{proxy.user}\"" );
                    Preferences = Preferences.Replace("\"public_ip\": \"\"", $"\"public_ip\": \"{proxy.ip}\"");
                } else
                {
                    Preferences = Preferences.Replace(pUserProxy, proxy.user);
                    Preferences = Preferences.Replace(pPassProxy, proxy.pass);
                    Preferences = Preferences.Replace(pPublicip, proxy.ip);
                }

                isUpdate = true;
                profile.proxy = proxy.origin;
            }

            if (!String.IsNullOrEmpty(this.eUserAgent.Text) && this.eUserAgent.Text != profile.ua)
            {
                Preferences = Preferences.Replace(profile.ua, this.eUserAgent.Text);
                isUpdate = true;
                profile.ua = this.eUserAgent.Text;
            }

            if (!String.IsNullOrEmpty(this.eStartUrl.Text) && this.eStartUrl.Text != profile.start_url)
            {
                Preferences = Preferences.Replace(profile.start_url, this.eStartUrl.Text);
                isUpdate = true;
                profile.start_url = this.eStartUrl.Text;
            }

            if (isUpdate)
            {
                File.WriteAllText($"{BaseProfilePath}\\{profile.profile_id}\\Default\\Preferences", Preferences);
                SQLite.UpdateProfile(profile);
            }

            this.DialogResult = DialogResult.OK;
        }

        private void eRandomUA_Click(object sender, EventArgs e)
        {
            this.eUserAgent.Text = GoLogin.GetUA();
        }

        private void eRandomProxy_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(pathProxy))
            {
                this.eProxy.Text = Utils.RandomLineFileTxt(pathProxy);
            }
        }

        private void LoadComboboxGroup()
        {
            List<ComboBoxEntity> l = new List<ComboBoxEntity>
            {
                new ComboBoxEntity() { id = 0, name = "Select Group" }
            };

            List<GroupEntity> groups = SQLite.GetGroupsCountProfiles();

            int i = 0;
            int iSelected = 0;
            foreach (GroupEntity item in groups)
            {
                i++;
                l.Add(new ComboBoxEntity() { id = item.id, name = item.name });
                if (item.id.ToString() == profile.group_id)
                {
                    iSelected = 1;
                }
            }

            Thread.Sleep(1000);

            this.Invoke((Action)(() =>
            {
                this.eGroups.DataSource = l;
                this.eGroups.DisplayMember = "name";
                this.eGroups.ValueMember = "id";
                this.eGroups.SelectedIndex = iSelected;
            }));
        }
    }
}
