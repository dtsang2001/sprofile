﻿namespace ManagerSProfile.Forms
{
    partial class AddProfileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tableLayoutPanel1 = new TableLayoutPanel();
            materialCard3 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel2 = new TableLayoutPanel();
            eGroups = new MaterialSkin.Controls.MaterialComboBox();
            materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            materialCard4 = new MaterialSkin.Controls.MaterialCard();
            panel1 = new Panel();
            materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            SelectFileProxy = new MaterialSkin.Controls.MaterialButton();
            materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            tFileProxy = new MaterialSkin.Controls.MaterialLabel();
            materialCard1 = new MaterialSkin.Controls.MaterialCard();
            numOfProf = new MaterialSkin.Controls.MaterialTextBox2();
            materialCard2 = new MaterialSkin.Controls.MaterialCard();
            createBtn = new MaterialSkin.Controls.MaterialButton();
            tableLayoutPanel1.SuspendLayout();
            materialCard3.SuspendLayout();
            tableLayoutPanel2.SuspendLayout();
            materialCard4.SuspendLayout();
            panel1.SuspendLayout();
            materialCard1.SuspendLayout();
            materialCard2.SuspendLayout();
            SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel1.Controls.Add(materialCard3, 0, 1);
            tableLayoutPanel1.Controls.Add(materialCard4, 0, 2);
            tableLayoutPanel1.Controls.Add(materialCard1, 0, 0);
            tableLayoutPanel1.Controls.Add(materialCard2, 0, 3);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(3, 24);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 4;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 20.4365082F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 22.817461F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 35.6107674F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 21.5320911F));
            tableLayoutPanel1.Size = new Size(734, 483);
            tableLayoutPanel1.TabIndex = 0;
            // 
            // materialCard3
            // 
            materialCard3.BackColor = Color.FromArgb(255, 255, 255);
            materialCard3.Controls.Add(tableLayoutPanel2);
            materialCard3.Depth = 0;
            materialCard3.Dock = DockStyle.Fill;
            materialCard3.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard3.Location = new Point(14, 112);
            materialCard3.Margin = new Padding(14);
            materialCard3.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard3.Name = "materialCard3";
            materialCard3.Padding = new Padding(14);
            materialCard3.Size = new Size(706, 81);
            materialCard3.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.ColumnCount = 2;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel2.Controls.Add(eGroups, 1, 0);
            tableLayoutPanel2.Controls.Add(materialLabel1, 0, 0);
            tableLayoutPanel2.Dock = DockStyle.Fill;
            tableLayoutPanel2.Location = new Point(14, 14);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 1;
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel2.Size = new Size(678, 53);
            tableLayoutPanel2.TabIndex = 0;
            // 
            // eGroups
            // 
            eGroups.AutoResize = false;
            eGroups.BackColor = Color.FromArgb(255, 255, 255);
            eGroups.Depth = 0;
            eGroups.Dock = DockStyle.Fill;
            eGroups.DrawMode = DrawMode.OwnerDrawVariable;
            eGroups.DropDownHeight = 174;
            eGroups.DropDownStyle = ComboBoxStyle.DropDownList;
            eGroups.DropDownWidth = 121;
            eGroups.Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            eGroups.ForeColor = Color.FromArgb(222, 0, 0, 0);
            eGroups.FormattingEnabled = true;
            eGroups.IntegralHeight = false;
            eGroups.ItemHeight = 43;
            eGroups.Location = new Point(67, 4);
            eGroups.Margin = new Padding(3, 4, 3, 4);
            eGroups.MaxDropDownItems = 4;
            eGroups.MouseState = MaterialSkin.MouseState.OUT;
            eGroups.Name = "eGroups";
            eGroups.Size = new Size(608, 49);
            eGroups.StartIndex = 0;
            eGroups.TabIndex = 16;
            // 
            // materialLabel1
            // 
            materialLabel1.AutoSize = true;
            materialLabel1.Depth = 0;
            materialLabel1.Dock = DockStyle.Fill;
            materialLabel1.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel1.Location = new Point(10, 10);
            materialLabel1.Margin = new Padding(10);
            materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel1.Name = "materialLabel1";
            materialLabel1.Size = new Size(44, 33);
            materialLabel1.TabIndex = 17;
            materialLabel1.Text = "Group";
            materialLabel1.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // materialCard4
            // 
            materialCard4.BackColor = Color.FromArgb(255, 255, 255);
            materialCard4.Controls.Add(panel1);
            materialCard4.Controls.Add(materialLabel9);
            materialCard4.Controls.Add(materialLabel4);
            materialCard4.Controls.Add(tFileProxy);
            materialCard4.Depth = 0;
            materialCard4.Dock = DockStyle.Fill;
            materialCard4.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard4.Location = new Point(14, 221);
            materialCard4.Margin = new Padding(14);
            materialCard4.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard4.Name = "materialCard4";
            materialCard4.Padding = new Padding(14);
            materialCard4.Size = new Size(706, 143);
            materialCard4.TabIndex = 3;
            // 
            // panel1
            // 
            panel1.Controls.Add(materialLabel2);
            panel1.Controls.Add(SelectFileProxy);
            panel1.Dock = DockStyle.Fill;
            panel1.Location = new Point(14, 83);
            panel1.Margin = new Padding(0);
            panel1.Name = "panel1";
            panel1.Size = new Size(678, 46);
            panel1.TabIndex = 4;
            // 
            // materialLabel2
            // 
            materialLabel2.Depth = 0;
            materialLabel2.Dock = DockStyle.Left;
            materialLabel2.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel2.Location = new Point(0, 0);
            materialLabel2.Margin = new Padding(12, 0, 12, 0);
            materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel2.Name = "materialLabel2";
            materialLabel2.Padding = new Padding(12);
            materialLabel2.Size = new Size(92, 46);
            materialLabel2.TabIndex = 0;
            materialLabel2.Text = "File Proxies";
            materialLabel2.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // SelectFileProxy
            // 
            SelectFileProxy.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            SelectFileProxy.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            SelectFileProxy.Depth = 0;
            SelectFileProxy.Dock = DockStyle.Right;
            SelectFileProxy.HighEmphasis = true;
            SelectFileProxy.Icon = null;
            SelectFileProxy.Location = new Point(572, 0);
            SelectFileProxy.Margin = new Padding(4, 6, 4, 6);
            SelectFileProxy.MouseState = MaterialSkin.MouseState.HOVER;
            SelectFileProxy.Name = "SelectFileProxy";
            SelectFileProxy.NoAccentTextColor = Color.Empty;
            SelectFileProxy.Size = new Size(106, 46);
            SelectFileProxy.TabIndex = 2;
            SelectFileProxy.Text = "Select File";
            SelectFileProxy.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            SelectFileProxy.UseAccentColor = false;
            SelectFileProxy.UseVisualStyleBackColor = true;
            SelectFileProxy.Click += SelectFileProxy_Click;
            // 
            // materialLabel9
            // 
            materialLabel9.BackColor = Color.White;
            materialLabel9.Depth = 0;
            materialLabel9.Dock = DockStyle.Top;
            materialLabel9.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel9.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel9.ForeColor = Color.LightCoral;
            materialLabel9.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel9.Location = new Point(14, 66);
            materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel9.Name = "materialLabel9";
            materialLabel9.Size = new Size(678, 17);
            materialLabel9.TabIndex = 6;
            materialLabel9.Text = "** File .txt";
            // 
            // materialLabel4
            // 
            materialLabel4.BackColor = Color.White;
            materialLabel4.Depth = 0;
            materialLabel4.Dock = DockStyle.Top;
            materialLabel4.Font = new Font("Roboto", 12F, FontStyle.Italic, GraphicsUnit.Pixel);
            materialLabel4.FontType = MaterialSkin.MaterialSkinManager.fontType.SubtleEmphasis;
            materialLabel4.ForeColor = Color.LightCoral;
            materialLabel4.LiveSetting = System.Windows.Forms.Automation.AutomationLiveSetting.Polite;
            materialLabel4.Location = new Point(14, 49);
            materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel4.Name = "materialLabel4";
            materialLabel4.Size = new Size(678, 17);
            materialLabel4.TabIndex = 5;
            materialLabel4.Text = "* Required syntax of proxy: ip:port:username:password";
            // 
            // tFileProxy
            // 
            tFileProxy.Depth = 0;
            tFileProxy.Dock = DockStyle.Top;
            tFileProxy.Font = new Font("Roboto Medium", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
            tFileProxy.FontType = MaterialSkin.MaterialSkinManager.fontType.Subtitle2;
            tFileProxy.Location = new Point(14, 14);
            tFileProxy.Margin = new Padding(0);
            tFileProxy.MouseState = MaterialSkin.MouseState.HOVER;
            tFileProxy.Name = "tFileProxy";
            tFileProxy.Size = new Size(678, 35);
            tFileProxy.TabIndex = 3;
            tFileProxy.Text = "Path File...";
            tFileProxy.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // materialCard1
            // 
            materialCard1.BackColor = Color.FromArgb(255, 255, 255);
            materialCard1.Controls.Add(numOfProf);
            materialCard1.Depth = 0;
            materialCard1.Dock = DockStyle.Fill;
            materialCard1.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard1.Location = new Point(14, 14);
            materialCard1.Margin = new Padding(14);
            materialCard1.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard1.Name = "materialCard1";
            materialCard1.Padding = new Padding(14);
            materialCard1.Size = new Size(706, 70);
            materialCard1.TabIndex = 4;
            // 
            // numOfProf
            // 
            numOfProf.AnimateReadOnly = false;
            numOfProf.BackgroundImageLayout = ImageLayout.None;
            numOfProf.CharacterCasing = CharacterCasing.Normal;
            numOfProf.Depth = 0;
            numOfProf.Dock = DockStyle.Fill;
            numOfProf.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            numOfProf.HideSelection = true;
            numOfProf.Hint = "Số profile cần tạo";
            numOfProf.LeadingIcon = null;
            numOfProf.Location = new Point(14, 14);
            numOfProf.Margin = new Padding(2);
            numOfProf.MaxLength = 32767;
            numOfProf.MouseState = MaterialSkin.MouseState.OUT;
            numOfProf.Name = "numOfProf";
            numOfProf.PasswordChar = '\0';
            numOfProf.PrefixSuffixText = null;
            numOfProf.ReadOnly = false;
            numOfProf.RightToLeft = RightToLeft.No;
            numOfProf.SelectedText = "";
            numOfProf.SelectionLength = 0;
            numOfProf.SelectionStart = 0;
            numOfProf.ShortcutsEnabled = true;
            numOfProf.Size = new Size(678, 48);
            numOfProf.TabIndex = 0;
            numOfProf.TabStop = false;
            numOfProf.TextAlign = HorizontalAlignment.Left;
            numOfProf.TrailingIcon = null;
            numOfProf.UseSystemPasswordChar = false;
            // 
            // materialCard2
            // 
            materialCard2.BackColor = Color.FromArgb(255, 255, 255);
            materialCard2.Controls.Add(createBtn);
            materialCard2.Depth = 0;
            materialCard2.Dock = DockStyle.Fill;
            materialCard2.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard2.Location = new Point(14, 392);
            materialCard2.Margin = new Padding(14);
            materialCard2.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard2.Name = "materialCard2";
            materialCard2.Padding = new Padding(14);
            materialCard2.Size = new Size(706, 77);
            materialCard2.TabIndex = 5;
            // 
            // createBtn
            // 
            createBtn.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            createBtn.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            createBtn.Depth = 0;
            createBtn.Dock = DockStyle.Right;
            createBtn.HighEmphasis = true;
            createBtn.Icon = null;
            createBtn.Location = new Point(628, 14);
            createBtn.Margin = new Padding(0);
            createBtn.MouseState = MaterialSkin.MouseState.HOVER;
            createBtn.Name = "createBtn";
            createBtn.NoAccentTextColor = Color.Empty;
            createBtn.Size = new Size(64, 49);
            createBtn.TabIndex = 0;
            createBtn.Text = "Tạo";
            createBtn.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            createBtn.UseAccentColor = false;
            createBtn.UseVisualStyleBackColor = true;
            createBtn.Click += createBtn_Click;
            // 
            // AddProfileForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(740, 510);
            Controls.Add(tableLayoutPanel1);
            FormStyle = FormStyles.ActionBar_None;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "AddProfileForm";
            Padding = new Padding(3, 24, 3, 3);
            ShowInTaskbar = false;
            Sizable = false;
            Load += AddProfileForm_Load;
            tableLayoutPanel1.ResumeLayout(false);
            materialCard3.ResumeLayout(false);
            tableLayoutPanel2.ResumeLayout(false);
            tableLayoutPanel2.PerformLayout();
            materialCard4.ResumeLayout(false);
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            materialCard1.ResumeLayout(false);
            materialCard2.ResumeLayout(false);
            materialCard2.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel tableLayoutPanel1;
        private MaterialSkin.Controls.MaterialTextBox2 numOfProf;
        private MaterialSkin.Controls.MaterialButton createBtn;
        private MaterialSkin.Controls.MaterialCard materialCard3;
        private MaterialSkin.Controls.MaterialCard materialCard4;
        private MaterialSkin.Controls.MaterialCard materialCard1;
        private MaterialSkin.Controls.MaterialCard materialCard2;
        private TableLayoutPanel tableLayoutPanel2;
        private MaterialSkin.Controls.MaterialComboBox eGroups;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private Panel panel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialButton SelectFileProxy;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialLabel tFileProxy;
    }
}