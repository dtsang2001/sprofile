﻿using CoreSProfile;
using CoreSProfile.Entity;
using MaterialSkin;
using MaterialSkin.Controls;

namespace ManagerSProfile.Forms
{
    public partial class SimulateOptForm : MaterialForm
    {
        public SimulateOptForm()
        {
            InitializeComponent();
            InitSkin();
        }

        private void InitSkin()
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Blue600,
                Primary.Blue800,
                Primary.Blue300,
                Accent.Blue100,
                TextShade.WHITE
            );
        }

        public Point size { get; set; } = new Point(375, 880);
        public int zoom { get; set; }

        private bool using_proxy = true;
        public List<ProfileEntity> profilesStarted { get; internal set; }

        private void materialButton1_Click(object sender, EventArgs e)
        {
            int width = int.Parse(widTxt.Text.Trim());
            int height = int.Parse(hTxt.Text.Trim());
            int screenWidth = Screen.PrimaryScreen.Bounds.Width;
            int screenHeight = Screen.PrimaryScreen.Bounds.Height;

            List<Point> lstPos = new List<Point>();

            size = new Point(width, height);
            zoom = int.Parse(zoomtxt.SelectedItem.ToString());

            //screenWidth = (zoomtxt.Text.Trim() != null) ? screenWidth * 8 / 10 : screenWidth;
            lstPos = CalculateWindowPositions(screenWidth, screenHeight, width * zoom / 100, height * zoom / 100, profilesStarted.Count);

            if (ratioProxy.CheckState == CheckState.Unchecked) { using_proxy = false; }

            List<Task> tasks = new List<Task>();

            foreach (ProfileEntity item in profilesStarted)
            {
                tasks.Add(Task.Run(() =>
                {
                    new Browser
                    {
                        baseProfilePath = Config.folderProfile,
                        zoom = zoom,
                        size = size,
                        pos = lstPos.ElementAt(item.id),
                        proxy = using_proxy,
                    }.Open(item, item.id);
                }));
            }
            Task.WhenAll(tasks);
           
            this.DialogResult = DialogResult.OK;
        }

        public List<Point> CalculateWindowPositions(int screenWidth, int screenHeight, int windowWidth, int windowHeight, int numWindows)
        {
            var listPositions = new List<Point> { };
            int x = 0;
            int y = 0;
            int index = 0;

            while (index < numWindows)
            {
                listPositions.Add(new Point(x, y));
                x += windowWidth;
                index++;

                if (x + windowWidth > screenWidth) // out range?
                {
                    x = 0; // new row
                    y += windowHeight; // next row
                    // when want limit u browser
                    //if (y + windowHeight > screenHeight)
                    //{
                    //    y = 0;
                    //}
                }
            }

            return listPositions;
        }
    }
}