﻿namespace ManagerSProfile.Forms
{
    partial class ViewContentMailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            materialCard2 = new MaterialSkin.Controls.MaterialCard();
            tableLayoutPanel2 = new TableLayoutPanel();
            tableLayoutPanel1 = new TableLayoutPanel();
            materialCard3 = new MaterialSkin.Controls.MaterialCard();
            ListBoxLink = new MaterialSkin.Controls.MaterialListBox();
            materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            materialCard4 = new MaterialSkin.Controls.MaterialCard();
            ListBoxMainContent = new MaterialSkin.Controls.MaterialListBox();
            materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            materialCard1 = new MaterialSkin.Controls.MaterialCard();
            materialButton1 = new MaterialSkin.Controls.MaterialButton();
            TextBoxShow = new MaterialSkin.Controls.MaterialMultiLineTextBox();
            materialCard2.SuspendLayout();
            tableLayoutPanel2.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            materialCard3.SuspendLayout();
            materialCard4.SuspendLayout();
            materialCard1.SuspendLayout();
            SuspendLayout();
            // 
            // materialCard2
            // 
            materialCard2.BackColor = Color.FromArgb(255, 255, 255);
            materialCard2.Controls.Add(tableLayoutPanel2);
            materialCard2.Depth = 0;
            materialCard2.Dock = DockStyle.Fill;
            materialCard2.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard2.Location = new Point(3, 64);
            materialCard2.Margin = new Padding(14);
            materialCard2.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard2.Name = "materialCard2";
            materialCard2.Padding = new Padding(14);
            materialCard2.Size = new Size(1269, 707);
            materialCard2.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.ColumnCount = 1;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel2.Controls.Add(tableLayoutPanel1, 0, 0);
            tableLayoutPanel2.Controls.Add(materialCard1, 0, 1);
            tableLayoutPanel2.Location = new Point(14, 17);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 2;
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 67.87565F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 32.12435F));
            tableLayoutPanel2.Size = new Size(1238, 673);
            tableLayoutPanel2.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 2;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 70.34549F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 29.65451F));
            tableLayoutPanel1.Controls.Add(materialCard3, 1, 0);
            tableLayoutPanel1.Controls.Add(materialCard4, 0, 0);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(3, 3);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 1;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.Size = new Size(1232, 450);
            tableLayoutPanel1.TabIndex = 0;
            // 
            // materialCard3
            // 
            materialCard3.BackColor = Color.FromArgb(255, 255, 255);
            materialCard3.Controls.Add(ListBoxLink);
            materialCard3.Controls.Add(materialLabel1);
            materialCard3.Depth = 0;
            materialCard3.Dock = DockStyle.Fill;
            materialCard3.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard3.Location = new Point(880, 14);
            materialCard3.Margin = new Padding(14);
            materialCard3.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard3.Name = "materialCard3";
            materialCard3.Padding = new Padding(14);
            materialCard3.Size = new Size(338, 422);
            materialCard3.TabIndex = 1;
            // 
            // ListBoxLink
            // 
            ListBoxLink.BackColor = Color.White;
            ListBoxLink.BorderColor = Color.LightGray;
            ListBoxLink.Depth = 0;
            ListBoxLink.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            ListBoxLink.Location = new Point(17, 55);
            ListBoxLink.MouseState = MaterialSkin.MouseState.HOVER;
            ListBoxLink.Name = "ListBoxLink";
            ListBoxLink.SelectedIndex = -1;
            ListBoxLink.SelectedItem = null;
            ListBoxLink.Size = new Size(308, 350);
            ListBoxLink.TabIndex = 2;
            ListBoxLink.SelectedValueChanged += onClickItemLink;
            // 
            // materialLabel1
            // 
            materialLabel1.AutoSize = true;
            materialLabel1.Depth = 0;
            materialLabel1.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel1.Location = new Point(17, 14);
            materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel1.Name = "materialLabel1";
            materialLabel1.Size = new Size(60, 19);
            materialLabel1.TabIndex = 1;
            materialLabel1.Text = "List link:";
            // 
            // materialCard4
            // 
            materialCard4.BackColor = Color.FromArgb(255, 255, 255);
            materialCard4.Controls.Add(ListBoxMainContent);
            materialCard4.Controls.Add(materialLabel2);
            materialCard4.Depth = 0;
            materialCard4.Dock = DockStyle.Fill;
            materialCard4.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard4.Location = new Point(14, 14);
            materialCard4.Margin = new Padding(14);
            materialCard4.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard4.Name = "materialCard4";
            materialCard4.Padding = new Padding(14);
            materialCard4.Size = new Size(838, 422);
            materialCard4.TabIndex = 2;
            // 
            // ListBoxMainContent
            // 
            ListBoxMainContent.BackColor = Color.White;
            ListBoxMainContent.BorderColor = Color.LightGray;
            ListBoxMainContent.Depth = 0;
            ListBoxMainContent.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            ListBoxMainContent.Location = new Point(17, 55);
            ListBoxMainContent.MouseState = MaterialSkin.MouseState.HOVER;
            ListBoxMainContent.Name = "ListBoxMainContent";
            ListBoxMainContent.SelectedIndex = -1;
            ListBoxMainContent.SelectedItem = null;
            ListBoxMainContent.Size = new Size(814, 350);
            ListBoxMainContent.TabIndex = 3;
            ListBoxMainContent.SelectedValueChanged += onClickItemContent;
            // 
            // materialLabel2
            // 
            materialLabel2.AutoSize = true;
            materialLabel2.Depth = 0;
            materialLabel2.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            materialLabel2.Location = new Point(17, 14);
            materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            materialLabel2.Name = "materialLabel2";
            materialLabel2.Size = new Size(94, 19);
            materialLabel2.TabIndex = 2;
            materialLabel2.Text = "Main content";
            // 
            // materialCard1
            // 
            materialCard1.BackColor = Color.FromArgb(255, 255, 255);
            materialCard1.Controls.Add(materialButton1);
            materialCard1.Controls.Add(TextBoxShow);
            materialCard1.Depth = 0;
            materialCard1.Dock = DockStyle.Fill;
            materialCard1.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard1.Location = new Point(14, 470);
            materialCard1.Margin = new Padding(14);
            materialCard1.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard1.Name = "materialCard1";
            materialCard1.Padding = new Padding(14);
            materialCard1.Size = new Size(1210, 189);
            materialCard1.TabIndex = 1;
            // 
            // materialButton1
            // 
            materialButton1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            materialButton1.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            materialButton1.Depth = 0;
            materialButton1.HighEmphasis = true;
            materialButton1.Icon = null;
            materialButton1.Location = new Point(1128, 76);
            materialButton1.Margin = new Padding(4, 6, 4, 6);
            materialButton1.MouseState = MaterialSkin.MouseState.HOVER;
            materialButton1.Name = "materialButton1";
            materialButton1.NoAccentTextColor = Color.Empty;
            materialButton1.Size = new Size(64, 36);
            materialButton1.TabIndex = 1;
            materialButton1.Text = "Copy";
            materialButton1.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            materialButton1.UseAccentColor = false;
            materialButton1.UseVisualStyleBackColor = true;
            // 
            // TextBoxShow
            // 
            TextBoxShow.BackColor = Color.FromArgb(255, 255, 255);
            TextBoxShow.BorderStyle = BorderStyle.None;
            TextBoxShow.Depth = 0;
            TextBoxShow.Font = new Font("Roboto", 14F, FontStyle.Regular, GraphicsUnit.Pixel);
            TextBoxShow.ForeColor = Color.FromArgb(222, 0, 0, 0);
            TextBoxShow.Location = new Point(14, 14);
            TextBoxShow.MouseState = MaterialSkin.MouseState.HOVER;
            TextBoxShow.Name = "TextBoxShow";
            TextBoxShow.Size = new Size(1093, 161);
            TextBoxShow.TabIndex = 0;
            TextBoxShow.Text = "";
            // 
            // ViewContentMailForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1275, 774);
            Controls.Add(materialCard2);
            Name = "ViewContentMailForm";
            Sizable = false;
            Text = "ViewContentMailForm";
            Load += ViewContentMailForm_Load;
            materialCard2.ResumeLayout(false);
            tableLayoutPanel2.ResumeLayout(false);
            tableLayoutPanel1.ResumeLayout(false);
            materialCard3.ResumeLayout(false);
            materialCard3.PerformLayout();
            materialCard4.ResumeLayout(false);
            materialCard4.PerformLayout();
            materialCard1.ResumeLayout(false);
            materialCard1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private MaterialSkin.Controls.MaterialCard materialCard1;
        private MaterialSkin.Controls.MaterialCard materialCard2;
        private TableLayoutPanel tableLayoutPanel1;
        private MaterialSkin.Controls.MaterialCard materialCard4;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialListBox ListBoxMainContent;
        private MaterialSkin.Controls.MaterialScrollBar materialScrollBar1;
        private TableLayoutPanel tableLayoutPanel2;
        private MaterialSkin.Controls.MaterialCard materialCard3;
        private MaterialSkin.Controls.MaterialListBox ListBoxLink;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialMultiLineTextBox TextBoxShow;
        private MaterialSkin.Controls.MaterialButton materialButton1;
    }
}