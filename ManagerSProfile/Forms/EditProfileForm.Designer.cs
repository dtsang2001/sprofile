﻿namespace ManagerSProfile.Forms
{
    partial class EditProfileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.materialCard2 = new MaterialSkin.Controls.MaterialCard();
            this.eSave = new MaterialSkin.Controls.MaterialButton();
            this.materialCard1 = new MaterialSkin.Controls.MaterialCard();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.eProfileID = new MaterialSkin.Controls.MaterialTextBox();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.eUserAgent = new MaterialSkin.Controls.MaterialTextBox();
            this.eRandomUA = new MaterialSkin.Controls.MaterialButton();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.eGroups = new MaterialSkin.Controls.MaterialComboBox();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.eProxy = new MaterialSkin.Controls.MaterialTextBox();
            this.eRandomProxy = new MaterialSkin.Controls.MaterialButton();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.eStartUrl = new MaterialSkin.Controls.MaterialTextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.materialCard2.SuspendLayout();
            this.materialCard1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.materialCard2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.materialCard1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(429, 673);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // materialCard2
            // 
            this.materialCard2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.materialCard2.Controls.Add(this.eSave);
            this.materialCard2.Depth = 0;
            this.materialCard2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialCard2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialCard2.Location = new System.Drawing.Point(14, 615);
            this.materialCard2.Margin = new System.Windows.Forms.Padding(14, 2, 14, 14);
            this.materialCard2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialCard2.Name = "materialCard2";
            this.materialCard2.Padding = new System.Windows.Forms.Padding(4);
            this.materialCard2.Size = new System.Drawing.Size(401, 44);
            this.materialCard2.TabIndex = 1;
            // 
            // eSave
            // 
            this.eSave.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.eSave.BackColor = System.Drawing.Color.YellowGreen;
            this.eSave.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.eSave.Depth = 0;
            this.eSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.eSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.eSave.HighEmphasis = true;
            this.eSave.Icon = null;
            this.eSave.Location = new System.Drawing.Point(333, 4);
            this.eSave.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.eSave.MouseState = MaterialSkin.MouseState.HOVER;
            this.eSave.Name = "eSave";
            this.eSave.NoAccentTextColor = System.Drawing.Color.Empty;
            this.eSave.Size = new System.Drawing.Size(64, 36);
            this.eSave.TabIndex = 0;
            this.eSave.Text = "Save";
            this.eSave.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            this.eSave.UseAccentColor = false;
            this.eSave.UseVisualStyleBackColor = false;
            this.eSave.Click += new System.EventHandler(this.eSave_Click);
            // 
            // materialCard1
            // 
            this.materialCard1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.materialCard1.Controls.Add(this.flowLayoutPanel1);
            this.materialCard1.Depth = 0;
            this.materialCard1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialCard1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialCard1.Location = new System.Drawing.Point(14, 14);
            this.materialCard1.Margin = new System.Windows.Forms.Padding(14);
            this.materialCard1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialCard1.Name = "materialCard1";
            this.materialCard1.Padding = new System.Windows.Forms.Padding(14);
            this.materialCard1.Size = new System.Drawing.Size(401, 585);
            this.materialCard1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.materialLabel1);
            this.flowLayoutPanel1.Controls.Add(this.eProfileID);
            this.flowLayoutPanel1.Controls.Add(this.materialLabel2);
            this.flowLayoutPanel1.Controls.Add(this.eUserAgent);
            this.flowLayoutPanel1.Controls.Add(this.eRandomUA);
            this.flowLayoutPanel1.Controls.Add(this.materialLabel4);
            this.flowLayoutPanel1.Controls.Add(this.eGroups);
            this.flowLayoutPanel1.Controls.Add(this.materialLabel3);
            this.flowLayoutPanel1.Controls.Add(this.eProxy);
            this.flowLayoutPanel1.Controls.Add(this.eRandomProxy);
            this.flowLayoutPanel1.Controls.Add(this.materialLabel6);
            this.flowLayoutPanel1.Controls.Add(this.eStartUrl);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(14, 14);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(373, 557);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // materialLabel1
            // 
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel1.Location = new System.Drawing.Point(3, 12);
            this.materialLabel1.Margin = new System.Windows.Forms.Padding(3, 12, 3, 0);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(367, 23);
            this.materialLabel1.TabIndex = 0;
            this.materialLabel1.Text = "Profile ID";
            this.materialLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // eProfileID
            // 
            this.eProfileID.AnimateReadOnly = false;
            this.eProfileID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.eProfileID.Depth = 0;
            this.eProfileID.Enabled = false;
            this.eProfileID.Font = new System.Drawing.Font("Roboto", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.eProfileID.LeadingIcon = null;
            this.eProfileID.Location = new System.Drawing.Point(3, 38);
            this.eProfileID.MaxLength = 50;
            this.eProfileID.MouseState = MaterialSkin.MouseState.OUT;
            this.eProfileID.Multiline = false;
            this.eProfileID.Name = "eProfileID";
            this.eProfileID.ReadOnly = true;
            this.eProfileID.Size = new System.Drawing.Size(367, 50);
            this.eProfileID.TabIndex = 1;
            this.eProfileID.Text = "";
            this.eProfileID.TrailingIcon = null;
            // 
            // materialLabel2
            // 
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel2.Location = new System.Drawing.Point(3, 103);
            this.materialLabel2.Margin = new System.Windows.Forms.Padding(3, 12, 3, 0);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(367, 23);
            this.materialLabel2.TabIndex = 2;
            this.materialLabel2.Text = "User Agent";
            this.materialLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // eUserAgent
            // 
            this.eUserAgent.AnimateReadOnly = false;
            this.eUserAgent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.eUserAgent.Depth = 0;
            this.eUserAgent.Font = new System.Drawing.Font("Roboto", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.eUserAgent.LeadingIcon = null;
            this.eUserAgent.Location = new System.Drawing.Point(3, 129);
            this.eUserAgent.MaxLength = 50;
            this.eUserAgent.MouseState = MaterialSkin.MouseState.OUT;
            this.eUserAgent.Multiline = false;
            this.eUserAgent.Name = "eUserAgent";
            this.eUserAgent.Size = new System.Drawing.Size(296, 50);
            this.eUserAgent.TabIndex = 3;
            this.eUserAgent.Text = "";
            this.eUserAgent.TrailingIcon = null;
            // 
            // eRandomUA
            // 
            this.eRandomUA.AutoSize = false;
            this.eRandomUA.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.eRandomUA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.eRandomUA.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.eRandomUA.Depth = 0;
            this.eRandomUA.HighEmphasis = true;
            this.eRandomUA.Icon = null;
            this.eRandomUA.Image = global::ManagerSProfile.Properties.Resources.rotate_right;
            this.eRandomUA.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.eRandomUA.Location = new System.Drawing.Point(306, 132);
            this.eRandomUA.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.eRandomUA.MouseState = MaterialSkin.MouseState.HOVER;
            this.eRandomUA.Name = "eRandomUA";
            this.eRandomUA.NoAccentTextColor = System.Drawing.Color.Empty;
            this.eRandomUA.Size = new System.Drawing.Size(63, 46);
            this.eRandomUA.TabIndex = 4;
            this.eRandomUA.Text = "Random";
            this.eRandomUA.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Outlined;
            this.eRandomUA.UseAccentColor = false;
            this.eRandomUA.UseVisualStyleBackColor = true;
            this.eRandomUA.Click += new System.EventHandler(this.eRandomUA_Click);
            // 
            // materialLabel4
            // 
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel4.Location = new System.Drawing.Point(3, 196);
            this.materialLabel4.Margin = new System.Windows.Forms.Padding(3, 12, 3, 0);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(367, 23);
            this.materialLabel4.TabIndex = 14;
            this.materialLabel4.Text = "Group";
            this.materialLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // eGroups
            // 
            this.eGroups.AutoResize = false;
            this.eGroups.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.eGroups.Depth = 0;
            this.eGroups.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.eGroups.DropDownHeight = 174;
            this.eGroups.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.eGroups.DropDownWidth = 121;
            this.eGroups.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.eGroups.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.eGroups.FormattingEnabled = true;
            this.eGroups.IntegralHeight = false;
            this.eGroups.ItemHeight = 43;
            this.eGroups.Location = new System.Drawing.Point(3, 222);
            this.eGroups.MaxDropDownItems = 4;
            this.eGroups.MouseState = MaterialSkin.MouseState.OUT;
            this.eGroups.Name = "eGroups";
            this.eGroups.Size = new System.Drawing.Size(366, 49);
            this.eGroups.StartIndex = 0;
            this.eGroups.TabIndex = 15;
            // 
            // materialLabel3
            // 
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel3.Location = new System.Drawing.Point(3, 286);
            this.materialLabel3.Margin = new System.Windows.Forms.Padding(3, 12, 3, 0);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(367, 23);
            this.materialLabel3.TabIndex = 5;
            this.materialLabel3.Text = "Proxy";
            this.materialLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // eProxy
            // 
            this.eProxy.AnimateReadOnly = false;
            this.eProxy.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.eProxy.Depth = 0;
            this.eProxy.Font = new System.Drawing.Font("Roboto", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.eProxy.LeadingIcon = null;
            this.eProxy.Location = new System.Drawing.Point(3, 312);
            this.eProxy.MaxLength = 50;
            this.eProxy.MouseState = MaterialSkin.MouseState.OUT;
            this.eProxy.Multiline = false;
            this.eProxy.Name = "eProxy";
            this.eProxy.Size = new System.Drawing.Size(296, 50);
            this.eProxy.TabIndex = 6;
            this.eProxy.Text = "";
            this.eProxy.TrailingIcon = null;
            // 
            // eRandomProxy
            // 
            this.eRandomProxy.AutoSize = false;
            this.eRandomProxy.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.eRandomProxy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.eRandomProxy.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            this.eRandomProxy.Depth = 0;
            this.eRandomProxy.HighEmphasis = true;
            this.eRandomProxy.Icon = null;
            this.eRandomProxy.Image = global::ManagerSProfile.Properties.Resources.rotate_right;
            this.eRandomProxy.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.eRandomProxy.Location = new System.Drawing.Point(306, 315);
            this.eRandomProxy.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.eRandomProxy.MouseState = MaterialSkin.MouseState.HOVER;
            this.eRandomProxy.Name = "eRandomProxy";
            this.eRandomProxy.NoAccentTextColor = System.Drawing.Color.Empty;
            this.eRandomProxy.Size = new System.Drawing.Size(63, 46);
            this.eRandomProxy.TabIndex = 7;
            this.eRandomProxy.Text = "Random";
            this.eRandomProxy.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Outlined;
            this.eRandomProxy.UseAccentColor = false;
            this.eRandomProxy.UseVisualStyleBackColor = true;
            this.eRandomProxy.Click += new System.EventHandler(this.eRandomProxy_Click);
            // 
            // materialLabel6
            // 
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.materialLabel6.Location = new System.Drawing.Point(3, 379);
            this.materialLabel6.Margin = new System.Windows.Forms.Padding(3, 12, 3, 0);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(367, 23);
            this.materialLabel6.TabIndex = 12;
            this.materialLabel6.Text = "Start Url";
            this.materialLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // eStartUrl
            // 
            this.eStartUrl.AnimateReadOnly = false;
            this.eStartUrl.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.eStartUrl.Depth = 0;
            this.eStartUrl.Font = new System.Drawing.Font("Roboto", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.eStartUrl.LeadingIcon = null;
            this.eStartUrl.Location = new System.Drawing.Point(3, 405);
            this.eStartUrl.MaxLength = 50;
            this.eStartUrl.MouseState = MaterialSkin.MouseState.OUT;
            this.eStartUrl.Multiline = false;
            this.eStartUrl.Name = "eStartUrl";
            this.eStartUrl.Size = new System.Drawing.Size(367, 50);
            this.eStartUrl.TabIndex = 13;
            this.eStartUrl.Text = "";
            this.eStartUrl.TrailingIcon = null;
            // 
            // EditProfileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 700);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormStyle = MaterialSkin.Controls.MaterialForm.FormStyles.ActionBar_None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditProfileForm";
            this.Padding = new System.Windows.Forms.Padding(3, 24, 3, 3);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Sizable = false;
            this.Text = "EditProfileForm";
            this.Load += new System.EventHandler(this.EditProfileForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.materialCard2.ResumeLayout(false);
            this.materialCard2.PerformLayout();
            this.materialCard1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TableLayoutPanel tableLayoutPanel1;
        private MaterialSkin.Controls.MaterialCard materialCard2;
        private MaterialSkin.Controls.MaterialButton eSave;
        private MaterialSkin.Controls.MaterialCard materialCard1;
        private FlowLayoutPanel flowLayoutPanel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialTextBox eProfileID;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialTextBox eUserAgent;
        private MaterialSkin.Controls.MaterialButton eRandomUA;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialTextBox eProxy;
        private MaterialSkin.Controls.MaterialButton eRandomProxy;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialTextBox eStartUrl;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialComboBox eGroups;
    }
}