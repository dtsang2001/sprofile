﻿namespace ManagerSProfile.Forms
{
    partial class AddGroupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tableLayoutPanel1 = new TableLayoutPanel();
            materialCard2 = new MaterialSkin.Controls.MaterialCard();
            CreateGroup = new MaterialSkin.Controls.MaterialButton();
            materialCard1 = new MaterialSkin.Controls.MaterialCard();
            NewGroupName = new MaterialSkin.Controls.MaterialTextBox();
            tableLayoutPanel1.SuspendLayout();
            materialCard2.SuspendLayout();
            materialCard1.SuspendLayout();
            SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.Controls.Add(materialCard2, 0, 1);
            tableLayoutPanel1.Controls.Add(materialCard1, 0, 0);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(3, 32);
            tableLayoutPanel1.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 60.50956F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 39.49044F));
            tableLayoutPanel1.Size = new Size(480, 209);
            tableLayoutPanel1.TabIndex = 0;
            // 
            // materialCard2
            // 
            materialCard2.BackColor = Color.FromArgb(255, 255, 255);
            materialCard2.Controls.Add(CreateGroup);
            materialCard2.Depth = 0;
            materialCard2.Dock = DockStyle.Fill;
            materialCard2.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard2.Location = new Point(16, 126);
            materialCard2.Margin = new Padding(16, 0, 16, 19);
            materialCard2.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard2.Name = "materialCard2";
            materialCard2.Padding = new Padding(16, 5, 16, 5);
            materialCard2.Size = new Size(448, 64);
            materialCard2.TabIndex = 1;
            // 
            // CreateGroup
            // 
            CreateGroup.AutoSize = false;
            CreateGroup.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            CreateGroup.Cursor = Cursors.Hand;
            CreateGroup.Density = MaterialSkin.Controls.MaterialButton.MaterialButtonDensity.Default;
            CreateGroup.Depth = 0;
            CreateGroup.Dock = DockStyle.Right;
            CreateGroup.HighEmphasis = true;
            CreateGroup.Icon = null;
            CreateGroup.Location = new Point(314, 5);
            CreateGroup.Margin = new Padding(5, 8, 5, 8);
            CreateGroup.MouseState = MaterialSkin.MouseState.HOVER;
            CreateGroup.Name = "CreateGroup";
            CreateGroup.NoAccentTextColor = Color.Empty;
            CreateGroup.Size = new Size(118, 54);
            CreateGroup.TabIndex = 0;
            CreateGroup.Text = "Create";
            CreateGroup.Type = MaterialSkin.Controls.MaterialButton.MaterialButtonType.Contained;
            CreateGroup.UseAccentColor = false;
            CreateGroup.UseVisualStyleBackColor = true;
            CreateGroup.Click += CreateGroup_Click;
            // 
            // materialCard1
            // 
            materialCard1.BackColor = Color.FromArgb(255, 255, 255);
            materialCard1.Controls.Add(NewGroupName);
            materialCard1.Depth = 0;
            materialCard1.Dock = DockStyle.Fill;
            materialCard1.ForeColor = Color.FromArgb(222, 0, 0, 0);
            materialCard1.Location = new Point(16, 19);
            materialCard1.Margin = new Padding(16, 19, 16, 19);
            materialCard1.MouseState = MaterialSkin.MouseState.HOVER;
            materialCard1.Name = "materialCard1";
            materialCard1.Padding = new Padding(16, 19, 16, 19);
            materialCard1.Size = new Size(448, 88);
            materialCard1.TabIndex = 0;
            // 
            // NewGroupName
            // 
            NewGroupName.AnimateReadOnly = false;
            NewGroupName.BorderStyle = BorderStyle.None;
            NewGroupName.Depth = 0;
            NewGroupName.Dock = DockStyle.Fill;
            NewGroupName.Font = new Font("Roboto", 16F, FontStyle.Regular, GraphicsUnit.Pixel);
            NewGroupName.Hint = "Enter name!";
            NewGroupName.LeadingIcon = null;
            NewGroupName.Location = new Point(16, 19);
            NewGroupName.Margin = new Padding(3, 4, 3, 4);
            NewGroupName.MaxLength = 50;
            NewGroupName.MouseState = MaterialSkin.MouseState.OUT;
            NewGroupName.Multiline = false;
            NewGroupName.Name = "NewGroupName";
            NewGroupName.Size = new Size(416, 50);
            NewGroupName.TabIndex = 0;
            NewGroupName.Tag = "";
            NewGroupName.Text = "";
            NewGroupName.TrailingIcon = null;
            NewGroupName.KeyDown += NewGroupName_KeyDown;
            // 
            // AddGroupForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(486, 245);
            Controls.Add(tableLayoutPanel1);
            FormStyle = FormStyles.ActionBar_None;
            Margin = new Padding(3, 4, 3, 4);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "AddGroupForm";
            Padding = new Padding(3, 32, 3, 4);
            Sizable = false;
            Text = "AddGroupForm";
            Load += AddGroupForm_Load;
            tableLayoutPanel1.ResumeLayout(false);
            materialCard2.ResumeLayout(false);
            materialCard1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel tableLayoutPanel1;
        private MaterialSkin.Controls.MaterialCard materialCard2;
        private MaterialSkin.Controls.MaterialButton CreateGroup;
        private MaterialSkin.Controls.MaterialCard materialCard1;
        private MaterialSkin.Controls.MaterialTextBox NewGroupName;
    }
}