﻿using CoreSProfile.Entity;
using CoreSProfile.Helper;
using KAutoHelper;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using WebSocketSharp;
using static ManagerSProfile.MainForm;

namespace ManagerSProfile
{
    public class Macro
    {
        private List<string> events;
        public int loopInterval { get; set; }
        public string APITxt { get; set; }
        public string channelTxt { get; set; }
        public string brandTxt { get; set; }
        public string sqlTabtxt { get; set; }
        public bool IsSuscess { get; private set; }

        private JObject data;

        public IntPtr hanlde;

        public Macro()
        {
            events = new List<string>();
        }

        public bool IsExistEvent()
        {
            if (events.Count == 0)
            {
                return true;
            }
            return false;
        }

        public void AddEvent(string action)
        {
            events.Add(action);
        }

        public void ClearEvents()
        {
            events.Clear();
        }

        public void Save(string path)
        {
            try
            {
                using (var streamWriter = new StreamWriter(path))
                {
                    streamWriter.AutoFlush = true;

                    events.ForEach(e =>
                    {
                        streamWriter.WriteLine(e);
                    });
                    streamWriter.Close();
                    ClearEvents();
                }
            }
            catch (Exception ex)
            {
                DialogResult dialogResult = MessageBox.Show($"Saved failed?{ex}", "Message", MessageBoxButtons.OK);
            }
        }

        public void Load(string path)
        {
            ClearEvents();

            string[] action = File.ReadAllLines(path);
            foreach (var item in action)
            {
                events.Add(item);
            }
        }

        public void Stop()
        {
            events.Clear();
        }

        public List<RecordEntity> LoadAllRecord(string folderPath)
        {
            ClearEvents();
            List<RecordEntity> rs = new();
            string[] files = Directory.GetFiles(folderPath);
            int index = 0;
            foreach (string item in files)
            {
                RecordEntity recordEntity = new();
                recordEntity.id = index++;
                recordEntity.record_link = item;
                recordEntity.record_name = Path.GetFileName(item);
                recordEntity.record_last_modify = File.GetLastWriteTime(item).ToString();
                recordEntity.record_time = Directory.GetCreationTime(item).ToString();
                rs.Add(recordEntity);
            }
            return rs;
        }

        public void Play()
        {
            BrowserService browserService = new();
            if (!APITxt.IsNullOrEmpty()   && !channelTxt.IsNullOrEmpty())
            {
                data = browserService.getTask(APITxt, channelTxt);
                data["yahoo"] = "false";
                data["getContent"] = "false";
            }
            Thread.Sleep(loopInterval * 1000);
            DoAction();
        }
        private void DoAction()
        {
            foreach (var item in events)
            {
                if (cancellationTokenSource.Token.IsCancellationRequested)
                {
                    break; // try end task ngay lập tức
                }
                string[] parts = item.Split(',');
                switch (parts[0].Trim())
                {
                    case "delay":
                        if (parts[1].Trim().Length > 8)
                        {
                            Thread.Sleep(500);
                        }
                        else
                        {
                            int millisecondsToSleep = (int)(int.Parse(parts[1].Trim()) / TimeSpan.TicksPerMillisecond);
                            Thread.Sleep(millisecondsToSleep);
                        }
                        break;

                    case "Left":
                        MouseAction(int.Parse(parts[1].Trim()), int.Parse(parts[2].Trim()), 0, EMouseKey.LEFT);
                        break;

                    case "Right":
                        MouseAction(int.Parse(parts[1].Trim()), int.Parse(parts[2].Trim()), 0, EMouseKey.RIGHT);
                        break;

                    case "Middle":
                        MouseActionScroll(int.Parse(parts[1].Trim()), int.Parse(parts[2].Trim()), int.Parse(parts[3].Trim()));
                        break;

                    case "down":
                        VKeys vKey1;
                        if (Enum.TryParse<VKeys>(parts[1].Trim(), out vKey1))
                        {
                            Debug.WriteLine($"Enum value: {vKey1}");
                        }
                        KeyAction(parts[0].Trim(), vKey1);
                        //
                        Random random = new Random();
                        int randomValue = random.Next(0, 2);
                        this.IsSuscess = randomValue == 1;
                        /*this.IsSuscess = false;*/

                        //
                        break;

                    case "up":
                        VKeys vKey2;
                        if (Enum.TryParse<VKeys>(parts[1].Trim(), out vKey2))
                        {
                            Debug.WriteLine($"Enum value: {vKey2}");
                        }
                        KeyAction(parts[0].Trim(), vKey2);
                        break;

                    case "full_name":

                    case "first_name":

                    case "last_name":

                    case "phone":

                    case "phone2":

                    case "email":

                    case "password":

                    case "password2":

                    case "username":

                    case "username2":

                    case "dob2":

                    case "dod":

                    case "dom":

                    case "doy":
                        TypeGetTask(parts[0].Trim());
                        break;

                    case "get OTP":
                        TypeOTP();
                        break;

                    case "saveAccout":
                        SaveAcc();
                        break;

                    case "yahoo mail":
                        BrowserService browserService1 = new();
                        var yahooMailData = browserService1.getYahooMail();
                        KeyboardHelper.SendVieChar(hanlde, yahooMailData["email"].ToString());
                        break;
                }
            }
        }
        // replay action
        private void SaveAcc()
        {
            BrowserService browserService = new();

            JObject save = browserService.saveAccount(APITxt, data, sqlTabtxt);
        }

        private void TypeOTP()
        {
            BrowserService browserService = new();
            string otp = browserService.getOTP(APITxt, data["phone"].ToString(), brandTxt, 10);
            KeyboardHelper.SendVieChar(hanlde, otp);
        }

        private void TypeGetTask(string action)
        {
            KeyboardHelper.SendVieChar(hanlde, data[action].ToString());
        }

        private void KeyAction(string? sender, VKeys key)
        {
            try
            {
                uint repeatCount = 0;
                uint scanCode = (UInt32)key;
                uint extended = 0;
                uint context = 0;
                uint previousState = 0;
                uint transition = 0;

                uint lParamDown;
                uint lParamUp;

                lParamDown = repeatCount
                    | (scanCode << 16)
                    | (extended << 24)
                    | (context << 29)
                    | (previousState << 30)
                    | (transition << 31);
                previousState = 1;
                transition = 1;
                lParamUp = repeatCount
                    | (scanCode << 16)
                    | (extended << 24)
                    | (context << 29)
                    | (previousState << 30)
                    | (transition << 31);

                if (sender == "down")
                {
                    KeyboardHelper.PostMessage(hanlde, WM_KEYDOWN, new IntPtr((int)key), unchecked((IntPtr)(int)lParamDown));
                }
                else if (sender == "up")
                {
                    KeyboardHelper.PostMessage(hanlde, WM_KEYUP, new IntPtr((int)key), unchecked((IntPtr)(int)lParamUp));
                }
            }
            catch (Exception) { }
        }

        private void MouseAction(int x, int y, int delta, EMouseKey buttons)
        {
            try
            {
                new Task(() =>
                {
                    RECT main = AutoControl.GetWindowRect(hanlde);
                    AutoControl.SendClickOnPosition(hanlde, x, y, buttons);
                }).Start();
            }
            catch (Exception)
            { }
        }

        private void MouseActionScroll(int x, int y, int delta)
        {
            try
            {
                new Task(() =>
                {
                    RECT main = AutoControl.GetWindowRect(hanlde);
                    IntPtr wParam = MAKEWPARAM(1, delta, WinMsgMouseKey.MK_LBUTTON);
                    IntPtr lParam = MAKELPARAM(x, y);
                    AutoControl.PostMessage(hanlde, WM_MOUSEWHEEL, wParam, lParam);
                }).Start();
            }
            catch (Exception)
            { }
        }

        internal static IntPtr MAKEWPARAM(int direction, int delta, WinMsgMouseKey button)
        {
            return (IntPtr)(((delta << 16) * Math.Sign(direction) | (ushort)button));
        }

        internal static IntPtr MAKELPARAM(int low, int high)
        {
            return (IntPtr)((high << 16) | (low & 0xFFFF));
        }

        
    }
}