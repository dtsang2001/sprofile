﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoreSProfile.Entity;
using CoreSProfile.Helper;

namespace CoreSProfile
{
    public class Browser
    {
        public Point size { get; set; } = new Point(640, 480);
        public float zoom { get; set; } = 100f;
        public int max { get; set; } = 2;
        public string extension { get; set; } = "";

        public int line;

        public string? baseProfilePath { get; set; }
        public bool proxy { get; set; }

        public int screenWidth = Screen.PrimaryScreen.Bounds.Width;
        public int screenHeight = Screen.PrimaryScreen.Bounds.Height;
        public Point pos { get; set; }

        public void Open(ProfileEntity profile, int index)
        {
            if (string.IsNullOrEmpty(baseProfilePath))
            {
                return;
            }

            Orbita O = new Orbita
            {
                size = new Point(size.X, size.Y),
                position = pos,
                zoom = (float)zoom / 100,
                proxy = proxy
            };
            O.Run(profile, baseProfilePath);
        }

        public void Open(ProfileEntity profile, int index, IntPtr panel)
        {
            if (string.IsNullOrEmpty(baseProfilePath))
            {
                return;
            }

            Orbita O = new Orbita
            {
                size = new Point(size.X, size.Y),
                position = new Point(size.X * 1, size.Y * line),
                extension = extension,
                zoom = zoom
            };
            O.AssignToPanel(panel);
            O.Run(profile, baseProfilePath);
        }

        public Selenium OpenSelenium(ProfileEntity profile, int index = 0)
        {
            if (string.IsNullOrEmpty(baseProfilePath))
            {
                return null;
            }

            Selenium S = new Selenium
            {
                size = new Point(size.X, size.Y),
                position = new Point(size.X * index, size.Y * line),
                zoom = zoom,
                profile = profile
            };

            S.Init(baseProfilePath);

            return S;
        }
    }
}