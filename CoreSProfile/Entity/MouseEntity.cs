﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSProfile.Entity
{
    public class MouseEntity
    {
        public MouseEntity.MouseEvent action { get; set; }

        public Point location { get; set; }

        public enum MouseEvent
        {
            LeftClick,
            RightClick,
            Move,
            Wheel
        }
    }
}
