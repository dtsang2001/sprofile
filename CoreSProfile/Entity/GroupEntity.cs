﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSProfile.Entity
{
    public class GroupEntity
    {
        [Browsable(false)]
        public int id { get; set; }
        [Browsable(false)]
        public int group_id { get; set; }
        [Browsable(false)]
        public string name { get; set; } = "";
        [Browsable(false)]
        public string count { get; set; } = "";
        public string group_name { get; set; } = "";
        public string group_total { get; set; } = "";
    }
}
