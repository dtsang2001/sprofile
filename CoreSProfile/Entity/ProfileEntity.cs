﻿using System.ComponentModel;

namespace CoreSProfile.Entity
{
    public class ProfileEntity
    {
        [Browsable(false)]
        public int id { get; set; }

        public string profile_id { get; set; } = "";

        public string profile_last_update { get; set; } = "";

        public string profile_proxy { get; set; } = "";

        public string profile_status { get; set; } = "";

        [Browsable(false)]
        public string last_update { get; set; } = "";

        [Browsable(false)]
        public string group_id { get; set; } = "";

        [Browsable(false)]
        public string proxy { get; set; } = "";

        [Browsable(false)]
        public string folders { get; set; } = "";

        [Browsable(false)]
        public string account_facebook { get; set; } = "";

        [Browsable(false)]
        public string account_google { get; set; } = "";

        [Browsable(false)]
        public string account_twitter { get; set; } = "";

        [Browsable(false)]
        public string account_tiktok { get; set; } = "";

        [Browsable(false)]
        public string account_discord { get; set; } = "";

        [Browsable(false)]
        public string status { get; set; } = "";

        [Browsable(false)]
        public string ua { get; set; } = "";

        [Browsable(false)]
        public string platform { get; set; } = "";

        [Browsable(false)]
        public string language { get; set; } = "";

        [Browsable(false)]
        public string screen_resolution { get; set; } = "";

        [Browsable(false)]
        public string cpu_core { get; set; } = "";

        [Browsable(false)]
        public int ram { get; set;}

        [Browsable(false)]
        public string video_inputs { get; set; } = "";

        [Browsable(false)]
        public string audio_inputs { get; set; } = "";

        [Browsable(false)]
        public string audio_outputs { get; set; } = "";

        [Browsable(false)]
        public string webGL_vendor { get; set; } = "";

        [Browsable(false)]
        public string web_renderer { get; set; } = "";

        [Browsable(false)]
        public string note { get; set; } = "";

        [Browsable(false)]
        public string start_url { get; set; } = "https://iphey.com/";

        [Browsable(false)]
        public IntPtr hanlde = IntPtr.Zero;

    }
}
