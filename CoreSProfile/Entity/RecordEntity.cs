﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSProfile.Entity
{
    public class RecordEntity
    {
        [Browsable(false)]
        public int id { get; set; }
        public string record_name { get; set; } = "";
        public string record_time { get; set; } = "";
        public string record_last_modify { get; set; } = "";
        public string record_link { get; set; } = "";


    }
}
