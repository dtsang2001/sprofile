﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSProfile.Entity
{
    public class Config
    {
        public static string folderProfile { get; set; } = "";
        public static string CONFIG_FOLDER_PROFILE = "CONFIG_FOLDER_PROFILE";

        public static string fileProxy { get; set; } = "";
        public static string CONFIG_FILE_PROXY = "CONFIG_FILE_PROXY";

        public static string fileName { get; set; } = "";
        public static string CONFIG_FILE_NAME = "CONFIG_FILE_NAME";

        public static string fileExt { get; set; } = "";
        public static string CONFIG_FILE_EXT = "CONFIG_FILE_EXT";

        public static string fileRecord { get; set; } = "";
        public static string CONFIG_FILE_RECORD = "CONFIG_FILE_RECORD";

    }
}
