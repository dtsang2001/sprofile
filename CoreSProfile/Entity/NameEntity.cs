﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSProfile.Entity
{
    public class NameEntity
    {
        public string first_name { get; set; } = "";
        public string last_name { get; set; } = "";
        public string full_name { get; set; } = "";
        public string last_name_group { get; set; } = "";
        public string gender { get; set; } = "";
    }
}
