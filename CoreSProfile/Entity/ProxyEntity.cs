﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSProfile.Entity
{
    public class ProxyEntity
    {
        public ProxyEntity(string p = "") 
        {
            if (!String.IsNullOrEmpty(p))
            {
                this.origin = p;
                this.ip = p.Split(':')[0];
                this.port = p.Split(':')[1];
                this.user = p.Split(':')[2];
                this.pass = p.Split(':')[3];

                this.proxy = $"{this.ip}:{this.port}";
            }
        }
        public string origin { get; set; } = "";
        public string proxy { get; set; } = "";
        public string ip { get; set; } = "";
        public string port { get; set; } = "";
        public string user { get; set; } = "";
        public string pass { get; set; } = "";
    }
}
