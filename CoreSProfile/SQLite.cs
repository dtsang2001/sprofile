﻿using System;
using System.Data.SQLite;
using System.Data;
using CoreSProfile.Entity;
using CoreSProfile.Helper;

namespace CoreSProfile
{
    public class SQLite
    {
        public static object LockDb = new object();

        public static string dbconnectionString = $"Data Source={AppDomain.CurrentDomain.BaseDirectory}\\Data\\data.db";

        public static SQLiteCommand? sqlCommand;

        public static SQLiteDataAdapter? database;

        public static DataSet dataSet = new DataSet();

        public static DataTable dataTable = new DataTable();

        public static DataTable ExecuteQuery(string CommandText)
        {
            try
            {
                lock (SQLite.LockDb)
                {
                    using (SQLiteConnection sqliteConnection = new SQLiteConnection(SQLite.dbconnectionString))
                    {
                        sqliteConnection.Open();
                        SQLite.sqlCommand = sqliteConnection.CreateCommand();
                        SQLite.database = new SQLiteDataAdapter(CommandText, sqliteConnection);
                        SQLite.dataSet.Reset();
                        SQLite.database.Fill(dataSet);
                        if (SQLite.dataSet.Tables.Count > 0)
                        {
                            SQLite.dataTable = dataSet.Tables[0];
                        }
                        sqliteConnection.Close();
                        return SQLite.dataTable;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            };
        }

        public static List<GroupEntity> GetGroups()
        {
            List<GroupEntity> result = new List<GroupEntity>();
            DataTable data = SQLite.ExecuteQuery("SELECT * FROM groups ORDER BY id DESC");
            if (data == null)
            {
                return SQLite.GetGroups();
            }
            foreach (DataRow item in data.Rows)
            {
                GroupEntity group = new GroupEntity
                {
                    id = int.Parse(item["id"]?.ToString()),
                    name = item["name"]?.ToString(),
                    group_name = item["name"]?.ToString(),
                };
                result.Add(group);
            }
            return result;
        }

        public static List<GroupEntity> GetGroupsCountProfiles()
        {
            List<GroupEntity> result = new List<GroupEntity>();
            DataTable data = SQLite.ExecuteQuery("SELECT groups.*, COUNT(profiles.profile_id) AS counts FROM groups LEFT JOIN profiles ON profiles.group_id = groups.id GROUP BY groups.id ORDER BY groups.id DESC");
            if (data == null)
            {
                return SQLite.GetGroups();
            }
            foreach (DataRow item in data.Rows)
            {
                GroupEntity group = new GroupEntity
                {
                    id = int.Parse(item["id"].ToString()),
                    group_id = int.Parse(item["id"]?.ToString()),
                    name = item["name"]?.ToString(),
                    count = item["counts"]?.ToString(),
                    group_name = item["name"]?.ToString(),
                    group_total = item["counts"]?.ToString()
                };
                result.Add(group);
            }
            return result;
        }

        public static void InsertGroup(GroupEntity g)
        {
            string command = $"INSERT INTO groups (name) VALUES ('{g.name}')";
            SQLite.ExecuteNonQuery(command);
        }

        public static void UpdateGroup(GroupEntity g)
        {
            string command = $"UPDATE groups SET name = '{g.group_name}' WHERE name = '{g.name}'";
            SQLite.ExecuteNonQuery(command);
        }

        public static void RemoveGroup(GroupEntity g)
        {

            string deleteProfilesCommand = $"DELETE FROM profiles WHERE group_id = '{g.id}'";
            ExecuteNonQuery(deleteProfilesCommand);

            string command = $"DELETE FROM groups WHERE name = '{g.name}'";
            SQLite.ExecuteNonQuery(command);
        }

        public static void InsertProfile(ProfileEntity p)
        {
            string time = Utils.GetCurrentTime();
            string command = $"INSERT INTO profiles " +
                $"(profile_id, group_id, status, proxy, ua, folders, note, screen, cpu, start_url, last_update) VALUES " +
                $"('{p.profile_id}', '{p.group_id}', '{p.status}', '{p.proxy}', '{p.ua}', '{p.folders}', '{p.note}', '{p.screen_resolution}', '{p.cpu_core}', '{p.start_url}', '{time}')";
            SQLite.ExecuteNonQuery(command);
        }

        public static ProfileEntity SelectProfile(string profile_id)
        {
            DataTable data = SQLite.ExecuteQuery($"SELECT * FROM profiles WHERE profile_id = '{profile_id}' ORDER BY id DESC");

            ProfileEntity p = new ProfileEntity();

            if (data == null || data.Rows.Count <= 0)
            {
                return p;
            }

            var item = data.Rows[0];

            p = new ProfileEntity
            {
                id = int.Parse(item["id"].ToString()),
                profile_id = item["profile_id"]?.ToString(),
                last_update = item["last_update"]?.ToString(),
                proxy = item["proxy"]?.ToString(),
                status = "Ready",
                group_id = item["group_id"]?.ToString(),
                ua = item["ua"]?.ToString(),
                folders = item["folders"]?.ToString(),
                note = item["note"]?.ToString(),
                screen_resolution = item["screen"]?.ToString(),
                cpu_core = item["cpu"]?.ToString(),
                start_url = item["start_url"]?.ToString()
            };

            return p;
        }

        public static void UpdateProfile(ProfileEntity p)
        {
            string time = Utils.GetCurrentTime();
            string command = $"UPDATE profiles SET group_id = '{p.group_id}', last_update = '{time}', proxy = '{p.proxy}', folders = '{p.folders}' WHERE profile_id = '{p.profile_id}'";
            SQLite.ExecuteNonQuery(command);
        }

        public static void RemoveProfile(ProfileEntity p)
        {
            string command = $"DELETE FROM profiles WHERE profile_id = '{p.profile_id}'";
            SQLite.ExecuteNonQuery(command);
        }

        public static List<ProfileEntity> GetProfiles(int group_id = 0)
        {
            List<ProfileEntity> result = new List<ProfileEntity>();
            DataTable data;
            if (group_id > 0)
            {
                data = SQLite.ExecuteQuery($"SELECT * FROM profiles WHERE group_id = '{group_id}' ORDER BY id DESC");
            }
            else
            {
                data = SQLite.ExecuteQuery($"SELECT * FROM profiles ORDER BY id DESC");
            }

            if (data == null)
            {
                return SQLite.GetProfiles(group_id);
            }

            if (data.Rows.Count > 0)
            {
                foreach (DataRow item in data.Rows)
                {
                    ProfileEntity profile = new ProfileEntity
                    {
                        id = int.Parse(item["id"].ToString()),
                        profile_id = item["profile_id"]?.ToString(),
                        last_update = item["last_update"]?.ToString(),
                        proxy = item["proxy"]?.ToString(),
                        status = "Ready",
                        group_id = item["group_id"]?.ToString(),
                        ua = item["ua"]?.ToString(),
                        folders = item["folders"]?.ToString(),
                    };
                    
                    profile.profile_last_update = profile.last_update;
                    profile.profile_proxy = profile.proxy;
                    profile.profile_status = profile.status;

                    result.Add(profile);
                }
            }
            return result;
        }

        public static void ExecuteNonQuery(string commandQuery)
        {
            lock (SQLite.LockDb)
            {
                using (SQLiteConnection sqliteConnection = new SQLiteConnection(SQLite.dbconnectionString))
                {
                    sqliteConnection.Open();
                    SQLite.sqlCommand = sqliteConnection.CreateCommand();
                    SQLite.sqlCommand.CommandText = commandQuery;
                    SQLite.sqlCommand.ExecuteNonQuery();
                    sqliteConnection.Close();
                }
            }
        }

        public static void CreateTable(string table, string cols)
        {
            string command = $"CREATE TABLE IF NOT EXISTS {table} ( {cols} )";
            SQLite.ExecuteNonQuery(command);
        }
    }
}
