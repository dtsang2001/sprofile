﻿using CoreSProfile.Entity;
using CoreSProfile.Helper;

namespace CoreSProfile
{
    public class GoLogin
    {
        public static ProfileEntity CreateProfile(string profilesPath, string namePath, string pathProxy = "")
        {
            Random rand = new Random();
            ProfileEntity profile = new ProfileEntity();

            try
            {
                if (string.IsNullOrEmpty(namePath) || string.IsNullOrEmpty(profilesPath))
                {
                    profile.note = "Empty profile id or profile path";
                    return profile;
                }

                string id = Utils.GetRandomName(namePath);
                id = Utils.ConvertToProfileID(id);

                profile = new ProfileEntity
                {
                    profile_id = id.Trim(),
                    group_id = "1",
                    ua = GoLogin.GetUA(),
                    platform = "Win32",
                    language = "en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7,ja-JP;q=0.6,ja;q=0.5,ko-KR;q=0.4,ko;q=0.3",
                    screen_resolution = GoLogin.GetScreenResolution(),
                    cpu_core = "2|4|8|16|32".Split('|')[rand.Next(0, 5)],
                    ram = int.Parse("1|2|4|8".Split('|')[rand.Next(0, 4)]),
                    video_inputs = rand.Next(1, 2).ToString(),
                    audio_inputs = rand.Next(1, 2).ToString(),
                    audio_outputs = rand.Next(1, 2).ToString(),
                    webGL_vendor = GoLogin.GetWebGL(),
                    start_url = "https://iphey.com/"
                };

                profile.web_renderer = GoLogin.GetGPU(profile.webGL_vendor);

                ProxyEntity proxy = new ProxyEntity();

                if (!String.IsNullOrEmpty(pathProxy))
                {
                    string txtProxy = Utils.RandomLineFileTxt(pathProxy);

                    if (!String.IsNullOrEmpty(txtProxy))
                    {
                        proxy = new ProxyEntity(txtProxy);
                    }
                }

                string Preferences = GoLogin.getPreferences(profile, proxy);

                try
                {
                    profile.proxy = proxy.origin;
                    profile.folders = $"{profilesPath}\\{profile.profile_id}";
                    DirectoryNew($"{profile.folders}");
                    Thread.Sleep(100);
                    File.WriteAllText($"{profile.folders}\\Default\\Preferences", Preferences);
                    profile.note = "Success";
                    return profile;
                }
                catch (Exception ex)
                {
                    Utils.Logs("GoLoginCreaterProfileSaveProfile:", ex.Message);
                    profile.note = ex.Message;
                    return profile;
                }
            }
            catch (Exception ex)
            {
                Utils.Logs("GoLoginCreaterProfile:", ex.Message);
                profile.note = ex.Message;
                return profile;
            }
        }

        public static ProfileEntity CloneProfile(ProfileEntity clone, string profilesPath, string namePath)
        {
            Random rand = new Random();
            ProfileEntity profile = new ProfileEntity();

            try
            {
                if (string.IsNullOrEmpty(namePath) || string.IsNullOrEmpty(profilesPath))
                {
                    profile.note = "Empty profile id or profile path";
                    return profile;
                }

                string id = Utils.GetRandomName(namePath);
                id = Utils.ConvertToProfileID(id);

                profile = new ProfileEntity
                {
                    profile_id = id.Trim(),
                    group_id = "1",
                    ua = clone.ua,
                    platform = "Win32",
                    language = "en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7,ja-JP;q=0.6,ja;q=0.5,ko-KR;q=0.4,ko;q=0.3",
                    screen_resolution = clone.screen_resolution,
                    cpu_core = clone.cpu_core,
                    ram = int.Parse("1|2|4|8".Split('|')[rand.Next(0, 4)]),
                    video_inputs = rand.Next(1, 2).ToString(),
                    audio_inputs = rand.Next(1, 2).ToString(),
                    audio_outputs = rand.Next(1, 2).ToString(),
                    webGL_vendor = GoLogin.GetWebGL(),
                    start_url = clone.start_url
                };

                profile.web_renderer = GoLogin.GetGPU(profile.webGL_vendor);

                ProxyEntity proxy = new ProxyEntity();

                if (!String.IsNullOrEmpty(clone.proxy))
                {
                    proxy = new ProxyEntity(clone.proxy);
                }

                string Preferences = GoLogin.getPreferences(profile, proxy);

                try
                {
                    profile.proxy = proxy.origin;
                    profile.folders = $"{profilesPath}\\{profile.profile_id}";
                    DirectoryNew($"{profile.folders}");
                    Thread.Sleep(100);
                    File.WriteAllText($"{profile.folders}\\Default\\Preferences", Preferences);
                    profile.note = "Success";
                    return profile;
                }
                catch (Exception ex)
                {
                    Utils.Logs("GoLoginCloneProfileSaveProfile:", ex.Message);
                    profile.note = ex.Message;
                    return profile;
                }
            }
            catch (Exception ex)
            {
                Utils.Logs("GoLoginCloneProfile:", ex.Message);
                profile.note = ex.Message;
                return profile;
            }
        }

        public static void RemoveProfile(ProfileEntity profile)
        {
            try
            {
                Directory.Delete(profile.folders, true);
            }
            catch (Exception ex)
            {
                Utils.Logs("RemoveProfile:", ex.Message);
            }

            try
            {
                SQLite.RemoveProfile(profile);
            }
            catch (Exception ex)
            {
                Utils.Logs("RemoveProfile:", ex.Message);
            }
        }

        public static void RemoveProfileWhenDelGroup(GroupEntity g)
        {
            try
            {
                List<ProfileEntity> profiles = SQLite.GetProfiles(g.id);
                foreach (ProfileEntity profile in profiles)
                {
                    Directory.Delete(profile.folders, true);
                }
            }
            catch (Exception ex)
            {
                Utils.Logs("RemoveProfile:", ex.Message);
            }

            try
            {
                SQLite.RemoveGroup(g);
            }
            catch (Exception ex)
            {
                Utils.Logs("RemoveProfile:", ex.Message);
            }
        }

        public static void MoveProfileToGroup(ProfileEntity profile)
        {
            try
            {
                SQLite.UpdateProfile(profile);
            }
            catch (Exception ex)
            {
                Utils.Logs("Move Profile:", ex.Message);
            }
        }

        private static string getPreferences(ProfileEntity profile, ProxyEntity proxy)
        {
            Random rand = new Random();

            //-------------------------------------------------------------------------- \\

            List<String> timezones = new List<String> { "Europe/London", "Europe/Athens", "Europe/Amsterdam", "Europe/Madrid", "Europe/Rome", "Europe/Helsinki", "Europe/Vilnius", "Europe/Riga" };
            string dtimez = "Asia/Ho_Chi_Minh";

            //-------------------------------------------------------------------------- \\

            // ------------------------------------ Data Profile ------------------------------------ \\
            string Preferences = File.ReadAllText("Data\\settings.txt");
            Preferences = Utils.Change(Preferences, "tb_name_profile_SSL", profile.profile_id);
            Preferences = Utils.Change(Preferences, "tb_user_agen", profile.ua);
            Preferences = Utils.Change(Preferences, "tb_platform", profile.platform);
            Preferences = Utils.Change(Preferences, "tb_language", profile.language);
            Preferences = Utils.Change(Preferences, "tb_screen_resolution_x", profile.screen_resolution.Split('x')[0]);
            Preferences = Utils.Change(Preferences, "tb_screen_resolution_y", profile.screen_resolution.Split('x')[1]);
            Preferences = Utils.Change(Preferences, "hardwareChanger", profile.cpu_core.Trim());

            switch (profile.ram)
            {
                case 1:
                    Preferences = Utils.Change(Preferences, "tb_RAM", "4096");
                    break;

                case 2:
                    Preferences = Utils.Change(Preferences, "tb_RAM", "8192");
                    break;

                case 4:
                    Preferences = Utils.Change(Preferences, "tb_RAM", "16384");
                    break;
                //case 8
                default:
                    Preferences = Utils.Change(Preferences, "tb_RAM", "32768");
                    break;
            }

            Preferences = Utils.Change(Preferences, "tb_video_inputs", profile.video_inputs);
            Preferences = Utils.Change(Preferences, "tb_audio_inputs", profile.audio_inputs);
            Preferences = Utils.Change(Preferences, "tb_audio_outputs", profile.audio_outputs);
            Preferences = Utils.Change(Preferences, "tb_webGL_vendor", profile.webGL_vendor);
            Preferences = Utils.Change(Preferences, "tb_web_Renderer", profile.web_renderer);
            Preferences = Utils.Change(Preferences, "tb_web_open_default", profile.start_url);

            // ------------------------------------ Fake noiseValue ------------------------------------ \\
            Preferences = Utils.Change(Preferences, "noiseValueChange", rand.Next(0, 7) + $".{Utils.RandomNum(11)}e-{rand.Next(8, 9)}");

            // ------------------------------------ Fake canvasNoise ------------------------------------ \\
            Preferences = Utils.Change(Preferences, "canvasNoiseChange", rand.Next(0, 4) + $".{Utils.RandomNum(8)}");

            // ------------------------------------ Fake getClientRectsNoice & get_client_rects_noise ------------------------------------ \\
            Preferences = Utils.Change(Preferences, "RectsNoiceChange", rand.Next(2, 9) + $".{Utils.RandomNum(4)}");

            // ------------------------------------ Fake uid ------------------------------------ \\
            Preferences = Utils.Change(Preferences, "sqzdbzkff4zv8ev9mgm1a6m8xw8xucyeyqjo1bibdjph9cechdqkhocecu", Utils.RandomStringAndNum(58));

            // ------------------------------------ Fake webglNoiseValue & webgl_noise_value ------------------------------------ \\
            Preferences = Utils.Change(Preferences, "20.089", rand.Next(9, 20) + $".{Utils.RandomNum(3)}");

            // ------------------------------------ Fake num_personal_suggestions ------------------------------------ \\
            Preferences = Utils.Change(Preferences, "suggestions_change", rand.Next(1, 10).ToString());

            // ------------------------------------ Fake device_scale_factor ------------------------------------ \\
            Preferences = Utils.Change(Preferences, "scalefactorChange", rand.Next(1, 3) + $".5");

            // ------------------------------------ Fake webglNoiseValue ------------------------------------ \\
            Preferences = Utils.Change(Preferences, "webglNoiseValueChange", rand.Next(10, 99) + $".{rand.Next(000, 999)}");

            // ------------------------------------ Fake PrevNavigationTime ------------------------------------ \\
            Preferences = Utils.Change(Preferences, "PrevNavigationTimeChange", $"13315906{rand.Next(000000000, 999999999)}");

            // ------------------------------------ Fake announcement_notification_service_first_run_time ------------------------------------ \\
            Preferences = Utils.Change(Preferences, "serviceruntimeChange", $"13315898{rand.Next(000000000, 999999999)}");

            switch (rand.Next(1, 3))
            {
                case 1:
                    string num11 = $"{rand.Next(700, 999)}";
                    Preferences = Utils.Change(Preferences, "bottomChange", num11);

                    string num12 = $"{rand.Next(20, 800)}";
                    Preferences = Utils.Change(Preferences, "leftChange", num12);

                    string num18 = $"{rand.Next(1300, 1900)}";
                    Preferences = Utils.Change(Preferences, "rightChange", num18);

                    string num19 = $"{rand.Next(50, 200)}";
                    Preferences = Utils.Change(Preferences, "topChange", num19);
                    break;

                case 2:
                    string num14 = $"{rand.Next(700, 999)}";
                    Preferences = Utils.Change(Preferences, "bottomChange", num14);

                    string num15 = "22";
                    Preferences = Utils.Change(Preferences, "leftChange", num15);

                    string num20 = $"{rand.Next(1300, 1900)}";
                    Preferences = Utils.Change(Preferences, "rightChange", num20);

                    string num21 = "60";
                    Preferences = Utils.Change(Preferences, "topChange", num21);
                    break;

                default:
                    break;
            }

            //------------------------------------- Fake Proxy -------------------------------------\\
            Preferences = Utils.Change(Preferences, "txtpass", proxy.pass);
            Preferences = Utils.Change(Preferences, "txtuser", proxy.user);
            Preferences = Utils.Change(Preferences, "txtpublicip", proxy.ip);
            Preferences = Utils.Change(Preferences, "dtime", dtimez);

            //--------------------------------------------------------------------------\\

            return Preferences;
        }

        public static string GetWebGL()
        {
            string[] data = new string[]
            {
                "Google Inc. (Intel)",
                "Google Inc. (AMD)",
                "Google Inc. (NVIDIA)"
            };

            return data[new Random().Next(data.Length)].Trim();
        }

        public static string GetGPU(string WebGL)
        {
            #region GPU

            string[] data;

            switch (WebGL)
            {
                case "Google Inc. (Intel)":
                    data = new string[]
                    {
                        "ANGLE(Intel(R) HD Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) UHD Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) HD Graphics 4600 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) HD Graphics 520 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) HD Graphics 630 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) Iris(R) Xe Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) UHD Graphics 630 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) HD Graphics 3000 Direct3D11 vs_4_1 ps_4_1)",
                        "ANGLE(Intel(R) HD Graphics 530 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) HD Graphics 500 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) UHD Graphics 620 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) UHD Graphics 630 Direct3D9Ex vs_3_0 ps_3_0)",
                        "ANGLE(Intel(R) HD Graphics 5500 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) HD Graphics Direct3D11 vs_4_0 ps_4_0)",
                        "ANGLE(Intel(R) HD Graphics Family Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) HD Graphics Direct3D9Ex vs_3_0 ps_3_0)",
                        "ANGLE(Intel(R) HD Graphics 4000 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) HD Graphics 610 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) HD Graphics Direct3D11 vs_4_1 ps_4_1)",
                        "ANGLE(Intel(R) HD Graphics 620 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) UHD Graphics 610 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) HD Graphics 510 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) Iris(R) Plus Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Mobile Intel(R) 4 Series Express Chipset Family Direct3D11 vs_4_0 ps_4_0)",
                        "ANGLE(Intel(R) UHD Graphics 600 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) HD Graphics 4600 Direct3D9Ex vs_3_0 ps_3_0)",
                        "ANGLE(Intel(R) HD Graphics 620 Direct3D9Ex vs_3_0 ps_3_0)",
                        "ANGLE(Intel(R) HD Graphics 4400 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(Intel(R) HD Graphics 4000 Direct3D9Ex vs_3_0 ps_3_0)",
                    };
                    break;

                case "Google Inc. (AMD)":
                    data = new string[]
                    {
                        "ANGLE(AMD Mobility Radeon HD 5000 Series Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon(TM) Vega 8 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon HD 8670 D + R7 240 Dual Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon R9 200 Series Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon R7 200 Series Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon HD 8470 D Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon(TM) Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon RX 5700 XT Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon(TM) Vega 3 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon(TM) R9 200 Series Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon RX 6900 XT Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon(TM) RX Vega 11 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon HD 8210 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon HD 6320 Graphics Direct3D9Ex vs_3_0 ps_3_0)",
                        "ANGLE(AMD Radeon HD 6470 M Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon HD 5450 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon HD 7480 D Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon R5 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon(TM) Vega 6 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon HD 7700 Series Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon R7 M260 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon HD 8650 G + HD 8600 M Dual Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon(TM) RX Vega 10 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon HD 6290 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon HD 7500 / 7600 Series Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon R7 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon(TM) R9 390 Series Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon RX 6600 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(AMD Radeon HD 6800 Series Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 5 3600 6 - Core Processor ",
                        "AMD Ryzen 5 5600 H with Radeon Graphics ",
                        "AMD Mobility Radeon HD 5000 Series Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 5 3500 U with Radeon Vega Mobile Gfx ",
                        "AMD Radeon(TM) Vega 8 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 7 3800 X 8 - Core Processor ",
                        "AMD A10 - 6700 APU with Radeon(tm) HD Graphics ",
                        "AMD Radeon HD 8670 D + R7 240 Dual Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 7 5800 X 8 - Core Processor ",
                        "AMD Ryzen 7 3700 X 8 - Core Processor ",
                        "AMD Phenom(tm) 8600 B Triple - Core Processor ",
                        "AMD Ryzen 9 5900 X 12 - Core Processor ",
                        "AMD Ryzen 5 1600 Six - Core Processor ",
                        "AMD Radeon R9 200 Series Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Radeon R7 200 Series Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 5 4600 G with Radeon Graphics ",
                        "AMD Radeon(TM) Graphics ",
                        "AMD Ryzen 5 5600 X 6 - Core Processor ",
                        "AMD Radeon RX 5700 XT ",
                        "AMD A4 PRO - 7300 B APU with Radeon HD Graphics ",
                        "AMD Radeon HD 8470 D Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 3 3200 G with Radeon Vega Graphics ",
                        "AMD Radeon(TM) Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "AMD FX(tm) - 6300 Six - Core Processor ",
                        "AMD Radeon RX 5700 XT Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 5 3600 X 6 - Core Processor ",
                        "AMD Ryzen 5 3450 U with Radeon Vega Mobile Gfx ",
                        "AMD Athlon 320 GE with Radeon Vega Graphics ",
                        "AMD Ryzen 5 3500 X 6 - Core Processor ",
                        "AMD Radeon R5 200 Series ",
                        "AMD Ryzen 5 4600 H with Radeon Graphics ",
                        "AMD 4700 S 8 - Core Processor Desktop Kit ",
                        "AMD Athlon 3000 G with Radeon Vega Graphics ",
                        "AMD Radeon(TM) Vega 3 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Radeon(TM) R9 200 Series Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 5 2400 G with Radeon Vega Graphics ",
                        "AMD Ryzen 5 2600 Six - Core Processor ",
                        "AMD Ryzen 5 3500 6 - Core Processor ",
                        "AMD Phenom(tm) II X4 960 T Processor ",
                        "AMD A6 - 7480 Radeon R5, 8 Compute Cores 2 C + 6 G ",
                        "AMD A10 - 5750 M APU with Radeon(tm) HD Graphics ",
                        "AMD Radeon HD 8650 G ",
                        "AMD FX(tm) - 8300 Eight - Core Processor ",
                        "AMD Radeon RX 5600 XT ",
                        "AMD Ryzen 7 2700 X Eight - Core Processor ",
                        "AMD Athlon(tm) II X2 250 Processor ",
                        "AMD Ryzen 7 1700 Eight - Core Processor ",
                        "AMD FX(tm) - 8120 Eight - Core Processor ",
                        "AMD Ryzen 7 4800 H with Radeon Graphics ",
                        "AMD Radeon RX 6900 XT Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 5 5600 6 - Core Processor ",
                        "AMD Athlon Silver 3050 U with Radeon Graphics ",
                        "AMD Ryzen 7 3700 U with Radeon Vega Mobile Gfx ",
                        "AMD Radeon(TM) RX Vega 10 Graphics ",
                        "AMD Ryzen 7 3750 H with Radeon Vega Mobile Gfx ",
                        "AMD Ryzen 3 3100 4 - Core Processor ",
                        "AMD Radeon(TM) RX Vega 11 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "AMD E1 - 2100 APU with Radeon(TM) HD Graphics ",
                        "AMD Radeon HD 8210 Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 5 3400 G with Radeon Vega Graphics ",
                        "AMD E - 300 APU with Radeon(tm) HD Graphics ",
                        "AMD Radeon HD 6320 Graphics Direct3D9Ex vs_3_0 ps_3_0)",
                        "AMD Ryzen 5 2500 U with Radeon Vega Mobile Gfx ",
                        "AMD Radeon(TM) Vega 8 Graphics ",
                        "AMD A8 - 5600 K APU with Radeon(tm) HD Graphics ",
                        "AMD Ryzen 9 6900 HX with Radeon Graphics ",
                        "AMD Ryzen 9 3900 XT 12 - Core Processor ",
                        "AMD Radeon HD 6470 M Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Radeon HD 5450 Direct3D11 vs_5_0 ps_5_0)",
                        "AMD A8 - 5500 APU with Radeon(tm) HD Graphics ",
                        "AMD Ryzen 7 1800 X Eight - Core Processor ",
                        "AMD A4 - 5300 APU with Radeon(tm) HD Graphics ",
                        "AMD Radeon HD 7480 D Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 7 1700 X Eight - Core Processor ",
                        "AMD A10 - 7850 K Radeon R7, 12 Compute Cores 4 C + 8 G ",
                        "AMD PRO A6 - 8570 R5, 8 COMPUTE CORES 2 C + 6 G ",
                        "AMD Radeon R5 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Athlon(tm) X4 860 K Quad Core Processor ",
                        "AMD Radeon(TM) RX Vega 11 Graphics ",
                        "AMD Ryzen 3 3300 U with Radeon Vega Mobile Gfx ",
                        "AMD Radeon(TM) Vega 6 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 5 3550 H with Radeon Vega Mobile Gfx ",
                        "AMD Ryzen 3 PRO 2200 G with Radeon Vega Graphics ",
                        "AMD FX(tm) - 8350 Eight - Core Processor ",
                        "AMD A4 - 5300 B APU with Radeon(tm) HD Graphics ",
                        "AMD Ryzen 7 5700 X 8 - Core Processor ",
                        "AMD Ryzen 7 4700 U with Radeon Graphics ",
                        "AMD 3020e with Radeon Graphics ",
                        "AMD Ryzen 5 1500 X Quad - Core Processor ",
                        "AMD FX - 8320E Eight - Core Processor ",
                        "AMD A4 - 4000 APU with Radeon(tm) HD Graphics ",
                        "AMD Radeon HD 7700 Series Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Phenom(tm) II X4 840 Processor ",
                        "AMD Ryzen 7 5700 U with Radeon Graphics ",
                        "AMD Ryzen 7 5700 G with Radeon Graphics ",
                        "AMD Athlon(tm) II X4 640 Processor ",
                        "AMD A4 - 9125 RADEON R3, 4 COMPUTE CORES 2 C + 2 G ",
                        "AMD Radeon(TM) R3 Graphics ",
                        "AMD Ryzen 5 5500 U with Radeon Graphics ",
                        "AMD Radeon R7 M260 Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 9 3900 X 12 - Core Processor ",
                        "AMD Ryzen 3 2200 G with Radeon Vega Graphics ",
                        "AMD Phenom(tm) II X4 965 Processor ",
                        "AMD Radeon HD 5800 Series ",
                        "AMD Radeon HD 8650 G + HD 8600 M Dual Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Radeon(TM) RX Vega 10 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "AMD FX(tm) - 4300 Quad - Core Processor ",
                        "AMD A6 - 7310 APU with AMD Radeon R4 Graphics ",
                        "AMD Radeon(TM) R4 Graphics ",
                        "AMD Ryzen 5 5600 G with Radeon Graphics ",
                        "AMD Athlon Gold 3150 U with Radeon Graphics ",
                        "AMD Ryzen 5 PRO 4650 U with Radeon Graphics ",
                        "AMD Athlon(tm) II X4 620 Processor ",
                        "AMD C - 60 APU with Radeon(tm) HD Graphics ",
                        "AMD Radeon HD 6290 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 5 4500 U with Radeon Graphics ",
                        "AMD Radeon HD 7500 / 7600 Series Direct3D11 vs_5_0 ps_5_0)",
                        "AMD A10 - 7860 K Radeon R7, 12 Compute Cores 4 C + 8 G ",
                        "AMD Ryzen 5 PRO 2400 G with Radeon Vega Graphics ",
                        "AMD Ryzen 5 2600 X Six - Core Processor ",
                        "AMD Athlon 200 GE with Radeon Vega Graphics ",
                        "AMD A12 - 9720 P RADEON R7, 12 COMPUTE CORES 4 C + 8 G ",
                        "AMD Radeon R7 Graphics Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Athlon(tm) II X2 215 Processor ",
                        "AMD Sempron(tm) 145 Processor ",
                        "AMD Ryzen 3 3200 U with Radeon Vega Mobile Gfx ",
                        "AMD Ryzen 9 5950 X 16 - Core Processor ",
                        "AMD A8 - 7600 Radeon R7, 10 Compute Cores 4 C + 6 G ",
                        "AMD Radeon(TM) R9 390 Series Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Radeon RX 6600 Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 7 2700 Eight - Core Processor ",
                        "AMD Phenom(tm) II X4 955 Processor ",
                        "AMD Radeon HD 6800 Series Direct3D11 vs_5_0 ps_5_0)",
                        "AMD Ryzen 5 3600 XT 6 - Core Processor ",
                        "AMD E1 - 2500 APU with Radeon(TM) HD Graphics ",
                        "AMD Radeon HD 8200 / R3 Series ",
                        "AMD Radeon R7 Graphics Direct3D11 vs_5_0 ps_5_0)"
                    };
                    break;

                default:
                    data = new string[]
                    {
                        "ANGLE(NVIDIA GeForce GT 1030 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1060 6 GB Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce 8400 GS Direct3D11 vs_4_1 ps_4_1)",
                        "ANGLE(NVIDIA GeForce RTX 2070 SUPER Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1050 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 970 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce RTX 3070 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 770 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1080 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1050 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GT 440 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 750 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1070 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce RTX 3080 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 750 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce 9500 GT(Microsoft Corporation - WDDM v1.1) Direct3D11 vs_4_0 ps_4_0)",
                        "ANGLE(NVIDIA GeForce GTX 1650 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce 9600 GT Direct3D11 vs_4_0 ps_4_0)",
                        "ANGLE(NVIDIA GeForce RTX 3050 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1660 SUPER Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce RTX 2060 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce RTX 3070 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1060 3 GB Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA NVS 310 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 760 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1080 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1660 Ti with Max - Q Design Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GT 710 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce RTX 3080 Direct3D9Ex vs_3_0 ps_3_0)",
                        "ANGLE(NVIDIA Quadro FX 1700(Microsoft Corporation - WDDM v1.1) Direct3D11 vs_4_0 ps_4_0)",
                        "ANGLE(NVIDIA GeForce GTX 650 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce RTX 2070 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 550 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GT 610 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1650 SUPER Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GT 730 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce 8500 GT Direct3D9Ex vs_3_0 ps_3_0)",
                        "ANGLE(NVIDIA GeForce GTX 960 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GT 720 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GT 335 M Direct3D11 vs_4_1 ps_4_1)",
                        "ANGLE(NVIDIA GeForce RTX 2060 SUPER Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce 9800 GT Direct3D11 vs_4_0 ps_4_0)",
                        "ANGLE(NVIDIA GeForce GTX 1660 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 760(192 - bit) Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce RTX 3050 Laptop GPU Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce G210 Direct3D11 vs_4_1 ps_4_1)",
                        "ANGLE(NVIDIA GeForce RTX 3060 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1660 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1060 5 GB Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GT 740 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 745 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce RTX 2080 Super with Max - Q Design Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce RTX 3060 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1070 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce RTX 2080 SUPER Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce RTX 2080 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce 9500 GT Direct3D11 vs_4_0 ps_4_0)",
                        "ANGLE(NVIDIA GeForce GT 640 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GT 420 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 980 Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GT 740 M Direct3D11 vs_5_0 ps_5_0)",
                        "ANGLE(NVIDIA GeForce GTX 1060 3 GB Direct3D9Ex vs_3_0 ps_3_0)",
                        "NVIDIA GeForce GT 1030 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1060 6 GB Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce 8400 GS Direct3D11 vs_4_1 ps_4_1)",
                        "NVIDIA GeForce RTX 2070 SUPER Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 3050 Laptop GPU / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 1050 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 970 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 2060 / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 1050 Ti / PCIe / SSE2 ",
                        "NVIDIA GeForce RTX 3070 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1060 6 GB / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 760 / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 770 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1080 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1650 / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 1050 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1070 / PCIe / SSE2 ",
                        "NVIDIA GeForce GT 440 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 3060 Ti / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 980 / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 750 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1070 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1660 Ti / PCIe / SSE2 ",
                        "NVIDIA GeForce RTX 3080 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 750 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce 9500 GT(Microsoft Corporation - WDDM v1.1) Direct3D11 vs_4_0 ps_4_0)",
                        "NVIDIA GeForce GTX 1650 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce 9600 GT Direct3D11 vs_4_0 ps_4_0)",
                        "NVIDIA GeForce RTX 3050 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1660 SUPER Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 2060 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 3070 Ti / PCIe / SSE2 ",
                        "NVIDIA GeForce GT 740 / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 1050 / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 750 Ti / PCIe / SSE2 ",
                        "NVIDIA GeForce RTX 3070 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1060 3 GB Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA NVS 310 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 760 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1080 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1080 / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 1080 Ti / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 1660 Ti with Max - Q Design Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1660 SUPER / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 660 / PCIe / SSE2 ",
                        "NVIDIA GeForce GT 710 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 3080 Direct3D9Ex vs_3_0 ps_3_0)",
                        "NVIDIA Quadro FX 1700(Microsoft Corporation - WDDM v1.1) Direct3D11 vs_4_0 ps_4_0)",
                        "NVIDIA GeForce GTX 650 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 2070 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 550 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1060 / PCIe / SSE2 ",
                        "NVIDIA GeForce GT 610 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1650 SUPER Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GT 730 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce 8500 GT Direct3D9Ex vs_3_0 ps_3_0)",
                        "NVIDIA GeForce GTX 960 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1070 Ti / PCIe / SSE2 ",
                        "NVIDIA GeForce GT 720 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GT 335 M Direct3D11 vs_4_1 ps_4_1)",
                        "NVIDIA GeForce RTX 2060 SUPER Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce 9800 GT Direct3D11 vs_4_0 ps_4_0)",
                        "NVIDIA GeForce GTX 1660 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 760(192 - bit) Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 2080 SUPER / PCIe / SSE2 ",
                        "NVIDIA GeForce RTX 3050 Laptop GPU Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce G210 Direct3D11 vs_4_1 ps_4_1)",
                        "NVIDIA GeForce GTX 970 / PCIe / SSE2 ",
                        "NVIDIA GeForce RTX 3060 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 650 Ti BOOST / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 1660 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 3050 / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 1060 5 GB Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GT 740 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 745 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 3070 / PCIe / SSE2 ",
                        "NVIDIA GeForce RTX 2080 Super with Max - Q Design Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 2080 Ti / PCIe / SSE2 ",
                        "NVIDIA GeForce RTX 2060 SUPER / PCIe / SSE2 ",
                        "NVIDIA GeForce RTX 3060 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1070 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 2080 SUPER Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 2080 Ti Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 960 / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 1060 3 GB / PCIe / SSE2 ",
                        "NVIDIA GeForce 9500 GT Direct3D11 vs_4_0 ps_4_0)",
                        "NVIDIA GeForce RTX 2070 / PCIe / SSE2 ",
                        "NVIDIA GeForce GT 1030 / PCIe / SSE2 ",
                        "NVIDIA GeForce GTX 750 / PCIe / SSE2 ",
                        "NVIDIA GeForce GT 640 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce RTX 3050 Ti Laptop GPU / PCIe / SSE2 ",
                        "NVIDIA GeForce GT 420 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 980 Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GT 730 / PCIe / SSE2 ",
                        "NVIDIA GeForce RTX 2070 SUPER / PCIe / SSE2 ",
                        "NVIDIA GeForce GT 740 M Direct3D11 vs_5_0 ps_5_0)",
                        "NVIDIA GeForce GTX 1060 3 GB Direct3D9Ex vs_3_0 ps_3_0)",
                        "NVIDIA GeForce GT 710 / PCIe / SSE2 ",
                        "NVIDIA GeForce RTX 3080 / PCIe / SSE2 ",
                        "NVIDIA GeForce RTX 2080 / PCIe / SSE2 ",
                    };
                    break;
            }

            #endregion GPU

            return data[new Random().Next(data.Length)].Trim();
        }

        public static string GetScreenResolution()
        {
            #region Screen Resolution

            string[] data = new string[]
            {
                "800x600",
                "960x720",
                "1024x576",
                "1024x600",
                "1024x640",
                "1024x768",
                "1152x648",
                "1152x864",
                "1280x720",
                "1280x768",
                "1280x800",
                "1280x960",
                "1280x1024",
                "1360x768",
                "1366x768",
                "1400x1080",
                "1440x900",
                "1440x1080",
                "1536x864",
                "1600x900",
                "1600x1200",
                "1680x1050",
                "1856x1392",
                "1920x1080",
                "1920x1200",
                "1920x1440",
                "2048x1152",
                "2048x1536",
                "2304x1440",
                "2560x1440",
                "2560x1600",
                "2560x2048",
                "2880x1800"
            };

            #endregion Screen Resolution

            return data[new Random().Next(data.Length)].Trim();
        }

        public static string GetUA()
        {
            Random rand = new Random();

            int version = rand.Next(117, 120);
            return "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/" + version + ".0." + rand.Next(1000, 9999) + "." + rand.Next(10, 99) + " Safari/537.36";
        }

        private static void DirectoryNew(string destDir)
        {
            if (!Directory.Exists(destDir))
            {
                Directory.CreateDirectory(destDir);
                Directory.CreateDirectory(destDir + "\\Default");
            }
        }
    }
}