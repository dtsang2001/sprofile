﻿using CoreSProfile.Entity;
using System.Configuration;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using KAutoHelper;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace CoreSProfile.Helper
{
    public class Utils
    {
        private static object lockFile = new object();

        public static bool IsJSON(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static string Change(string data, string origin, string dataNew)
        {
            return data.Replace(origin, dataNew);
        }

        public static string RandomNum(int a)
        {
            string text = "0123456789";
            char[] array = new char[a];
            Random random = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = text[random.Next(text.Length)];
            }
            return new string(array);
        }

        public static string RandomStringAndNum(int a)
        {
            string text = "abcdefghijklmnopqrstuvwxyz0123456789";
            char[] array = new char[a];
            Random random = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = text[random.Next(text.Length)];
            }
            return new string(array);
        }

        public static string RandomLineFileTxt(string exits)
        {
            List<string> list = new List<string>();
            list = new List<string>(File.ReadAllLines(exits));
            return list[new Random().Next(0, list.Count())];
        }

        public static void Logs(string function, string exception)
        {
            lock (Utils.lockFile)
            {
                try
                {
                    Utils.Logs(string.Format("Logs\\log{0}.txt", DateTime.Now.ToString("ddMMyyyy")), function, exception);
                    if (File.Exists(string.Format("Logs\\log{0}.txt", DateTime.Now.AddDays(-7.0).ToString("ddMMyyyy"))))
                    {
                        File.Delete(string.Format("Logs\\log{0}.txt", DateTime.Now.AddDays(-7.0).ToString("ddMMyyyy")));
                    }
                }
                catch
                {
                }
            }
        }

        public static void Logs(string file, string log, string actionName = "")
        {
            lock (Utils.lockFile)
            {
                if (!Directory.Exists("Logs"))
                {
                    Directory.CreateDirectory("Logs");
                }
                File.AppendAllText(file, string.Format("{0}{1}-{2}-{3}", new object[]
                {
                    Environment.NewLine,
                    DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"),
                    actionName,
                    log
                }), Encoding.UTF8);
            }
        }

        public static string GetRandomFiles(string path)
        {
            string file = "";
            if (!string.IsNullOrEmpty(path))
            {
                var extensions = new string[] { ".png", ".jpg", ".gif", ".jpeg" };
                try
                {
                    var di = new DirectoryInfo(path);
                    var rgFiles = di.GetFiles("*.*").Where(f => extensions.Contains(f.Extension.ToLower()));
                    Random R = new Random();
                    file = rgFiles.ElementAt(R.Next(0, rgFiles.Count())).FullName;
                }
                catch { }
            }
            return file;
        }

        public static string GetRandomName(string path)
        {
            string file = "";
            if (!string.IsNullOrEmpty(path))
            {
                try
                {
                    string jsonString = File.ReadAllText(path);
                    List<NameEntity> names = System.Text.Json.JsonSerializer.Deserialize<List<NameEntity>>(jsonString);
                    int index = new Random().Next(names.Count);
                    file = names[index].full_name;
                }
                catch { }
            }
            return file;
        }

        public static string GetCurrentTime(string format = "MM/dd/yyyy HH:mm")
        {
            DateTime currentDateTime = DateTime.Now;
            return currentDateTime.ToString(format);
        }

        public static string ConvertToProfileID(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static void SaveConfig(string key, string value)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = config.AppSettings.Settings;
            if (settings[key] == null)
            {
                settings.Add(key, value);
            }
            else
            {
                settings[key].Value = value;
            }
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);
        }

        public static void MoveUp(IntPtr hWnd, int turn, int sleep = 100)
        {
            for (int i = 0; i < turn; i++)
            {
                AutoControl.SendKeyBoardDown(hWnd, VKeys.VK_W);
                Thread.Sleep(sleep);
                AutoControl.SendKeyBoardUp(hWnd, VKeys.VK_W);
            }
            Thread.Sleep(500);
        }

        public static void MoveDown(IntPtr hWnd, int turn, int sleep = 100)
        {
            for (int i = 0; i < turn; i++)
            {
                AutoControl.SendKeyBoardDown(hWnd, VKeys.VK_S);
                Thread.Sleep(sleep);
                AutoControl.SendKeyBoardUp(hWnd, VKeys.VK_S);
            }
            Thread.Sleep(500);
        }

        public static void MoveRight(IntPtr hWnd, int turn, int sleep = 100)
        {
            for (int i = 0; i < turn; i++)
            {
                AutoControl.SendKeyBoardDown(hWnd, VKeys.VK_D);
                Thread.Sleep(sleep);
                AutoControl.SendKeyBoardUp(hWnd, VKeys.VK_D);
            }
            Thread.Sleep(500);
        }

        public static void MoveLeft(IntPtr hWnd, int turn, int sleep = 100)
        {
            for (int i = 0; i < turn; i++)
            {
                AutoControl.SendKeyBoardDown(hWnd, VKeys.VK_A);
                Thread.Sleep(sleep);
                AutoControl.SendKeyBoardUp(hWnd, VKeys.VK_A);
            }
            Thread.Sleep(500);
        }

        public static void ChangeGridView(DataGridView dataGridView, string column, string searchValue, string msg, Color color)
        {
            dataGridView.Invoke((Action)(() =>
            {
                IEnumerable<DataGridViewRow> enumerable()
                {
                    foreach (DataGridViewRow r in dataGridView.Rows)
                    {
                        if (r.Cells[column].Value.ToString().Equals(searchValue, StringComparison.Ordinal))
                        {
                            yield return r;
                        }
                    }
                }

                DataGridViewRow dataGridViewRow = enumerable().FirstOrDefault();
                int index = dataGridViewRow.Index;
                if (index != -1 && index < dataGridView.Rows.Count)
                {
                    if (dataGridView.Name.Contains("Simulator"))
                    {
                        dataGridView.Rows[index].Cells["simu_status"].Value = msg;
                        dataGridView.Rows[index].Cells["simu_status"].Style.ForeColor = color;
                    }
                    if (dataGridView.Name.Contains("RunRecord"))
                    {
                        dataGridView.Rows[index].Cells["run_record_status"].Value = msg;
                        dataGridView.Rows[index].Cells["run_record_status"].Style.ForeColor = color;
                    }
                    /*if (dataGridView.Name.Contains("Simulator"))
                    {
                        dataGridView.Rows[index].Cells["profile_status"].Value = msg;
                        dataGridView.Rows[index].Cells["profile_status"].Style.ForeColor = color;
                    }*/
                    else
                    {
                        dataGridView.Rows[index].Cells["profile_status"].Value = msg;
                        dataGridView.Rows[index].Cells["profile_status"].Style.ForeColor = color;
                    }

                }
            }));
        }
    }
}
