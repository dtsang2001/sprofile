﻿using CoreSProfile.Entity;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreSProfile.Helper
{
    public class BrowserService
    {

        public string CallAPI(string url, object? data = null, string type = "json", bool withBaseURL = true, string method = "post")
        {
            RestClient client = null;
            client = new RestClient(url);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            if (method.Equals("post"))
            {
                request = new RestRequest(Method.POST);
            }
            if (type == "json")
            {
                request.AddHeader("Content-Type", "application/json");
            }
            else if (type == "urlencoded")
            {
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            }
            request.AddHeader("secret", "f63c73335063a608c06156dff5565f24");
            if (data != null)
            {
                if (type == "json")
                {
                    request.AddParameter("application/json", data, ParameterType.RequestBody);
                }
                else if (type == "urlencoded")
                {
                    request.AddParameter("application/x-www-form-urlencoded", data, ParameterType.RequestBody);
                }
            }
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        public JObject getTask(string API, string Channel)
        {

            string response = this.CallAPI(API + "/api/general/getTask/web?channel=" + Channel, null, "json", false, "get");
            if (Utils.IsJSON(response))
            {
                return JObject.Parse(response);
            }
            else
            {
                Thread.Sleep(1000);
                return this.getTask(API,Channel);
            }
        }
        public string getOTP(string API,string phone,string brand, int tries = 30)
        {
            string data = $"br={brand}&phone={phone}";
            string response = this.CallAPI(API + "/api/general/getOTP", data, "urlencoded", false);
            JObject obj;

            if (Utils.IsJSON(response))
            {
                obj = JObject.Parse(response);

                if (obj["code"].ToString() == "200")
                {
                    return obj["data"]["otp"].ToString();
                }
            }

            if (tries > 0)
            {
                Thread.Sleep(1000);
                tries--;
                return this.getOTP(API, phone, brand, tries);
            }
            else
            {
                return "Không thành công";
            }      
        }
        public JObject saveAccount(string API,JObject data, string tb ,int tries = 5)
        {
            string post = $"username={data["username"]}&email={data["email"]}&password={data["password"]}&phone={data["phone2"]}&first_name={data["first_name"]}&last_name={data["last_name"]}";
            string response = this.CallAPI($"{API}/api/account_social/create/{tb}", post, "urlencoded", false);
            if (Utils.IsJSON(response))
            {
                return JObject.Parse(response);
            }
            if (tries > 0)
            {
                Thread.Sleep(1000);
                tries--;
                return this.saveAccount(API, data, tb, tries);
            }
            else
            {
                return null;
            }

        }
        public JObject getYahooMail()
        {
            string response = this.CallAPI("http://account.sscs.vn/api/mail/imap?c=common&type=yahoo&action=account", null, "json", false, "get");
            if (Utils.IsJSON(response))
            {
                return JObject.Parse(response);
            }
            else
            {
                Thread.Sleep(1000);
                return this.getYahooMail();
            }
        }
        public JObject getContentMail(string mail, string password, string type, string from, int tries = 0)
        {
            tries--;
            string response = this.CallAPI($"http://account.sscs.vn/api/mail/imap?type={type}&action=mail&account={mail}&password={password}&from={from}", null, "json", false,"get");
            
            JObject obj = null;

            if (Utils.IsJSON(response))
            {
                obj = JObject.Parse(response);

                if (!String.IsNullOrEmpty(obj["data"]["body"].ToString()))
                {
                    Thread.Sleep(500);
                    return obj;
                }
            }

            if (tries > 0)
            {
                Thread.Sleep(1000);
                tries--;
                return this.getContentMail(mail, password, type, from, tries);
            }
            else
            {
                return obj;
            }

        }

    }
}
