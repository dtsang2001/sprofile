﻿using KAutoHelper;
using System;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace CoreSProfile.Helper
{
    public class KeyboardHelper
    {
        private const uint WM_CHAR = 0x0102;
        internal const int WM_KEYDOWN = 0x0100;
        internal const int WM_KEYUP = 0x0101;
        const int WM_COPY = 0x0301;

        const uint KEYEVENTF_KEYUP = 2;
        const byte VK_CONTROL = 0x11;
        private const int MOUSEEVENTF_WHEEL = 0x0800;

        [DllImport("user32.dll")]
        static extern IntPtr GetMenu(IntPtr hWnd);

        [DllImport("user32.dll")]
        static extern IntPtr GetSubMenu(IntPtr hMenu, int nPos);

        [DllImport("user32.dll")]
        static extern uint GetMenuItemID(IntPtr hMenu, int nPos);
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("User32.dll")]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder s, int nMaxCount);
        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll")]
        static extern bool PostMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImport("User32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static public extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, uint dwExtraInfo);

        [DllImport("user32.dll")]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

        [DllImport("user32.dll")]
        static extern int GetWindowThreadProcessId(IntPtr hWnd, out int processId);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);

        const uint SWP_NOSIZE = 0x0001;
        const uint SWP_NOMOVE = 0x0002;
        const uint SWP_NOACTIVATE = 0x0010;

        public static string SendCopy(IntPtr hWnd)
        {
            SetForegroundWindow(hWnd);
            PressCtrlShortcuts((byte)VKeys.VK_C);
            string copiedText = Clipboard.GetText();
            return copiedText;
        }
        public static void OpenMailVerify(IntPtr hWnd, string message)
        {
            SetForegroundWindow(hWnd);
            PressCtrlShortcuts((byte)VKeys.VK_T);
            SendVieChar(hWnd, message);
            PressKey((byte)VKeys.VK_RETURN);           
            Thread.Sleep(100);
        }


        public static void SendVieChar(IntPtr handle, string message)
        {
            foreach (var ch in message)
            {
                PostMessage(handle, 6, new IntPtr(1), new IntPtr(0));
                PostMessage(handle, WM_CHAR, (IntPtr)ch, IntPtr.Zero);
            }
            Thread.Sleep(200);
        }
        public static IntPtr FindWindowHandleFromProcesses(string windowName)
        {
            Process[] processes = Process.GetProcesses();
            IntPtr result = IntPtr.Zero;
            foreach (Process item in processes.Where((Process p) => p.MainWindowHandle != IntPtr.Zero))
            {
                IntPtr mainWindowHandle = item.MainWindowHandle;

                string text = GetText(mainWindowHandle);
                if (text.Contains(windowName))
                {
                    result = mainWindowHandle;
                    break;
                }
            }
            return result;
        }
        private static string GetText(IntPtr hWnd)
        {
            StringBuilder stringBuilder = new StringBuilder(256);
            GetWindowText(hWnd, stringBuilder, 256);
            return stringBuilder.ToString().Trim();
        }
        private static void PressCtrlShortcuts(byte VK_Char)
        {
            keybd_event(VK_CONTROL, 0, 0, 0);
            keybd_event(VK_Char, 0, 0, 0);
            keybd_event(VK_Char, 0, KEYEVENTF_KEYUP, 0);
            keybd_event(VK_CONTROL, 0, KEYEVENTF_KEYUP, 0);
            Thread.Sleep(100);
        }
        private static void PressKey(byte VK_Char)
        {
            keybd_event(VK_Char, 0, 0, 0);
            keybd_event(VK_Char, 0, KEYEVENTF_KEYUP, 0);          
            Thread.Sleep(100);
        }
        // using keyboard, mouse real
        public static void PressKeyDown(byte VK_Char)
        {

            keybd_event(VK_Char, 0, 0, 0);
            Thread.Sleep(100);

        }
        public static void PressKeyUp(byte VK_Char)
        {
            keybd_event(VK_Char, 0, KEYEVENTF_KEYUP, 0);
            Thread.Sleep(100);

        }
        public static void SetCursorPosition(int x, int y)
        {
            Cursor.Position = new System.Drawing.Point(x, y);
        }
        // Function to perform mouse event
        public static void MouseEvent(int flags)
        {
            mouse_event(flags, 0, 0, 0, 0);
        }
        public static void Scroll(int flags, int delta)
        {
            mouse_event(flags, 0, 0, delta, 0);

        }
        // window control
        public static string GetNameWindowByHandle(IntPtr ptr)
        {
            var name = AutoControl.GetText(ptr);
            return name;
        }
        public static Process FindWindowReturnPsc(string windowName)
        {
            Process[] processes = Process.GetProcesses();
            IntPtr result = IntPtr.Zero;
            foreach (Process item in processes.Where((Process p) => p.MainWindowHandle != IntPtr.Zero))
            {
                IntPtr mainWindowHandle = item.MainWindowHandle;

                string text = GetText(mainWindowHandle);
                //Debug.WriteLine("text: " + text);

                if (text.Contains(windowName))
                {
                    return item;
                    //Debug.WriteLine(item.Id + "P_name: " + text +" dawda  "+ item.MainWindowHandle);
                }
            }
            return null;

        }
        private static void sendtoBack(IntPtr hWnd)
        {
            if (hWnd != IntPtr.Zero)
            {
                SetWindowPos(hWnd, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE);
            }
            else
            {
                Debug.WriteLine("Window not found.");
            }
        }
        public static void SendAllToBack(IntPtr main, List<IntPtr> other)
        {
            sendtoBack(main);
            foreach (var item in other)
            {
                sendtoBack(item);
            }
        }
        private static IntPtr BringToFront(IntPtr hWnd)
        {
            SetForegroundWindow(hWnd);
            return hWnd;
        }
        public static void BringAllToFront(IntPtr main, List<IntPtr> other)
        {
            BringToFront(main);
            foreach (var item in other)
            {
                BringToFront(item);
            }
        }

    }
}