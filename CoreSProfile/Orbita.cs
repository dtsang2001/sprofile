﻿using CoreSProfile.Entity;
using CoreSProfile.Helper;
using KAutoHelper;
using OpenQA.Selenium;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace CoreSProfile
{
    public class Orbita
    {
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool MoveWindow(IntPtr hwnd, int x, int y, int cx, int cy, bool repaint);

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowRect(IntPtr hWnd, ref Rect lpRect);

        [StructLayout(LayoutKind.Sequential)]
        private struct Rect
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        private const int SW_HIDE = 0;

        private const int SW_SHOW = 5;

        public string pathChrome = AppDomain.CurrentDomain.BaseDirectory + "Data\\orbita_v120\\chrome.exe";

        public string extensionAutoma = AppDomain.CurrentDomain.BaseDirectory + "Data\\automa";

        public string extensionAutomaReal = AppDomain.CurrentDomain.BaseDirectory + "Data\\automa_real";

        public string extensionRoonin = AppDomain.CurrentDomain.BaseDirectory + "Data\\roonin";

        public string extensionNopecha = AppDomain.CurrentDomain.BaseDirectory + "Data\\nopecha";

        //public FlowLayoutPanel? layoutWorkflows { get; set; }
        public Point size { get; set; }

        public Point position { get; set; }
        public float zoom { get; set; } = 1;
        public string extension { get; set; } = "";
        public bool proxy { get; internal set; }

        public Dictionary<string, Process> listProcess = new Dictionary<string, Process>();

        public Process process;

        private TaskCompletionSource<bool> eventHandled;

        public IntPtr panelHandle;

        private bool hasPanel = false;

        public void KillAllProcess()
        {
            foreach (var item in listProcess)
            {
                if (item.Value != null)
                {
                    item.Value.Kill();
                }
            }
        }

        public void Run(ProfileEntity profile, string baseProfilePath)
        {
            using (process = new Process())
            {
                try
                {
                    // changed pre
                    string fileContent = File.ReadAllText(baseProfilePath + "\\" + profile.profile_id + "\\Default\\Preferences");

                    string patternHeight = @"(""screenHeight"":)\s*\d+";
                    string patternWidth = @"(""screenWidth"":)\s*\d+";

                    string result = Regex.Replace(fileContent, patternHeight, $"$1 {size.Y}");
                    result = Regex.Replace(result, patternWidth, $"$1 {size.X}");


                    File.WriteAllText(baseProfilePath + "\\" + profile.profile_id + "\\Default\\Preferences", result);

                    //string extension = $"{this.extension}";
                    string[] extensions = File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "Data\\ext.txt");

                    foreach (string ext in extensions)
                    {
                        extension += $"{AppDomain.CurrentDomain.BaseDirectory}Data\\{ext},";
                    }

                    if (proxy == true)
                    {
                        ProxyEntity proxy = new ProxyEntity(profile.proxy);
                    }

                    process.StartInfo = new ProcessStartInfo(pathChrome);
                    string arguments = $"--user-data-dir=\"{baseProfilePath}\\{profile.profile_id}\" ";

                    if (proxy == true)
                    {
                        ProxyEntity proxy = new ProxyEntity(profile.proxy);
                        arguments += $"--proxy-server=\"{proxy.proxy}\" " +
                                     $"--host-resolver-rules=\"MAP * 0.0.0.0 , EXCLUDE {proxy.ip}\" ";
                    }
                    arguments += $"--load-extension=\"{extension}\" " +
                                     $"--force-device-scale-factor=\"{zoom}\" " +
                                     $"--high-dpi-support={zoom} " +
                                     //$"--window-size=\"{size.X},{size.Y}\" " +
                                     $"--window-position=\"{position.X / zoom},{position.Y / zoom}\"" +
                                     $"";

                    process.StartInfo.Arguments = arguments;
                    process.EnableRaisingEvents = true;
                 
                    process.Start();
                    

                    Thread.Sleep(5000);

                    var rct = new Rect();
                    GetWindowRect(process.MainWindowHandle, ref rct);

                    if (this.hasPanel)
                    {
                        SetParent(process.MainWindowHandle, panelHandle);
                        MoveWindow(process.MainWindowHandle, 0, 0, size.X, size.Y, true);
                    }
                    //else
                    //{
                    //    MoveWindow(process.MainWindowHandle, position.X, position.Y, size.X, size.Y, true);
                    //}

                    Thread.Sleep(5000);
                }
                catch (Exception ex)
                {
                    Utils.Logs("Process Start Error", ex.Message);
                    return;
                }
            }
        }
            
        public async Task Run(int index, ProfileEntity profile, string baseProfilePath, int timeout = 3600)
        {
            eventHandled = new TaskCompletionSource<bool>();

            using (process = new Process())
            {
                try
                {
                    string extension = "";
                    /*                    string extension = extensionAutoma;

                                        if (Config.useNopecha == 1)
                                        {
                                            extension += $",{extensionNopecha}";
                                        }*/

                    process.StartInfo = new ProcessStartInfo(pathChrome);
                    string arguments = $"--user-data-dir=\"{baseProfilePath}\\{profile.profile_id}\" " +
                                        $"--proxy-server=\"{profile.proxy}\" " +
                                        $"--host-resolver-rules=\"MAP * 0.0.0.0 , EXCLUDE {profile.proxy.Split(':')[0]}\" " +
                                        $"--load-extension=\"{extension}\" " +
                                        $"--force-device-scale-factor=\"{zoom}\" " +
                                        $"--high-dpi-support=0.25{zoom} " +
                                        $"--window-size=\"{size.X},{size.Y}\" " +
                                        $"--window-position=\"{position.X},{position.Y}\"";

                    process.StartInfo.Arguments = arguments;
                    process.EnableRaisingEvents = true;
                    process.Exited += new System.EventHandler(ProcessExited);

                    process.Start();

                    Thread.Sleep(5000);

                    //ShowWindow(process.MainWindowHandle, SW_HIDE);

                    listProcess.Add(index.ToString(), process);
                }
                catch (Exception ex)
                {
                    Utils.Logs("Process Start Error", ex.Message);
                    return;
                }

                // Wait for Exited event, but not more than 30 seconds.
                await Task.WhenAny(eventHandled.Task, ProcessTimeout(process, timeout));

                listProcess.Remove(index.ToString());
            }

            await Task.Delay(1000);
        }

        public void AssignToPanel(IntPtr p)
        {
            this.hasPanel = true;
            this.panelHandle = p;
        }

        public void ViewProcess(Process process)
        {
            ShowWindow(process.MainWindowHandle, SW_SHOW);
        }

        private async Task ProcessTimeout(Process process, int timeout)
        {
            await Task.Delay(timeout);
            process.Kill();
        }

        private void ProcessExited(object sender, System.EventArgs e)
        {
            eventHandled.TrySetResult(true);
        }
    }
}

