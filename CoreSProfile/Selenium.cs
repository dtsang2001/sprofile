﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumUndetectedChromeDriver;
using System.Collections.ObjectModel;
using CoreSProfile.Entity;
using System.Drawing;
using OpenQA.Selenium.Interactions;

namespace CoreSProfile
{
    public class Selenium
    {
        public static string pathChrome = AppDomain.CurrentDomain.BaseDirectory + "Data\\orbita-browser\\chrome.exe";

        public static string pathChromeDriver = AppDomain.CurrentDomain.BaseDirectory + "Data\\chromedriver.exe";

        public UndetectedChromeDriver driver;

        public IJavaScriptExecutor js;

        public Actions actions;

        public IWebElement element;

        public ProfileEntity profile { get; set; }

        public Point size { get; set; }

        public Point position { get; set; }

        public float zoom { get; set; } = 1;

        public Selenium Init(string baseProfilePath)
        {
            ChromeOptions options = new ChromeOptions();

            ProxyEntity proxy = new ProxyEntity(profile.proxy);

            options.AddArgument($"--proxy-server={proxy.proxy}");
            options.AddArgument($"--host-resolver-rules=\"MAP * 0.0.0.0 , EXCLUDE {proxy.ip}\"");

            if (zoom != 1)
            {
                options.AddArgument($"--force-device-scale-factor=" + zoom);
                options.AddArgument($"--high-dpi-support=0.25" + zoom);
            }

            driver = UndetectedChromeDriver.Create(
                    browserExecutablePath: pathChrome,
                    driverExecutablePath: pathChromeDriver,
                    userDataDir: $"{baseProfilePath}\\{profile.profile_id}",
                    hideCommandPromptWindow: true,
                    commandTimeout: TimeSpan.FromMinutes(3),
                    options: options
                );

            driver.Manage().Timeouts().PageLoad.Add(TimeSpan.FromSeconds(10));
            driver.Manage().Window.Size = new Size(size.X, size.Y);
            driver.Manage().Window.Position = new Point(position.X, position.Y);

            this.js = (IJavaScriptExecutor)driver;

            this.actions = new Actions(driver);

            return this;
        }

        public void LoadUrl(string url)
        {
            driver.GoToUrl(url);
            Thread.Sleep(1000);
        }
        public void SendKeysSlow(IWebElement e, string key)
        {
            foreach (char k in key)
            {
                e.SendKeys(k.ToString());
                Thread.Sleep(new Random().Next(100, 700));
            }
        }

        public void SendKeysSlow(string key)
        {
            foreach (char k in key)
            {
                element.SendKeys(k.ToString());
                Thread.Sleep(new Random().Next(100, 1000));
            }
        }

        public Selenium ThisByXpath(string xpath, int tries = 10)
        {
            element = FindBy(By.XPath(xpath), tries);
            return this;
        }

        public Selenium ThisById(string id, int tries = 10)
        {
            element = FindBy(By.Id(id), tries);
            return this;
        }

        public Selenium ThisByCss(string css, int tries = 10)
        {
            element = FindBy(By.CssSelector(css), tries);
            return this;
        }

        public Selenium ThisByTagName(string tagName, int tries = 10)
        {
            element = FindBy(By.TagName(tagName), tries);
            return this;
        }

        public IWebElement FindByXpath(string xpath, int tries = 10)
        {
            return FindBy(By.XPath(xpath), tries);
        }

        public IWebElement FindById(string id, int tries = 10)
        {
            return FindBy(By.Id(id), tries);
        }

        public IWebElement FindByCss(string css, int tries = 10)
        {
            return FindBy(By.CssSelector(css), tries);
        }

        public IWebElement FindByTagName(string tagName, int tries = 10)
        {
            return FindBy(By.TagName(tagName), tries);
        }

        public IWebElement FindBy(By by, int tries = 10)
        {
            IWebElement result;
            try
            {
                IWebElement readOnlyCollection = this.driver.FindElement(by);
                if (readOnlyCollection != null)
                {
                    result = readOnlyCollection;
                }
                else
                {
                    if (tries > 0)
                    {
                        tries--;
                        Thread.Sleep(1000);
                        return FindBy(by, tries);
                    }
                    result = null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                if (tries > 0)
                {
                    tries--;
                    Thread.Sleep(2000);
                    return FindBy(by, tries);
                }
                result = null;
            }

            this.element = result;

            return result;
        }

        public List<IWebElement> FindMultiBy(By by, int tries = 10)
        {
            List<IWebElement> result = new List<IWebElement>();
            try
            {
                ReadOnlyCollection<IWebElement> readOnlyCollection = this.driver.FindElements(by);
                if (readOnlyCollection != null && readOnlyCollection.Count > 0)
                {
                    foreach (IWebElement element in readOnlyCollection)
                    {
                        result.Add(element);
                    }
                }
                else
                {
                    if (tries > 0)
                    {
                        tries--;
                        Thread.Sleep(1000);
                        return FindMultiBy(by, tries);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                if (tries > 0)
                {
                    tries--;
                    Thread.Sleep(1000);
                    return FindMultiBy(by, tries);
                }
            }

            return result;
        }

        public void SwitchToIframe(IWebElement iframe)
        {
            driver.SwitchTo().Frame(iframe);
        }

        public void SwitchToIframe(IWebElement iframe, By by)
        {
            driver.SwitchTo().Frame(iframe).FindElement(by)?.Click();
        }

        public void OutIframe()
        {
            driver.SwitchTo().ParentFrame();
        }

        public void Click()
        {
            try
            {
                this.element?.Click();
            }
            catch (Exception ex) { }
        }

        public void Click(IWebElement element)
        {
            try
            {
                element?.Click();
            }
            catch (Exception ex) { }
        }

        public void ExecuteScript(string script)
        {
            try
            {
                this.js.ExecuteScript(script);
            }
            catch (Exception ex) { }
        }

        public void Dispose()
        {
            driver.Quit();
        }
    }
}
